<?xml version="1.0" encoding="utf-8"?>
<sf:FeatureCollection xmlns="http://www.xtrasse.de/2.0" xmlns:xtrasse="http://www.xtrasse.de/2.0"
    xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xml="http://www.w3.org/XML/1998/namespace"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:sf="http://www.opengis.net/ogcapi-features-1/1.0/sf"
    xsi:schemaLocation="http://www.xtrasse.de/2.0 https://repository.gdi-de.org/schemas/de.xleitstelle.xtrasse/2.0/XML/XTrasse.xsd
                        http://www.opengis.net/ogcapi-features-1/1.0/sf http://schemas.opengis.net/ogcapi/features/part1/1.0/xml/core-sf.xsd">
    <sf:featureMember>
        <PFS_Plan gml:id="_pfs_plan.1">
            <name>Gasanbindung Wilhelmshaven - Leer (GWL)</name>
            <beschreibung>PFS_Plan enthält Daten des Planfeststellungsverfahren für die Errichtung
                und den Betrieb der LNG-Anbindungsleitung Wilhelmshaven - Leer (GWL) durch die EWE
                Netz GmbH. Aus den eingereichten Unterlagen wurden für den XTrasse-Plan die
                Übersichtspläne vollständig übernommen, aus dem Erläuterungsbericht wurden neben
                Attributen der Gasleitung die alternativen Planungsabschnitte integriert.
                Exemplarisch sind Detailbeschreibungen aus den Stationsplänen, den Plänen der
                Rohrlagerplätze sowie den Trassenplänen Los1, Los2 (v.a. zu geschlossenen Querungen)
                eingepflegt.</beschreibung>
            <gesetzlicheGrundlage>
                <XP_GesetzlicheGrundlage>
                    <name>Gesetz zur Beschleunigung des Einsatzes verflüssigten Erdgases
                        (LNG-Beschleunigungsgesetz - LNGG)</name>
                    <detail>§ 2 Abs. 1 Nr. 1 und Nr. 3</detail>
                    <ausfertigungDatum>2022-05-24</ausfertigungDatum>
                </XP_GesetzlicheGrundlage>
            </gesetzlicheGrundlage>
            <gesetzlicheGrundlage>
                <XP_GesetzlicheGrundlage>
                    <name>Energiewirtschaftsgesetz (EnWG)</name>
                    <detail>§ 43 Abs. 1 Nr. 5</detail>
                    <ausfertigungDatum>2005-07-07</ausfertigungDatum>
                </XP_GesetzlicheGrundlage>
            </gesetzlicheGrundlage>
            <technHerstellDatum>2023-03-01</technHerstellDatum>
            <externeReferenz>
                <XP_NetzExterneReferenz>
                    <referenzName>Antragsunterlagen</referenzName>
                    <referenzURL>
                        https://www.lbeg.niedersachsen.de/bergbau/genehmigungsverfahren/aktuelle_planfeststellungsverfahren/planfeststellungsverfahren-fur-die-errichtung-und-den-betrieb-einer-mittelbaren-lng-anbindungsleitung-wilhelmshaven-leer-gwl-durch-die-ewe-netz-gmbh-217941.html</referenzURL>
                    <datum>2023-03-01</datum>
                </XP_NetzExterneReferenz>
            </externeReferenz>
            <position>
                <gml:Polygon srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>390473.656 5898091.714 439659.908 5898091.714 439659.908
                                5929338.711 390473.656 5929338.711 390473.656 5898091.714</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </position>
            <hatBSTObjekt xlink:title="Speicher Jemgum" xlink:href="#_bst_energiespeicher.1" />
            <hatBSTObjekt xlink:title="Speicher Nütemoor" xlink:href="#_bst_energiespeicher.2" />
            <hatBSTObjekt xlink:title="WAL" xlink:href="#_bst_gasleitung.1" />
            <hatBSTObjekt xlink:title="GTG DN 400" xlink:href="#_bst_gasleitung.2" />
            <hatIPObjekt xlink:title="16" xlink:href="#_ip_stationierungspunkt.1" />
            <hatIPObjekt xlink:title="4" xlink:href="#_ip_stationierungspunkt.2" />
            <hatIPObjekt xlink:title="7" xlink:href="#_ip_stationierungspunkt.3" />
            <hatIPObjekt xlink:title="8" xlink:href="#_ip_stationierungspunkt.4" />
            <hatIPObjekt xlink:title="5" xlink:href="#_ip_stationierungspunkt.5" />
            <hatIPObjekt xlink:title="6" xlink:href="#_ip_stationierungspunkt.6" />
            <hatIPObjekt xlink:title="11" xlink:href="#_ip_stationierungspunkt.7" />
            <hatIPObjekt xlink:title="12" xlink:href="#_ip_stationierungspunkt.8" />
            <hatIPObjekt xlink:title="9" xlink:href="#_ip_stationierungspunkt.9" />
            <hatIPObjekt xlink:title="10" xlink:href="#_ip_stationierungspunkt.10" />
            <hatIPObjekt xlink:title="15" xlink:href="#_ip_stationierungspunkt.11" />
            <hatIPObjekt xlink:title="13" xlink:href="#_ip_stationierungspunkt.12" />
            <hatIPObjekt xlink:title="14" xlink:href="#_ip_stationierungspunkt.13" />
            <hatIPObjekt xlink:title="17" xlink:href="#_ip_stationierungspunkt.14" />
            <hatIPObjekt xlink:title="3" xlink:href="#_ip_stationierungspunkt.15" />
            <hatIPObjekt xlink:title="1" xlink:href="#_ip_stationierungspunkt.16" />
            <hatIPObjekt xlink:title="2" xlink:href="#_ip_stationierungspunkt.17" />
            <hatIPObjekt xlink:title="18" xlink:href="#_ip_stationierungspunkt.18" />
            <hatIPObjekt xlink:title="21" xlink:href="#_ip_stationierungspunkt.19" />
            <hatIPObjekt xlink:title="22" xlink:href="#_ip_stationierungspunkt.20" />
            <hatIPObjekt xlink:title="19" xlink:href="#_ip_stationierungspunkt.21" />
            <hatIPObjekt xlink:title="20" xlink:href="#_ip_stationierungspunkt.22" />
            <hatIPObjekt xlink:title="25" xlink:href="#_ip_stationierungspunkt.23" />
            <hatIPObjekt xlink:title="36" xlink:href="#_ip_stationierungspunkt.24" />
            <hatIPObjekt xlink:title="23" xlink:href="#_ip_stationierungspunkt.25" />
            <hatIPObjekt xlink:title="24" xlink:href="#_ip_stationierungspunkt.26" />
            <hatIPObjekt xlink:title="29" xlink:href="#_ip_stationierungspunkt.27" />
            <hatIPObjekt xlink:title="30" xlink:href="#_ip_stationierungspunkt.28" />
            <hatIPObjekt xlink:title="27" xlink:href="#_ip_stationierungspunkt.29" />
            <hatIPObjekt xlink:title="28" xlink:href="#_ip_stationierungspunkt.30" />
            <hatIPObjekt xlink:title="33" xlink:href="#_ip_stationierungspunkt.31" />
            <hatIPObjekt xlink:title="K1" xlink:href="#_ip_stationierungspunkt.32" />
            <hatIPObjekt xlink:title="36" xlink:href="#_ip_stationierungspunkt.33" />
            <hatIPObjekt xlink:title="34" xlink:href="#_ip_stationierungspunkt.34" />
            <hatIPObjekt xlink:title="35" xlink:href="#_ip_stationierungspunkt.35" />
            <hatIPObjekt xlink:title="62" xlink:href="#_ip_stationierungspunkt.36" />
            <hatIPObjekt xlink:title="32" xlink:href="#_ip_stationierungspunkt.37" />
            <hatIPObjekt xlink:title="37" xlink:href="#_ip_stationierungspunkt.38" />
            <hatIPObjekt xlink:title="40" xlink:href="#_ip_stationierungspunkt.39" />
            <hatIPObjekt xlink:title="41" xlink:href="#_ip_stationierungspunkt.40" />
            <hatIPObjekt xlink:title="38" xlink:href="#_ip_stationierungspunkt.41" />
            <hatIPObjekt xlink:title="39" xlink:href="#_ip_stationierungspunkt.42" />
            <hatIPObjekt xlink:title="44" xlink:href="#_ip_stationierungspunkt.43" />
            <hatIPObjekt xlink:title="45" xlink:href="#_ip_stationierungspunkt.44" />
            <hatIPObjekt xlink:title="42" xlink:href="#_ip_stationierungspunkt.45" />
            <hatIPObjekt xlink:title="63" xlink:href="#_ip_stationierungspunkt.46" />
            <hatIPObjekt xlink:title="31" xlink:href="#_ip_stationierungspunkt.47" />
            <hatIPObjekt xlink:title="43" xlink:href="#_ip_stationierungspunkt.48" />
            <hatIPObjekt xlink:title="48" xlink:href="#_ip_stationierungspunkt.49" />
            <hatIPObjekt xlink:title="49" xlink:href="#_ip_stationierungspunkt.50" />
            <hatIPObjekt xlink:title="46" xlink:href="#_ip_stationierungspunkt.51" />
            <hatIPObjekt xlink:title="47" xlink:href="#_ip_stationierungspunkt.52" />
            <hatIPObjekt xlink:title="52" xlink:href="#_ip_stationierungspunkt.53" />
            <hatIPObjekt xlink:title="53" xlink:href="#_ip_stationierungspunkt.54" />
            <hatIPObjekt xlink:title="50" xlink:href="#_ip_stationierungspunkt.55" />
            <hatIPObjekt xlink:title="51" xlink:href="#_ip_stationierungspunkt.56" />
            <hatIPObjekt xlink:title="56" xlink:href="#_ip_stationierungspunkt.57" />
            <hatIPObjekt xlink:title="57" xlink:href="#_ip_stationierungspunkt.58" />
            <hatIPObjekt xlink:title="54" xlink:href="#_ip_stationierungspunkt.59" />
            <hatIPObjekt xlink:title="55" xlink:href="#_ip_stationierungspunkt.60" />
            <hatIPObjekt xlink:title="61" xlink:href="#_ip_stationierungspunkt.61" />
            <hatIPObjekt xlink:title="58" xlink:href="#_ip_stationierungspunkt.62" />
            <hatIPObjekt xlink:title="59" xlink:href="#_ip_stationierungspunkt.63" />
            <hatIPObjekt xlink:title="60" xlink:href="#_ip_stationierungspunkt.64" />
            <hatIPObjekt xlink:title="68" xlink:href="#_ip_stationierungspunkt.65" />
            <hatIPObjekt xlink:title="69" xlink:href="#_ip_stationierungspunkt.66" />
            <hatIPObjekt xlink:title="66" xlink:href="#_ip_stationierungspunkt.67" />
            <hatIPObjekt xlink:title="67" xlink:href="#_ip_stationierungspunkt.68" />
            <hatIPObjekt xlink:title="70" xlink:href="#_ip_stationierungspunkt.69" />
            <hatIPObjekt xlink:title="64" xlink:href="#_ip_stationierungspunkt.70" />
            <hatIPObjekt xlink:title="65" xlink:href="#_ip_stationierungspunkt.71" />
            <hatIPObjekt xlink:title="K2" xlink:href="#_ip_stationierungspunkt.72" />
            <hatIPObjekt xlink:title="K3" xlink:href="#_ip_stationierungspunkt.73" />
            <hatIPObjekt xlink:title="K7" xlink:href="#_ip_stationierungspunkt.74" />
            <hatIPObjekt xlink:title="K4/K5" xlink:href="#_ip_stationierungspunkt.75" />
            <hatIPObjekt xlink:title="K6" xlink:href="#_ip_stationierungspunkt.76" />
            <hatIPObjekt xlink:title="K8" xlink:href="#_ip_stationierungspunkt.77" />
            <hatIPObjekt xlink:title="A2" xlink:href="#_ip_stationierungspunkt.78" />
            <hatIPObjekt xlink:title="A1" xlink:href="#_ip_stationierungspunkt.79" />
            <hatPFSObjekt xlink:title="Umgehung NSG" xlink:href="#_pfs_alternativetrasseabschnitt.1" />
            <hatPFSObjekt xlink:title="Alternative Brinkum"
                xlink:href="#_pfs_alternativetrasseabschnitt.2" />
            <hatPFSObjekt xlink:title="Alternative A "
                xlink:href="#_pfs_alternativetrasseabschnitt.3" />
            <hatPFSObjekt xlink:title="Alternative C "
                xlink:href="#_pfs_alternativetrasseabschnitt.4" />
            <hatPFSObjekt xlink:title="Alternative D"
                xlink:href="#_pfs_alternativetrasseabschnitt.5" />
            <hatPFSObjekt xlink:title="Alternative Süd"
                xlink:href="#_pfs_alternativetrasseabschnitt.6" />
            <hatPFSObjekt xlink:title="Alternative B"
                xlink:href="#_pfs_alternativetrasseabschnitt.7" />
            <hatPFSObjekt xlink:title="Alternative A,B,C"
                xlink:href="#_pfs_alternativetrasseabschnitt.8" />
            <hatPFSObjekt xlink:title="Variante Veenhusen"
                xlink:href="#_pfs_alternativetrasseabschnitt.9" />
            <hatPFSObjekt xlink:title="Streckenarmatur Westerstede"
                xlink:href="#_pfs_armaturengruppe.1" />
            <hatPFSObjekt xlink:title="Streckenarmatur Nüttermoor"
                xlink:href="#_pfs_armaturengruppe.2" />
            <hatPFSObjekt xlink:title="Streckenarmatur Bockhorn"
                xlink:href="#_pfs_armaturengruppe.3" />
            <hatPFSObjekt xlink:title="Streckenarmatur Hollen" xlink:href="#_pfs_armaturengruppe.4" />
            <hatPFSObjekt xlink:title="Start-/Zielgrube" xlink:href="#_pfs_baugrube.1" />
            <hatPFSObjekt xlink:title="Start-/Zielgrube" xlink:href="#_pfs_baugrube.2" />
            <hatPFSObjekt xlink:title="Start-/Zielgrube" xlink:href="#_pfs_baugrube.3" />
            <hatPFSObjekt xlink:title="Start-/Zielgrube" xlink:href="#_pfs_baugrube.4" />
            <hatPFSObjekt xlink:title="Start-/Zielgrube" xlink:href="#_pfs_baugrube.9" />
            <hatPFSObjekt xlink:title="Start-/Zielgrube" xlink:href="#_pfs_baugrube.10" />
            <hatPFSObjekt xlink:title="Start-/Zielgrube" xlink:href="#_pfs_baugrube.11" />
            <hatPFSObjekt xlink:title="Start-/Zielgrube" xlink:href="#_pfs_baugrube.15" />
            <hatPFSObjekt xlink:title="Start-/Zielgrube" xlink:href="#_pfs_baugrube.19" />
            <hatPFSObjekt xlink:title="Start-/Zielgrube" xlink:href="#_pfs_baugrube.23" />
            <hatPFSObjekt xlink:title="Start-/Zielgrube" xlink:href="#_pfs_baugrube.39" />
            <hatPFSObjekt xlink:title="Start-/Zielgrube" xlink:href="#_pfs_baugrube.55" />
            <hatPFSObjekt xlink:title="Start-/Zielgrube" xlink:href="#_pfs_baugrube.57" />
            <hatPFSObjekt xlink:title="Start-/Zielgrube	" xlink:href="#_pfs_baugrube.58" />
            <hatPFSObjekt xlink:title="Start-/Zielgrube" xlink:href="#_pfs_baugrube.59" />
            <hatPFSObjekt xlink:title="Start-/Zielgrube" xlink:href="#_pfs_baugrube.60" />
            <hatPFSObjekt xlink:title="Start-/Zielgrube" xlink:href="#_pfs_baugrube.61" />
            <hatPFSObjekt xlink:title="Start-/Zielgrube" xlink:href="#_pfs_baugrube.62" />
            <hatPFSObjekt xlink:title="Start-/Zielgrube" xlink:href="#_pfs_baugrube.63" />
            <hatPFSObjekt xlink:title="Start-/Zielgrube" xlink:href="#_pfs_baugrube.64" />
            <hatPFSObjekt xlink:title="Start-/Zielgrube" xlink:href="#_pfs_baugrube.65" />
            <hatPFSObjekt xlink:title="Start-/Zielgrube" xlink:href="#_pfs_baugrube.66" />
            <hatPFSObjekt xlink:title="Start-/Zielgrube" xlink:href="#_pfs_baugrube.67" />
            <hatPFSObjekt xlink:title="Rohrlagerplatz" xlink:href="#_pfs_baustelle.1" />
            <hatPFSObjekt xlink:title="Erdbecken" xlink:href="#_pfs_baustelle.2" />
            <hatPFSObjekt xlink:title="Enteisungsanlage" xlink:href="#_pfs_baustelle.3" />
            <hatPFSObjekt xlink:title="Zufahrt" xlink:href="#_pfs_baustelle.4" />
            <hatPFSObjekt xlink:title="Lagerplatz" xlink:href="#_pfs_baustelle.5" />
            <hatPFSObjekt xlink:title="Rohrlagerplatz" xlink:href="#_pfs_baustelle.6" />
            <hatPFSObjekt xlink:title="Zufahrt" xlink:href="#_pfs_baustelle.7" />
            <hatPFSObjekt xlink:title="Querung Graben"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.1" />
            <hatPFSObjekt xlink:title="Querung B 436"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.2" />
            <hatPFSObjekt xlink:title="Neuenburger Urwald"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.3" />
            <hatPFSObjekt xlink:title="Anbindung Netzverknüpfungpunkt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.4" />
            <hatPFSObjekt xlink:title="Unterquerung K 91"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.5" />
            <hatPFSObjekt xlink:title="Querung K 15"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.6" />
            <hatPFSObjekt xlink:title="Anbindung Nütemoor"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.8" />
            <hatPFSObjekt xlink:title="Querung Mühlenweg"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.9" />
            <hatPFSObjekt xlink:title="Querung A 28"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.10" />
            <hatPFSObjekt xlink:title="Querung L 24"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.11" />
            <hatPFSObjekt xlink:title="Querung Graben"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.12" />
            <hatPFSObjekt xlink:title="vorhandener Düker"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.13" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.14" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.15" />
            <hatPFSObjekt xlink:title="Betriebsgelände Jemgum"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.16" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.17" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.18" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.19" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.20" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.21" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.23" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.24" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.25" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.26" />
            <hatPFSObjekt xlink:title="Querung B 437"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.27" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.28" />
            <hatPFSObjekt xlink:title="Querung  A 28 (bei L 827)"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.29" />
            <hatPFSObjekt xlink:title="Querung L 827"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.30" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.31" />
            <hatPFSObjekt xlink:title="Querung B 72"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.32" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.34" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.35" />
            <hatPFSObjekt xlink:title="Querung Verbindungsgraben"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.36" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.37" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.39" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.40" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.42" />
            <hatPFSObjekt xlink:title="Querung Holtlander Ehetief"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.43" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.44" />
            <hatPFSObjekt xlink:title="Querung B 436"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.45" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.47" />
            <hatPFSObjekt xlink:title="Querung Sandrahmer Straße"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.48" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.49" />
            <hatPFSObjekt xlink:title="Querung Friedburger Tief"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.50" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.51" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.52" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.53" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.54" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.55" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.56" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.57" />
            <hatPFSObjekt xlink:title="Querung L 815 "
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.58" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.59" />
            <hatPFSObjekt xlink:title="Querung Alte Bäke"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.60" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.61" />
            <hatPFSObjekt xlink:title="Querung Zettler Tief"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.62" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.63" />
            <hatPFSObjekt xlink:title="Querung Wappenkamper Bäke"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.64" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.65" />
            <hatPFSObjekt xlink:title="Querung Urwaldstraße"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.66" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.67" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.68" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.69" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.70" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.71" />
            <hatPFSObjekt xlink:title="Querung L 815"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.72" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.75" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.76" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.77" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.78" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.79" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.80" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.81" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.82" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.83" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.84" />
            <hatPFSObjekt xlink:title="Querung K 114"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.85" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.86" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.87" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.88" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.89" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.90" />
            <hatPFSObjekt xlink:title="Querung Halsbeker Moorgraben"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.91" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.92" />
            <hatPFSObjekt xlink:title="Querung Wasserzug"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.93" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.94" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.95" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.96" />
            <hatPFSObjekt xlink:title="Querung Große Norderbäke 1"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.97" />
            <hatPFSObjekt xlink:title="Querung Große Norderbäke 2"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.98" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.99" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.100" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.101" />
            <hatPFSObjekt xlink:title="Querung Gehölze"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.102" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.103" />
            <hatPFSObjekt xlink:title="Querung K 114"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.104" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.105" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.106" />
            <hatPFSObjekt xlink:title="Querung K 68"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.107" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.108" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.109" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.110" />
            <hatPFSObjekt xlink:title="Querung Stapeler Hauptvorfluter"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.111" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.112" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.113" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.114" />
            <hatPFSObjekt xlink:title="Querung K 11"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.115" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.117" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.118" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.119" />
            <hatPFSObjekt xlink:title="Querung Nordgeorgsfehnkanal"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.120" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.121" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.122" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.123" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.124" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.127" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.128" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.129" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.130" />
            <hatPFSObjekt xlink:title="Querung Heimschloot"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.131" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.132" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.133" />
            <hatPFSObjekt xlink:title="Querung K 17"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.134" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.135" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.136" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.137" />
            <hatPFSObjekt xlink:title="Querung K 55"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.138" />
            <hatPFSObjekt xlink:title="Querung Wanderweg"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.139" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.140" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.141" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.142" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.143" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.144" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.145" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.146" />
            <hatPFSObjekt xlink:title="Querung A 31"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.147" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.148" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.149" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.150" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.151" />
            <hatPFSObjekt xlink:title="Querung B 70"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.152" />
            <hatPFSObjekt xlink:title="Querung Gleise "
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.153" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.154" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.155" />
            <hatPFSObjekt xlink:title="Querung K 2"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.156" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.157" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.158" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.159" />
            <hatPFSObjekt xlink:title="Querung Nüttermoorer Sieltief"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.160" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.161" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.162" />
            <hatPFSObjekt xlink:title="Abschnitt"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.163" />
            <hatPFSObjekt xlink:title="Straßenquerung"
                xlink:href="#_pfs_gasversorgungsleitungabschnitt.164" />
            <hatPFSObjekt xlink:title="GDRM Westerstede" xlink:href="#_pfs_station.1" />
            <hatPFSObjekt xlink:title="Station Jemgum" xlink:href="#_pfs_station.2" />
            <hatPFSObjekt xlink:title="Station Leer Nord" xlink:href="#_pfs_stationflaeche.1" />
            <hatPFSObjekt xlink:title="Station NKP Sande" xlink:href="#_pfs_stationflaeche.2" />
            <status>1000</status>
            <beteiligte>
                <XP_Akteur>
                    <nameOrganisation>EWE Netz GmbH</nameOrganisation>
                    <namePerson>Thorsten Soppa</namePerson>
                    <strasseHausnr>Cloppenburger Straße 302</strasseHausnr>
                    <postleitzahl>26133</postleitzahl>
                    <ort>Oldenburg</ort>
                    <telefon>0151 74625063</telefon>
                    <mail>thorsten.soppa@ewe-netz.de</mail>
                    <rolle>6000</rolle>
                </XP_Akteur>
            </beteiligte>
            <beteiligte>
                <XP_Akteur>
                    <nameOrganisation>Ingenieur- und Planungsbüro Lange GmbH &amp; Co. KG</nameOrganisation>
                    <namePerson>Gregor Stanislowski</namePerson>
                    <strasseHausnr>Carl-Peschken-Straße 12</strasseHausnr>
                    <postleitzahl>47441</postleitzahl>
                    <ort>Moers</ort>
                    <telefon>02841 79050</telefon>
                    <mail>gregor.stanislowski@lange-planung.de</mail>
                    <rolle>4000</rolle>
                </XP_Akteur>
            </beteiligte>
            <beteiligte>
                <XP_Akteur>
                    <nameOrganisation>Landesamt für Bergbau, Energie und Geologie</nameOrganisation>
                    <strasseHausnr>Stilleweg 2</strasseHausnr>
                    <postleitzahl>30655</postleitzahl>
                    <ort>Hannover</ort>
                    <rolle>7100</rolle>
                </XP_Akteur>
            </beteiligte>
            <auslegungGemeinden>
                <XP_Auslegung>
                    <gemeinde>Stadt Leer (Ostfriesland)</gemeinde>
                </XP_Auslegung>
            </auslegungGemeinden>
            <auslegungGemeinden>
                <XP_Auslegung>
                    <gemeinde>Stadt Westerstede</gemeinde>
                </XP_Auslegung>
            </auslegungGemeinden>
            <auslegungGemeinden>
                <XP_Auslegung>
                    <gemeinde>Samtgemeinde Jümme</gemeinde>
                </XP_Auslegung>
            </auslegungGemeinden>
            <auslegungGemeinden>
                <XP_Auslegung>
                    <gemeinde>Samtgemeine Hesel</gemeinde>
                </XP_Auslegung>
            </auslegungGemeinden>
            <auslegungGemeinden>
                <XP_Auslegung>
                    <gemeinde>Gemeinde Bockhorn</gemeinde>
                </XP_Auslegung>
            </auslegungGemeinden>
            <auslegungGemeinden>
                <XP_Auslegung>
                    <gemeinde>Gemeinde Jemgum</gemeinde>
                </XP_Auslegung>
            </auslegungGemeinden>
            <auslegungGemeinden>
                <XP_Auslegung>
                    <gemeinde>Gemeinde Sande</gemeinde>
                </XP_Auslegung>
            </auslegungGemeinden>
            <auslegungGemeinden>
                <XP_Auslegung>
                    <gemeinde>Gemeinde Uplengen</gemeinde>
                </XP_Auslegung>
            </auslegungGemeinden>
            <auslegungGemeinden>
                <XP_Auslegung>
                    <gemeinde>Gemeinde Zetel</gemeinde>
                </XP_Auslegung>
            </auslegungGemeinden>
            <auslegungInternetStartDatum>2023-01-03</auslegungInternetStartDatum>
            <auslegungInternetEndDatum>2023-01-09</auslegungInternetEndDatum>
        </PFS_Plan>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.1">
            <title>16</title>
            <aufschrift>16</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>433097.975 5914612.696</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.2">
            <title>4</title>
            <aufschrift>4</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>434610.035 5925220.455</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.3">
            <title>7</title>
            <aufschrift>7</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>434405.740 5922649.460</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.4">
            <title>8</title>
            <aufschrift>8</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>434297.713 5921756.636</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.5">
            <title>5</title>
            <aufschrift>5</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>434725.703 5924238.408</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.6">
            <title>6</title>
            <aufschrift>6</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>434699.794 5923343.844</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.7">
            <title>11</title>
            <aufschrift>11</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>434120.083 5919127.393</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.8">
            <title>12</title>
            <aufschrift>12</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>434325.762 5918238.332</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.9">
            <title>9</title>
            <aufschrift>9</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>434024.153 5920814.621</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.10">
            <title>10</title>
            <aufschrift>10</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>434222.452 5920016.512</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.11">
            <title>15</title>
            <aufschrift>15</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>433322.326 5915711.114</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.12">
            <title>13</title>
            <aufschrift>13</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>434170.034 5917275.777</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.13">
            <title>14</title>
            <aufschrift>14</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>433443.719 5916669.278</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.14">
            <title>17</title>
            <aufschrift>17</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>432404.230 5913940.427</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.15">
            <title>3</title>
            <aufschrift>3</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>434276.300 5926159.539</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.16">
            <title>1</title>
            <aufschrift>1</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>433458.425 5927870.619</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.17">
            <title>2</title>
            <aufschrift>2</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>433727.424 5926929.481</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.18">
            <title>18</title>
            <aufschrift>18</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>431767.204 5913219.442</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.19">
            <title>21</title>
            <aufschrift>21</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>429502.641 5911627.172</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.20">
            <title>22</title>
            <aufschrift>22</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>428856.466 5910910.008</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.21">
            <title>19</title>
            <aufschrift>19</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>430913.687 5912854.061</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.22">
            <title>20</title>
            <aufschrift>20</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>430103.012 5912412.611</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.23">
            <title>25</title>
            <aufschrift>25</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>426941.479 5908832.694</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.24">
            <title>36</title>
            <aufschrift>26</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>426397.858 5908064.214</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.25">
            <title>23</title>
            <aufschrift>23</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>428047.636 5910324.354</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.26">
            <title>24</title>
            <aufschrift>24</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>427269.192 5909708.675</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.27">
            <title>29</title>
            <aufschrift>29</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>426923.294 5905477.277</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.28">
            <title>30</title>
            <aufschrift>30</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>427127.514 5904514.508</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.29">
            <title>27</title>
            <aufschrift>27</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>426955.630 5907296.760</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.30">
            <title>28</title>
            <aufschrift>28</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>426865.556 5906383.220</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.31">
            <title>33</title>
            <aufschrift>33</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>425112.210 5904635.386</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.32">
            <title>K1</title>
            <aufschrift>K1</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>434105.354 5921016.000</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.33">
            <title>36</title>
            <aufschrift>36</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>422345.214 5904468.869</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.34">
            <title>34</title>
            <aufschrift>34</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>424223.755 5904847.662</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.35">
            <title>35</title>
            <aufschrift>35</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>423246.899 5904876.325</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.36">
            <title>62</title>
            <aufschrift>62</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>399358.333 5903330.957</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.37">
            <title>32</title>
            <aufschrift>32</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>426001.926 5904231.974</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.38">
            <title>37</title>
            <aufschrift>37</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>421613.568 5903829.780</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.39">
            <title>40</title>
            <aufschrift>40</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>419046.764 5902793.778</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.40">
            <title>41</title>
            <aufschrift>41</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>418177.881 5902503.694</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.41">
            <title>38</title>
            <aufschrift>38</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>420755.234 5903539.003</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.42">
            <title>39</title>
            <aufschrift>39</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>419910.958 5903110.886</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.43">
            <title>44</title>
            <aufschrift>44</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>415875.759 5900867.976</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.44">
            <title>45</title>
            <aufschrift>45</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>414956.454 5900949.582</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.45">
            <title>42</title>
            <aufschrift>42</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>417341.959 5901955.150</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.46">
            <title>63</title>
            <aufschrift>63</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>398377.680 5903303.858</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.47">
            <title>31</title>
            <aufschrift>31</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>426758.314 5904196.319</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.48">
            <title>43</title>
            <aufschrift>43</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>416622.850 5901511.626</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.49">
            <title>48</title>
            <aufschrift>48</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>412012.282 5900533.276</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.50">
            <title>49</title>
            <aufschrift>49</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>411049.289 5900618.174</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.51">
            <title>46</title>
            <aufschrift>46</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>413991.553 5900686.970</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.52">
            <title>47</title>
            <aufschrift>47</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>413005.074 5900548.382</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.53">
            <title>52</title>
            <aufschrift>52</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>408190.804 5901250.612</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.54">
            <title>53</title>
            <aufschrift>53</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>407261.211 5901553.429</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.55">
            <title>50</title>
            <aufschrift>50</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>410059.062 5900753.503</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.56">
            <title>51</title>
            <aufschrift>51</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>409081.869 5900964.953</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.57">
            <title>56</title>
            <aufschrift>56</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>404427.999 5902276.107</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.58">
            <title>57</title>
            <aufschrift>57</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>403500.543 5902439.448</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.59">
            <title>54</title>
            <aufschrift>54</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>406315.345 5901825.110</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.60">
            <title>55</title>
            <aufschrift>55</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>405408.491 5902079.546</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.61">
            <title>61</title>
            <aufschrift>61</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>400356.226 5903388.774</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.62">
            <title>58</title>
            <aufschrift>58</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>402676.226 5902751.815</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.63">
            <title>59</title>
            <aufschrift>59</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>402128.852 5903313.912</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.64">
            <title>60</title>
            <aufschrift>60</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>401232.188 5903331.315</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.65">
            <title>68</title>
            <aufschrift>68</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>394468.127 5902146.534</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.66">
            <title>69</title>
            <aufschrift>69</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>394046.336 5901543.102</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.67">
            <title>66</title>
            <aufschrift>66</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>396189.603 5902432.956</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.68">
            <title>67</title>
            <aufschrift>67</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>395415.280 5902111.309</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.69">
            <title>70</title>
            <aufschrift>70</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>393199.175 5901361.386</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.70">
            <title>64</title>
            <aufschrift>64</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>397411.876 5903376.482</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.71">
            <title>65</title>
            <aufschrift>65</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>397015.667 5902877.316</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.72">
            <title>K2</title>
            <aufschrift>K2</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>434166.535 5917192.777</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.73">
            <title>K3</title>
            <aufschrift>K3</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>406440.326 5901786.708</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.74">
            <title>K7</title>
            <aufschrift>K7</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>398313.713 5903302.856</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.75">
            <title>K4/K5</title>
            <aufschrift>K4/K5</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>402395.611 5903307.406</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.76">
            <title>K6</title>
            <aufschrift>K6</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>397942.233 5903393.853</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.77">
            <title>K8</title>
            <aufschrift>K8</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>396011.562 5902449.475</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.78">
            <title>A2</title>
            <aufschrift>A2</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>397119.485 5902905.582</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.79">
            <title>A1</title>
            <aufschrift>A1</aufschrift>
            <gehoertZuIP xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>397100.160 5903278.820</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_StationFlaeche gml:id="_pfs_stationflaeche.1">
            <title>Station Leer Nord</title>
            <beschreibung>Absperr- und Abzweigstation/Molchschleuse Leer</beschreibung>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiSurface srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:surfaceMember>
                        <gml:Polygon>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:posList>398383.791 5903282.299 398383.239 5903191.630
                                        398450.961 5903191.522 398445.507 5903285.608 398383.791
                                        5903282.299</gml:posList>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </gml:surfaceMember>
                </gml:MultiSurface>
            </position>
            <art>10004</art>
            <begrenzung>2000</begrenzung>
        </PFS_StationFlaeche>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_StationFlaeche gml:id="_pfs_stationflaeche.2">
            <title>Station NKP Sande</title>
            <beschreibung>Gasdruckregel und Messanlage, Absperr- und Abzweigstation und
                Molchschleuse</beschreibung>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiSurface srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:surfaceMember>
                        <gml:Polygon>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:posList>432684.504 5927952.472 432673.497 5927908.889
                                        432683.369 5927910.451 432689.874 5927907.530 432691.663
                                        5927897.836 432752.850 5927908.625 432743.237 5927962.856
                                        432684.504 5927952.472</gml:posList>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </gml:surfaceMember>
                </gml:MultiSurface>
            </position>
            <art>10003</art>
            <begrenzung>2000</begrenzung>
        </PFS_StationFlaeche>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Station gml:id="_pfs_station.1">
            <title>GDRM Westerstede</title>
            <beschreibung>Station ist auch Absperr- und Abzweigstation</beschreibung>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>426970.856 5903997.514</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
            <art>10004</art>
        </PFS_Station>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Station gml:id="_pfs_station.2">
            <title>Station Jemgum</title>
            <beschreibung>Absperr- und Abzweigstation/Molchschleuse Jemgum</beschreibung>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>392517.012 5901277.545</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
            <art>10001</art>
        </PFS_Station>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Armaturengruppe gml:id="_pfs_armaturengruppe.1">
            <title>Streckenarmatur Westerstede</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>427127.578 5903879.796</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Armaturengruppe>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Armaturengruppe gml:id="_pfs_armaturengruppe.2">
            <title>Streckenarmatur Nüttermoor</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>394549.874 5902141.993</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Armaturengruppe>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Armaturengruppe gml:id="_pfs_armaturengruppe.3">
            <title>Streckenarmatur Bockhorn</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>432897.793 5914275.273</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Armaturengruppe>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Armaturengruppe gml:id="_pfs_armaturengruppe.4">
            <title>Streckenarmatur Hollen</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>412742.424 5900547.026</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Armaturengruppe>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baugrube gml:id="_pfs_baugrube.1">
            <title>Start-/Zielgrube</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>433616.412 5927292.777</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 433655.888 5927253.301</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baugrube gml:id="_pfs_baugrube.2">
            <title>Start-/Zielgrube</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>426781.330 5904206.746</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 426684.005 5904161.923</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 426427.440 5904254.944</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 426386.338 5904179.662</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 426243.994 5904046.837</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 426211.218 5904069.422</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 426037.810 5904188.912</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 426069.012 5904167.412</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 425346.106 5904556.897</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 425307.829 5904569.742</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baugrube gml:id="_pfs_baugrube.3">
            <title>Start-/Zielgrube</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>433737.279 5926895.976</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 433758.333 5926823.811</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 433779.164 5926806.551</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 433826.467 5926801.855</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baugrube gml:id="_pfs_baugrube.4">
            <title>Start-/Zielgrube</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>412723.373 5900547.726</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 412804.296 5900541.502</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 411401.093 5900574.511</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 411333.803 5900631.449</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baugrube gml:id="_pfs_baugrube.9">
            <title>Start-/Zielgrube</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>433404.611 5928062.109</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 433420.812 5928004.604</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baugrube gml:id="_pfs_baugrube.10">
            <title>Start-/Zielgrube</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>433488.254 5927765.218</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 433497.555 5927732.203</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baugrube gml:id="_pfs_baugrube.11">
            <title>Start-/Zielgrube</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>434443.868 5922698.134</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 434416.977 5922663.805</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 434416.365 5922512.862</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 434431.995 5922481.230</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baugrube gml:id="_pfs_baugrube.15">
            <title>Start-/Zielgrube</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>434053.010 5920898.610</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 434019.849 5920870.574</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 434025.075 5920802.633</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 434072.266 5920781.607</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baugrube gml:id="_pfs_baugrube.19">
            <title>Start-/Zielgrube</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>434082.707 5919462.690</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 434129.811 5919438.915</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 434259.257 5918275.741</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 434326.333 5918238.011</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baugrube gml:id="_pfs_baugrube.23">
            <title>Start-/Zielgrube</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>433506.322 5916312.435</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 433480.191 5916254.948</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baugrube gml:id="_pfs_baugrube.39">
            <title>Start-/Zielgrube</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>427146.797 5909625.170</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 427090.736 5909586.922</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 426472.185 5907943.375</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 426511.457 5907900.638</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 426962.541 5907409.753</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 426955.448 5907368.375</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 426862.995 5905995.150</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 426872.907 5905949.358</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 426907.529 5905655.908</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 426921.322 5905616.499</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 426926.443 5906564.090</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 426915.864 5906532.665</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 427104.001 5904718.079</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 427110.610 5904660.860</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baugrube gml:id="_pfs_baugrube.55">
            <title>Start-/Zielgrube</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>420055.600 5903086.953</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 420030.989 5903067.709</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 419932.002 5903103.316</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 419847.412 5903133.744</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 421314.142 5903576.066</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 421354.896 5903617.476</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 418964.798 5902740.255</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 419014.658 5902772.813</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
            <art>1000</art>
        </PFS_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baugrube gml:id="_pfs_baugrube.57">
            <title>Start-/Zielgrube</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>417264.238 5901752.442</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 417324.907 5901791.947</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baugrube gml:id="_pfs_baugrube.58">
            <title>Start-/Zielgrube </title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>415307.788 5900923.538</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 415255.425 5901030.884</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 415179.531 5901010.296</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 414287.747 5900767.583</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 414235.674 5900753.411</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baugrube gml:id="_pfs_baugrube.59">
            <title>Start-/Zielgrube</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>408866.108 5901116.081</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 408769.616 5901071.747</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 408412.242 5901182.055</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 408340.439 5901204.285</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baugrube gml:id="_pfs_baugrube.60">
            <title>Start-/Zielgrube</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>405902.844 5902015.262</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 405945.234 5901963.511</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baugrube gml:id="_pfs_baugrube.61">
            <title>Start-/Zielgrube</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>404304.468 5902311.560</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 404281.371 5902315.313</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baugrube gml:id="_pfs_baugrube.62">
            <title>Start-/Zielgrube</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>402956.830 5902690.734</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 402995.495 5902681.316</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 403087.218 5902691.072</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 403088.756 5902643.385</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 402414.560 5903105.856</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 402411.030 5903143.409</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baugrube gml:id="_pfs_baugrube.63">
            <title>Start-/Zielgrube</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>400664.033 5903273.350</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 400743.373 5903289.765</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 400815.990 5903304.773</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 400855.665 5903278.641</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baugrube gml:id="_pfs_baugrube.64">
            <title>Start-/Zielgrube</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>398780.799 5903329.340</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 398812.368 5903331.898</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baugrube gml:id="_pfs_baugrube.65">
            <title>Start-/Zielgrube</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>398182.663 5903310.064</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 398165.768 5903310.993</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 397454.739 5903389.695</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 397321.862 5903348.280</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 397258.037 5903328.387</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 397187.120 5903306.283</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baugrube gml:id="_pfs_baugrube.66">
            <title>Start-/Zielgrube</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>396343.781 5902416.776</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 396330.031 5902418.129</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 396016.486 5902341.165</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 396017.909 5902309.856</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 395754.586 5902176.941</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 395735.775 5902178.577</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 395151.531 5902111.751</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 395093.754 5902111.465</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baugrube gml:id="_pfs_baugrube.67">
            <title>Start-/Zielgrube</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>432896.421 5914275.005</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 432880.202 5914247.831</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 432232.607 5913667.563</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 432229.356 5913648.647</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 431452.519 5913017.366</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 431395.150 5912980.525</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 431279.698 5912828.466</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 431187.401 5912834.921</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 430725.545 5912683.298</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 430653.002 5912654.281</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 430375.925 5912444.690</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 430308.174 5912453.791</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 429903.808 5912131.265</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 429883.286 5912110.496</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 429518.705 5911644.504</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 429493.108 5911616.885</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 428286.878 5910480.470</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 428246.427 5910454.074</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </PFS_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.1">
            <title>Querung Graben</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>433826.467 5926801.855 433779.164 5926806.551</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <gasArt>1000</gasArt>
            <nennweite>DN 600</nennweite>
            <werkstoff>4000</werkstoff>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.2">
            <title>Querung B 436</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>433758.525 5926823.739 433737.279 5926895.976</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.3">
            <title>Neuenburger Urwald</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>433112.347 5914636.775 433165.835 5914726.391 433274.857
                        5914885.287 433367.547 5915082.662 433435.422 5915297.886 433469.713
                        5915529.309</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>3000</legeverfahren>
            <gasArt>1000</gasArt>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.4">
            <title>Anbindung Netzverknüpfungpunkt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>432751.778 5927914.809 432742.817 5927913.128 432740.302
                        5927928.054 432715.640 5927923.755 432717.019 5927914.588 432702.904
                        5927911.505 432695.116 5927948.660 432686.436 5927947.037 432677.026
                        5927911.911 432648.470 5927906.719 432642.645 5927907.611</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>2000</leitungstyp>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 800</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.5">
            <title>Unterquerung K 91</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>433655.888 5927253.301 433616.646 5927292.543</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.6">
            <title>Querung K 15</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>412723.373 5900547.726 412804.296 5900541.502</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.8">
            <title>Anbindung Nütemoor</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>394339.360 5902088.938 394419.915 5902088.208 394420.159
                        5902149.781 394550.361 5902147.590 394549.874 5902141.993</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>2000</leitungstyp>
            <bauweise>3000</bauweise>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.9">
            <title>Querung Mühlenweg</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>433420.812 5928004.604 433404.665 5928061.918</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.10">
            <title>Querung A 28</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>426385.272 5904178.821 426386.338 5904179.662 426426.500
                        5904254.511</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.11">
            <title>Querung L 24</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>426684.213 5904160.754 426780.336 5904206.276</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.12">
            <title>Querung Graben</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>433497.428 5927732.654 433488.254 5927765.218</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.13">
            <title>vorhandener Düker</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>392688.100 5901313.199 393698.425 5901408.171</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>10006</leitungstyp>
            <gasArt>1000</gasArt>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.14">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>433616.646 5927292.543 433616.412 5927292.777 433611.173
                        5927428.998 433497.555 5927732.203 433497.428 5927732.654</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.15">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>433779.164 5926806.551 433763.112 5926808.144 433758.525
                        5926823.739</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.16">
            <title>Betriebsgelände Jemgum</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>392278.996 5901062.407 392245.898 5901241.040 392415.770
                        5901276.085 392515.308 5901288.254 392517.012 5901277.545 392688.100
                        5901313.199</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>2000</leitungstyp>
            <bauweise>1000</bauweise>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.17">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>433404.665 5928061.918 433404.611 5928062.109 433214.400
                        5928012.747 432775.038 5927935.842 432760.111 5927916.373 432751.778
                        5927914.809</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.18">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>433488.254 5927765.218 433420.812 5928004.604</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.19">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>434400.180 5925870.774 434321.094 5926052.380 434307.996
                        5926102.153 434059.130 5926552.730 433924.974 5926772.486 433910.372
                        5926787.837 433896.020 5926794.951 433826.467 5926801.855</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.20">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>433737.279 5926895.976 433723.817 5926941.746 433708.099
                        5927159.175 433679.283 5927229.906 433655.888 5927253.301</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.21">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>426780.336 5904206.276 426781.330 5904206.746 426823.321
                        5904197.834 426857.068 5904182.691 426904.909 5904144.379 426929.321
                        5904093.564 426936.244 5904026.934 426970.856 5903997.514 427127.578
                        5903879.796</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.23">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>426244.029 5904046.872 426337.015 5904140.723 426385.272
                        5904178.821</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.24">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>426426.500 5904254.511 426443.448 5904286.096 426671.762
                        5904154.857 426684.213 5904160.754</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.25">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>411401.093 5900574.511 411401.914 5900573.817 412110.679
                        5900526.741</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.26">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>432896.421 5914275.005 433112.347 5914636.775</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.27">
            <title>Querung B 437</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>433480.279 5916255.141 433506.322 5916312.435</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.28">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>433469.713 5915529.309 433317.774 5915699.585 433477.572
                        5916104.319 433480.191 5916254.948 433480.279 5916255.141</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.29">
            <title>Querung A 28 (bei L 827)</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>415255.425 5901030.884 415307.788 5900923.538 415308.065
                        5900923.511</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.30">
            <title>Querung L 827</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>415179.531 5901010.296 415255.396 5901030.943 415255.425
                        5901030.884</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.31">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>408412.242 5901182.055 408769.181 5901071.548 408769.616
                        5901071.747</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.32">
            <title>Querung B 72</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>408769.616 5901071.747 408866.108 5901116.081 408868.166
                        5901115.787</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.34">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>393698.425 5901408.171 393769.155 5901400.312 394114.947
                        5901578.447 394479.767 5901626.470</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.35">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>394453.753 5901873.809 394424.783 5902143.940 394609.743
                        5902141.019 394610.058 5902101.064 394967.496 5902110.841 395093.754
                        5902111.465</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.36">
            <title>Querung Verbindungsgraben</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>395093.754 5902111.465 395151.531 5902111.751</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.37">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>397258.037 5903328.387 397321.862 5903348.280</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.39">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>396343.709 5902416.783 396343.781 5902416.776 396748.555
                        5902625.220 396776.052 5902641.373 397035.839 5902877.786 396923.195
                        5902875.166 396904.858 5903257.633 397035.839 5903260.252 397099.006
                        5903278.820 397187.120 5903306.283</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.40">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>398812.368 5903331.898 398870.043 5903336.570 398914.336
                        5903296.658 399739.408 5903363.501</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.42">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>406732.261 5901735.122 407135.215 5901596.709</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.43">
            <title>Querung Holtlander Ehetief</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>407135.215 5901596.709 407197.532 5901575.303</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.44">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>402414.560 5903105.856 402424.427 5903000.908 402471.580
                        5902788.718 402791.176 5902731.086 402956.830 5902690.734</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.45">
            <title>Querung B 436</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>402956.830 5902690.734 402995.495 5902681.316</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.47">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>412575.523 5900525.691 412593.375 5900528.415 412721.073
                        5900547.903 412723.373 5900547.726</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>3000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.48">
            <title>Querung Sandrahmer Straße</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>434571.540 5925381.833 434533.284 5925523.214 434462.554
                        5925727.545 434400.180 5925870.774</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>3000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.49">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>434723.064 5924268.829 434700.941 5924523.823 434661.646
                        5924581.455 434656.407 5924995.358 434593.536 5925300.545 434571.540
                        5925381.833</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.50">
            <title>Querung Friedburger Tief</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>434846.091 5923409.781 434847.640 5923410.479 434779.530
                        5923751.031 434750.714 5923950.123 434723.064 5924268.829</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>3000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.51">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>434443.868 5922698.134 434493.990 5922762.119 434397.063
                        5922861.665 434546.382 5923083.025 434446.836 5923211.387 434475.652
                        5923242.822 434846.091 5923409.781</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.52">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>434416.977 5922663.805 434443.868 5922698.134</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.53">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>434416.365 5922512.862 434370.867 5922604.941 434416.977
                        5922663.805</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.54">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>434431.995 5922481.230 434416.365 5922512.862</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.55">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>434053.010 5920898.610 434085.327 5920925.756 434105.354
                        5921016.000 434158.676 5921230.943 434242.505 5921500.765 434310.615
                        5921816.431 434412.781 5922078.395 434535.904 5922270.938 434431.995
                        5922481.230</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.56">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>434019.849 5920870.574 434019.836 5920870.743 434053.010
                        5920898.610</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.57">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>434025.140 5920802.604 434025.075 5920802.633 434019.849
                        5920870.574</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.58">
            <title>Querung L 815 </title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>434072.266 5920781.607 434072.229 5920781.676 434025.140
                        5920802.604</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.59">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>434354.845 5920049.152 434289.658 5920257.749 434292.278
                        5920380.872 434072.266 5920781.607</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.60">
            <title>Querung Alte Bäke</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>434290.108 5920032.657 434355.149 5920048.178 434354.845
                        5920049.152</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.61">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>434082.753 5919463.269 434124.621 5919993.166 434290.108
                        5920032.657</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.62">
            <title>Querung Zettler Tief</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>434129.811 5919438.915 434129.860 5919439.113 434082.707
                        5919462.690 434082.753 5919463.269</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.63">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>434259.257 5918275.741 434158.676 5918332.318 434161.296
                        5918497.355 434119.382 5918598.211 434142.959 5918661.082 434108.903
                        5919355.285 434129.811 5919438.915</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.64">
            <title>Querung Wappenkamper Bäke</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>434325.994 5918238.202 434259.257 5918275.741</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.65">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>434014.642 5917096.461 434166.535 5917192.777 434190.112
                        5917752.069 434273.940 5917841.136 434326.333 5918238.011 434325.994
                        5918238.202</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.66">
            <title>Querung Urwaldstraße</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>433524.442 5916785.626 434014.642 5917096.461</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>3000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.67">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>433506.322 5916312.435 433506.388 5916312.580 433482.811
                        5916380.690 433438.277 5916709.454 433482.811 5916759.227 433524.442
                        5916785.626</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.68">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>431395.150 5912980.525 431452.519 5913017.366</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.69">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>431279.764 5912828.553 431394.962 5912980.405 431395.150
                        5912980.525</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.70">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>430725.545 5912683.298 430865.796 5912739.399 430905.091
                        5912854.663 431187.401 5912834.921</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.71">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>431187.401 5912834.921 431279.698 5912828.466 431279.764
                        5912828.553</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.72">
            <title>Querung L 815</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>430653.002 5912654.281 430725.545 5912683.298</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.75">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>429903.808 5912131.265 430014.415 5912242.978 430072.047
                        5912394.917 430200.409 5912468.267 430308.174 5912453.791</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.76">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>430308.174 5912453.791 430375.898 5912444.693</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.77">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>430375.898 5912444.693 430375.925 5912444.690 430577.636
                        5912624.135 430653.002 5912654.281</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.78">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>429883.286 5912110.496 429883.433 5912110.686 429903.808
                        5912131.265</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.79">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>429518.705 5911644.504 429542.881 5911670.588 429883.286
                        5912110.496</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.80">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>428286.878 5910480.470 428502.886 5910621.424 429037.291
                        5911057.594 429147.316 5911235.729 429461.672 5911467.566 429493.108
                        5911616.885 429493.118 5911616.896</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.81">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>429493.118 5911616.896 429518.705 5911644.504</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.82">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>428246.427 5910454.074 428286.878 5910480.470</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.83">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>427146.797 5909625.170 427879.413 5910125.004 428039.211
                        5910318.857 428246.427 5910454.074</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.84">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>426472.185 5907943.375 426383.602 5908039.775 426585.314
                        5908385.567 426938.964 5908640.981 426944.204 5909040.475 427009.695
                        5909200.273 427067.327 5909570.951 427090.736 5909586.922</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.85">
            <title>Querung K 114</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>427090.736 5909586.922 427146.797 5909625.170</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.86">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>426962.488 5907409.440 426962.541 5907409.753 426511.457
                        5907900.638</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.87">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>426511.457 5907900.638 426472.185 5907943.375</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.88">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>426926.443 5906564.090 426973.020 5906702.452 427109.241
                        5906925.120 426946.823 5907318.066 426955.448 5907368.375</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.89">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>426955.448 5907368.375 426962.488 5907409.440</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.90">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>426862.970 5905995.184 426794.885 5906086.837 426881.332
                        5906276.761 426839.418 5906305.577 426915.864 5906532.665</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.91">
            <title>Querung Halsbeker Moorgraben</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>426915.864 5906532.665 426926.443 5906564.090</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.92">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>426910.616 5905647.087 426907.529 5905655.908 426928.486
                        5905692.583 426872.907 5905949.358</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.93">
            <title>Querung Wasserzug</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>426872.907 5905949.358 426862.995 5905995.150 426862.970
                        5905995.184</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.94">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>427104.085 5904717.356 427104.001 5904718.079 427054.228
                        5904762.612 426993.977 5905224.978 426899.670 5905561.601 426925.866
                        5905603.515 426921.322 5905616.499</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.95">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>426921.322 5905616.499 426910.616 5905647.087</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.96">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>426970.856 5903997.514 427153.774 5904287.149 427110.610
                        5904660.860</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.97">
            <title>Querung Große Norderbäke 1</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>427110.610 5904660.860 427104.085 5904717.356</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.98">
            <title>Querung Große Norderbäke 2</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>426211.218 5904069.422 426243.994 5904046.837 426244.029
                        5904046.872</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.99">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>426069.012 5904167.412 426211.218 5904069.422</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.100">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>426037.749 5904188.986 426037.810 5904188.912 426069.012
                        5904167.412</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.101">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>425346.106 5904556.897 425880.632 5904377.526 426037.749
                        5904188.986</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.102">
            <title>Querung Gehölze</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>425307.829 5904569.742 425346.106 5904556.897</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.103">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>423839.993 5904909.147 424196.207 5904877.876 424358.625
                        5904699.741 424497.465 5904691.882 424633.686 5904778.330 425099.981
                        5904639.490 425307.829 5904569.742</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.104">
            <title>Querung K 114</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>422940.823 5904804.254 422941.403 5904804.527 423509.863
                        5904938.128 423839.993 5904909.147</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>3000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.105">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>421354.896 5903617.476 421563.475 5903832.642 421655.162
                        5903827.403 421723.273 5903887.655 421799.242 5904050.072 422451.531
                        5904550.422 422718.734 5904699.741 422940.823 5904804.254</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.106">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>420055.600 5903086.953 420523.480 5903452.795 421065.744
                        5903654.507 421314.142 5903576.066</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.107">
            <title>Querung K 68</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>421314.142 5903576.066 421314.610 5903575.918 421354.896
                        5903617.476</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.108">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>419932.002 5903103.316 420030.164 5903068.006</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.109">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>420030.164 5903068.006 420030.989 5903067.709 420055.600
                        5903086.953</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.110">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>419014.658 5902772.813 419666.860 5903198.691 419847.412
                        5903133.744</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.111">
            <title>Querung Stapeler Hauptvorfluter</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>419847.412 5903133.744 419932.002 5903103.316</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.112">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>417324.399 5901791.617 417324.907 5901791.947 417262.035
                        5901901.972 417594.729 5902123.331 418108.177 5902463.883 418770.945
                        5902842.420 418964.294 5902740.520</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.113">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>418964.294 5902740.520 418964.798 5902740.255 419014.658
                        5902772.813</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.114">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>415308.065 5900923.511 415910.304 5900864.597 416525.918
                        5901455.324 416963.397 5901709.429 417212.262 5901718.597 417264.238
                        5901752.442</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.115">
            <title>Querung K 11</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>417264.238 5901752.442 417324.399 5901791.617</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.117">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>414287.747 5900767.583 414956.454 5900949.582 415179.531
                        5901010.296</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.118">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>412804.719 5900541.469 412943.934 5900543.224 413005.074
                        5900548.382 413160.945 5900562.527 413631.222 5900619.661 413793.640
                        5900674.673 413991.553 5900686.970 414235.674 5900753.411</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.119">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>414235.674 5900753.411 414287.747 5900767.583</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.120">
            <title>Querung Nordgeorgsfehnkanal</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>412110.679 5900526.741 412111.835 5900526.664 412424.517
                        5900525.997 412575.424 5900525.676 412575.523 5900525.691</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>3000</legeverfahren>
            <gasArt>1000</gasArt>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.121">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>409577.853 5900851.060 409769.882 5900806.965 410393.354
                        5900691.701 411281.410 5900592.155 411333.564 5900631.270</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.122">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>411333.564 5900631.270 411333.803 5900631.449 411401.093
                        5900574.511</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.123">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>408868.166 5901115.787 408976.132 5901100.364 409062.580
                        5900969.382 409338.587 5900906.003</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.124">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>409338.587 5900906.003 409577.853 5900851.060</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>3000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.127">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>407407.547 5901503.164 407794.677 5901370.186 407854.929
                        5901393.763 407923.039 5901333.511 408340.439 5901204.285</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.128">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>408340.439 5901204.285 408412.242 5901182.055</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.129">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>407197.532 5901575.303 407407.547 5901503.164</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.130">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>405945.234 5901963.511 405971.412 5901930.788 406440.326
                        5901786.708 406505.817 5901812.904 406732.261 5901735.122</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.131">
            <title>Querung Heimschloot</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>405902.844 5902015.262 405945.234 5901963.511</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.132">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>405435.324 5902074.167 405457.964 5902069.628 405596.804
                        5902116.782 405866.626 5902061.769 405902.844 5902015.262</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.133">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>404304.468 5902311.560 404326.282 5902308.015 404347.239
                        5902292.297 405207.726 5902119.794</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.134">
            <title>Querung K 17</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>405207.726 5902119.794 405408.491 5902079.546 405435.324
                        5902074.167</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>3000</legeverfahren>
            <gasArt>1000</gasArt>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.135">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>404281.371 5902315.313 404304.468 5902311.560</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.136">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>404125.547 5902340.634 404281.371 5902315.313</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.137">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>403088.756 5902643.385 403092.434 5902529.374 403336.060
                        5902469.123 403590.164 5902423.279 403639.937 5902478.291 403697.569
                        5902410.181 403936.518 5902371.352</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.138">
            <title>Querung K 55</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>403936.518 5902371.352 404125.547 5902340.634</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>3000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.139">
            <title>Querung Wanderweg</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>403087.218 5902691.072 403088.756 5902643.385</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.140">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>402995.495 5902681.316 402995.507 5902681.313 403087.195
                        5902691.792 403087.218 5902691.072</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.141">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>400855.665 5903278.641 400996.726 5903296.927 401043.999
                        5903336.956 401232.188 5903331.315 401575.666 5903323.123 401578.285
                        5903380.755 401633.298 5903383.375 401643.776 5903325.743 402395.611
                        5903307.406 402411.030 5903143.409</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.142">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>402411.030 5903143.409 402414.560 5903105.856</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.143">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>400743.373 5903289.765 400815.972 5903304.786 400815.990
                        5903304.773</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.144">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>400815.990 5903304.773 400855.266 5903278.590 400855.665
                        5903278.641</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.145">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>400564.510 5903393.811 400606.401 5903317.884 400637.836
                        5903330.982 400664.033 5903273.350 400664.260 5903273.397</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.146">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>400664.260 5903273.397 400743.373 5903289.765</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.147">
            <title>Querung A 31</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>399739.408 5903363.501 399920.057 5903378.136 400564.487
                        5903393.853 400564.510 5903393.811</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>3000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.148">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>398182.663 5903310.064 398313.713 5903302.856 398425.651
                        5903304.446 398425.601 5903274.216 398437.940 5903273.969 398436.459
                        5903301.442 398780.799 5903329.340</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.149">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>398780.799 5903329.340 398812.368 5903331.898</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.150">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>397454.739 5903389.695 397468.079 5903393.853 397942.233
                        5903393.853 397984.147 5903336.222 398156.484 5903311.503 398165.768
                        5903310.993</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.151">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>398165.768 5903310.993 398182.663 5903310.064</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.152">
            <title>Querung B 70</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>397187.120 5903306.283 397258.037 5903328.387</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.153">
            <title>Querung Gleise </title>
            <beschreibung>Vortrieb mit Mantelrohr DN1000</beschreibung>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>397321.862 5903348.280 397412.065 5903376.395 397454.739
                        5903389.695</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.154">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>396016.486 5902341.165 396011.562 5902449.475 396330.031
                        5902418.129</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.155">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>396330.031 5902418.129 396343.709 5902416.783</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.156">
            <title>Querung K 2</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>396017.909 5902309.856 396016.486 5902341.165</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.157">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>395754.586 5902176.941 395783.654 5902174.414 396019.421
                        5902276.579 396017.909 5902309.856</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.158">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>395151.531 5902111.751 395639.574 5902114.162 395723.403
                        5902179.653 395735.775 5902178.577</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.159">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>395735.775 5902178.577 395754.586 5902176.941</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.160">
            <title>Querung Nüttermoorer Sieltief</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>394479.767 5901626.470 394480.271 5901626.537 394453.753
                        5901873.809</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>3000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.161">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>432232.607 5913667.563 432243.724 5913732.240 432285.638
                        5913800.351 432466.393 5914013.851 432867.197 5914226.041 432880.202
                        5914247.831</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.162">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>432880.202 5914247.831 432896.421 5914275.005</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.163">
            <title>Abschnitt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>431452.519 5913017.366 431929.368 5913323.577 432214.908
                        5913564.583 432229.356 5913648.647</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>1000</bauweise>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">10</schutzstreifen>
                    <SchutzstreifenGehoelzfrei uom="m">2.5</SchutzstreifenGehoelzfrei>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <arbeitsstreifen>
                <PFS_ArbeitsstreifenData>
                    <arbeitsstreifenFlur uom="m">42</arbeitsstreifenFlur>
                </PFS_ArbeitsstreifenData>
            </arbeitsstreifen>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_GasversorgungsleitungAbschnitt gml:id="_pfs_gasversorgungsleitungabschnitt.164">
            <title>Straßenquerung</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>432229.356 5913648.647 432232.607 5913667.563</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>2000</legeverfahren>
            <gasArt>1000</gasArt>
            <regelueberdeckung uom="m">1.2</regelueberdeckung>
            <nennweite>DN 600</nennweite>
        </PFS_GasversorgungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_AlternativeTrasseAbschnitt gml:id="_pfs_alternativetrasseabschnitt.1">
            <title>Umgehung NSG</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>434166.535 5917192.777 434396.099 5917602.437 434408.405
                        5917873.184 434565.315 5918473.134 434872.982 5919156.154 435103.732
                        5919436.131 435336.021 5920091.461 434586.852 5920237.603 434105.354
                        5921016.000</gml:posList>
                </gml:LineString>
            </position>
        </PFS_AlternativeTrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_AlternativeTrasseAbschnitt gml:id="_pfs_alternativetrasseabschnitt.2">
            <title>Alternative Brinkum</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>402395.611 5903307.406 402836.635 5903292.692 403496.292
                        5903433.175 403622.522 5903399.242 403964.567 5903579.765 404037.862
                        5903537.688 404349.365 5903476.432 405258.008 5902744.185 405895.903
                        5902502.154 406440.326 5901786.708</gml:posList>
                </gml:LineString>
            </position>
        </PFS_AlternativeTrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_AlternativeTrasseAbschnitt gml:id="_pfs_alternativetrasseabschnitt.3">
            <title>Alternative A </title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>397119.485 5902905.582 397099.006 5903278.820 397100.160
                        5903278.820 397097.852 5903277.666 397097.852 5903278.820 397097.852
                        5903278.820</gml:posList>
                </gml:LineString>
            </position>
        </PFS_AlternativeTrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_AlternativeTrasseAbschnitt gml:id="_pfs_alternativetrasseabschnitt.4">
            <title>Alternative C </title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>396748.555 5902625.220 396711.635 5903229.785</gml:posList>
                </gml:LineString>
            </position>
        </PFS_AlternativeTrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_AlternativeTrasseAbschnitt gml:id="_pfs_alternativetrasseabschnitt.5">
            <title>Alternative D</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>396011.562 5902449.475 396088.609 5902881.353 396300.899
                        5903229.785 396711.635 5903229.785 396904.858 5903257.633 397035.839
                        5903260.252 397100.160 5903278.820 397100.160 5903278.820</gml:posList>
                </gml:LineString>
            </position>
        </PFS_AlternativeTrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_AlternativeTrasseAbschnitt gml:id="_pfs_alternativetrasseabschnitt.6">
            <title>Alternative Süd</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>397035.839 5902877.786 397078.889 5902903.820 397119.485
                        5902905.582 397497.531 5902924.426 398096.199 5903065.341 398310.131
                        5903172.817 398313.713 5903302.856</gml:posList>
                </gml:LineString>
            </position>
        </PFS_AlternativeTrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_AlternativeTrasseAbschnitt gml:id="_pfs_alternativetrasseabschnitt.7">
            <title>Alternative B</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>397035.839 5902877.786 396923.195 5902875.166 396904.858
                        5903257.633</gml:posList>
                </gml:LineString>
            </position>
        </PFS_AlternativeTrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_AlternativeTrasseAbschnitt gml:id="_pfs_alternativetrasseabschnitt.8">
            <title>Alternative A,B,C</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>396011.562 5902449.475 396343.781 5902416.776 396748.555
                        5902625.220 396776.052 5902641.373 397035.839 5902877.786</gml:posList>
                </gml:LineString>
            </position>
        </PFS_AlternativeTrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_AlternativeTrasseAbschnitt gml:id="_pfs_alternativetrasseabschnitt.9">
            <title>Variante Veenhusen</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>397942.233 5903393.853 398865.690 5903446.847 399370.881
                        5903856.943 401835.420 5904162.038 401898.817 5904027.321 402271.271
                        5904059.019 402702.169 5903299.252 402395.611 5903307.406</gml:posList>
                </gml:LineString>
            </position>
        </PFS_AlternativeTrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baustelle gml:id="_pfs_baustelle.1">
            <title>Rohrlagerplatz</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiSurface srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:surfaceMember>
                        <gml:Polygon>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:posList>433595.249 5926776.730 433612.471 5926734.048
                                        433715.557 5926772.986 433713.061 5926780.723 433723.295
                                        5926787.463 433732.780 5926784.967 433735.775 5926773.734
                                        433809.408 5926765.248 433806.413 5926778.727 433742.514
                                        5926786.963 433731.033 5926829.895 433595.249 5926776.730</gml:posList>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </gml:surfaceMember>
                </gml:MultiSurface>
            </position>
            <art>1000</art>
        </PFS_Baustelle>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baustelle gml:id="_pfs_baustelle.2">
            <title>Erdbecken</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiSurface srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:surfaceMember>
                        <gml:Polygon>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:posList>433695.464 5926934.354 433686.354 5926932.295
                                        433699.458 5926883.622 433708.569 5926885.931 433695.464
                                        5926934.354</gml:posList>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </gml:surfaceMember>
                </gml:MultiSurface>
            </position>
            <art>2000</art>
        </PFS_Baustelle>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baustelle gml:id="_pfs_baustelle.3">
            <title>Enteisungsanlage</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiSurface srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:surfaceMember>
                        <gml:Polygon>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:posList>433695.464 5926934.354 433703.358 5926905.494
                                        433713.333 5926908.222 433705.439 5926937.082 433695.464
                                        5926934.354</gml:posList>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </gml:surfaceMember>
                </gml:MultiSurface>
            </position>
            <art>2000</art>
        </PFS_Baustelle>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baustelle gml:id="_pfs_baustelle.4">
            <title>Zufahrt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiSurface srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:surfaceMember>
                        <gml:Polygon>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:posList>433702.961 5926956.261 433584.580 5927025.464
                                        433506.149 5927087.643 433501.221 5927081.352 433580.700
                                        5927018.544 433716.382 5926940.323 433717.640 5926948.292
                                        433702.961 5926956.261</gml:posList>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </gml:surfaceMember>
                </gml:MultiSurface>
            </position>
            <art>3000</art>
        </PFS_Baustelle>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baustelle gml:id="_pfs_baustelle.5">
            <title>Lagerplatz</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiSurface srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:surfaceMember>
                        <gml:Polygon>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:posList>426419.328 5904179.878 426251.890 5904003.030
                                        426369.031 5903906.980 426547.178 5903930.993 426523.814
                                        5904127.311 426419.328 5904179.878</gml:posList>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </gml:surfaceMember>
                </gml:MultiSurface>
            </position>
            <art>1000</art>
        </PFS_Baustelle>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baustelle gml:id="_pfs_baustelle.6">
            <title>Rohrlagerplatz</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiSurface srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:surfaceMember>
                        <gml:Polygon>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:posList>412800.186 5900615.902 412799.455 5900571.447
                                        412883.978 5900573.787 412884.270 5900618.242 412800.186
                                        5900615.902</gml:posList>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </gml:surfaceMember>
                </gml:MultiSurface>
            </position>
            <art>1000</art>
        </PFS_Baustelle>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_Baustelle gml:id="_pfs_baustelle.7">
            <title>Zufahrt</title>
            <gehoertZuPFS xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:MultiSurface srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:surfaceMember>
                        <gml:Polygon>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:posList>426735.708 5903843.704 426581.249 5903819.043
                                        426562.753 5903946.893 426545.880 5903946.244 426547.178
                                        5903930.993 426552.369 5903932.615 426570.866 5903807.361
                                        426737.655 5903832.996 426735.708 5903843.704</gml:posList>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </gml:surfaceMember>
                </gml:MultiSurface>
            </position>
            <art>3000</art>
        </PFS_Baustelle>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Gasleitung gml:id="_bst_gasleitung.1">
            <title>WAL</title>
            <gehoertZuPlan xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>430067.162 5924270.731 430390.204 5924614.133 430452.640
                                5924681.998 430574.799 5925019.971 430648.094 5924998.254 430905.985
                                5925068.834 430930.417 5925060.690 431087.866 5925189.636 431888.684
                                5925769.211 432103.140 5926013.528 432048.847 5926202.195 432591.775
                                5926666.398 432868.668 5927027.445 432542.911 5927620.593 432642.645
                                5927907.611 432944.678 5928722.736 432901.243 5928850.324 432898.529
                                5929015.917 432852.380 5929026.776 432860.524 5929211.371</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <gasArt>1000</gasArt>
        </BST_Gasleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Gasleitung gml:id="_bst_gasleitung.2">
            <title>GTG DN 400</title>
            <gehoertZuPlan xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>414760.679 5905283.200 414934.416 5905310.346 415043.002
                                5905207.190 415995.839 5904751.131 417540.468 5904707.697 419508.580
                                5904593.682 420162.808 5904479.667 421313.814 5904229.921 421411.541
                                5904110.477 421894.746 5903627.271 422231.361 5903361.237 422901.877
                                5902747.729 424109.890 5903296.086 424674.535 5903122.349 425505.214
                                5903388.383 425526.931 5903350.378 426362.109 5903671.724 426722.944
                                5903824.884 426786.977 5903827.913 427025.411 5903763.003 427127.578
                                5903879.796 427334.879 5903746.715 427877.807 5904072.472 428225.281
                                5904072.472 428773.637 5904371.082 429419.721 5904007.321</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>394279.005 5902077.622 395660.491 5902085.357 396081.260
                                5902265.202 396192.560 5902276.061 396248.210 5902368.358 396776.052
                                5902641.373 397078.889 5902903.820 397497.531 5902924.426 398096.199
                                5903065.341 398523.647 5903279.250 398766.036 5903302.194 400531.221
                                5903399.515 400548.196 5903368.702 400665.604 5903356.486 402707.690
                                5903326.625 402836.635 5903292.692 403112.171 5903346.306 403272.334
                                5903396.527 403496.292 5903433.175 403622.522 5903399.242 403964.567
                                5903579.765 404037.862 5903537.688 405368.034 5903293.371 407021.248
                                5903553.976 407851.927 5904124.050 408487.152 5904232.635 408644.601
                                5904183.772 409795.608 5904449.806 414760.679 5905283.200</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <nennweite>DN400</nennweite>
            <gasArt>1000</gasArt>
            <druckstufe>3000</druckstufe>
        </BST_Gasleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Energiespeicher gml:id="_bst_energiespeicher.1">
            <title>Speicher Jemgum</title>
            <gehoertZuPlan xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiSurface srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:surfaceMember>
                        <gml:Polygon>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:posList>392322.989 5902003.411 392368.920 5901801.009
                                        392449.460 5901485.155 392490.701 5901270.461 392563.478
                                        5900946.844 392735.233 5900970.133 392718.737 5901341.783
                                        392651.054 5901330.866 392613.937 5901358.279 392587.738
                                        5901506.745 392685.745 5901532.945 392577.387 5902051.606
                                        392322.989 5902003.411</gml:posList>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </gml:surfaceMember>
                    <gml:surfaceMember>
                        <gml:Polygon>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:posList>392466.684 5901326.015 392342.477 5901304.666
                                        392319.674 5901323.589 392202.502 5901298.844 392306.816
                                        5900805.534 392440.242 5900825.427 392428.597 5900879.767
                                        392560.567 5900898.689 392526.119 5901059.770 392466.684
                                        5901326.015</gml:posList>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </gml:surfaceMember>
                </gml:MultiSurface>
            </position>
            <netzSparte>2000</netzSparte>
            <begrenzung>1000</begrenzung>
            <art>20001</art>
            <gasArt>1000</gasArt>
        </BST_Energiespeicher>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Energiespeicher gml:id="_bst_energiespeicher.2">
            <title>Speicher Nütemoor</title>
            <gehoertZuPlan xlink:title="Gasanbindung Wilhelmshaven - Leer (GWL)"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiSurface srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:surfaceMember>
                        <gml:Polygon>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:posList>394169.193 5902346.234 394169.648 5902346.416
                                        394160.368 5902347.235 394158.185 5902220.329 394156.275
                                        5902203.681 394149.725 5902196.585 394144.266 5902191.127
                                        394113.973 5902190.581 394103.693 5902198.495 394076.765
                                        5902197.040 394071.671 5902128.629 394097.143 5902130.812
                                        394098.599 5902010.730 394231.781 5902021.646 394261.620
                                        5902020.191 394286.364 5902012.185 394375.881 5902006.363
                                        394369.331 5902224.695 394536.719 5902225.423 394530.897
                                        5902325.856 394364.509 5902322.126 394339.947 5902327.585
                                        394169.193 5902346.234</gml:posList>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </gml:surfaceMember>
                </gml:MultiSurface>
            </position>
            <netzSparte>2000</netzSparte>
            <begrenzung>1000</begrenzung>
            <art>20001</art>
            <gasArt>10002</gasArt>
        </BST_Energiespeicher>
    </sf:featureMember>
</sf:FeatureCollection>