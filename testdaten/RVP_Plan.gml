<?xml version="1.0" encoding="utf-8"?>
<sf:FeatureCollection xmlns="http://www.xtrasse.de/2.0" xmlns:xtrasse="http://www.xtrasse.de/2.0"
    xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xml="http://www.w3.org/XML/1998/namespace"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:sf="http://www.opengis.net/ogcapi-features-1/1.0/sf"
    xsi:schemaLocation="http://www.xtrasse.de/2.0 https://repository.gdi-de.org/schemas/de.xleitstelle.xtrasse/2.0/XML/XTrasse.xsd
                        http://www.opengis.net/ogcapi-features-1/1.0/sf http://schemas.opengis.net/ogcapi/features/part1/1.0/xml/core-sf.xsd">
    <sf:featureMember>
        <RVP_Plan gml:id="_rvp_plan.1">
            <name>380-kV-Leitung UW Emden/Ost UW Conneforde</name>
            <beschreibung>Das Raumordnungsverfahren betrifft eine neue 380-kV-Höchstspannungsleitung
                zwischen den Umspannwerken Emden/Ost und Conneforde (Gemeinde Wiefelstede, Landkreis
                Ammerland), die die bestehende 220-kV-Höchstspannungsleitung ersetzen soll. Die neue
                Trasse ist nötig, weil die Kapazität der bestehenden Leitung für den Abtransport der
                zukünftig aus On- und Offshore-Windparks ankommenden Strommengen nicht ausreicht.</beschreibung>
            <gesetzlicheGrundlage>
                <XP_GesetzlicheGrundlage>
                    <name>Raumordnungsgesetz (ROG)</name>
                    <detail>§ 15</detail>
                    <ausfertigungDatum>2008-12-22</ausfertigungDatum>
                    <letzteAenderungDatum>2012-09-01</letzteAenderungDatum>
                </XP_GesetzlicheGrundlage>
            </gesetzlicheGrundlage>
            <gesetzlicheGrundlage>
                <XP_GesetzlicheGrundlage>
                    <name>Raumordnungsverordnung (ROV)</name>
                    <detail>§ 1</detail>
                    <ausfertigungDatum>1990-12-13</ausfertigungDatum>
                    <letzteAenderungDatum>2012-06-01</letzteAenderungDatum>
                </XP_GesetzlicheGrundlage>
            </gesetzlicheGrundlage>
            <technischePlanerstellung>XLeitstelle</technischePlanerstellung>
            <technHerstellDatum>2024-06-17</technHerstellDatum>
            <externeReferenz>
                <XP_NetzExterneReferenz>
                    <referenzName>Landesplanerische Festlegung</referenzName>
                    <referenzURL>
                        https://www.arl-we.niedersachsen.de/download/98019/Landesplanerische_Feststellung.pdf</referenzURL>
                    <beschreibung>Landesplanerische Feststellung
                        Raumordnungsverfahren
                        mit integrierter Umweltverträglichkeitsprüfung
                        380-kV-Freileitung Emden/Ost – Conneforde </beschreibung>
                    <datum>2015-06-24</datum>
                </XP_NetzExterneReferenz>
            </externeReferenz>
            <externeReferenz>
                <XP_NetzExterneReferenz>
                    <referenzName>Karte landesplanerische Feststellung</referenzName>
                    <referenzURL>
                        https://www.arl-we.niedersachsen.de/download/98018/Karte_landesplanerische_Feststellung.pdf</referenzURL>
                    <beschreibung>Karte zur Landesplanerischen Feststellung
                        vom 24.06.2015</beschreibung>
                </XP_NetzExterneReferenz>
            </externeReferenz>
            <position>
                <gml:Polygon srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>380339.250 5899487.296 438721.912 5899487.296 438721.912
                                5923931.708 380339.250 5923931.708 380339.250 5899487.296</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </position>
            <hatObjekt xlink:title="Karte3_Raumordnung_Naturschutzfachliches_Blatt1"
                xlink:href="#_xp_planreferenz.1" />
            <hatObjekt xlink:title="Karte3_Raumordnung_Naturschutzfachliches_Blatt2"
                xlink:href="#_xp_planreferenz.2" />
            <hatObjekt xlink:title="Karte3_Raumordnung_Naturschutzfachliches_Blatt3"
                xlink:href="#_xp_planreferenz.3" />
            <hatObjekt xlink:title="Karte3_Raumordnung_Naturschutzfachliches_Blatt4"
                xlink:href="#_xp_planreferenz.4" />
            <hatObjekt xlink:title="Karte3_Raumordnung_Naturschutzfachliches_Blatt5"
                xlink:href="#_xp_planreferenz.5" />
            <hatBSTObjekt xlink:title="Konverterstation Emden-Ost"
                xlink:href="#_bst_stationflaeche.1" />
            <hatBSTObjekt xlink:title="Varel-Conneforde" xlink:href="#_bst_stromleitung.1" />
            <hatBSTObjekt xlink:title="Berne-Conneforde" xlink:href="#_bst_stromleitung.2" />
            <hatBSTObjekt xlink:title="Unterweser-Conneforde" xlink:href="#_bst_stromleitung.3" />
            <hatBSTObjekt xlink:title="Farge-Conneforde" xlink:href="#_bst_stromleitung.4" />
            <hatBSTObjekt xlink:title="Abzweigung Conneforde" xlink:href="#_bst_stromleitung.5" />
            <hatBSTObjekt xlink:title="Cloppenburg-Conneforde" xlink:href="#_bst_stromleitung.6" />
            <hatBSTObjekt xlink:title="Diele-Conneforde" xlink:href="#_bst_stromleitung.7" />
            <hatBSTObjekt xlink:title="Wiesmoor-Conneforde" xlink:href="#_bst_stromleitung.8" />
            <hatBSTObjekt xlink:title="Emden-Leer" xlink:href="#_bst_stromleitung.9" />
            <hatBSTObjekt xlink:title="Anschluss Conneford" xlink:href="#_bst_stromleitung.10" />
            <hatBSTObjekt xlink:title="Verbindung Konverter" xlink:href="#_bst_stromleitung.11" />
            <hatBSTObjekt xlink:title="Maade-Conneforde" xlink:href="#_bst_stromleitung.12" />
            <hatBSTObjekt xlink:title="Bestandsleitung" xlink:href="#_bst_stromleitung.13" />
            <hatBSTObjekt xlink:title="Umspannwerk Emden-Borsum" xlink:href="#_bst_umspannwerk.3" />
            <hatBSTObjekt xlink:title="Umspannwerk Conneforde" xlink:href="#_bst_umspannwerk.4" />
            <hatIPObjekt xlink:title="32" xlink:href="#_ip_stationierungspunkt.53" />
            <hatIPObjekt xlink:title="2" xlink:href="#_ip_stationierungspunkt.54" />
            <hatIPObjekt xlink:title="3" xlink:href="#_ip_stationierungspunkt.55" />
            <hatIPObjekt xlink:title="29" xlink:href="#_ip_stationierungspunkt.56" />
            <hatIPObjekt xlink:title="7" xlink:href="#_ip_stationierungspunkt.57" />
            <hatIPObjekt xlink:title="4" xlink:href="#_ip_stationierungspunkt.58" />
            <hatIPObjekt xlink:title="1" xlink:href="#_ip_stationierungspunkt.59" />
            <hatIPObjekt xlink:title="6" xlink:href="#_ip_stationierungspunkt.60" />
            <hatIPObjekt xlink:title="11" xlink:href="#_ip_stationierungspunkt.61" />
            <hatIPObjekt xlink:title="8" xlink:href="#_ip_stationierungspunkt.62" />
            <hatIPObjekt xlink:title="5" xlink:href="#_ip_stationierungspunkt.63" />
            <hatIPObjekt xlink:title="10" xlink:href="#_ip_stationierungspunkt.64" />
            <hatIPObjekt xlink:title="15" xlink:href="#_ip_stationierungspunkt.65" />
            <hatIPObjekt xlink:title="12" xlink:href="#_ip_stationierungspunkt.66" />
            <hatIPObjekt xlink:title="9" xlink:href="#_ip_stationierungspunkt.67" />
            <hatIPObjekt xlink:title="14" xlink:href="#_ip_stationierungspunkt.68" />
            <hatIPObjekt xlink:title="21" xlink:href="#_ip_stationierungspunkt.69" />
            <hatIPObjekt xlink:title="26" xlink:href="#_ip_stationierungspunkt.70" />
            <hatIPObjekt xlink:title="23" xlink:href="#_ip_stationierungspunkt.71" />
            <hatIPObjekt xlink:title="20" xlink:href="#_ip_stationierungspunkt.72" />
            <hatIPObjekt xlink:title="25" xlink:href="#_ip_stationierungspunkt.73" />
            <hatIPObjekt xlink:title="30" xlink:href="#_ip_stationierungspunkt.74" />
            <hatIPObjekt xlink:title="27" xlink:href="#_ip_stationierungspunkt.75" />
            <hatIPObjekt xlink:title="24" xlink:href="#_ip_stationierungspunkt.76" />
            <hatIPObjekt xlink:title="31" xlink:href="#_ip_stationierungspunkt.77" />
            <hatIPObjekt xlink:title="28" xlink:href="#_ip_stationierungspunkt.78" />
            <hatIPObjekt xlink:title="37" xlink:href="#_ip_stationierungspunkt.79" />
            <hatIPObjekt xlink:title="42" xlink:href="#_ip_stationierungspunkt.80" />
            <hatIPObjekt xlink:title="39" xlink:href="#_ip_stationierungspunkt.81" />
            <hatIPObjekt xlink:title="36" xlink:href="#_ip_stationierungspunkt.82" />
            <hatIPObjekt xlink:title="41" xlink:href="#_ip_stationierungspunkt.83" />
            <hatIPObjekt xlink:title="46" xlink:href="#_ip_stationierungspunkt.84" />
            <hatIPObjekt xlink:title="43" xlink:href="#_ip_stationierungspunkt.85" />
            <hatIPObjekt xlink:title="40" xlink:href="#_ip_stationierungspunkt.86" />
            <hatIPObjekt xlink:title="45" xlink:href="#_ip_stationierungspunkt.87" />
            <hatIPObjekt xlink:title="18" xlink:href="#_ip_stationierungspunkt.88" />
            <hatIPObjekt xlink:title="47" xlink:href="#_ip_stationierungspunkt.89" />
            <hatIPObjekt xlink:title="44" xlink:href="#_ip_stationierungspunkt.90" />
            <hatIPObjekt xlink:title="17" xlink:href="#_ip_stationierungspunkt.91" />
            <hatIPObjekt xlink:title="22" xlink:href="#_ip_stationierungspunkt.92" />
            <hatIPObjekt xlink:title="19" xlink:href="#_ip_stationierungspunkt.93" />
            <hatIPObjekt xlink:title="16" xlink:href="#_ip_stationierungspunkt.94" />
            <hatIPObjekt xlink:title="51" xlink:href="#_ip_stationierungspunkt.95" />
            <hatIPObjekt xlink:title="48" xlink:href="#_ip_stationierungspunkt.96" />
            <hatIPObjekt xlink:title="13" xlink:href="#_ip_stationierungspunkt.97" />
            <hatIPObjekt xlink:title="50" xlink:href="#_ip_stationierungspunkt.98" />
            <hatIPObjekt xlink:title="55" xlink:href="#_ip_stationierungspunkt.99" />
            <hatIPObjekt xlink:title="54" xlink:href="#_ip_stationierungspunkt.100" />
            <hatIPObjekt xlink:title="49" xlink:href="#_ip_stationierungspunkt.101" />
            <hatIPObjekt xlink:title="52" xlink:href="#_ip_stationierungspunkt.102" />
            <hatIPObjekt xlink:title="57" xlink:href="#_ip_stationierungspunkt.103" />
            <hatIPObjekt xlink:title="34" xlink:href="#_ip_stationierungspunkt.104" />
            <hatIPObjekt xlink:title="53" xlink:href="#_ip_stationierungspunkt.105" />
            <hatIPObjekt xlink:title="56" xlink:href="#_ip_stationierungspunkt.106" />
            <hatIPObjekt xlink:title="33" xlink:href="#_ip_stationierungspunkt.107" />
            <hatIPObjekt xlink:title="38" xlink:href="#_ip_stationierungspunkt.108" />
            <hatIPObjekt xlink:title="35" xlink:href="#_ip_stationierungspunkt.109" />
            <hatRVPObjekt xlink:title="Trassenkorridor" xlink:href="#_rvp_trassenkorridor.1" />
            <hatRVPObjekt xlink:title="Abschnitt" xlink:href="#_rvp_trassenkorridorachse.1" />
            <hatRVPObjekt xlink:title="Umgehung EU VS V07 Fehntjer Tief"
                xlink:href="#_rvp_trassenkorridorachse.2" />
            <hatRVPObjekt xlink:title="B" xlink:href="#_rvp_trassenkorridorachse.3" />
            <hatRVPObjekt xlink:title="C1, C2" xlink:href="#_rvp_trassenkorridorachse.4" />
            <hatRVPObjekt xlink:title="A" xlink:href="#_rvp_trassenkorridorachse.5" />
            <hatRVPObjekt xlink:title="A1" xlink:href="#_rvp_trassenkorridorachse.6" />
            <hatRVPObjekt xlink:title="Umgehung Stapeler Moor Süd"
                xlink:href="#_rvp_trassenkorridorachse.7" />
            <hatRVPObjekt xlink:title="O2" xlink:href="#_rvp_trassenkorridorachse.8" />
            <hatRVPObjekt xlink:title="H1" xlink:href="#_rvp_trassenkorridorachse.9" />
            <hatRVPObjekt xlink:title="??" xlink:href="#_rvp_trassenkorridorachse.10" />
            <hatRVPObjekt xlink:title="F" xlink:href="#_rvp_trassenkorridorachse.11" />
            <hatRVPObjekt xlink:title="D" xlink:href="#_rvp_trassenkorridorachse.12" />
            <hatRVPObjekt xlink:title="S1" xlink:href="#_rvp_trassenkorridorachse.13" />
            <hatRVPObjekt xlink:title="S2" xlink:href="#_rvp_trassenkorridorachse.14" />
            <hatRVPObjekt xlink:title="A2" xlink:href="#_rvp_trassenkorridorachse.15" />
            <hatRVPObjekt xlink:title="C1" xlink:href="#_rvp_trassenkorridorachse.16" />
            <hatRVPObjekt xlink:title="C2" xlink:href="#_rvp_trassenkorridorachse.17" />
            <hatRVPObjekt xlink:title="C" xlink:href="#_rvp_trassenkorridorachse.18" />
            <hatRVPObjekt xlink:title="H, E, J" xlink:href="#_rvp_trassenkorridorachse.19" />
            <hatRVPObjekt xlink:title="G" xlink:href="#_rvp_trassenkorridorachse.20" />
            <hatRVPObjekt xlink:title="E" xlink:href="#_rvp_trassenkorridorachse.21" />
            <hatRVPObjekt xlink:title="J" xlink:href="#_rvp_trassenkorridorachse.22" />
            <hatRVPObjekt xlink:title="H" xlink:href="#_rvp_trassenkorridorachse.23" />
            <hatRVPObjekt xlink:title="H2" xlink:href="#_rvp_trassenkorridorachse.24" />
            <hatRVPObjekt xlink:title="J2" xlink:href="#_rvp_trassenkorridorachse.25" />
            <hatRVPObjekt xlink:title="J1" xlink:href="#_rvp_trassenkorridorachse.26" />
            <hatRVPObjekt xlink:title="G2" xlink:href="#_rvp_trassenkorridorachse.27" />
            <hatRVPObjekt xlink:title="G1" xlink:href="#_rvp_trassenkorridorachse.28" />
            <hatRVPObjekt xlink:title="B, C1, C2" xlink:href="#_rvp_trassenkorridorachse.29" />
            <hatRVPObjekt xlink:title="C2" xlink:href="#_rvp_trassenkorridorachse.30" />
            <hatRVPObjekt xlink:title="C, C2" xlink:href="#_rvp_trassenkorridorachse.31" />
            <hatRVPObjekt xlink:title="O" xlink:href="#_rvp_trassenkorridorachse.32" />
            <hatRVPObjekt xlink:title="Umgehung Stapeler Moor Nord"
                xlink:href="#_rvp_trassenkorridorachse.33" />
            <hatRVPObjekt xlink:title="O1" xlink:href="#_rvp_trassenkorridorachse.34" />
            <hatRVPObjekt xlink:title="O1, O2" xlink:href="#_rvp_trassenkorridorachse.35" />
            <hatRVPObjekt xlink:title="G" xlink:href="#_rvp_trassenkorridorachse.36" />
            <hatRVPObjekt xlink:title="H1, J1" xlink:href="#_rvp_trassenkorridorachse.37" />
            <hatRVPObjekt xlink:title="E, J2" xlink:href="#_rvp_trassenkorridorachse.38" />
            <hatRVPObjekt xlink:title="Vorzugstrasse" xlink:href="#_rvp_trassenkorridorachse.39" />
            <hatRVPObjekt xlink:title="Abschnitt" xlink:href="#_rvp_trassenkorridorachse.40" />
            <hatRVPObjekt xlink:title="Abschnitt" xlink:href="#_rvp_trassenkorridorachse.41" />
            <hatRVPObjekt xlink:title="Abschnitt" xlink:href="#_rvp_trassenkorridorachse.42" />
            <hatRVPObjekt xlink:title="Abschnitt" xlink:href="#_rvp_trassenkorridorachse.43" />
            <hatRVPObjekt xlink:title="Abschnitt" xlink:href="#_rvp_trassenkorridorachse.44" />
            <hatRVPObjekt xlink:title="C" xlink:href="#_rvp_trassenkorridorachse.45" />
            <externerDienst>
                <IP_Webservice>
                    <name>Naturschutzgebiete (NDS)</name>
                    <beschreibung>Kartendienst zum Naturschutz</beschreibung>
                    <typ>1000</typ>
                    <url>
                        https://www.umweltkarten-niedersachsen.de/arcgis/services/Natur_wms/MapServer/WmsServer?SERVICE=WMS&amp;REQUEST=GetCapabilities&amp;VERSION=1.3.0</url>
                </IP_Webservice>
            </externerDienst>
            <version>
                <RVP_Version>
                    <versionName>Finale Version</versionName>
                    <datum>2015-01-01</datum>
                </RVP_Version>
            </version>
            <beteiligte>
                <XP_Akteur>
                    <nameOrganisation>Amt für regionale Landesentwicklung Weser-Ems</nameOrganisation>
                    <namePerson>Herr Bernhard Heidrich</namePerson>
                    <strasseHausnr>Theodor-Tantzen-Platz 8</strasseHausnr>
                    <postleitzahl>26122</postleitzahl>
                    <ort>Oldenburg</ort>
                    <telefon>0441/9215-474</telefon>
                    <rolle>7100</rolle>
                </XP_Akteur>
            </beteiligte>
            <festlegungDatum>2015-06-24</festlegungDatum>
        </RVP_Plan>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Trassenkorridor gml:id="_rvp_trassenkorridor.1">
            <title>Trassenkorridor</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiSurface srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:surfaceMember>
                        <gml:Polygon>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:posList>381340.598 5912918.571 381336.452 5912910.972
                                        381332.440 5912903.302 381328.561 5912895.563 381324.818
                                        5912887.758 381321.212 5912879.888 381317.743 5912871.957
                                        381314.413 5912863.967 381311.222 5912855.920 381308.172
                                        5912847.819 381305.264 5912839.665 381302.498 5912831.463
                                        381299.876 5912823.213 381297.397 5912814.919 381295.064
                                        5912806.583 381292.876 5912798.208 381290.834 5912789.796
                                        381288.940 5912781.349 381287.192 5912772.871 381285.593
                                        5912764.363 381284.142 5912755.830 381282.840 5912747.272
                                        381281.687 5912738.692 381280.684 5912730.094 381279.830
                                        5912721.480 381279.127 5912712.852 381278.575 5912704.213
                                        381278.173 5912695.566 381277.921 5912686.913 381277.821
                                        5912678.258 381277.871 5912669.601 381278.072 5912660.947
                                        381278.424 5912652.298 381278.926 5912643.656 381279.579
                                        5912635.024 381280.382 5912626.405 381281.335 5912617.802
                                        381282.438 5912609.216 381283.690 5912600.650 381285.091
                                        5912592.108 381286.641 5912583.591 381288.339 5912575.103
                                        381290.185 5912566.646 381292.177 5912558.222 381294.316
                                        5912549.834 381296.601 5912541.484 381299.031 5912533.176
                                        381301.606 5912524.911 381304.324 5912516.693 381307.184
                                        5912508.523 381310.187 5912500.404 381313.330 5912492.338
                                        381316.614 5912484.329 381320.037 5912476.378 381323.597
                                        5912468.487 381327.295 5912460.660 381331.128 5912452.899
                                        381335.096 5912445.206 381339.198 5912437.583 381343.431
                                        5912430.032 381347.796 5912422.556 381352.290 5912415.158
                                        381356.912 5912407.839 381361.661 5912400.601 381366.535
                                        5912393.448 381371.533 5912386.380 381376.654 5912379.401
                                        381381.895 5912372.511 381387.256 5912365.715 381392.734
                                        5912359.012 381398.328 5912352.406 381404.037 5912345.899
                                        381409.858 5912339.492 381415.789 5912333.187 381421.830
                                        5912326.986 381427.978 5912320.892 381434.230 5912314.906
                                        381440.587 5912309.029 381447.044 5912303.265 381453.601
                                        5912297.613 381460.256 5912292.077 381467.006 5912286.657
                                        381473.849 5912281.356 381480.784 5912276.175 381487.808
                                        5912271.116 381494.919 5912266.179 381502.114 5912261.367
                                        381509.393 5912256.682 381516.752 5912252.123 381524.189
                                        5912247.694 381531.703 5912243.395 381539.290 5912239.227
                                        381546.949 5912235.193 381554.676 5912231.292 381562.471
                                        5912227.526 381570.330 5912223.897 381578.251 5912220.406
                                        381586.231 5912217.052 381594.269 5912213.839 381602.362
                                        5912210.766 381610.507 5912207.834 381618.701 5912205.045
                                        381626.943 5912202.399 381635.230 5912199.896 381643.560
                                        5912197.539 381651.929 5912195.327 381660.335 5912193.261
                                        381668.776 5912191.342 381677.249 5912189.570 381685.752
                                        5912187.946 381694.282 5912186.471 381702.836 5912185.144
                                        381711.412 5912183.966 381720.007 5912182.939 381728.619
                                        5912182.061 381737.244 5912181.333 381745.881 5912180.755
                                        381754.527 5912180.328 381763.179 5912180.052 381771.835
                                        5912179.926 381780.491 5912179.952 381789.146 5912180.128
                                        381797.796 5912180.455 381806.439 5912180.932 381815.073
                                        5912181.560 381823.694 5912182.338 381832.301 5912183.267
                                        381840.890 5912184.345 381849.625 5912184.085 381859.462
                                        5912184.657 381874.104 5912186.488 381894.236 5912189.690
                                        381930.841 5912196.554 381966.530 5912207.535 382076.342
                                        5912261.526 382815.747 5912720.909 385450.219 5912420.068
                                        387941.133 5912520.730 390087.969 5912586.159 390351.519
                                        5912523.932 392200.031 5912538.574 395084.440 5912619.103
                                        396731.629 5912648.387 398854.672 5912652.047 399235.355
                                        5911905.322 399685.587 5911612.488 400476.237 5911261.088
                                        400959.412 5911158.596 401398.091 5911064.684 402346.139
                                        5911324.573 402955.599 5912009.072 403027.892 5912116.139
                                        403063.581 5912203.989 403084.628 5912306.481 403066.326
                                        5912499.568 403315.235 5912694.485 404155.301 5912705.466
                                        406457.704 5912718.278 408489.236 5912520.615 409501.342
                                        5912447.407 410163.878 5912275.367 410355.592 5912222.749
                                        411556.210 5912288.636 411784.986 5912308.768 412131.810
                                        5912264.843 413586.827 5911759.706 415250.487 5911210.643
                                        415420.697 5911212.473 416322.990 5911368.041 416478.558
                                        5911239.926 416577.389 5911181.359 416678.050 5911155.737
                                        416868.392 5911163.057 419341.005 5911651.723 419444.412
                                        5911693.818 419692.405 5911882.330 420919.103 5912064.893
                                        421585.299 5911633.879 421898.265 5911173.581 421978.794
                                        5911100.373 422835.332 5910661.122 423022.013 5910591.575
                                        423490.547 5910525.687 425668.496 5911257.771 426626.611
                                        5910964.480 427775.982 5910744.855 430177.217 5910305.604
                                        431260.472 5910392.082 433420.119 5910666.613 434281.232
                                        5910123.498 434167.759 5909519.529 434180.571 5909437.628
                                        434211.684 5909336.966 434270.251 5909239.965 434376.403
                                        5909131.983 434972.136 5908709.204 435105.742 5908674.430
                                        435270.460 5908672.600 435442.500 5908734.827 435786.579
                                        5908926.999 436057.450 5908897.716 436178.244 5908926.999
                                        436335.642 5908985.566 436566.249 5909205.191 436681.552
                                        5909300.819 436781.298 5909263.300 436890.196 5909253.234
                                        436995.433 5909259.640 437079.622 5909284.348 437136.359
                                        5909311.801 437242.511 5909390.500 437343.172 5909516.784
                                        437396.248 5909618.361 437418.211 5909742.815 437405.399
                                        5909881.911 437374.286 5909997.214 437275.455 5910125.329
                                        437172.963 5910204.028 437088.773 5910279.066 436532.390
                                        5911184.562 436611.089 5912096.007 436611.089 5912588.333
                                        436583.636 5912659.711 436532.390 5912753.052 436462.842
                                        5912831.751 436391.464 5912899.468 435817.693 5913095.301
                                        435513.878 5913186.811 435389.424 5913188.642 435286.932
                                        5913166.679 434574.981 5912862.864 434441.376 5912764.033
                                        433562.875 5911687.870 430228.463 5911304.899 427792.454
                                        5911754.673 427344.053 5911848.013 427102.465 5912034.695
                                        426983.501 5912093.261 426886.500 5912109.733 425786.545
                                        5912267.131 425689.543 5912281.773 425590.712 5912272.622
                                        423519.830 5911587.208 422332.482 5912347.203 421852.967
                                        5913075.626 421743.155 5913174.457 420934.202 5913385.847
                                        420432.725 5913491.999 420308.271 5913481.017 420187.477
                                        5913455.395 419316.297 5912836.784 419148.833 5912810.246
                                        416586.540 5912418.581 416480.388 5912444.204 415104.070
                                        5912316.089 414269.495 5912596.111 413160.388 5912974.965
                                        412795.261 5913200.995 412241.623 5913564.292 412148.282
                                        5913603.641 412030.234 5913620.113 411925.912 5913617.368
                                        411833.486 5913601.811 411743.806 5913570.698 411652.296
                                        5913502.065 411445.482 5913309.893 410193.161 5913391.795
                                        408683.239 5913501.607 406637.065 5913717.572 404520.428
                                        5913710.251 404099.479 5914098.256 403652.908 5915138.730
                                        403611.729 5915245.797 403553.162 5915315.345 403498.256
                                        5915359.270 403454.331 5915392.214 402645.378 5915626.480
                                        401876.690 5915567.914 400538.464 5915042.300 400277.087
                                        5914850.472 399389.093 5914083.271 397595.488 5913647.681
                                        396706.006 5913636.700 393656.877 5913629.379 392203.691
                                        5913567.152 390523.559 5913526.887 390124.573 5913673.304
                                        387956.690 5913854.037 387414.948 5913894.302 386422.060
                                        5914188.965 386360.748 5914200.862 386296.690 5914202.692
                                        386234.463 5914198.116 386159.425 5914179.814 385551.795
                                        5913868.679 384817.881 5913942.345 384543.350 5913953.783
                                        384505.831 5913940.972 384134.298 5913828.414 383779.238
                                        5913714.712 383680.406 5913815.374 383622.297 5913850.605
                                        383555.952 5913881.261 383475.423 5913893.615 383405.875
                                        5913899.563 383323.515 5913899.563 383242.986 5913888.582
                                        383164.287 5913854.266 383111.211 5913820.864 383006.889
                                        5913718.373 382933.681 5913712.882 382756.151 5913738.505
                                        382631.696 5913742.165 382521.884 5913714.712 382420.422
                                        5913652.485 381455.901 5913063.158 381413.806 5913028.384
                                        381340.598 5912918.571</gml:posList>
                                </gml:LinearRing>
                            </gml:exterior>
                            <gml:interior>
                                <gml:LinearRing>
                                    <gml:posList>400569.921 5913790.780 401034.794 5914182.445
                                        402092.655 5914585.091 402542.886 5914614.375 402835.720
                                        5914526.525 403227.385 5913688.289 400687.054 5913659.005
                                        400569.921 5913790.780</gml:posList>
                                </gml:LinearRing>
                            </gml:interior>
                        </gml:Polygon>
                    </gml:surfaceMember>
                </gml:MultiSurface>
            </position>
            <art>1000</art>
            <status>2000</status>
        </RVP_Trassenkorridor>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.1">
            <title>Abschnitt</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>410544.104 5913346.497 411604.585 5914245.166 412647.132
                                5913940.166 412979.860 5913302.437 413756.226 5913058.437 414865.319
                                5913152.710 416626.118 5913866.620</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>5000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.2">
            <title>Umgehung EU VS V07 Fehntjer Tief</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>397694.433 5913174.801 396171.699 5912164.525 395834.941
                                5911403.158 394985.723 5910510.016 393536.198 5910319.674 392251.391
                                5912340.225 391578.674 5913054.236</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.3">
            <title>B</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>404305.035 5913199.280 403774.274 5913708.078 403206.909
                                5914919.676 402888.453 5914978.243 402661.507 5915091.716 401969.688
                                5915113.679 400758.089 5914612.201 399666.369 5913884.693</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.4">
            <title>C1, C2</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>399666.369 5913884.693 400010.449 5913743.767 400471.661
                                5913158.100</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.5">
            <title>A</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>386345.053 5913683.384 387182.626 5913407.237 390024.827
                                5913195.162 390407.341 5913026.782</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.6">
            <title>A1</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>383330.457 5913393.511 383500.245 5913146.432 383802.230
                                5913192.188 384627.654 5913452.077 385674.655 5913414.784 386345.053
                                5913683.384</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.7">
            <title>Umgehung Stapeler Moor Süd</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>428624.970 5911018.814 427611.034 5910001.218 427603.714
                                5909195.926 425356.217 5908192.971 423701.707 5908881.130 423255.136
                                5909986.576 422098.215 5911768.857</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.8">
            <title>O2</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>422098.215 5911768.857 421166.639 5912517.412 420266.405
                                5912889.288</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.9">
            <title>H1</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>429009.314 5911004.172 428280.891 5911853.389 426527.551
                                5912409.773 425802.788 5911868.031</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.10">
            <title>??</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>400961.450 5914864.872 401177.723 5916323.330 405176.004
                                5918602.516 407186.235 5920178.815 408284.237 5920278.633 408658.556
                                5920622.452 412365.700 5922768.547 414215.113 5923073.548 415168.933
                                5923112.366 415898.162 5922424.728 416524.799 5922546.728 416802.072
                                5922835.093 417245.710 5922851.729 417808.575 5923334.185 418518.394
                                5923550.458 419627.487 5923117.911 420833.626 5921673.318 421138.627
                                5921545.772 422522.220 5920962.112 424241.315 5920030.473 424765.361
                                5919629.814 425136.907 5919086.358 425558.363 5918291.970 426736.774
                                5917203.672 428112.050 5916715.671 428466.959 5916015.556 428500.232
                                5915394.464 428855.142 5915350.100 429662.007 5914938.350 430111.190
                                5914028.893 430912.510 5914023.348 431411.602 5914195.257 431988.330
                                5914256.257 432842.332 5914389.349 433696.334 5914378.258 434067.880
                                5914028.893 434738.881 5914112.075 435836.883 5913629.620 436105.036
                                5912474.860</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>5000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.11">
            <title>F</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>432463.854 5911031.569 431593.216 5910044.476 432602.490
                                5909362.384 433312.310 5908608.201 434022.130 5908730.201 434659.858
                                5909284.747 435036.950 5909123.929 435191.075 5909171.790</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>5000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.12">
            <title>D</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>426972.291 5911218.307 427700.299 5909811.567 427988.663
                                5909700.657 428387.937 5909750.567 429546.939 5909295.838 430417.577
                                5909722.839 431593.216 5910044.476</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>5000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.13">
            <title>S1</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>409673.382 5912947.054 409647.759 5912932.412 410386.706
                                5912718.735 411587.323 5912801.095</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.14">
            <title>S2</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>409898.498 5912926.922 410544.104 5913346.497 410974.203
                                5913192.759 411223.876 5912807.535</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.15">
            <title>A2</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>383330.457 5913393.511 383332.781 5913258.075 383485.285
                                5913081.349 384652.680 5912949.547 385674.655 5913414.784 386345.053
                                5913683.384</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.16">
            <title>C1</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>402365.013 5913184.638 401675.024 5912919.258 400990.526
                                5912846.049 400723.315 5913040.051 400471.661 5913158.100</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.17">
            <title>C2</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>402365.013 5913184.638 400807.505 5911705.829</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.18">
            <title>C</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>403137.699 5913186.416 402533.392 5912734.406 402599.280
                                5912364.704 402145.388 5911790.018 401354.737 5911588.695 400807.505
                                5911705.829</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.19">
            <title>H, E, J</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>436852.397 5909570.079 436705.344 5909465.996 436472.908
                                5909736.867 436476.568 5910100.049 436277.075 5910737.991 435976.921
                                5910939.314</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.20">
            <title>G</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>436949.442 5909789.125 436544.972 5909868.184 436051.960
                                5909411.890 435666.014 5909431.679</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.21">
            <title>E</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>436088.564 5911850.759 436103.206 5911962.401 436105.036
                                5912474.860 435420.538 5912692.655 434792.776 5912405.312 433845.871
                                5911212.473</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.22">
            <title>J</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>434786.599 5912054.369 433845.871 5911212.473</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.23">
            <title>H</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>435682.257 5911586.179 434811.078 5911725.275 434217.732
                                5911255.174 433845.871 5911212.473</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.24">
            <title>H2</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>426785.610 5911630.104 426972.291 5911218.307 428624.970
                                5911018.814</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.25">
            <title>J2</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>436088.564 5911850.759 435777.428 5912153.544 435027.043
                                5912285.319 434786.599 5912054.369</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.26">
            <title>J1</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>435682.257 5911586.179 435173.459 5912076.675 434786.599
                                5912054.369</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.27">
            <title>G2</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>435666.014 5909431.679 435191.075 5909171.790 434668.321
                                5909521.703 434811.078 5910305.032</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.28">
            <title>G1</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>435666.014 5909431.679 434845.165 5910301.029</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.29">
            <title>B, C1, C2</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>398474.903 5913148.949 399666.369 5913884.693</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.30">
            <title>C2</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>400471.661 5913158.100 400170.592 5912547.725 400110.195
                                5911958.398</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.31">
            <title>C, C2</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>400807.505 5911705.829 400624.484 5911738.773 400110.195
                                5911958.398</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.32">
            <title>O</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>420266.405 5912889.288 419215.636 5912144.507 416763.155
                                5911657.671 416444.699 5911935.863 415031.777 5911811.409</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.33">
            <title>Umgehung Stapeler Moor Nord</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>429009.314 5911004.172 428280.891 5911853.389 426736.194
                                5912417.094 426392.115 5914269.266 425023.118 5915074.558 422072.821
                                5914320.512 421077.187 5915140.445 416626.118 5913866.620 415535.313
                                5912629.398 414041.863 5912182.827</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.34">
            <title>O1</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>422098.215 5911768.857 421470.453 5912752.137 420374.158
                                5912980.913 420266.405 5912889.288</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.35">
            <title>O1, O2</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>423450.969 5911026.135 422819.318 5911190.511 422098.215
                                5911768.857</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.36">
            <title>G</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>434845.165 5910301.029 434120.403 5910981.867 433585.981
                                5911157.567</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.37">
            <title>H1, J1</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>436002.544 5911255.940 435682.257 5911586.179</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.38">
            <title>E, J2</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>435976.921 5910939.314 436088.564 5911850.759</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.39">
            <title>Vorzugstrasse</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>436852.397 5909570.079 436531.691 5910087.615 436350.255
                                5910437.849 436261.747 5910754.921 435969.600 5910955.786 435950.600
                                5911262.321 435471.284 5911782.257 435311.512 5912120.757 435070.501
                                5912258.864 433973.762 5911300.233 430236.725 5910829.042 427100.635
                                5911384.970 426785.610 5911630.104 425404.304 5911657.689 423673.894
                                5911059.222 422801.919 5911021.310 421990.873 5911733.513 421190.660
                                5912424.052 420275.357 5912890.505 419351.930 5912144.452 416780.687
                                5911685.446 416482.807 5911931.874 415049.599 5911842.510 414064.512
                                5912144.965 412955.640 5912553.359 411996.375 5913149.750 411588.238
                                5912803.382 410386.706 5912718.735 409691.827 5912871.549 408915.985
                                5913052.985 406661.578 5913278.425 404033.466 5913259.469 403774.274
                                5913708.078 403111.393 5914947.906 402363.986 5915153.714 402011.946
                                5915133.404 401045.546 5914741.460 400683.674 5914506.502 398754.225
                                5913215.126 394946.781 5913183.984 393882.908 5913143.916 390930.821
                                5913076.341 390251.114 5913203.617 387182.626 5913407.237 386345.053
                                5913683.384 385674.655 5913414.784 384627.654 5913452.077 383802.230
                                5913192.188 383500.245 5913146.432 383330.457 5913393.511 383332.781
                                5913258.075 383181.994 5913317.427</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>2000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.40">
            <title>Abschnitt</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>386281.216 5912046.389 390407.341 5913026.782</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>5000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.41">
            <title>Abschnitt</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>404305.035 5913199.280 403137.699 5913186.416 402365.013
                                5913184.638</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.42">
            <title>Abschnitt</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>382787.091 5913261.020 386281.216 5912046.389 391676.954
                                5911721.980 392231.501 5911333.797 392885.866 5910102.704 393030.048
                                5908671.973 393551.321 5907463.062 395464.507 5905372.421 397250.147
                                5903786.418 398281.604 5903708.782 399002.514 5903897.328 400449.881
                                5904684.784 400682.790 5904662.602 401303.882 5905172.785 403915.797
                                5904562.783 404517.480 5903646.395 404927.844 5903064.121 406111.801
                                5901856.596 408074.896 5901290.959 409389.171 5900692.048 409766.263
                                5900270.593 411185.902 5899937.865 412261.723 5900337.139 414779.364
                                5900736.412 415589.002 5900492.412 416725.822 5901216.095 419997.647
                                5903412.099 421933.015 5904268.874 422997.744 5905322.512 423019.926
                                5905666.331 424794.475 5908172.882 425356.217 5908192.971</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>5000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.43">
            <title>Abschnitt</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>397668.696 5913151.694 399460.015 5914171.689 400961.450
                                5914864.872 402024.793 5914648.599 402191.157 5914371.326 402889.886
                                5913938.779 403688.433 5913417.506 404309.525 5912763.141 406996.303
                                5910524.159 407417.758 5910407.704 407678.395 5909736.703 407528.668
                                5908716.337 408058.260 5908222.791 410143.355 5908261.609 411610.130
                                5908910.428 412999.269 5908472.337 413298.725 5908134.063 416401.413
                                5908394.700 418544.735 5908527.791 419484.691 5908945.088 420959.785
                                5910187.272 423372.063 5911035.728</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>5000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.44">
            <title>Abschnitt</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>391676.954 5911721.980 395306.461 5912247.412 397180.829
                                5912286.231 398744.650 5911981.230 399260.378 5912502.504 399604.142
                                5912264.043</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>5000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorAchse gml:id="_rvp_trassenkorridorachse.45">
            <title>C</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>397668.696 5913151.694 399060.570 5913208.431 399291.176
                                5912908.276 399604.142 5912264.043 399929.919 5912035.266 400110.195
                                5911958.398</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <art>4000</art>
        </RVP_TrassenkorridorAchse>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.53">
            <title>32</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>413475.190 5912322.541</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.54">
            <title>2</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>383485.285 5913081.349</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.55">
            <title>3</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>384652.680 5912949.547</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.56">
            <title>29</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>410548.330 5912858.445</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.57">
            <title>7</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>388575.096 5913021.607</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.58">
            <title>4</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>385576.829 5912919.739</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.59">
            <title>1</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>382625.723 5913167.362</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.60">
            <title>6</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>387575.721 5912986.267</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.61">
            <title>11</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>392566.096 5913028.398</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.62">
            <title>8</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>389574.472 5913056.947</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.63">
            <title>5</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>386576.291 5912952.517</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.64">
            <title>10</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>391566.164 5913016.885</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.65">
            <title>15</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>396564.870 5913114.793</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.66">
            <title>12</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>393565.519 5913061.445</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.67">
            <title>9</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>390566.168 5913013.836</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.68">
            <title>14</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>395564.923 5913104.475</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.69">
            <title>21</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>402564.606 5913165.012</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.70">
            <title>26</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>407559.049 5913109.034</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.71">
            <title>23</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>404564.463 5913188.916</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.72">
            <title>20</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>401564.677 5913153.041</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.73">
            <title>25</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>406564.245 5913210.836</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.74">
            <title>30</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>411545.735 5912786.595</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.75">
            <title>27</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>408553.973 5913008.568</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.76">
            <title>24</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>405564.393 5913200.742</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.77">
            <title>31</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>412528.210 5912643.833</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.78">
            <title>28</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>409551.152 5912933.506</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.79">
            <title>37</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>418335.198 5912168.774</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.80">
            <title>42</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>422940.677 5911341.504</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.81">
            <title>39</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>420312.488 5912469.302</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.82">
            <title>36</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>417346.159 5912021.120</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.83">
            <title>41</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>422099.961 5911882.982</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.84">
            <title>46</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>426666.577 5911455.326</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.85">
            <title>43</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>423822.632 5911140.359</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.86">
            <title>40</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>421259.246 5912424.459</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.87">
            <title>45</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>425714.361 5911760.752</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.88">
            <title>18</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>399564.710 5913145.746</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.89">
            <title>47</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>427646.033 5911256.398</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.90">
            <title>44</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>424768.344 5911465.366</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.91">
            <title>17</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>398564.763 5913135.428</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.92">
            <title>22</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>403564.534 5913176.982</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.93">
            <title>19</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>400564.703 5913148.016</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.94">
            <title>16</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>397564.817 5913125.111</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.95">
            <title>51</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>431588.667 5910932.588</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.96">
            <title>48</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>428627.954 5911067.103</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.97">
            <title>13</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>394564.976 5913094.157</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.98">
            <title>50</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>430596.035 5910811.423</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.99">
            <title>55</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>435513.150 5911216.592</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.100">
            <title>54</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>432581.300 5911053.753</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.101">
            <title>49</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>429609.874 5910877.807</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.102">
            <title>52</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>434566.564 5911296.083</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.103">
            <title>57</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>436791.083 5909820.706</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.104">
            <title>34</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>415371.274 5911705.203</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.105">
            <title>53</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>433573.932 5911174.918</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.106">
            <title>56</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>436301.781 5910618.423</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.107">
            <title>33</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>414422.170 5912001.250</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.108">
            <title>38</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>419323.937 5912318.414</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <IP_Stationierungspunkt gml:id="_ip_stationierungspunkt.109">
            <title>35</title>
            <gehoertZuIP xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Point srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pos>416358.620 5911863.782</gml:pos>
                </gml:Point>
            </position>
        </IP_Stationierungspunkt>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Stromleitung gml:id="_bst_stromleitung.1">
            <title>Varel-Conneforde</title>
            <gehoertZuPlan xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>438719.658 5910737.173 437910.870 5909876.391 437061.642
                                5909982.304</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>438723.509 5910642.814 438057.222 5909849.431 437910.870
                                5909876.391</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>437020.781 5910022.382 437061.642 5909982.304</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <leitungstyp>20001</leitungstyp>
            <spannung>30001</spannung>
        </BST_Stromleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Stromleitung gml:id="_bst_stromleitung.2">
            <title>Berne-Conneforde</title>
            <gehoertZuPlan xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>438721.584 5909404.598 437902.204 5909670.342 437115.391
                                5909944.184 437019.815 5909966.388 436991.818 5909995.350</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <leitungstyp>20001</leitungstyp>
            <spannung>30001</spannung>
        </BST_Stromleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Stromleitung gml:id="_bst_stromleitung.3">
            <title>Unterweser-Conneforde</title>
            <gehoertZuPlan xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>438719.658 5909050.993 438006.191 5908945.080 437569.060
                                5909319.386 437160.283 5909757.376 437112.978 5909786.580</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>437035.262 5909841.608 437053.363 5909824.231 437112.978
                                5909786.580</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <leitungstyp>20001</leitungstyp>
            <spannung>30002</spannung>
        </BST_Stromleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Stromleitung gml:id="_bst_stromleitung.4">
            <title>Farge-Conneforde</title>
            <gehoertZuPlan xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>438721.584 5908350.525 437670.159 5909145.111 437085.071
                                5909764.861</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>437012.575 5909826.403 437041.054 5909798.889 437085.071
                                5909764.861</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <leitungstyp>20001</leitungstyp>
            <spannung>30002</spannung>
        </BST_Stromleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Stromleitung gml:id="_bst_stromleitung.5">
            <title>Abzweigung Conneforde</title>
            <gehoertZuPlan xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>429515.837 5899495.414 429931.786 5899911.362 430245.031
                                5900652.109 430090.976 5901155.356 431921.958 5903510.705 433790.807
                                5906027.930 435296.057 5908212.915 435723.222 5908832.051 436094.947
                                5909385.581 436836.056 5909837.669</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <leitungstyp>20001</leitungstyp>
            <spannung>30001</spannung>
        </BST_Stromleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Stromleitung gml:id="_bst_stromleitung.6">
            <title>Cloppenburg-Conneforde</title>
            <gehoertZuPlan xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>436650.507 5899477.440 436873.887 5900938.074 436521.615
                                5903859.048 436183.442 5905974.534 435734.664 5908701.952 436137.312
                                5909270.040 436459.152 5909524.410 436607.584 5909579.438</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <leitungstyp>20001</leitungstyp>
            <spannung>30002</spannung>
        </BST_Stromleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Stromleitung gml:id="_bst_stromleitung.7">
            <title>Diele-Conneforde</title>
            <gehoertZuPlan xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>412630.772 5899480.008 415439.707 5900445.419 426679.300
                                5907937.625 434695.935 5909897.975 435921.635 5909555.042 436585.997
                                5909655.178 436714.021 5909541.787</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <leitungstyp>20001</leitungstyp>
            <spannung>30003</spannung>
        </BST_Stromleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Stromleitung gml:id="_bst_stromleitung.8">
            <title>Wiesmoor-Conneforde</title>
            <gehoertZuPlan xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>385890.041 5912079.858 384961.859 5912221.075 382498.478
                                5912982.497 382365.819 5913061.236 381928.474 5912785.649</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>436977.337 5910028.174 436440.326 5910239.600 435758.914
                                5910478.008 434213.230 5911001.794 430341.315 5912312.545 422972.995
                                5916298.958 420903.523 5916981.935 416993.095 5918962.825 415809.439
                                5918921.744 415583.492 5918993.636 409477.780 5916636.595 406876.819
                                5915686.590 405783.671 5915572.092 398604.710 5914830.060 387941.133
                                5912520.730 385890.041 5912079.858</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>400455.976 5900426.886 400357.473 5900528.436 399285.106
                                5902230.412 399053.572 5902319.776 399515.586 5903893.950 398636.992
                                5904673.251 396806.386 5907961.964 395462.015 5909238.935 389236.029
                                5910561.641 385890.041 5912079.858</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>381832.648 5912729.641 381928.474 5912785.649</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>395259.414 5899505.575 395145.932 5900616.784 396275.167
                                5901928.809 397964.957 5902131.909 398761.108 5902375.629 399053.572
                                5902319.776</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <leitungstyp>20001</leitungstyp>
            <spannung>30001</spannung>
        </BST_Stromleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Stromleitung gml:id="_bst_stromleitung.9">
            <title>Emden-Leer</title>
            <gehoertZuPlan xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>395261.503 5899486.534 395259.414 5899505.575 395214.003
                                5899669.153 395177.412 5899995.172 394936.704 5902365.115 393550.209
                                5906421.254 389797.687 5909560.124 386126.044 5910785.181 385652.111
                                5911792.423 384970.846 5912141.480 382341.855 5912949.974 381972.934
                                5912716.970 381873.919 5912658.321</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <leitungstyp>20001</leitungstyp>
            <spannung>30001</spannung>
        </BST_Stromleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Stromleitung gml:id="_bst_stromleitung.10">
            <title>Anschluss Conneford</title>
            <gehoertZuPlan xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>437569.060 5909319.386 437376.716 5909318.053 437168.187
                                5909567.853</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>437113.159 5909612.021 437168.187 5909567.853</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <leitungstyp>20001</leitungstyp>
            <spannung>30001</spannung>
        </BST_Stromleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Stromleitung gml:id="_bst_stromleitung.11">
            <title>Verbindung Konverter</title>
            <gehoertZuPlan xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>382793.484 5913277.665 382222.035 5912925.368 381902.799
                                5912724.669 381851.111 5912697.783</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <leitungstyp>20001</leitungstyp>
            <spannung>30002</spannung>
        </BST_Stromleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Stromleitung gml:id="_bst_stromleitung.12">
            <title>Maade-Conneforde</title>
            <gehoertZuPlan xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>433895.812 5914446.046 434036.387 5914062.834 434265.544
                                5913974.253 434375.308 5913345.035 434806.662 5913169.797 435016.562
                                5912387.487 435379.554 5912141.962 435512.426 5911824.223 436352.989
                                5910942.740 436391.503 5910546.048 436827.029 5909887.464</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>436837.292 5909876.665 436827.029 5909887.464</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <leitungstyp>20001</leitungstyp>
            <spannung>30002</spannung>
        </BST_Stromleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Stromleitung gml:id="_bst_stromleitung.13">
            <title>Bestandsleitung</title>
            <gehoertZuPlan xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>436812.493 5909755.928 436721.442 5909854.943 436150.702
                                5910717.479 435286.899 5911252.559 430203.051 5910763.454 426745.557
                                5911429.993 425669.623 5911775.102 423451.180 5911012.705 421021.577
                                5912577.534 418717.115 5912225.791 417233.730 5912004.336 415319.332
                                5911696.860 412241.835 5912740.995 411215.087 5912808.255 408504.090
                                5913012.323 406549.083 5913212.388 404305.246 5913185.850 401209.447
                                5913148.788 399682.138 5913146.958 394538.334 5913093.882 393755.005
                                5913068.259 392686.163 5913029.825 391608.169 5913017.013 390407.552
                                5913013.353 390153.153 5913077.410 387196.564 5912972.859 385438.648
                                5912915.208 384652.680 5912949.547 383485.285 5913081.349 383139.518
                                5913216.575 383047.749 5913395.898</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <leitungstyp>20001</leitungstyp>
            <statusAenderung>3000</statusAenderung>
            <spannung>30002</spannung>
        </BST_Stromleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Umspannwerk gml:id="_bst_umspannwerk.3">
            <title>Umspannwerk Emden-Borsum</title>
            <gehoertZuPlan xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiSurface srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:surfaceMember>
                        <gml:Polygon>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:posList>381665.390 5912675.586 381754.992 5912522.040
                                        381939.899 5912634.450 381831.154 5912823.430 381759.880
                                        5912780.665 381764.767 5912770.890 381746.847 5912763.152
                                        381753.770 5912747.675 381696.343 5912714.278 381683.310
                                        5912700.837 381665.390 5912675.586</gml:posList>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </gml:surfaceMember>
                </gml:MultiSurface>
            </position>
            <netzSparte>3000</netzSparte>
            <statusAenderung>5000</statusAenderung>
            <begrenzung>2000</begrenzung>
        </BST_Umspannwerk>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Umspannwerk gml:id="_bst_umspannwerk.4">
            <title>Umspannwerk Conneforde</title>
            <gehoertZuPlan xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiSurface srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:surfaceMember>
                        <gml:Polygon>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:posList>436672.297 5909683.069 436679.628 5909673.295
                                        436668.224 5909659.447 436682.072 5909643.970 436630.754
                                        5909589.394 436612.833 5909613.831 436588.396 5909597.540
                                        436614.463 5909564.957 436638.900 5909584.507 436711.396
                                        5909514.454 436780.634 5909582.063 436852.316 5909512.825
                                        436822.992 5909483.501 436903.634 5909402.858 436970.428
                                        5909466.395 437005.455 5909437.070 437090.170 5909525.043
                                        437109.719 5909526.673 437183.030 5909604.056 437174.885
                                        5909612.202 437200.951 5909634.195 437142.302 5909690.401
                                        437165.110 5909712.394 437149.633 5909731.944 437129.269
                                        5909714.023 437044.554 5909792.221 437103.203 5909845.983
                                        437064.918 5909888.340 437109.719 5909930.698 437044.554
                                        5909999.122 437057.587 5910012.155 436969.614 5910096.870
                                        436862.091 5909991.790 436879.197 5909973.870 436855.574
                                        5909949.433 436836.025 5909964.910 436783.893 5909901.373
                                        436828.694 5909858.201 436804.257 5909832.950 436831.137
                                        5909806.069 436813.217 5909787.334 436783.893 5909788.963
                                        436672.297 5909683.069</gml:posList>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </gml:surfaceMember>
                </gml:MultiSurface>
            </position>
            <netzSparte>3000</netzSparte>
            <statusAenderung>5000</statusAenderung>
            <begrenzung>2000</begrenzung>
        </BST_Umspannwerk>
    </sf:featureMember>
    <sf:featureMember>
        <BST_StationFlaeche gml:id="_bst_stationflaeche.1">
            <title>Konverterstation Emden-Ost</title>
            <gehoertZuPlan xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:MultiSurface srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:surfaceMember>
                        <gml:Polygon>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:posList>382669.118 5913305.993 382802.345 5913166.973
                                        382862.442 5913196.660 382945.708 5913077.914 383078.935
                                        5913166.973 383035.492 5913236.483 383077.487 5913261.825
                                        383049.973 5913308.165 383086.900 5913332.783 383160.754
                                        5913229.242 383416.347 5913405.913 383375.800 5913468.182
                                        383388.109 5913476.871 383361.319 5913515.970 383398.970
                                        5913542.036 383300.498 5913688.296 383225.196 5913686.848
                                        382669.118 5913305.993</gml:posList>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </gml:surfaceMember>
                </gml:MultiSurface>
            </position>
            <netzSparte>3000</netzSparte>
            <begrenzung>2000</begrenzung>
            <art>20002</art>
        </BST_StationFlaeche>
    </sf:featureMember>
    <sf:featureMember>
        <XP_Planreferenz gml:id="_xp_planreferenz.1">
            <title>Karte3_Raumordnung_Naturschutzfachliches_Blatt1</title>
            <gehoertZuPlan xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Polygon srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>380622.051 5910122.544 388036.426 5910122.544 388036.426
                                5916195.503 380622.051 5916195.503 380622.051 5910122.544</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </position>
            <referenz>
                <XP_NetzExterneReferenz>
                    <referenzName>Link zum Amt für regionale Landesentwicklung Weser-Ems</referenzName>
                    <referenzURL>
                        https://www.arl-we.niedersachsen.de/download/88160/Karte_3_Raumordnung_Naturschutzfachliches_Blatt_1.pdf</referenzURL>
                </XP_NetzExterneReferenz>
            </referenz>
        </XP_Planreferenz>
    </sf:featureMember>
    <sf:featureMember>
        <XP_Planreferenz gml:id="_xp_planreferenz.2">
            <title>Karte3_Raumordnung_Naturschutzfachliches_Blatt2</title>
            <gehoertZuPlan xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Polygon srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>386643.182 5910131.690 400971.950 5910131.690 400971.950
                                5916204.649 386643.182 5916204.649 386643.182 5910131.690</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </position>
            <referenz>
                <XP_NetzExterneReferenz>
                    <referenzName>Link zum Amt für regionale Landesentwicklung Weser-Ems</referenzName>
                    <referenzURL>
                        https://www.arl-we.niedersachsen.de/download/88159/Karte_3_Raumordnung_Naturschutzfachliches_Blatt_2.pdf</referenzURL>
                </XP_NetzExterneReferenz>
            </referenz>
        </XP_Planreferenz>
    </sf:featureMember>
    <sf:featureMember>
        <XP_Planreferenz gml:id="_xp_planreferenz.3">
            <title>Karte3_Raumordnung_Naturschutzfachliches_Blatt3</title>
            <gehoertZuPlan xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Polygon srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>398728.730 5910112.060 411795.052 5910109.314 411809.694
                                5916214.893 398728.730 5916217.638 398728.730 5910112.060</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </position>
            <referenz>
                <XP_NetzExterneReferenz>
                    <referenzName>Link zum Amt für regionale Landesentwicklung Weser-Ems</referenzName>
                    <referenzURL>
                        https://www.arl-we.niedersachsen.de/download/88158/Karte_3_Raumordnung_Naturschutzfachliches_Blatt_3.pdf</referenzURL>
                </XP_NetzExterneReferenz>
            </referenz>
        </XP_Planreferenz>
    </sf:featureMember>
    <sf:featureMember>
        <XP_Planreferenz gml:id="_xp_planreferenz.4">
            <title>Karte3_Raumordnung_Naturschutzfachliches_Blatt4</title>
            <gehoertZuPlan xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Polygon srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>411583.205 5909403.769 425858.838 5909403.769 425858.838
                                5915699.689 411583.205 5915699.689 411583.205 5909403.769</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </position>
            <referenz>
                <XP_NetzExterneReferenz>
                    <referenzName>Link zum Amt für regionale Landesentwicklung Weser-Ems</referenzName>
                    <referenzURL>
                        https://www.arl-we.niedersachsen.de/download/88157/Karte_3_Raumordnung_Naturschutzfachliches_Blatt_4.pdf</referenzURL>
                </XP_NetzExterneReferenz>
            </referenz>
        </XP_Planreferenz>
    </sf:featureMember>
    <sf:featureMember>
        <XP_Planreferenz gml:id="_xp_planreferenz.5">
            <title>Karte3_Raumordnung_Naturschutzfachliches_Blatt5</title>
            <gehoertZuPlan xlink:title="380-kV-Leitung UW Emden/Ost UW Conneforde"
                xlink:href="#_rvp_plan.1" />
            <position>
                <gml:Polygon srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>424442.027 5907958.818 438703.018 5907958.818 438703.018
                                5914152.247 424442.027 5914152.247 424442.027 5907958.818</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </position>
            <referenz>
                <XP_NetzExterneReferenz>
                    <referenzName>Link zum Amt für regionale Landesentwicklung Weser-Ems</referenzName>
                    <referenzURL>
                        https://www.arl-we.niedersachsen.de/download/88156/Karte_3_Raumordnung_Naturschutzfachliches_Blatt_5.pdf</referenzURL>
                </XP_NetzExterneReferenz>
            </referenz>
        </XP_Planreferenz>
    </sf:featureMember>
</sf:FeatureCollection>