<?xml version="1.0" encoding="utf-8"?>
<sf:FeatureCollection xmlns="http://www.xtrasse.de/2.0" xmlns:xtrasse="http://www.xtrasse.de/2.0"
    xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xml="http://www.w3.org/XML/1998/namespace"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:sf="http://www.opengis.net/ogcapi-features-1/1.0/sf"
    xsi:schemaLocation="http://www.xtrasse.de/2.0 https://repository.gdi-de.org/schemas/de.xleitstelle.xtrasse/2.0/XML/XTrasse.xsd
                        http://www.opengis.net/ogcapi-features-1/1.0/sf http://schemas.opengis.net/ogcapi/features/part1/1.0/xml/core-sf.xsd">
    <sf:featureMember>
        <PFS_Plan gml:id="_pfs_plan.1">
            <name>Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida</name>
            <nummer>BBPIG Nr.14</nummer>
            <beschreibung>Das länderübergreifende Vorhaben (§ 2 Abs. 1 NABEG) ist in zwei Abschnitte
                geteilt. Dieser Plan betrifft den östlichen Abschnitt zwischen den Umspannwerken
                (UW) Röhrsdorf (Sachsen) und Weida (Thüringen), der westlichen Abschnitt zwischen
                den UW Weida und Remptendorf (beide Thüringen) wird in einem eigenen Verfahren
                durchgeführt. Der östliche Abschnitt umfasst eine eine Länge von ca. 66 km. Das
                Vorhaben umfasst den Ersatzneubau einer zweisystemigen 380-kV-Freileitung sowie die
                Anpassung der betroffenen Schaltfelder und Schaltanlagen in den UW. Nach
                Inbetriebnahme der neuen 380-kV-Freileitung wird die Bestandsleitung komplett
                zurückgebaut.</beschreibung>
            <gesetzlicheGrundlage>
                <XP_GesetzlicheGrundlage>
                    <name>Netzausbaubeschleunigungsgesetz Übertragungsnetz (NABEG)</name>
                    <detail>§ 19</detail>
                    <ausfertigungDatum>2011-07-28</ausfertigungDatum>
                </XP_GesetzlicheGrundlage>
            </gesetzlicheGrundlage>
            <technHerstellDatum>2023-12-06</technHerstellDatum>
            <externeReferenz>
                <XP_NetzExterneReferenz>
                    <referenzName>Link zu Netzausbau.de</referenzName>
                    <referenzURL>
                        https://www.netzausbau.de/Vorhaben/ansicht/de.html?cms_gruppe=bbplg&amp;cms_nummer=14</referenzURL>
                    <datum>2023-03-29</datum>
                </XP_NetzExterneReferenz>
            </externeReferenz>
            <position>
                <gml:Polygon srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>286000.355 5621250.344 349747.830 5621250.344 349747.830
                                5652038.425 286000.355 5652038.425 286000.355 5621250.344</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </position>
            <hatBSTObjekt xlink:title="1" xlink:href="#_bst_stromleitung.1" />
            <hatBSTObjekt xlink:title="2" xlink:href="#_bst_stromleitung.2" />
            <hatBSTObjekt xlink:title="5" xlink:href="#_bst_stromleitung.3" />
            <hatBSTObjekt xlink:title="6" xlink:href="#_bst_stromleitung.4" />
            <hatBSTObjekt xlink:title="7" xlink:href="#_bst_stromleitung.5" />
            <hatBSTObjekt xlink:title="8" xlink:href="#_bst_stromleitung.6" />
            <hatBSTObjekt xlink:title="9" xlink:href="#_bst_stromleitung.7" />
            <hatBSTObjekt xlink:title="10" xlink:href="#_bst_stromleitung.8" />
            <hatBSTObjekt xlink:title="11" xlink:href="#_bst_stromleitung.9" />
            <hatBSTObjekt xlink:title="12" xlink:href="#_bst_stromleitung.10" />
            <hatBSTObjekt xlink:title="13" xlink:href="#_bst_stromleitung.11" />
            <hatBSTObjekt xlink:title="14" xlink:href="#_bst_stromleitung.12" />
            <hatBSTObjekt xlink:title="15" xlink:href="#_bst_stromleitung.13" />
            <hatBSTObjekt xlink:title="16" xlink:href="#_bst_stromleitung.14" />
            <hatBSTObjekt xlink:title="17" xlink:href="#_bst_stromleitung.15" />
            <hatBSTObjekt xlink:title="3" xlink:href="#_bst_stromleitung.16" />
            <hatBSTObjekt xlink:title="4" xlink:href="#_bst_stromleitung.17" />
            <hatBSTObjekt xlink:title="Umspannwerk Röhrsdorf" xlink:href="#_bst_umspannwerk.1" />
            <hatBSTObjekt xlink:title="Umspannwerk Weide" xlink:href="#_bst_umspannwerk.2" />
            <hatRVPObjekt xlink:title="km 34" xlink:href="#_rvp_stationierungslinie.1" />
            <hatRVPObjekt xlink:title="km 35" xlink:href="#_rvp_stationierungslinie.2" />
            <hatRVPObjekt xlink:title="km 36" xlink:href="#_rvp_stationierungslinie.3" />
            <hatRVPObjekt xlink:title="km 37" xlink:href="#_rvp_stationierungslinie.4" />
            <hatRVPObjekt xlink:title="km 38" xlink:href="#_rvp_stationierungslinie.5" />
            <hatRVPObjekt xlink:title="km 39" xlink:href="#_rvp_stationierungslinie.6" />
            <hatRVPObjekt xlink:title="km 40" xlink:href="#_rvp_stationierungslinie.7" />
            <hatRVPObjekt xlink:title="km 41" xlink:href="#_rvp_stationierungslinie.8" />
            <hatRVPObjekt xlink:title="km 42" xlink:href="#_rvp_stationierungslinie.9" />
            <hatRVPObjekt xlink:title="km 43" xlink:href="#_rvp_stationierungslinie.10" />
            <hatRVPObjekt xlink:title="km 44" xlink:href="#_rvp_stationierungslinie.11" />
            <hatRVPObjekt xlink:title="km 45" xlink:href="#_rvp_stationierungslinie.12" />
            <hatRVPObjekt xlink:title="km 46" xlink:href="#_rvp_stationierungslinie.13" />
            <hatRVPObjekt xlink:title="km 47" xlink:href="#_rvp_stationierungslinie.14" />
            <hatRVPObjekt xlink:title="km 48" xlink:href="#_rvp_stationierungslinie.15" />
            <hatRVPObjekt xlink:title="km 49" xlink:href="#_rvp_stationierungslinie.16" />
            <hatRVPObjekt xlink:title="km 50" xlink:href="#_rvp_stationierungslinie.17" />
            <hatRVPObjekt xlink:title="km 51" xlink:href="#_rvp_stationierungslinie.18" />
            <hatRVPObjekt xlink:title="km 52" xlink:href="#_rvp_stationierungslinie.19" />
            <hatRVPObjekt xlink:title="km 53" xlink:href="#_rvp_stationierungslinie.20" />
            <hatRVPObjekt xlink:title="km 54" xlink:href="#_rvp_stationierungslinie.21" />
            <hatRVPObjekt xlink:title="km 55" xlink:href="#_rvp_stationierungslinie.22" />
            <hatRVPObjekt xlink:title="km 56" xlink:href="#_rvp_stationierungslinie.23" />
            <hatRVPObjekt xlink:title="km 57" xlink:href="#_rvp_stationierungslinie.24" />
            <hatRVPObjekt xlink:title="km 58" xlink:href="#_rvp_stationierungslinie.25" />
            <hatRVPObjekt xlink:title="km 59" xlink:href="#_rvp_stationierungslinie.26" />
            <hatRVPObjekt xlink:title="km 60" xlink:href="#_rvp_stationierungslinie.27" />
            <hatRVPObjekt xlink:title="km 61" xlink:href="#_rvp_stationierungslinie.28" />
            <hatRVPObjekt xlink:title="km 62" xlink:href="#_rvp_stationierungslinie.29" />
            <hatRVPObjekt xlink:title="km 63" xlink:href="#_rvp_stationierungslinie.30" />
            <hatRVPObjekt xlink:title="km 64" xlink:href="#_rvp_stationierungslinie.31" />
            <hatRVPObjekt xlink:title="km 0" xlink:href="#_rvp_stationierungslinie.32" />
            <hatRVPObjekt xlink:title="km 1" xlink:href="#_rvp_stationierungslinie.33" />
            <hatRVPObjekt xlink:title="km 2" xlink:href="#_rvp_stationierungslinie.34" />
            <hatRVPObjekt xlink:title="km 3" xlink:href="#_rvp_stationierungslinie.35" />
            <hatRVPObjekt xlink:title="km 4" xlink:href="#_rvp_stationierungslinie.36" />
            <hatRVPObjekt xlink:title="km 5" xlink:href="#_rvp_stationierungslinie.37" />
            <hatRVPObjekt xlink:title="km 6" xlink:href="#_rvp_stationierungslinie.38" />
            <hatRVPObjekt xlink:title="km 8" xlink:href="#_rvp_stationierungslinie.39" />
            <hatRVPObjekt xlink:title="km 9" xlink:href="#_rvp_stationierungslinie.40" />
            <hatRVPObjekt xlink:title="km 10" xlink:href="#_rvp_stationierungslinie.41" />
            <hatRVPObjekt xlink:title="km 11" xlink:href="#_rvp_stationierungslinie.42" />
            <hatRVPObjekt xlink:title="km 12" xlink:href="#_rvp_stationierungslinie.43" />
            <hatRVPObjekt xlink:title="km 13" xlink:href="#_rvp_stationierungslinie.44" />
            <hatRVPObjekt xlink:title="km 14" xlink:href="#_rvp_stationierungslinie.45" />
            <hatRVPObjekt xlink:title="km 15" xlink:href="#_rvp_stationierungslinie.46" />
            <hatRVPObjekt xlink:title="km 16" xlink:href="#_rvp_stationierungslinie.47" />
            <hatRVPObjekt xlink:title="km 17" xlink:href="#_rvp_stationierungslinie.48" />
            <hatRVPObjekt xlink:title="km 18" xlink:href="#_rvp_stationierungslinie.49" />
            <hatRVPObjekt xlink:title="km 19" xlink:href="#_rvp_stationierungslinie.50" />
            <hatRVPObjekt xlink:title="km 20" xlink:href="#_rvp_stationierungslinie.51" />
            <hatRVPObjekt xlink:title="km 21" xlink:href="#_rvp_stationierungslinie.52" />
            <hatRVPObjekt xlink:title="km 22" xlink:href="#_rvp_stationierungslinie.53" />
            <hatRVPObjekt xlink:title="km 23" xlink:href="#_rvp_stationierungslinie.54" />
            <hatRVPObjekt xlink:title="km 24" xlink:href="#_rvp_stationierungslinie.55" />
            <hatRVPObjekt xlink:title="km 25" xlink:href="#_rvp_stationierungslinie.56" />
            <hatRVPObjekt xlink:title="km 26" xlink:href="#_rvp_stationierungslinie.57" />
            <hatRVPObjekt xlink:title="km 27" xlink:href="#_rvp_stationierungslinie.58" />
            <hatRVPObjekt xlink:title="km 28" xlink:href="#_rvp_stationierungslinie.59" />
            <hatRVPObjekt xlink:title="km 29" xlink:href="#_rvp_stationierungslinie.60" />
            <hatRVPObjekt xlink:title="km 30" xlink:href="#_rvp_stationierungslinie.61" />
            <hatRVPObjekt xlink:title="km 31" xlink:href="#_rvp_stationierungslinie.62" />
            <hatRVPObjekt xlink:title="km 32" xlink:href="#_rvp_stationierungslinie.63" />
            <hatRVPObjekt xlink:title="km 33" xlink:href="#_rvp_stationierungslinie.64" />
            <hatRVPObjekt xlink:title="km 7" xlink:href="#_rvp_stationierungslinie.65" />
            <hatRVPObjekt xlink:title="Trassenkorridor" xlink:href="#_rvp_trassenkorridor.1" />
            <hatRVPObjekt xlink:title="Rückbau" xlink:href="#_rvp_trassenkorridorsegment.1" />
            <hatPFSObjekt xlink:title="Hainichen 2" xlink:href="#_pfs_alternativetrasseabschnitt.1" />
            <hatPFSObjekt xlink:title="Langenchursdorf 2"
                xlink:href="#_pfs_alternativetrasseabschnitt.2" />
            <hatPFSObjekt xlink:title="Dürrenuhlsdorf 2"
                xlink:href="#_pfs_alternativetrasseabschnitt.3" />
            <hatPFSObjekt xlink:title="Liebschwitz/ Elstertal 2"
                xlink:href="#_pfs_alternativetrasseabschnitt.4" />
            <hatPFSObjekt xlink:title="Hainichen 2" xlink:href="#_pfs_alternativetrasseabschnitt.5" />
            <hatPFSObjekt xlink:title="Langenchursdorf 2"
                xlink:href="#_pfs_alternativetrasseabschnitt.6" />
            <hatPFSObjekt xlink:title="Dürrenuhlsdorf 2"
                xlink:href="#_pfs_alternativetrasseabschnitt.7" />
            <hatPFSObjekt xlink:title="Liebschwitz/ Elstertal 2"
                xlink:href="#_pfs_alternativetrasseabschnitt.8" />
            <hatPFSObjekt xlink:title="Langenchursdorf 1"
                xlink:href="#_pfs_hochspannungsleitungabschnitt.1" />
            <hatPFSObjekt xlink:title="Dürrenuhlsdorf 1"
                xlink:href="#_pfs_hochspannungsleitungabschnitt.2" />
            <hatPFSObjekt xlink:title="Oberwiera - Tettau"
                xlink:href="#_pfs_hochspannungsleitungabschnitt.3" />
            <hatPFSObjekt xlink:title="Hainichen 1"
                xlink:href="#_pfs_hochspannungsleitungabschnitt.4" />
            <hatPFSObjekt xlink:title="Gößnitz – Thonhausen - Niebra"
                xlink:href="#_pfs_hochspannungsleitungabschnitt.5" />
            <hatPFSObjekt xlink:title="Liebschwitz/ Elstertal 1"
                xlink:href="#_pfs_hochspannungsleitungabschnitt.6" />
            <hatPFSObjekt xlink:title="Wolfsgefährt - Weida"
                xlink:href="#_pfs_hochspannungsleitungabschnitt.7" />
            <hatPFSObjekt xlink:title="Niederwinckel"
                xlink:href="#_pfs_hochspannungsleitungabschnitt.8" />
            <hatPFSObjekt xlink:title="Südumgehung Limbach - Oberfrohna"
                xlink:href="#_pfs_hochspannungsleitungabschnitt.9" />
            <status>1000</status>
            <beteiligte>
                <XP_Akteur>
                    <nameOrganisation>50Hertz Transmission GmbH</nameOrganisation>
                    <namePerson>Elke Korn</namePerson>
                    <strasseHausnr>Heidestraße 2</strasseHausnr>
                    <postleitzahl>10557</postleitzahl>
                    <ort>Berlin</ort>
                    <telefon>+49 (0)30 5150-2350</telefon>
                    <mail>elke.korn@50hertz.com</mail>
                    <rolle>6000</rolle>
                </XP_Akteur>
            </beteiligte>
            <beteiligte>
                <XP_Akteur>
                    <nameOrganisation>Bundesnetzagentur für Elektrizität, Gas, Telekommunikation,
                        Post und Eisenbahnen
                        Abteilung 8, Netzausbau,
                        803, Bundesfachplanung und Planfeststellung</nameOrganisation>
                    <strasseHausnr>Tulpenfeld 4</strasseHausnr>
                    <postleitzahl>53113</postleitzahl>
                    <ort>Bonn</ort>
                    <rolle>7100</rolle>
                </XP_Akteur>
            </beteiligte>
            <beteiligte>
                <XP_Akteur>
                    <nameOrganisation>BHF Bendfeldt Herrmann Franke Landschaftsarchitekten GmbH</nameOrganisation>
                    <strasseHausnr>Ostorfer Ufer 4</strasseHausnr>
                    <postleitzahl>19053</postleitzahl>
                    <ort>Schwerin</ort>
                    <rolle>4000</rolle>
                </XP_Akteur>
            </beteiligte>
            <beteiligte>
                <XP_Akteur>
                    <nameOrganisation>Fugmann Janotta und Partner mbB
                        Landschaftsarchitekten und Landschaftsplaner bdla</nameOrganisation>
                    <strasseHausnr>Belziger Straße 25</strasseHausnr>
                    <postleitzahl>10823</postleitzahl>
                    <ort>Berlin</ort>
                    <rolle>4000</rolle>
                </XP_Akteur>
            </beteiligte>
            <beteiligte>
                <XP_Akteur>
                    <nameOrganisation>Ökologische Dienste Ortlieb GmbH</nameOrganisation>
                    <strasseHausnr>Tannenweg 22m</strasseHausnr>
                    <postleitzahl>18059</postleitzahl>
                    <ort>Rostock</ort>
                    <rolle>4000</rolle>
                </XP_Akteur>
            </beteiligte>
            <beteiligte>
                <XP_Akteur>
                    <nameOrganisation>EQOS Energie Deutschland GmbH</nameOrganisation>
                    <strasseHausnr>Handelsplatz 2</strasseHausnr>
                    <postleitzahl>04319</postleitzahl>
                    <ort>Leipzig</ort>
                    <rolle>4000</rolle>
                </XP_Akteur>
            </beteiligte>
            <antragDatum>2020-03-12</antragDatum>
            <auslegungInternetStartDatum>2022-01-17</auslegungInternetStartDatum>
            <auslegungInternetEndDatum>2022-03-16</auslegungInternetEndDatum>
            <planfeststellungsbeschlussDatum>2022-09-28</planfeststellungsbeschlussDatum>
        </PFS_Plan>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Trassenkorridor gml:id="_rvp_trassenkorridor.1">
            <title>Trassenkorridor</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiSurface srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:surfaceMember>
                        <gml:Polygon>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:posList>346631.790 5636443.651 346623.091 5636502.806
                                        346619.652 5636529.829 346590.034 5636595.017 346538.709
                                        5636682.008 346446.498 5636782.919 346329.059 5636854.252
                                        346213.360 5636892.528 346088.962 5636904.924 345935.857
                                        5636877.087 345815.808 5636816.193 345743.605 5636760.518
                                        345671.402 5636663.088 345599.199 5636485.190 345576.581
                                        5636316.426 345587.020 5636178.979 343460.944 5634179.911
                                        341404.462 5633206.910 339647.231 5633436.568 339476.727
                                        5633979.396 339323.622 5634403.915 337514.196 5637031.062
                                        336379.825 5639106.683 336308.492 5639234.126 336186.703
                                        5639350.694 336068.395 5639415.068 335957.045 5639444.645
                                        335676.932 5639462.044 334873.130 5639482.922 332931.476
                                        5641253.202 332790.550 5641338.453 332710.517 5641364.551
                                        332607.867 5641385.429 332477.380 5641371.510 332357.331
                                        5641343.673 330852.376 5641006.145 330737.547 5640969.609
                                        330666.214 5640924.373 330612.279 5640870.438 330290.410
                                        5640419.822 328019.928 5639870.470 322694.300 5640037.493
                                        320585.622 5639599.056 317321.696 5639177.146 313567.137
                                        5637664.361 311131.371 5636877.087 310665.095 5636741.380
                                        310571.145 5636713.543 310522.429 5636692.665 310449.356
                                        5636633.511 308034.468 5633604.462 305278.573 5633618.380
                                        304666.152 5633534.868 302808.010 5633517.470 302602.710
                                        5633583.584 302439.166 5633625.340 298583.696 5633590.543
                                        296509.816 5634279.517 294888.291 5634513.524 293917.465
                                        5634638.792 293802.636 5634635.313 293712.164 5634600.516
                                        293653.010 5634576.158 293559.059 5634499.606 292198.510
                                        5632764.122 291620.885 5631172.175 291565.211 5631046.907
                                        291558.251 5630928.599 291579.129 5630824.209 291610.446
                                        5630646.746 291243.375 5629881.505 291219.235 5629828.879
                                        291200.683 5629776.015 291190.199 5629734.854 291183.399
                                        5629693.824 291179.088 5629637.965 291179.119 5629609.944
                                        291181.430 5629572.656 291187.948 5629526.420 291198.450
                                        5629480.923 291206.633 5629454.122 291216.200 5629427.785
                                        291227.126 5629401.981 291239.381 5629376.781 291252.931
                                        5629352.253 291262.666 5629336.307 291278.292 5629313.047
                                        291295.109 5629290.632 291313.069 5629269.123 291338.712
                                        5629241.955 291352.226 5629229.054 291380.081 5629206.636
                                        291412.997 5629185.612 291439.409 5629171.627 291466.606
                                        5629158.609 291519.380 5629136.948 291551.298 5629130.207
                                        291587.857 5629122.516 291624.861 5629117.380 291662.133
                                        5629114.824 291708.824 5629115.277 291773.752 5629122.704
                                        291846.211 5629140.753 291898.531 5629160.787 291948.483
                                        5629186.154 291987.910 5629211.170 292025.020 5629239.510
                                        292072.550 5629284.362 292108.706 5629327.157 292130.251
                                        5629357.678 292149.626 5629389.620 292286.371 5629643.515
                                        292631.728 5630384.248 292650.866 5630469.500 292614.330
                                        5630950.129 293111.922 5632247.174 294141.903 5633563.358
                                        296180.987 5633284.985 298380.136 5632561.214 302301.719
                                        5632582.092 302649.686 5632477.702 304306.006 5632484.662
                                        305360.345 5632571.653 308164.956 5632543.816 308297.183
                                        5632555.995 308394.614 5632568.174 308493.784 5632602.970
                                        308579.036 5632655.165 308669.507 5632719.539 311061.777
                                        5635781.645 312345.774 5636169.628 313160.016 5636458.440
                                        314022.973 5636744.207 314640.614 5636974.735 315416.579
                                        5637303.564 317319.956 5638057.346 317441.744 5638119.980
                                        317507.858 5638140.858 317789.711 5638172.175 319647.852
                                        5638429.670 320350.745 5638488.825 321920.074 5638798.515
                                        322803.909 5638977.718 325361.463 5638911.604 326415.802
                                        5638876.808 328054.725 5638835.052 330511.369 5639395.278
                                        330692.311 5639437.034 330817.579 5639464.871 330915.010
                                        5639534.464 330995.042 5639611.017 331304.732 5640044.235
                                        332435.624 5640301.731 334384.237 5638542.760 334492.106
                                        5638480.126 334599.976 5638452.288 335553.404 5638424.451
                                        336579.906 5636541.952 338368.454 5633953.081 338420.649
                                        5633837.382 338775.575 5632736.937 338841.688 5632625.588
                                        338925.200 5632559.474 338994.794 5632507.279 339092.224
                                        5632465.523 339203.574 5632441.166 341465.356 5632161.923
                                        341601.063 5632168.882 341722.851 5632189.760 341848.119
                                        5632266.313 343946.358 5633240.619 344022.910 5633278.895
                                        346416.920 5635509.361 346538.709 5635634.629 346629.180
                                        5635736.409 346665.717 5635797.303 346674.416 5635872.116
                                        346679.635 5635920.832 346676.156 5635983.465 346667.456
                                        5636087.855 346631.790 5636443.651</gml:posList>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </gml:surfaceMember>
                </gml:MultiSurface>
            </position>
            <art>10001</art>
            <status>2000</status>
        </RVP_Trassenkorridor>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_TrassenkorridorSegment gml:id="_rvp_trassenkorridorsegment.1">
            <title>Rückbau</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiSurface srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:surfaceMember>
                        <gml:Polygon>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:posList>335727.275 5639414.819 335718.800 5639411.453
                                        335710.386 5639407.940 335702.033 5639404.282 335693.746
                                        5639400.478 335685.526 5639396.530 335677.376 5639392.440
                                        335669.299 5639388.208 335661.296 5639383.837 335653.371
                                        5639379.326 335645.526 5639374.678 335637.763 5639369.895
                                        335630.085 5639364.976 335622.493 5639359.925 335614.990
                                        5639354.742 335607.579 5639349.430 335600.262 5639343.989
                                        335593.040 5639338.421 335585.916 5639332.729 335578.893
                                        5639326.913 335571.972 5639320.976 335565.156 5639314.919
                                        335558.446 5639308.744 335551.845 5639302.453 335545.354
                                        5639296.048 335538.976 5639289.532 335532.712 5639282.905
                                        335526.565 5639276.170 335520.536 5639269.328 335514.627
                                        5639262.383 335508.840 5639255.336 335503.177 5639248.189
                                        335497.639 5639240.945 335492.228 5639233.605 335486.946
                                        5639226.173 335481.794 5639218.649 335476.774 5639211.036
                                        335471.887 5639203.338 335467.135 5639195.555 335462.519
                                        5639187.691 335458.041 5639179.748 335453.703 5639171.727
                                        335449.504 5639163.633 335445.447 5639155.466 335441.533
                                        5639147.230 335437.763 5639138.927 335434.139 5639130.560
                                        335430.660 5639122.131 335427.329 5639113.643 335424.147
                                        5639105.097 335421.113 5639096.498 335418.230 5639087.847
                                        335415.499 5639079.147 335412.919 5639070.401 335410.492
                                        5639061.612 335408.218 5639052.781 335406.098 5639043.912
                                        335404.134 5639035.008 335402.324 5639026.070 335400.671
                                        5639017.103 335399.174 5639008.108 335397.834 5638999.088
                                        335396.652 5638990.046 335395.627 5638980.986 335394.760
                                        5638971.908 335394.052 5638962.817 335393.501 5638953.715
                                        335393.110 5638944.605 335392.877 5638935.489 335392.803
                                        5638926.371 335392.888 5638917.253 335393.132 5638908.137
                                        335393.535 5638899.027 335394.096 5638889.926 335394.816
                                        5638880.836 335395.694 5638871.760 335396.729 5638862.700
                                        335397.923 5638853.660 335399.274 5638844.642 335400.782
                                        5638835.649 335402.446 5638826.683 335404.266 5638817.748
                                        335406.241 5638808.846 335408.372 5638799.980 335410.656
                                        5638791.152 335413.094 5638782.365 335415.684 5638773.622
                                        335418.427 5638764.926 335421.320 5638756.278 335424.364
                                        5638747.683 335427.557 5638739.141 335430.899 5638730.657
                                        335434.387 5638722.232 335438.022 5638713.869 335441.802
                                        5638705.571 335445.726 5638697.340 335449.793 5638689.178
                                        335454.001 5638681.089 335458.350 5638673.074 335462.837
                                        5638665.136 335467.462 5638657.277 335472.224 5638649.500
                                        335477.120 5638641.808 335482.149 5638634.201 335487.311
                                        5638626.684 335492.602 5638619.257 335498.022 5638611.924
                                        335503.569 5638604.687 335509.241 5638597.547 335515.036
                                        5638590.507 335520.953 5638583.569 335526.991 5638576.735
                                        335533.146 5638570.008 335539.418 5638563.388 335545.804
                                        5638556.879 335552.302 5638550.482 335558.911 5638544.200
                                        335565.629 5638538.033 335572.452 5638531.984 335579.381
                                        5638526.056 335586.411 5638520.248 335593.541 5638514.564
                                        335600.770 5638509.006 335608.094 5638503.574 335615.511
                                        5638498.270 335623.020 5638493.096 335630.618 5638488.054
                                        335638.303 5638483.145 335646.072 5638478.371 335653.922
                                        5638473.733 335661.853 5638469.232 335669.861 5638464.870
                                        340860.819 5638062.319 341374.940 5637929.221 341802.939
                                        5638036.221 342163.084 5638044.050 342337.937 5638004.904
                                        342601.522 5637895.295 343514.934 5637827.441 344126.594
                                        5637312.831 344444.984 5637051.856 344806.760 5636855.962
                                        345119.930 5636842.913 345487.905 5636534.963 345599.199
                                        5636485.190 345576.581 5636316.426 345587.020 5636178.979
                                        345647.099 5636121.970 345691.465 5636032.586 345734.526
                                        5635978.434 345752.794 5635956.251 345801.074 5635907.971
                                        345846.928 5635887.876 345904.000 5635869.139 345952.842
                                        5635858.145 346002.455 5635851.442 346052.463 5635849.081
                                        346142.263 5635855.807 346191.361 5635865.592 346248.878
                                        5635882.914 346330.985 5635919.896 346406.310 5635969.245
                                        346459.018 5636015.413 346505.718 5636067.650 346550.847
                                        5636133.781 346586.353 5636205.541 346611.545 5636281.537
                                        346623.369 5636340.430 346629.311 5636410.246 346617.723
                                        5636539.657 346538.709 5636682.008 346446.498 5636782.919
                                        346409.146 5636803.115 346403.927 5637040.602 346255.171
                                        5637250.034 345564.892 5637756.978 345316.966 5637854.844
                                        345079.479 5637852.234 344464.883 5638333.733 343953.372
                                        5638769.561 343766.775 5638806.097 342910.777 5638860.902
                                        342649.802 5639007.048 341822.512 5639048.804 341352.757
                                        5638952.243 341094.392 5639030.536 336634.330 5639377.632
                                        335727.275 5639414.819</gml:posList>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </gml:surfaceMember>
                </gml:MultiSurface>
            </position>
            <artSegment>3000</artSegment>
            <status>2000</status>
        </RVP_TrassenkorridorSegment>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.1">
            <title>km 34</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>317806.542 5638082.302 317640.737 5639312.345</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.2">
            <title>km 35</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>317008.364 5637862.513 316545.652 5639003.870</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.3">
            <title>km 36</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>316079.084 5637484.632 315627.940 5638598.997</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.4">
            <title>km 37</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>315153.660 5637087.471 314690.948 5638232.683</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.5">
            <title>km 38</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>314224.380 5636725.013 313769.380 5637850.946</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.6">
            <title>km 39</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>313237.261 5636385.691 312863.235 5637542.471</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.7">
            <title>km 40</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>312288.701 5636057.936 311922.387 5637218.572</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.8">
            <title>km 41</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>311355.565 5635753.318 310969.972 5636956.369</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.9">
            <title>km 42</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>310881.285 5635367.724 309932.725 5636150.479</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.10">
            <title>km 43</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>310256.624 5634569.546 309296.496 5635375.436</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.11">
            <title>km 44</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>309643.530 5633809.927 308687.259 5634569.546</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.12">
            <title>km 45</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>309018.869 5633019.460 308081.877 5633775.223</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.13">
            <title>km 46</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>307665.436 5632471.918 307665.436 5633690.393</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.14">
            <title>km 47</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>306662.894 5632491.197 306655.182 5633698.105</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.15">
            <title>km 48</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>305671.919 5632510.477 305664.207 5633698.105</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.16">
            <title>km 49</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>304669.376 5632421.791 304669.376 5633651.834</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.17">
            <title>km 50</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>303670.689 5632390.943 303662.977 5633613.274</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.18">
            <title>km 51</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>302675.858 5632406.367 302660.434 5633659.545</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.19">
            <title>km 52</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>301673.315 5632495.053 301657.891 5633701.961</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.20">
            <title>km 53</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>300670.772 5632491.197 300670.772 5633701.961</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.21">
            <title>km 54</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>299679.797 5632479.630 299672.085 5633701.961</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.22">
            <title>km 55</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>298673.399 5632468.062 298661.831 5633694.249</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.23">
            <title>km 56</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>297524.330 5632761.113 297894.500 5633914.037</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.24">
            <title>km 57</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>296575.770 5633088.867 296949.796 5634245.647</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.25">
            <title>km 58</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>295719.753 5633273.952 295862.423 5634480.859</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.26">
            <title>km 59</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>294740.346 5633389.630 294875.304 5634604.249</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.27">
            <title>km 60</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>294485.854 5633482.172 293340.642 5634388.317</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.28">
            <title>km 61</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>293691.532 5632826.664 292719.837 5633605.562</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.29">
            <title>km 62</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>293232.676 5632190.435 292068.184 5632622.299</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.30">
            <title>km 63</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>292827.803 5631272.722 291717.294 5631673.740</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.31">
            <title>km 64</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>292700.557 5630289.459 291505.217 5630709.756</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.32">
            <title>km 0</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>346504.330 5635465.568 345474.796 5636514.382</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.33">
            <title>km 1</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>345779.415 5634777.284 344917.613 5635660.293</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.34">
            <title>km 2</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>345050.643 5634101.532 344179.202 5634972.973</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.35">
            <title>km 3</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>344306.448 5633428.671 343452.359 5634290.473</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.36">
            <title>km 4</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>343290.409 5632814.132 342758.291 5633940.065</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.37">
            <title>km 5</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>342353.417 5632393.835 341840.578 5633519.768</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.38">
            <title>km 6</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>341169.646 5632124.884 341293.036 5633316.367</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.39">
            <title>km 8</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>338647.865 5632849.799 339993.586 5633447.469</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.40">
            <title>km 9</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>338335.534 5633805.107 339476.891 5634283.243</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.41">
            <title>km 10</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>337868.966 5634508.815 338906.213 5635202.883</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.42">
            <title>km 11</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>337321.424 5635320.489 338308.543 5636026.125</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.43">
            <title>km 12</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>336758.457 5636153.371 337741.721 5636828.159</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.44">
            <title>km 13</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>336129.940 5637188.689 337205.746 5637740.087</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.45">
            <title>km 14</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>335632.525 5638063.022 336754.601 5638645.268</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.46">
            <title>km 15</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>335555.406 5638253.891 335674.940 5639549.484</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.47">
            <title>km 16</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>334217.397 5638566.221 335050.279 5639576.476</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.48">
            <title>km 17</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>333492.481 5639214.018 334244.389 5640147.154</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.49">
            <title>km 18</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>332740.574 5639877.239 333554.176 5640841.222</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.50">
            <title>km 19</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>332455.235 5640139.442 332173.752 5641404.189</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.51">
            <title>km 20</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>331460.404 5639983.277 331236.760 5641163.193</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.52">
            <title>km 21</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>331040.107 5639528.277 330076.124 5640461.413</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.53">
            <title>km 22</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>329752.226 5639131.116 329459.175 5640322.599</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.54">
            <title>km 23</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>328772.818 5638884.336 328499.047 5640087.387</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.55">
            <title>km 24</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>327635.318 5638745.522 327662.309 5639979.421</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.56">
            <title>km 25</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>326625.063 5638784.082 326663.623 5640006.413</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.57">
            <title>km 26</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>325645.656 5638780.226 325676.503 5640037.260</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.58">
            <title>km 27</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>324635.401 5638845.777 324677.817 5640068.108</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.59">
            <title>km 28</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>323644.426 5638876.624 323675.274 5640083.531</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.60">
            <title>km 29</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>322772.985 5638892.048 322530.061 5640095.099</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.61">
            <title>km 30</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>321797.434 5638703.107 321562.222 5639902.302</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.62">
            <title>km 31</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>320814.171 5638510.310 320575.103 5639701.794</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.63">
            <title>km 32</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>319784.636 5638344.505 319641.967 5639574.548</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.64">
            <title>km 33</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>318793.661 5638217.259 318643.280 5639424.167</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <RVP_Stationierungslinie gml:id="_rvp_stationierungslinie.65">
            <title>km 7</title>
            <gehoertZuIP
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>340170.959 5632232.850 340309.772 5633451.325</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </RVP_Stationierungslinie>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_AlternativeTrasseAbschnitt gml:id="_pfs_alternativetrasseabschnitt.1">
            <title>Hainichen 2</title>
            <beschreibung>Kreuzung der 380-kV-Bestandsleitung mit südlichem Verlauf</beschreibung>
            <aufschrift>F2</aufschrift>
            <gehoertZuPFS
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:posList>319407.837 5638829.778 320475.963 5638715.670 320667.595
                        5639056.069 322957.092 5639507.276</gml:posList>
                </gml:LineString>
            </position>
        </PFS_AlternativeTrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_AlternativeTrasseAbschnitt gml:id="_pfs_alternativetrasseabschnitt.2">
            <title>Langenchursdorf 2</title>
            <beschreibung>Kreuzung der 380-kV-Bestandsleitung mit Provisorium</beschreibung>
            <aufschrift>B2</aufschrift>
            <gehoertZuPFS
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:posList>336063.169 5638954.500 336235.202 5638943.863 336770.534
                        5637391.403</gml:posList>
                </gml:LineString>
            </position>
        </PFS_AlternativeTrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_AlternativeTrasseAbschnitt gml:id="_pfs_alternativetrasseabschnitt.3">
            <title>Dürrenuhlsdorf 2</title>
            <aufschrift>D2</aufschrift>
            <gehoertZuPFS
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:posList>330135.174 5639932.833 332732.296 5640785.093</gml:posList>
                </gml:LineString>
            </position>
        </PFS_AlternativeTrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_AlternativeTrasseAbschnitt gml:id="_pfs_alternativetrasseabschnitt.4">
            <title>Liebschwitz/ Elstertal
                2</title>
            <beschreibung>Verlauf am nördlichen Korridorrand entlang mit zweifacher Kreuzung einer
                110-kVLeitung</beschreibung>
            <aufschrift>H2</aufschrift>
            <gehoertZuPFS
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:posList>294030.126 5634304.894 294206.207 5634529.384 296416.831
                        5634236.065 296604.555 5634148.562</gml:posList>
                </gml:LineString>
            </position>
        </PFS_AlternativeTrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_AlternativeTrasseAbschnitt gml:id="_pfs_alternativetrasseabschnitt.5">
            <title>Hainichen 2</title>
            <beschreibung>Kreuzung der 380-kV-Bestandsleitung mit südlichem Verlauf</beschreibung>
            <aufschrift>F2</aufschrift>
            <gehoertZuPFS
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:posList>319407.837 5638829.778 320475.963 5638715.670 320667.595
                        5639056.069 322957.092 5639507.276</gml:posList>
                </gml:LineString>
            </position>
        </PFS_AlternativeTrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_AlternativeTrasseAbschnitt gml:id="_pfs_alternativetrasseabschnitt.6">
            <title>Langenchursdorf 2</title>
            <beschreibung>Kreuzung der 380-kV-Bestandsleitung mit Provisorium</beschreibung>
            <aufschrift>B2</aufschrift>
            <gehoertZuPFS
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:posList>336063.169 5638954.500 336235.202 5638943.863 336770.534
                        5637391.403</gml:posList>
                </gml:LineString>
            </position>
        </PFS_AlternativeTrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_AlternativeTrasseAbschnitt gml:id="_pfs_alternativetrasseabschnitt.7">
            <title>Dürrenuhlsdorf 2</title>
            <aufschrift>D2</aufschrift>
            <gehoertZuPFS
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:posList>330135.174 5639932.833 332732.296 5640785.093</gml:posList>
                </gml:LineString>
            </position>
        </PFS_AlternativeTrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_AlternativeTrasseAbschnitt gml:id="_pfs_alternativetrasseabschnitt.8">
            <title>Liebschwitz/ Elstertal
                2</title>
            <beschreibung>Verlauf am nördlichen Korridorrand entlang mit zweifacher Kreuzung einer
                110-kVLeitung</beschreibung>
            <aufschrift>H2</aufschrift>
            <gehoertZuPFS
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:posList>294030.126 5634304.894 294206.207 5634529.384 296416.831
                        5634236.065 296604.555 5634148.562</gml:posList>
                </gml:LineString>
            </position>
        </PFS_AlternativeTrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_HochspannungsleitungAbschnitt gml:id="_pfs_hochspannungsleitungabschnitt.1">
            <title>Langenchursdorf 1</title>
            <beschreibung>Kreuzung der 380-kV-Bestandsleitung mit Provisorium</beschreibung>
            <aufschrift>B1</aufschrift>
            <gehoertZuPFS
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:posList>335733.208 5638980.472 336063.169 5638954.500 336269.931
                        5638319.087 336721.848 5637481.406 336770.534 5637391.403 336899.227
                        5637153.500</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>20001</leitungstyp>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">72</schutzstreifen>
                    <schutzstreifenWald uom="m">104</schutzstreifenWald>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <spannung>3000</spannung>
        </PFS_HochspannungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_HochspannungsleitungAbschnitt gml:id="_pfs_hochspannungsleitungabschnitt.2">
            <title>Dürrenuhlsdorf 1</title>
            <beschreibung>Verlauf mit nordwestlicher Umgehung von Dürrenuhlsdorf, Querung der OU
                Dürrenuhlsdorf</beschreibung>
            <aufschrift>D1</aufschrift>
            <gehoertZuPFS
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:posList>328889.782 5639504.941 330135.174 5639932.833 331688.404
                        5641047.327 332732.296 5640785.093 332908.759 5640627.157</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>20001</leitungstyp>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">72</schutzstreifen>
                    <schutzstreifenWald uom="m">104</schutzstreifenWald>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <spannung>3000</spannung>
        </PFS_HochspannungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_HochspannungsleitungAbschnitt gml:id="_pfs_hochspannungsleitungabschnitt.3">
            <title>Oberwiera - Tettau</title>
            <beschreibung>parallel zwischen 110-kV-Bahnstromleitung und 380-kV-Bestandsleitung</beschreibung>
            <aufschrift>E</aufschrift>
            <gehoertZuPFS
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:posList>323146.244 5639500.242 325345.626 5639418.452 328740.796
                        5639453.752 328889.782 5639504.941</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>20001</leitungstyp>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">72</schutzstreifen>
                    <schutzstreifenWald uom="m">104</schutzstreifenWald>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <spannung>3000</spannung>
        </PFS_HochspannungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_HochspannungsleitungAbschnitt gml:id="_pfs_hochspannungsleitungabschnitt.4">
            <title>Hainichen 1</title>
            <beschreibung>nörlich Kautitzer Berg mit Kreuzung der 380-kV-Leitung</beschreibung>
            <aufschrift>F1</aufschrift>
            <gehoertZuPFS
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:posList>319262.321 5638817.739 319407.837 5638829.778 319696.253
                        5638853.641 320248.457 5639307.507 321006.161 5639579.826 322957.092
                        5639507.276 323146.244 5639500.242</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>20001</leitungstyp>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">72</schutzstreifen>
                    <schutzstreifenWald uom="m">104</schutzstreifenWald>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <spannung>3000</spannung>
        </PFS_HochspannungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_HochspannungsleitungAbschnitt gml:id="_pfs_hochspannungsleitungabschnitt.5">
            <title>Gößnitz – Thonhausen
                - Niebra</title>
            <beschreibung>Trassengleicher Verlauf durch das Wasserschutzgebiet der Pleiße, dann
                nördlich der
                380-kV-Bestandsleitung folgend. Querung der BAB 4.</beschreibung>
            <aufschrift>G</aufschrift>
            <gehoertZuPFS
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:posList>296766.346 5634051.922 297355.956 5633699.739 299322.709
                        5633129.885 304877.525 5633150.057 308294.128 5633129.885 310976.981
                        5636634.740 311930.100 5636962.532 312643.679 5636836.458 313884.246
                        5637204.594 316526.755 5638591.407 319262.321 5638817.739</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>20001</leitungstyp>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">72</schutzstreifen>
                    <schutzstreifenWald uom="m">104</schutzstreifenWald>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <spannung>3000</spannung>
        </PFS_HochspannungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_HochspannungsleitungAbschnitt gml:id="_pfs_hochspannungsleitungabschnitt.6">
            <title>Liebschwitz/ Elstertal 1</title>
            <beschreibung>Verlauf südlich parallel zur 380-kV-Bestandsleitung mit zweifacher
                Kreuzung dieser</beschreibung>
            <aufschrift>H1</aufschrift>
            <gehoertZuPFS
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:posList>293996.986 5634269.466 294030.126 5634304.894 295270.693
                        5634239.335 296296.935 5634067.875 296604.555 5634148.562 296766.346
                        5634051.922</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>20001</leitungstyp>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">72</schutzstreifen>
                    <schutzstreifenWald uom="m">104</schutzstreifenWald>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <spannung>3000</spannung>
        </PFS_HochspannungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_HochspannungsleitungAbschnitt gml:id="_pfs_hochspannungsleitungabschnitt.7">
            <title>Wolfsgefährt -
                Weida</title>
            <beschreibung>Kreuzung der 380-kV-Bestandsleitung, UW Weida</beschreibung>
            <aufschrift>I</aufschrift>
            <gehoertZuPFS
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:posList>291685.151 5629676.720 291793.280 5629684.956 291908.302
                        5629723.368 292010.421 5630003.252 292111.281 5630391.559 292378.557
                        5630956.371 292459.245 5632625.589 293996.986 5634269.466</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>20001</leitungstyp>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">72</schutzstreifen>
                    <schutzstreifenWald uom="m">104</schutzstreifenWald>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <spannung>3000</spannung>
        </PFS_HochspannungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_HochspannungsleitungAbschnitt gml:id="_pfs_hochspannungsleitungabschnitt.8">
            <title>Niederwinckel</title>
            <beschreibung>Querung des Langenberger Bachs, Querung des Tals der Zwickauer Mulde mit
                enger
                Bündelung und parallelem Verlauf zwischen 110-kV-Bahnstromleitung und
                380-kVBestandsleitung</beschreibung>
            <aufschrift>C</aufschrift>
            <gehoertZuPFS
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:posList>332908.759 5640627.157 334626.156 5639071.311 335733.208
                        5638980.472</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>20001</leitungstyp>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">72</schutzstreifen>
                    <schutzstreifenWald uom="m">104</schutzstreifenWald>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <spannung>3000</spannung>
        </PFS_HochspannungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <PFS_HochspannungsleitungAbschnitt gml:id="_pfs_hochspannungsleitungabschnitt.9">
            <title>Südumgehung
                Limbach-
                Oberfrohna</title>
            <beschreibung>Anschluss an das UW Röhrsdorf, Querung des Sachsenforst</beschreibung>
            <aufschrift>A</aufschrift>
            <gehoertZuPFS
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <nachrichtlUebernahme>false</nachrichtlUebernahme>
            <planErgaenzAenderung>false</planErgaenzAenderung>
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:posList>336899.227 5637153.500 337084.368 5636811.243 338993.127
                        5634350.280 339184.759 5633548.450 339825.215 5632751.663 341391.053
                        5632554.988 343811.672 5633735.040 345828.855 5635747.179 346155.368
                        5636077.216 346121.572 5636323.285</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>20001</leitungstyp>
            <schutzstreifen>
                <PFS_SchutzstreifenData>
                    <schutzstreifen uom="m">72</schutzstreifen>
                    <schutzstreifenWald uom="m">104</schutzstreifenWald>
                </PFS_SchutzstreifenData>
            </schutzstreifen>
            <spannung>3000</spannung>
        </PFS_HochspannungsleitungAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Umspannwerk gml:id="_bst_umspannwerk.1">
            <title>Umspannwerk Röhrsdorf</title>
            <gehoertZuPlan
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiSurface srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:surfaceMember>
                        <gml:Polygon>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:posList>345724.188 5636374.346 345714.538 5636244.081
                                        345792.422 5636242.013 345936.644 5636226.161 345935.955
                                        5636096.585 346380.511 5636067.637 346382.579 5636162.751
                                        346441.853 5636233.053 346462.530 5636230.985 346468.733
                                        5636252.352 346495.613 5636273.718 346496.303 5636333.681
                                        346481.829 5636350.223 346485.964 5636416.390 346440.475
                                        5636510.125 346411.527 5636511.504 346374.997 5636503.922
                                        345979.894 5636525.289 345968.866 5636297.841 345958.527
                                        5636297.152 345959.906 5636373.657 345945.432 5636375.035
                                        345953.013 5636448.784 345846.871 5636453.608 345842.047
                                        5636377.792 345804.828 5636377.792 345762.785 5636379.171
                                        345724.188 5636374.346</gml:posList>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </gml:surfaceMember>
                </gml:MultiSurface>
            </position>
            <netzSparte>3000</netzSparte>
            <begrenzung>2000</begrenzung>
        </BST_Umspannwerk>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Umspannwerk gml:id="_bst_umspannwerk.2">
            <title>Umspannwerk Weide</title>
            <gehoertZuPlan
                xlink:title="Netzverstärkung 380-kV-Höchstspannungsleitung Röhrsdorf - Weida"
                xlink:href="#_pfs_plan.1" />
            <position>
                <gml:MultiSurface srsName="http://www.opengis.net/def/crs/EPSG/0/25833">
                    <gml:surfaceMember>
                        <gml:Polygon>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:posList>291565.967 5629803.872 291572.859 5629576.769
                                        291616.051 5629579.067 291617.889 5629555.633 291656.946
                                        5629557.930 291657.865 5629517.495 291815.929 5629521.631
                                        291804.901 5629813.981 291843.039 5629816.279 291838.904
                                        5629908.176 291748.384 5629906.339 291748.384 5629868.660
                                        291701.057 5629865.903 291703.354 5629821.792 291653.729
                                        5629820.414 291652.810 5629807.548 291565.967 5629803.872</gml:posList>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </gml:surfaceMember>
                </gml:MultiSurface>
            </position>
            <netzSparte>3000</netzSparte>
            <begrenzung>2000</begrenzung>
        </BST_Umspannwerk>
    </sf:featureMember>
</sf:FeatureCollection>