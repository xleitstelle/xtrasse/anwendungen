Die Validierung der GML-Instanzen erfolgt - in einer für diesen Zweck geeigneten Software - gegen die bei der GDI-DE [hinterlegten](https://repository.gdi-de.org/schemas/de.xleitstelle.xtrasse/2.0/XML/) XML-Schemata. Auf Open-Source-Basis ist eine Validierung mit *Visual Studio Code* und der XML-Extension von *Red Hat* möglich.  

Die einfachste Form der Validierung von JSON-Instanzen ist über eine der zahlreichen im Internet verfügbaren Validator-Seiten möglich, z.B. [JSON Schema Validator](https://www.jsonschemavalidator.net/). Auf der linken Seite wird hier das Schema [featurecollection.json](https://gitlab.opencode.de/xleitstelle/xtrasse/spezifikation/-/blob/main/json/featurecollection.json) eingefügt, rechts einer der vier Testpläne. 


![JSON-Validierung](JSONvalidator.jpg)



Die hier abgelegten Pläne sind ebenso über die [OGC API Features](https://xtrasse.ldproxy.devops.diplanung.de) der XLeitstelle verfügbar (Download über "Vordefinierte Abfrage für diese API", Ausgabeformat: GML, JSON-FG). 