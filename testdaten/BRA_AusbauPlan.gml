<?xml version="1.0" encoding="utf-8"?>
<sf:FeatureCollection xmlns="http://www.xtrasse.de/2.0" xmlns:xtrasse="http://www.xtrasse.de/2.0"
    xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xml="http://www.w3.org/XML/1998/namespace"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:sf="http://www.opengis.net/ogcapi-features-1/1.0/sf"
    xsi:schemaLocation="http://www.xtrasse.de/2.0 https://repository.gdi-de.org/schemas/de.xleitstelle.xtrasse/2.0/XML/XTrasse.xsd
                        http://www.opengis.net/ogcapi-features-1/1.0/sf http://schemas.opengis.net/ogcapi/features/part1/1.0/xml/core-sf.xsd">
    <sf:featureMember>
        <BRA_AusbauPlan gml:id="_bra_ausbauplan.1">
            <name>Trassenverbindung Oeserstraße mit Westerbachstraße</name>
            <beschreibung>Der Trassenplan zeigt eine umfangreiche Planung für eine 2,1 km lange
                TK-Linie im Frankfurter Stadtteil Sossenheim. Die Trasse zweigt von einer
                Backbone-Trasse an der Oeserstraße in nördlicher Richtung ab, unterquert die A648
                und endet in der Westerbachstraße. Der Plan wurde im August 2019 als Anhang zu einem
                &quot;Antrag für eine Trassenzustimmung gem. § 68 Abs. 3 TKG&quot; beim Frankfurter
                Amt für Strassenbau und Erschliessung (ASE) als PDF-Dokument eingereicht. Die
                Planung der neuen Trasse umfasst Schutzrohre; die am Beginn und Abschluss der Trasse
                eingefügten Mikrorohre und Kabel dienen allein der Darstellung der
                XTrasse-Objektklassen. </beschreibung>
            <gesetzlicheGrundlage>
                <XP_GesetzlicheGrundlage>
                    <name>Telekommunikationsgesetz (TKG)</name>
                    <detail>§ 68 Abs. 3</detail>
                    <ausfertigungDatum>2004-06-22</ausfertigungDatum>
                </XP_GesetzlicheGrundlage>
            </gesetzlicheGrundlage>
            <technischePlanerstellung>XLeitstelle</technischePlanerstellung>
            <technHerstellDatum>2024-06-07</technHerstellDatum>
            <position>
                <gml:Polygon srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>470049.916 5550695.838 471119.571 5550697.841 471131.466
                                5552634.291 470049.916 5552630.569 470049.916 5550695.838</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </position>
            <hatBSTObjekt xlink:title="Abwasser 2.2" xlink:href="#_bst_abwasserleitung.1" />
            <hatBSTObjekt xlink:title="Abwasser 1.2" xlink:href="#_bst_abwasserleitung.2" />
            <hatBSTObjekt xlink:title="Abwasser 1.1" xlink:href="#_bst_abwasserleitung.3" />
            <hatBSTObjekt xlink:title="Abwasser 2.1" xlink:href="#_bst_abwasserleitung.4" />
            <hatBSTObjekt xlink:title="fiktive Beispiele" xlink:href="#_bst_baum.1" />
            <hatBSTObjekt xlink:title="BestandGas Westerbachstraße" xlink:href="#_bst_gasleitung.6" />
            <hatBSTObjekt xlink:title="BestandGas Nidda 2" xlink:href="#_bst_gasleitung.7" />
            <hatBSTObjekt xlink:title="BestandGas Nidda 1" xlink:href="#_bst_gasleitung.8" />
            <hatBSTObjekt xlink:title="BestandGas Oeserstraße" xlink:href="#_bst_gasleitung.9" />
            <hatBSTObjekt xlink:title="Leuchten Westerbachstraße" xlink:href="#_bst_mast.1" />
            <hatBSTObjekt xlink:title="Abwasserschächte" xlink:href="#_bst_schacht.1" />
            <hatBSTObjekt xlink:title="TK-Schächte Wilhelm-Fay-Straße" xlink:href="#_bst_schacht.2" />
            <hatBSTObjekt xlink:title="Westerbachstraße" xlink:href="#_bst_strassenbeleuchtung.1" />
            <hatBSTObjekt xlink:title="Steuerleitung Gas" xlink:href="#_bst_stromleitung.1" />
            <hatBSTObjekt xlink:title="Strom südl. Niidda" xlink:href="#_bst_stromleitung.2" />
            <hatBSTObjekt xlink:title="Freileitung" xlink:href="#_bst_stromleitung.3" />
            <hatBSTObjekt xlink:title="Strom Oeserstraße" xlink:href="#_bst_stromleitung.4" />
            <hatBSTObjekt xlink:title="Strom Westerbachstraße" xlink:href="#_bst_stromleitung.5" />
            <hatBSTObjekt xlink:title="Leitung ITK Wilhelm-Fay-Straße"
                xlink:href="#_bst_telekommunikationsleitung.1" />
            <hatBSTObjekt xlink:title="Leitung Schütenhausenweg"
                xlink:href="#_bst_telekommunikationsleitung.2" />
            <hatBSTObjekt xlink:title="Leitung Holler"
                xlink:href="#_bst_telekommunikationsleitung.3" />
            <hatBSTObjekt xlink:title="Leitung am Schwemmteich"
                xlink:href="#_bst_telekommunikationsleitung.4" />
            <hatBSTObjekt xlink:title="Leitung Oeserstraße Nord 1"
                xlink:href="#_bst_telekommunikationsleitung.5" />
            <hatBSTObjekt xlink:title="Leitung Oeserstraße Nord 2"
                xlink:href="#_bst_telekommunikationsleitung.6" />
            <hatBSTObjekt xlink:title="Leitung Oeserstraße Süd"
                xlink:href="#_bst_telekommunikationsleitung.7" />
            <hatBSTObjekt xlink:title="Leitung Nidda Nord"
                xlink:href="#_bst_telekommunikationsleitung.8" />
            <hatBSTObjekt xlink:title="Leitung südlich A648"
                xlink:href="#_bst_telekommunikationsleitung.9" />
            <hatBSTObjekt xlink:title="Leitung Westerbachstraße Süd"
                xlink:href="#_bst_telekommunikationsleitung.10" />
            <hatBSTObjekt xlink:title="Interroute entlang A648"
                xlink:href="#_bst_telekommunikationsleitung.11" />
            <hatBSTObjekt xlink:title="Leitungen Westerbachstraße Nord"
                xlink:href="#_bst_telekommunikationsleitung.12" />
            <hatBSTObjekt xlink:title="Verteiler Wilhelm-Fay-Straße" xlink:href="#_bst_verteiler.1" />
            <hatBSTObjekt xlink:title="Verteiler Wilhelm-Fay und Westerbachstraße"
                xlink:href="#_bst_verteiler.2" />
            <hatBSTObjekt xlink:title="Verteiler um Westerbachstraße" xlink:href="#_bst_verteiler.3" />
            <hatBSTObjekt xlink:title="Wasser Oeserstraße" xlink:href="#_bst_wasserleitung.1" />
            <hatBSTObjekt xlink:title="Wasser Westerbachstraße" xlink:href="#_bst_wasserleitung.2" />
            <hatBSTObjekt xlink:title="Beispiel" xlink:href="#_bst_wegekante.1" />
            <hatBRAObjekt xlink:title="A648" xlink:href="#_bra_baugrube.1" />
            <hatBRAObjekt xlink:title="Graben beim Holler" xlink:href="#_bra_baugrube.2" />
            <hatBRAObjekt xlink:title="Graben am Schwemmteich" xlink:href="#_bra_baugrube.3" />
            <hatBRAObjekt xlink:title="A648" xlink:href="#_bra_baugrube.4" />
            <hatBRAObjekt xlink:title="Grill&apos;scher Altarm" xlink:href="#_bra_baugrube.5" />
            <hatBRAObjekt xlink:title="Grill&apos;scher Altarm" xlink:href="#_bra_baugrube.6" />
            <hatBRAObjekt xlink:title="Abschnitt 1" xlink:href="#_bra_baustelle.1" />
            <hatBRAObjekt xlink:title="Abschnitt 2" xlink:href="#_bra_baustelle.2" />
            <hatBRAObjekt xlink:title="Abschnitt 3" xlink:href="#_bra_baustelle.3" />
            <hatBRAObjekt xlink:title="Querung  Nidda" xlink:href="#_bra_breitbandtrasseabschnitt.1" />
            <hatBRAObjekt xlink:title="Querung A 648" xlink:href="#_bra_breitbandtrasseabschnitt.2" />
            <hatBRAObjekt xlink:title="Verbindung zur A 648"
                xlink:href="#_bra_breitbandtrasseabschnitt.3" />
            <hatBRAObjekt xlink:title="Bohrung nördlich Querung Nidda"
                xlink:href="#_bra_breitbandtrasseabschnitt.4" />
            <hatBRAObjekt xlink:title="Verlegung unter Entwässerungsgraben"
                xlink:href="#_bra_breitbandtrasseabschnitt.5" />
            <hatBRAObjekt xlink:title="Uferbereich Grill&apos;Scher Altarm"
                xlink:href="#_bra_breitbandtrasseabschnitt.6" />
            <hatBRAObjekt xlink:title="Anschluss Westerbachstraße"
                xlink:href="#_bra_breitbandtrasseabschnitt.7" />
            <hatBRAObjekt xlink:title="Anschluss an bestehende Trasse Oeserstraße"
                xlink:href="#_bra_breitbandtrasseabschnitt.8" />
            <hatBRAObjekt xlink:title="Bohrung südlich Querung Nidda (Süd)"
                xlink:href="#_bra_breitbandtrasseabschnitt.9" />
            <hatBRAObjekt xlink:title="Testanschlüsse" xlink:href="#_bra_hausanschluss.1" />
            <hatBRAObjekt xlink:title="testweise hinzugefügtes Kabel 1" xlink:href="#_bra_kabel.50" />
            <hatBRAObjekt xlink:title="testweise hinzugefügtes Kabel 2" xlink:href="#_bra_kabel.53" />
            <hatBRAObjekt xlink:title="testweise hinzugefügtes  Kabel 3" xlink:href="#_bra_kabel.54" />
            <hatBRAObjekt xlink:title="Teststation" xlink:href="#_bra_kompaktstation.2" />
            <hatBRAObjekt xlink:title="Testmasten" xlink:href="#_bra_mast.1" />
            <hatBRAObjekt xlink:title="testweise hinzugefügtes Mikrorohr1"
                xlink:href="#_bra_mikrorohr.1" />
            <hatBRAObjekt xlink:title="testweise hinzugefügtes Mikrorohr2"
                xlink:href="#_bra_mikrorohr.2" />
            <hatBRAObjekt xlink:title="testweise hinzugefügter Rohrverbund"
                xlink:href="#_bra_mikrorohrverbund.1" />
            <hatBRAObjekt xlink:title="Testmuffen" xlink:href="#_bra_rohrmuffe.1" />
            <hatBRAObjekt xlink:title="Testschächte" xlink:href="#_bra_schacht.1" />
            <hatBRAObjekt xlink:title="Schutzrohr 1/4  (unbelegt)" xlink:href="#_bra_schutzrohr.1" />
            <hatBRAObjekt xlink:title="Schutzrohr 2/4 (mit Mikrorohr)"
                xlink:href="#_bra_schutzrohr.2" />
            <hatBRAObjekt xlink:title="Schutzrohr 3/4 (unbelegt)" xlink:href="#_bra_schutzrohr.3" />
            <hatBRAObjekt xlink:title="Schutzrohr 4/4 (mit Mikrorohrverbund)"
                xlink:href="#_bra_schutzrohr.4" />
            <hatBRAObjekt xlink:title="beispiel" xlink:href="#_bra_umfeld.1" />
            <hatBRAObjekt xlink:title="Verteiler Colt" xlink:href="#_bra_verteiler.1" />
            <beteiligte>
                <XP_Akteur>
                    <nameOrganisation>Klenk und Sohn GmbH</nameOrganisation>
                    <ort>Modautal</ort>
                    <rolle>3000</rolle>
                </XP_Akteur>
            </beteiligte>
            <beteiligte>
                <XP_Akteur>
                    <nameOrganisation>Colt Technology Services GmbH</nameOrganisation>
                    <ort>Frankfurt am Main</ort>
                    <rolle>1000</rolle>
                </XP_Akteur>
            </beteiligte>
            <beteiligte>
                <XP_Akteur>
                    <nameOrganisation>ksk Kabeltiefbau und Montage GmbH</nameOrganisation>
                    <ort>Offenbach am Main</ort>
                    <rolle>5000</rolle>
                </XP_Akteur>
            </beteiligte>
            <bauBeginn>2019-05-06</bauBeginn>
            <bauEnde>2019-07-12</bauEnde>
            <status>1000</status>
        </BRA_AusbauPlan>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Baugrube gml:id="_bra_baugrube.1">
            <title>A648</title>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>470260.207 5552030.973</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
            <art>1000</art>
        </BRA_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Baugrube gml:id="_bra_baugrube.2">
            <title>Graben beim Holler</title>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>470434.497 5551324.686</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
            <art>2000</art>
        </BRA_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Baugrube gml:id="_bra_baugrube.3">
            <title>Graben am Schwemmteich</title>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>470502.281 5551509.853</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
            <art>1000</art>
        </BRA_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Baugrube gml:id="_bra_baugrube.4">
            <title>A648</title>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>470316.759 5552034.651</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
            <art>2000</art>
        </BRA_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Baugrube gml:id="_bra_baugrube.5">
            <title>Grill&apos;scher Altarm</title>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>470594.476 5551084.087</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
            <art>1000</art>
            <startDatum>2019-07-22</startDatum>
            <endDatum>2019-08-01</endDatum>
        </BRA_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Baugrube gml:id="_bra_baugrube.6">
            <title>Grill&apos;scher Altarm</title>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>470494.879 5551229.456</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
            <art>2000</art>
            <startDatum>2019-07-22</startDatum>
            <endDatum>2019-08-01</endDatum>
        </BRA_Baugrube>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Baustelle gml:id="_bra_baustelle.1">
            <title>Abschnitt 1</title>
            <aufschrift>Abschnitt 1: 6.5.19 - 24.5.19</aufschrift>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiSurface srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:surfaceMember>
                        <gml:Polygon>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:posList>470445.764 5551243.053 470857.259 5550728.276
                                        471072.376 5550900.233 470660.881 5551415.010 470445.764
                                        5551243.053</gml:posList>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </gml:surfaceMember>
                </gml:MultiSurface>
            </position>
            <art>3000</art>
            <startDatum>2019-05-06</startDatum>
            <endDatum>2019-05-24</endDatum>
        </BRA_Baustelle>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Baustelle gml:id="_bra_baustelle.2">
            <title>Abschnitt 2</title>
            <aufschrift>Abschnitt 2: 27.5.19 - 28.6.19</aufschrift>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiSurface srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:surfaceMember>
                        <gml:Polygon>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:posList>470223.273 5552346.316 470240.172 5551250.400
                                        470545.130 5551255.103 470528.231 5552351.019 470223.273
                                        5552346.316</gml:posList>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </gml:surfaceMember>
                </gml:MultiSurface>
            </position>
            <art>3000</art>
            <startDatum>2019-05-27</startDatum>
            <endDatum>2019-06-28</endDatum>
        </BRA_Baustelle>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Baustelle gml:id="_bra_baustelle.3">
            <title>Abschnitt 3</title>
            <aufschrift>Abschnitt 3: 1.7.19 - 12.7.19</aufschrift>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiSurface srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:surfaceMember>
                        <gml:Polygon>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:posList>470362.577 5552347.887 470664.824 5552349.461
                                        470664.034 5552501.107 470361.787 5552499.532 470362.577
                                        5552347.887</gml:posList>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </gml:surfaceMember>
                </gml:MultiSurface>
            </position>
            <art>3000</art>
            <startDatum>2019-07-01</startDatum>
            <endDatum>2019-07-12</endDatum>
        </BRA_Baustelle>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_BreitbandtrasseAbschnitt gml:id="_bra_breitbandtrasseabschnitt.1">
            <title>Querung Nidda</title>
            <netzbetreiber>Colt GmbH</netzbetreiber>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>470469.432 5551289.824 470475.341 5551254.394</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>20003</leitungstyp>
            <bauweise>3000</bauweise>
            <istOrtsdurchfahrt>false</istOrtsdurchfahrt>
            <istUeberfuehrungsbauwerk>true</istUeberfuehrungsbauwerk>
            <istGruenflaeche>false</istGruenflaeche>
            <kreuztStrasse>false</kreuztStrasse>
        </BRA_BreitbandtrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_BreitbandtrasseAbschnitt gml:id="_bra_breitbandtrasseabschnitt.2">
            <title>Querung A 648</title>
            <netzbetreiber>Colt GmbH</netzbetreiber>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>470316.759 5552034.651 470260.207 5552030.973</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>10003</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>3000</legeverfahren>
            <istOrtsdurchfahrt>false</istOrtsdurchfahrt>
            <istUeberfuehrungsbauwerk>false</istUeberfuehrungsbauwerk>
            <istGruenflaeche>false</istGruenflaeche>
            <kreuztStrasse>true</kreuztStrasse>
        </BRA_BreitbandtrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_BreitbandtrasseAbschnitt gml:id="_bra_breitbandtrasseabschnitt.3">
            <title>Verbindung zur A 648</title>
            <netzbetreiber>Colt GmbH</netzbetreiber>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>470260.207 5552030.973 470301.872 5551969.079 470286.208
                        5551894.184 470236.414 5551769.556 470231.797 5551664.965 470244.448
                        5551571.719 470293.723 5551589.246 470301.689 5551588.271 470329.585
                        5551489.936 470334.174 5551487.035 470392.533 5551495.239 470397.932
                        5551493.456 470492.706 5551504.716 470499.628 5551504.498 470502.281
                        5551509.853</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>10003</leitungstyp>
            <bauweise>1000</bauweise>
            <istOrtsdurchfahrt>false</istOrtsdurchfahrt>
            <istUeberfuehrungsbauwerk>false</istUeberfuehrungsbauwerk>
            <istGruenflaeche>false</istGruenflaeche>
            <kreuztStrasse>false</kreuztStrasse>
        </BRA_BreitbandtrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_BreitbandtrasseAbschnitt gml:id="_bra_breitbandtrasseabschnitt.4">
            <title>Bohrung nördlich Querung Nidda</title>
            <netzbetreiber>Colt GmbH</netzbetreiber>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>470434.497 5551324.686 470451.102 5551306.779 470465.308
                        5551289.124 470469.432 5551289.824</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>10003</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>3000</legeverfahren>
            <baugrund>30002</baugrund>
            <istOrtsdurchfahrt>false</istOrtsdurchfahrt>
            <istUeberfuehrungsbauwerk>false</istUeberfuehrungsbauwerk>
            <istGruenflaeche>false</istGruenflaeche>
            <kreuztStrasse>false</kreuztStrasse>
        </BRA_BreitbandtrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_BreitbandtrasseAbschnitt gml:id="_bra_breitbandtrasseabschnitt.5">
            <title>Verlegung unter Entwässerungsgraben</title>
            <netzbetreiber>Colt GmbH</netzbetreiber>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>470502.281 5551509.853 470502.449 5551506.307 470434.497
                        5551324.686</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>10003</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>3000</legeverfahren>
            <baugrund>30002</baugrund>
            <istOrtsdurchfahrt>false</istOrtsdurchfahrt>
            <istUeberfuehrungsbauwerk>false</istUeberfuehrungsbauwerk>
            <istGruenflaeche>false</istGruenflaeche>
            <kreuztStrasse>false</kreuztStrasse>
        </BRA_BreitbandtrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_BreitbandtrasseAbschnitt gml:id="_bra_breitbandtrasseabschnitt.6">
            <title>Uferbereich Grill&apos;Scher Altarm</title>
            <netzbetreiber>Colt GmbH</netzbetreiber>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>470494.879 5551229.456 470505.102 5551220.412 470580.458
                        5551102.825 470593.525 5551085.552 470594.476 5551084.087 470597.387
                        5551081.098</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>10003</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>3000</legeverfahren>
            <istOrtsdurchfahrt>false</istOrtsdurchfahrt>
            <istUeberfuehrungsbauwerk>false</istUeberfuehrungsbauwerk>
            <istGruenflaeche>false</istGruenflaeche>
            <kreuztStrasse>false</kreuztStrasse>
        </BRA_BreitbandtrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_BreitbandtrasseAbschnitt gml:id="_bra_breitbandtrasseabschnitt.7">
            <title>Anschluss Westerbachstraße</title>
            <netzbetreiber>Colt GmbH</netzbetreiber>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>470632.648 5552406.105 470630.227 5552407.668 470584.783
                        5552415.425 470521.490 5552425.985 470397.878 5552445.947 470382.300
                        5552332.867 470316.759 5552034.651</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>10003</leitungstyp>
            <ueberdeckung uom="m">60</ueberdeckung>
            <bauweise>1000</bauweise>
            <strassenart>5000</strassenart>
            <istOrtsdurchfahrt>false</istOrtsdurchfahrt>
            <istUeberfuehrungsbauwerk>false</istUeberfuehrungsbauwerk>
            <istGruenflaeche>false</istGruenflaeche>
            <kreuztStrasse>false</kreuztStrasse>
        </BRA_BreitbandtrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_BreitbandtrasseAbschnitt gml:id="_bra_breitbandtrasseabschnitt.8">
            <title>Anschluss an bestehende Trasse Oeserstraße</title>
            <netzbetreiber>Colt GmbH</netzbetreiber>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>470597.387 5551081.098 470679.058 5551032.003 470728.344
                        5551015.906 470828.861 5550990.517 470869.356 5550978.588 470910.073
                        5550970.866 471006.338 5550943.804 471009.472 5550941.990 471012.832
                        5550932.699</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>10003</leitungstyp>
            <ueberdeckung uom="m">100</ueberdeckung>
            <bauweise>1000</bauweise>
            <istOrtsdurchfahrt>false</istOrtsdurchfahrt>
            <istUeberfuehrungsbauwerk>false</istUeberfuehrungsbauwerk>
            <istGruenflaeche>false</istGruenflaeche>
            <kreuztStrasse>true</kreuztStrasse>
        </BRA_BreitbandtrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_BreitbandtrasseAbschnitt gml:id="_bra_breitbandtrasseabschnitt.9">
            <title>Bohrung südlich Querung Nidda (Süd)</title>
            <netzbetreiber>Colt GmbH</netzbetreiber>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>470475.341 5551254.394 470477.294 5551253.671 470479.441
                        5551251.910 470489.016 5551235.871 470494.879 5551229.456</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>10003</leitungstyp>
            <bauweise>2000</bauweise>
            <legeverfahren>3000</legeverfahren>
            <istOrtsdurchfahrt>false</istOrtsdurchfahrt>
            <istUeberfuehrungsbauwerk>false</istUeberfuehrungsbauwerk>
            <istGruenflaeche>false</istGruenflaeche>
            <kreuztStrasse>false</kreuztStrasse>
        </BRA_BreitbandtrasseAbschnitt>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Hausanschluss gml:id="_bra_hausanschluss.1">
            <title>Testanschlüsse</title>
            <beschreibung>testweise eingefügte Hausanschlüsse</beschreibung>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>470585.593 5552405.688</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470596.345 5552401.134</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470601.610 5552400.512</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470607.644 5552399.854</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
            <technik>6000</technik>
        </BRA_Hausanschluss>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Kabel gml:id="_bra_kabel.50">
            <title>testweise hinzugefügtes Kabel 1</title>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>470632.648 5552406.105 470630.227 5552407.668 470584.783
                        5552415.425 470521.490 5552425.985 470489.224 5552431.363</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <art>4000</art>
        </BRA_Kabel>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Kabel gml:id="_bra_kabel.53">
            <title>testweise hinzugefügtes Kabel 2</title>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>470632.648 5552406.105 470630.227 5552407.668 470584.783
                        5552415.425 470521.490 5552425.985 470489.224 5552431.363</gml:posList>
                </gml:LineString>
            </position>
            <art>1000</art>
            <mikrorohrverbund xlink:title="testweise hinzugefügter Rohrverbund"
                xlink:href="#_bra_mikrorohrverbund.1" />
            <schutzrohr xlink:title="Schutzrohr 4/4 (mit Mikrorohrverbund)"
                xlink:href="#_bra_schutzrohr.4" />
        </BRA_Kabel>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Kabel gml:id="_bra_kabel.54">
            <title>testweise hinzugefügtes Kabel 3</title>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>470632.648 5552406.105 470630.227 5552407.668 470584.783
                        5552415.425 470521.490 5552425.985 470489.224 5552431.363</gml:posList>
                </gml:LineString>
            </position>
            <art>1000</art>
            <mikrorohr xlink:title="testweise hinzugefügtes Mikrorohr1"
                xlink:href="#_bra_mikrorohr.1" />
            <schutzrohr xlink:title="Schutzrohr 2/4 (mit Mikrorohr)" xlink:href="#_bra_schutzrohr.2" />
        </BRA_Kabel>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Kompaktstation gml:id="_bra_kompaktstation.2">
            <title>Teststation</title>
            <beschreibung>testweise eingefügte Station</beschreibung>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>471012.832 5550932.699</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
            <technik>7000</technik>
            <werkstoff>4000</werkstoff>
        </BRA_Kompaktstation>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Mast gml:id="_bra_mast.1">
            <title>Testmasten</title>
            <beschreibung>testweise hinzugefügte Telefonmasten</beschreibung>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>470393.646 5552443.069</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470387.269 5552444.278</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470381.099 5552445.404</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470374.513 5552446.529</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470368.427 5552447.655</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </BRA_Mast>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Mikrorohr gml:id="_bra_mikrorohr.1">
            <title>testweise hinzugefügtes Mikrorohr1</title>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>470869.356 5550978.588 470910.073 5550970.866 470932.866
                        5550964.637 470957.858 5550957.967 471006.338 5550943.804 471009.472
                        5550941.990 471012.832 5550932.699</gml:posList>
                </gml:LineString>
            </position>
            <farbe>1500</farbe>
            <istReserveRohr>false</istReserveRohr>
            <werkstoff>10001</werkstoff>
            <rohrtyp>3200</rohrtyp>
            <schutzrohr xlink:title="Schutzrohr 2/4 (mit Mikrorohr)" xlink:href="#_bra_schutzrohr.2" />
        </BRA_Mikrorohr>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Mikrorohr gml:id="_bra_mikrorohr.2">
            <title>testweise hinzugefügtes Mikrorohr2</title>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>470869.356 5550978.588 470910.073 5550970.866 470932.866
                        5550964.637 470957.858 5550957.967 471006.338 5550943.804 471009.472
                        5550941.990 471012.832 5550932.699</gml:posList>
                </gml:LineString>
            </position>
            <farbe>1100</farbe>
            <istReserveRohr>false</istReserveRohr>
            <rohrtyp>2100</rohrtyp>
            <mikrorohrverbund xlink:title="testweise hinzugefügter Rohrverbund"
                xlink:href="#_bra_mikrorohrverbund.1" />
            <schutzrohr xlink:title="Schutzrohr 4/4 (mit Mikrorohrverbund)"
                xlink:href="#_bra_schutzrohr.4" />
        </BRA_Mikrorohr>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Mikrorohrverbund gml:id="_bra_mikrorohrverbund.1">
            <title>testweise hinzugefügter Rohrverbund</title>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>470632.648 5552406.105 470630.227 5552407.668 470584.783
                        5552415.425 470521.490 5552425.985 470489.224 5552431.363</gml:posList>
                </gml:LineString>
            </position>
            <anzahlMikrorohr1>1</anzahlMikrorohr1>
            <anzahlMikrorohr2>12</anzahlMikrorohr2>
            <davonReserveRohre>4</davonReserveRohre>
            <artMikrorohr1>4200</artMikrorohr1>
            <artMikrorohr2>2100</artMikrorohr2>
            <farbe>1200</farbe>
            <werkstoff>10001</werkstoff>
            <master>false</master>
            <schutzrohr xlink:title="Schutzrohr 4/4 (mit Mikrorohrverbund)"
                xlink:href="#_bra_schutzrohr.4" />
        </BRA_Mikrorohrverbund>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Rohrmuffe gml:id="_bra_rohrmuffe.1">
            <title>Testmuffen</title>
            <beschreibung>testweise eingefügte Rohrmuffen</beschreibung>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>470608.913 5552411.306</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470602.871 5552412.338</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470597.615 5552413.235</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470588.155 5552414.850</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
            <art>1000</art>
        </BRA_Rohrmuffe>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Schacht gml:id="_bra_schacht.1">
            <title>Testschächte</title>
            <beschreibung>testweise eingefügte Schächte</beschreibung>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>470397.878 5552445.947</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470404.414 5552444.910</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470409.417 5552444.081</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
        </BRA_Schacht>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Schutzrohr gml:id="_bra_schutzrohr.1">
            <title>Schutzrohr 1/4 (unbelegt)</title>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>470632.648 5552406.105 470630.227 5552407.668 470608.913
                        5552411.306 470602.871 5552412.338 470597.615 5552413.235 470584.783
                        5552415.425 470521.490 5552425.985 470397.878 5552445.947 470382.300
                        5552332.867 470316.759 5552034.651 470260.207 5552030.973 470301.872
                        5551969.079 470286.208 5551894.184 470236.414 5551769.556 470231.797
                        5551664.965 470244.448 5551571.719 470293.723 5551589.246 470301.689
                        5551588.271 470329.585 5551489.936 470334.174 5551487.035 470392.533
                        5551495.239 470397.932 5551493.456 470492.706 5551504.716 470499.628
                        5551504.498 470502.281 5551509.853 470502.449 5551506.307 470434.497
                        5551324.686 470451.102 5551306.779 470465.308 5551289.124 470469.432
                        5551289.824 470475.341 5551254.394 470477.294 5551253.671 470479.441
                        5551251.910 470489.016 5551235.871 470494.879 5551229.456 470505.102
                        5551220.412 470580.458 5551102.825 470593.525 5551085.552 470594.476
                        5551084.087 470597.387 5551081.098 470679.058 5551032.003 470728.344
                        5551015.906 470828.861 5550990.517 470869.356 5550978.588 470910.073
                        5550970.866 471006.338 5550943.804 471009.472 5550941.990 471012.832
                        5550932.699</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <nennweite>50</nennweite>
            <istReserveRohr>true</istReserveRohr>
            <master>true</master>
            <werkstoff>10001</werkstoff>
            <rohrtyp>05040</rohrtyp>
        </BRA_Schutzrohr>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Schutzrohr gml:id="_bra_schutzrohr.2">
            <title>Schutzrohr 2/4 (mit Mikrorohr)</title>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>470632.648 5552406.105 470630.227 5552407.668 470584.783
                        5552415.425 470521.490 5552425.985 470397.878 5552445.947 470382.300
                        5552332.867 470316.759 5552034.651 470260.207 5552030.973 470301.872
                        5551969.079 470286.208 5551894.184 470236.414 5551769.556 470231.797
                        5551664.965 470244.448 5551571.719 470293.723 5551589.246 470301.689
                        5551588.271 470329.585 5551489.936 470334.174 5551487.035 470392.533
                        5551495.239 470397.932 5551493.456 470492.706 5551504.716 470499.628
                        5551504.498 470502.281 5551509.853 470502.449 5551506.307 470434.497
                        5551324.686 470451.102 5551306.779 470465.308 5551289.124 470469.432
                        5551289.824 470475.341 5551254.394 470477.294 5551253.671 470479.441
                        5551251.910 470489.016 5551235.871 470494.879 5551229.456 470505.102
                        5551220.412 470580.458 5551102.825 470593.525 5551085.552 470594.476
                        5551084.087 470597.387 5551081.098 470679.058 5551032.003 470728.344
                        5551015.906 470828.861 5550990.517 470869.356 5550978.588 470910.073
                        5550970.866 471006.338 5550943.804 471009.472 5550941.990 471012.832
                        5550932.699</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <nennweite>50</nennweite>
            <master>true</master>
            <werkstoff>10001</werkstoff>
            <rohrtyp>05040</rohrtyp>
        </BRA_Schutzrohr>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Schutzrohr gml:id="_bra_schutzrohr.3">
            <title>Schutzrohr 3/4 (unbelegt)</title>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>470632.648 5552406.105 470630.227 5552407.668 470584.783
                        5552415.425 470521.490 5552425.985 470397.878 5552445.947 470382.300
                        5552332.867 470316.759 5552034.651 470260.207 5552030.973 470301.872
                        5551969.079 470286.208 5551894.184 470236.414 5551769.556 470231.797
                        5551664.965 470244.448 5551571.719 470293.723 5551589.246 470301.689
                        5551588.271 470329.585 5551489.936 470334.174 5551487.035 470392.533
                        5551495.239 470397.932 5551493.456 470492.706 5551504.716 470499.628
                        5551504.498 470502.281 5551509.853 470502.449 5551506.307 470434.497
                        5551324.686 470451.102 5551306.779 470465.308 5551289.124 470469.432
                        5551289.824 470475.341 5551254.394 470477.294 5551253.671 470479.441
                        5551251.910 470489.016 5551235.871 470494.879 5551229.456 470505.102
                        5551220.412 470580.458 5551102.825 470593.525 5551085.552 470594.476
                        5551084.087 470597.387 5551081.098 470679.058 5551032.003 470728.344
                        5551015.906 470828.861 5550990.517 470869.356 5550978.588 470910.073
                        5550970.866 471006.338 5550943.804 471009.472 5550941.990 471012.832
                        5550932.699</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <nennweite>50</nennweite>
            <istReserveRohr>true</istReserveRohr>
            <master>true</master>
            <werkstoff>10001</werkstoff>
            <rohrtyp>05040</rohrtyp>
        </BRA_Schutzrohr>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Schutzrohr gml:id="_bra_schutzrohr.4">
            <title>Schutzrohr 4/4 (mit Mikrorohrverbund)</title>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>470632.648 5552406.105 470630.227 5552407.668 470588.155
                        5552414.850 470584.783 5552415.425 470521.490 5552425.985 470397.878
                        5552445.947 470382.300 5552332.867 470316.759 5552034.651 470260.207
                        5552030.973 470301.872 5551969.079 470286.208 5551894.184 470236.414
                        5551769.556 470231.797 5551664.965 470244.448 5551571.719 470293.723
                        5551589.246 470301.689 5551588.271 470329.585 5551489.936 470334.174
                        5551487.035 470392.533 5551495.239 470397.932 5551493.456 470492.706
                        5551504.716 470499.628 5551504.498 470502.281 5551509.853 470502.449
                        5551506.307 470434.497 5551324.686 470451.102 5551306.779 470465.308
                        5551289.124 470469.432 5551289.824 470475.341 5551254.394 470477.294
                        5551253.671 470479.441 5551251.910 470489.016 5551235.871 470494.879
                        5551229.456 470505.102 5551220.412 470580.458 5551102.825 470593.525
                        5551085.552 470594.476 5551084.087 470597.387 5551081.098 470679.058
                        5551032.003 470728.344 5551015.906 470828.861 5550990.517 470869.356
                        5550978.588 470910.073 5550970.866 471006.338 5550943.804 471009.472
                        5550941.990 471012.832 5550932.699</gml:posList>
                </gml:LineString>
            </position>
            <leitungstyp>1000</leitungstyp>
            <nennweite>50</nennweite>
            <master>true</master>
            <werkstoff>10001</werkstoff>
            <rohrtyp>05040</rohrtyp>
        </BRA_Schutzrohr>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Umfeld gml:id="_bra_umfeld.1">
            <title>beispiel</title>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiSurface srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:surfaceMember>
                        <gml:Polygon>
                            <gml:exterior>
                                <gml:LinearRing>
                                    <gml:posList>470569.427 5552470.312 470566.125 5552446.991
                                        470565.761 5552444.519 470565.771 5552442.291 470566.370
                                        5552439.378 470566.566 5552438.496 470566.906 5552438.185
                                        470567.170 5552438.098 470567.481 5552438.060 470567.721
                                        5552438.108 470567.937 5552438.213 470568.123 5552438.367
                                        470568.282 5552438.539 470568.588 5552439.229 470569.278
                                        5552440.532 470569.930 5552442.276 470570.428 5552443.532
                                        470570.869 5552445.314 470571.271 5552447.020 470571.578
                                        5552448.687 470571.674 5552450.182 470571.712 5552451.887
                                        470571.592 5552470.197 470571.496 5552470.638 470571.161
                                        5552471.088 470570.778 5552471.251 470570.423 5552471.280
                                        470570.040 5552471.203 470569.733 5552470.983 470569.551
                                        5552470.724 470569.427 5552470.312</gml:posList>
                                </gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </gml:surfaceMember>
                </gml:MultiSurface>
            </position>
            <art>2000</art>
        </BRA_Umfeld>
    </sf:featureMember>
    <sf:featureMember>
        <BRA_Verteiler gml:id="_bra_verteiler.1">
            <title>Verteiler Colt</title>
            <beschreibung>In diesem Verteiler erfolgt der Anschluss an das Colt-TK-Netz.</beschreibung>
            <netzbetreiber>Colt</netzbetreiber>
            <gehoertZuBRA xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>470632.648 5552406.105</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
            <art>2000</art>
            <technik>4000</technik>
            <werkstoff>1000</werkstoff>
        </BRA_Verteiler>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Strassenbeleuchtung gml:id="_bst_strassenbeleuchtung.1">
            <title>Westerbachstraße</title>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470629.937 5552405.136 470629.937 5552406.885</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470599.614 5552410.277 470599.569 5552411.966</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470570.262 5552415.239 470570.218 5552416.674</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470539.394 5552420.488 470539.461 5552421.755</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470510.494 5552425.387 470510.677 5552426.687</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470530.061 5552445.913 470530.464 5552446.758</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470568.095 5552442.626 470567.557 5552439.599</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </BST_Strassenbeleuchtung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Stromleitung gml:id="_bst_stromleitung.1">
            <title>Steuerleitung Gas</title>
            <beschreibung>vermutliche Lage</beschreibung>
            <netzbetreiber>NRM Netzdienste Rhein-Main GmbH</netzbetreiber>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470489.115 5551307.897 470475.675 5551296.111 470476.873
                                5551288.975 470497.947 5551249.628 470489.400 5551227.762 470569.352
                                5551106.006 470636.595 5551004.939</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </BST_Stromleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Stromleitung gml:id="_bst_stromleitung.2">
            <title>Strom südl. Niidda</title>
            <beschreibung>vermutliche Lage</beschreibung>
            <netzbetreiber>NRM Netzdienste Rhein-Main GmbH</netzbetreiber>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470569.904 5551271.627 470507.919 5551260.505 470508.309
                                5551258.423 470506.827 5551257.840 470502.845 5551256.602 470495.105
                                5551243.204</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470490.390 5551257.578 470506.325 5551260.245 470507.919
                                5551260.505</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470506.827 5551257.840 470506.325 5551260.245</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470640.957 5551003.346 470488.601 5551231.821 470495.105
                                5551243.204</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </BST_Stromleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Stromleitung gml:id="_bst_stromleitung.3">
            <title>Freileitung</title>
            <netzbetreiber>Syna GmbH</netzbetreiber>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470253.669 5551534.907 470329.845 5551690.589 470329.820
                                5551693.446 470253.482 5551538.658</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <leitungstyp>2000</leitungstyp>
        </BST_Stromleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Stromleitung gml:id="_bst_stromleitung.4">
            <title>Strom Oeserstraße</title>
            <netzbetreiber>NRM Netzdienste Rhein-Main GmbH</netzbetreiber>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470905.055 5550904.323 470833.856 5551087.222</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>471027.080 5550949.282 470968.450 5550924.746</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </BST_Stromleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Stromleitung gml:id="_bst_stromleitung.5">
            <title>Strom Westerbachstraße</title>
            <netzbetreiber>NRM Netzdienste Rhein-Main GmbH und Syna GmbH</netzbetreiber>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470218.464 5551561.869 470245.853 5551571.960 470235.925
                                5551644.643 470233.123 5551664.540 470238.077 5551771.251 470245.792
                                5551787.615 470287.453 5551893.312 470293.463 5551921.749 470302.491
                                5551965.076 470294.883 5551976.475</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470294.883 5551976.475 470272.474 5552010.671 470309.499
                                5552035.382 470315.240 5552035.826 470318.346 5552052.577 470345.400
                                5552184.162 470382.134 5552338.169 470395.998 5552444.980</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470337.695 5551929.539 470304.229 5551976.447 470294.883
                                5551976.475</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470362.051 5552450.659 470395.998 5552444.980</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470387.168 5552465.261 470390.288 5552464.179</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470390.725 5552466.426 470390.288 5552464.179</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470390.288 5552464.179 470391.619 5552464.034</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470391.869 5552466.301 470391.619 5552464.034</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470391.619 5552464.034 470437.464 5552456.899</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470395.998 5552444.980 470435.873 5552438.116</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470408.177 5552183.288 470425.857 5552326.573 470435.873
                                5552438.116</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470435.873 5552438.116 470437.464 5552456.899</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470435.873 5552438.116 470445.108 5552436.868</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470439.419 5552479.405 470437.464 5552456.899</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470437.464 5552456.899 470447.709 5552455.766</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470447.709 5552455.766 470445.108 5552436.868</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470496.404 5552427.782 470489.243 5552428.996 470445.108
                                5552436.868</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470455.311 5552458.688 470448.291 5552459.988 470447.709
                                5552455.766</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470447.709 5552455.766 470454.770 5552454.985 470455.311
                                5552458.688</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470468.041 5552458.646 470465.981 5552457.055 470455.311
                                5552458.688</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470489.205 5552418.743 470495.114 5552417.938 470496.404
                                5552427.782</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470494.372 5552408.262 470496.967 5552427.686</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470496.404 5552427.782 470496.406 5552427.793</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470496.967 5552427.686 470496.404 5552427.782</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470496.967 5552427.686 470496.969 5552427.697</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470513.372 5552424.903 470496.967 5552427.686</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470497.357 5552449.449 470500.657 5552450.728 470507.321
                                5552450.370 470508.173 5552450.104</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470508.101 5552453.919 470508.552 5552453.213</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470508.173 5552450.104 470508.461 5552452.463</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470509.907 5552448.313 470507.948 5552448.259 470508.173
                                5552450.104</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470508.173 5552450.104 470508.589 5552449.974</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470508.461 5552452.463 470508.509 5552452.858</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470508.461 5552452.463 470508.267 5552452.813 470508.509
                                5552452.858</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470508.842 5552451.774 470508.461 5552452.463</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470508.509 5552452.858 470508.750 5552452.903</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470508.509 5552452.858 470508.552 5552453.213</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470508.552 5552453.213 470509.124 5552457.903</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470508.552 5552453.213 470508.750 5552452.903</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470508.589 5552449.974 470508.842 5552451.774</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470510.022 5552448.882 470508.434 5552448.867 470508.589
                                5552449.974</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470508.589 5552449.974 470510.097 5552449.503</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470508.750 5552452.903 470509.007 5552452.951</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470508.750 5552452.903 470508.955 5552452.582</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470508.842 5552451.774 470508.955 5552452.582</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470510.097 5552449.503 470508.842 5552451.774</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470508.955 5552452.582 470509.007 5552452.951</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470508.955 5552452.582 470510.634 5552449.954 470512.117
                                5552448.931 470512.205 5552448.902</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470509.007 5552452.951 470511.414 5552453.401</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470509.007 5552452.951 470509.687 5552457.794</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470557.059 5552444.073 470541.497 5552446.788 470541.753
                                5552446.251 470532.109 5552444.000 470526.686 5552445.509 470518.525
                                5552446.788 470513.550 5552448.413 470509.907 5552448.313</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470509.907 5552448.313 470510.022 5552448.882</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470547.847 5552419.368 470511.874 5552425.345 470511.823
                                5552425.805 470509.418 5552445.906 470509.907 5552448.313</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470512.028 5552448.901 470510.022 5552448.882</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470510.022 5552448.882 470510.135 5552449.436 470510.097
                                5552449.503</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470510.097 5552449.503 470512.028 5552448.901</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470512.205 5552448.902 470512.028 5552448.901</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470512.028 5552448.901 470517.323 5552447.249</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470522.813 5552446.680 470518.385 5552447.306 470513.831
                                5552448.918 470512.205 5552448.902</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470512.205 5552448.902 470517.323 5552447.249</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470513.065 5552419.661 470513.372 5552424.903</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470513.372 5552424.903 470513.372 5552424.908</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470520.392 5552423.712 470513.372 5552424.903</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470517.323 5552447.249 470526.622 5552446.142</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470517.323 5552447.249 470522.813 5552446.680</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470519.704 5552418.843 470520.392 5552423.712</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470520.392 5552423.712 470520.394 5552423.729</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470561.092 5552416.807 470520.392 5552423.712</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470525.215 5552446.341 470522.813 5552446.680</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470522.813 5552446.680 470523.378 5552446.622</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470523.378 5552446.622 470522.797 5552447.354 470522.848
                                5552448.403</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470523.386 5552446.612 470523.378 5552446.622</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470523.378 5552446.622 470525.225 5552446.430</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470525.215 5552446.341 470525.225 5552446.430</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470526.714 5552446.228 470525.215 5552446.341</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470525.225 5552446.430 470525.432 5552448.147</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470525.225 5552446.430 470526.760 5552446.271</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470526.622 5552446.142 470526.714 5552446.228</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470526.622 5552446.142 470532.045 5552444.735 470540.461
                                5552446.552 470540.281 5552447.252</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470526.714 5552446.228 470526.760 5552446.271</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470528.403 5552446.101 470526.714 5552446.228</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470526.760 5552446.271 470528.502 5552447.891</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470526.760 5552446.271 470528.403 5552446.101</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470528.403 5552446.101 470532.390 5552445.688 470536.662
                                5552446.277 470538.075 5552446.657</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470538.119 5552447.001 470532.710 5552445.778 470528.403
                                5552446.101</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470535.660 5552416.217 470535.519 5552421.142</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470538.075 5552446.657 470538.511 5552446.775</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470538.075 5552446.657 470538.119 5552447.001</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470538.511 5552446.775 470538.069 5552446.612 470538.075
                                5552446.657</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470540.223 5552447.477 470538.119 5552447.001</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470538.119 5552447.001 470538.427 5552449.401</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470538.511 5552446.775 470540.281 5552447.252</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470540.240 5552447.411 470538.511 5552446.775</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470540.730 5552447.591 470540.223 5552447.477</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470540.223 5552447.477 470540.167 5552447.696 470545.871
                                5552449.461 470549.146 5552451.322 470553.290 5552455.211 470559.058
                                5552464.919 470560.030 5552471.832 470563.533 5552496.085</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470540.240 5552447.411 470540.223 5552447.477</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470540.730 5552447.591 470540.240 5552447.411</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470540.281 5552447.252 470540.240 5552447.411</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470540.281 5552447.252 470541.215 5552447.504</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470541.215 5552447.504 470540.730 5552447.591</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470541.234 5552447.509 470540.730 5552447.591</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470541.215 5552447.504 470541.234 5552447.509</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470557.059 5552444.073 470557.140 5552444.643 470541.215
                                5552447.504</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470541.234 5552447.509 470545.488 5552448.656 470549.568
                                5552450.996 470558.342 5552459.464 470561.054 5552467.349 470562.154
                                5552475.215 470561.361 5552477.568 470563.995 5552495.629 470563.533
                                5552496.085</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470592.250 5552439.677 470590.868 5552441.327 470587.747
                                5552442.606 470566.131 5552443.168 470559.071 5552444.601 470541.234
                                5552447.509</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470547.835 5552419.292 470547.847 5552419.368</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470547.847 5552419.368 470550.252 5552433.752 470557.005
                                5552443.690 470557.059 5552444.073</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470559.832 5552417.377 470547.847 5552419.368</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470626.873 5552427.084 470626.387 5552425.089 470612.356
                                5552428.945 470606.063 5552430.838 470601.932 5552432.879 470590.472
                                5552440.844 470587.619 5552442.235 470565.837 5552442.542 470557.059
                                5552444.073</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470559.832 5552417.377 470559.789 5552417.033</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470563.497 5552416.768 470563.562 5552417.186 470559.878
                                5552417.749 470559.832 5552417.377</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470563.497 5552416.768 470559.832 5552417.377</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470560.799 5552414.807 470561.092 5552416.807</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470561.092 5552416.807 470561.094 5552416.815</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470563.442 5552416.408 470561.092 5552416.807</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470563.063 5552413.963 470563.442 5552416.408</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470563.533 5552496.085 470562.051 5552497.547 470563.177
                                5552506.859</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470563.442 5552416.408 470563.497 5552416.768</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470599.425 5552410.304 470563.442 5552416.408</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470649.014 5552402.559 470563.497 5552416.768</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470563.533 5552496.085 470563.791 5552497.873 470564.481
                                5552506.750</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470571.339 5552407.686 470572.209 5552414.913</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470578.310 5552408.057 470578.565 5552413.800</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470598.544 5552400.517 470599.425 5552410.304</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470599.286 5552400.402 470600.411 5552410.136</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470599.425 5552410.304 470599.427 5552410.321</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470600.411 5552410.136 470599.425 5552410.304</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470600.411 5552410.136 470600.412 5552410.142</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470606.702 5552409.069 470600.411 5552410.136</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470604.810 5552399.664 470606.702 5552409.069</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470606.702 5552409.069 470606.703 5552409.071</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470649.035 5552401.887 470606.702 5552409.069</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470626.873 5552427.084 470626.132 5552426.215 470625.901
                                5552425.626 470621.220 5552426.880 470621.322 5552427.443 470625.927
                                5552455.633 470635.852 5552465.661</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470639.638 5552422.761 470626.976 5552425.422 470627.052
                                5552425.984 470626.873 5552427.084</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </BST_Stromleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Abwasserleitung gml:id="_bst_abwasserleitung.1">
            <title>Abwasser 2.2</title>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470649.065 5552407.051 470578.513 5552418.662</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <nennweite>DN 1300 B</nennweite>
            <art>3000</art>
            <netzEbene>2000</netzEbene>
        </BST_Abwasserleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Abwasserleitung gml:id="_bst_abwasserleitung.2">
            <title>Abwasser 1.2</title>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470572.910 5552420.811 470617.095 5552413.690 470649.039
                                5552408.343</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <nennweite>DN 400 STZ</nennweite>
            <art>3000</art>
            <netzEbene>2000</netzEbene>
        </BST_Abwasserleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Abwasserleitung gml:id="_bst_abwasserleitung.3">
            <title>Abwasser 1.1</title>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470489.313 5552434.903 470572.910 5552420.811</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <nennweite>DN 300 STZ</nennweite>
            <art>3000</art>
            <netzEbene>2000</netzEbene>
        </BST_Abwasserleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Abwasserleitung gml:id="_bst_abwasserleitung.4">
            <title>Abwasser 2.1</title>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470489.094 5552433.953 470578.513 5552418.662</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <nennweite>EI 700/1050 B</nennweite>
            <art>3000</art>
            <netzEbene>2000</netzEbene>
        </BST_Abwasserleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Baum gml:id="_bst_baum.1">
            <title>fiktive Beispiele</title>
            <beschreibung>Während der Baumaßnahme geschützter Baumbestand</beschreibung>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>470641.468 5552401.948</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470632.253 5552403.374</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470622.709 5552405.129</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
            <stammumfang uom="m">1.45</stammumfang>
        </BST_Baum>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Gasleitung gml:id="_bst_gasleitung.6">
            <title>BestandGas Westerbachstraße</title>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470554.136 5552440.159 470554.136 5552440.159 470554.136
                                5552440.159 470554.310 5552442.589 470554.874 5552446.494 470555.438
                                5552449.879 470556.067 5552452.298 470558.085 5552458.384 470560.689
                                5552466.585 470561.643 5552470.295 470562.142 5552473.246 470562.793
                                5552476.999 470564.464 5552491.817</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470649.176 5552419.743 470649.176 5552419.743 470640.367
                                5552421.132 470610.427 5552425.926 470605.958 5552427.315 470589.534
                                5552431.589 470568.705 5552437.317 470540.956 5552442.914 470524.380
                                5552444.737 470511.363 5552446.212 470489.276 5552448.859</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470636.137 5552399.566 470640.367 5552421.132</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470602.833 5552409.242 470605.958 5552427.315</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470585.737 5552407.984 470589.534 5552431.589</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470579.879 5552407.778 470580.508 5552413.592 470565.451
                                5552416.521 470567.707 5552427.543 470568.705 5552437.317</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470538.613 5552415.697 470539.481 5552421.023 470537.485
                                5552421.457 470540.956 5552442.914</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470520.301 5552418.680 470524.380 5552444.737</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470510.017 5552420.068 470510.929 5552427.217 470508.282
                                5552427.521 470511.363 5552446.212</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <leitungstyp>1000</leitungstyp>
            <gasArt>1000</gasArt>
            <druckstufe>9999</druckstufe>
            <netzEbene>3000</netzEbene>
        </BST_Gasleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Gasleitung gml:id="_bst_gasleitung.7">
            <title>BestandGas Nidda 2</title>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470637.570 5551003.989 470569.830 5551106.035 470484.564
                                5551235.922 470484.191 5551243.255 470474.993 5551296.329 470488.666
                                5551308.510 470512.654 5551340.453</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <leitungstyp>1000</leitungstyp>
            <nennweite>DN 400</nennweite>
            <gasArt>1000</gasArt>
            <druckstufe>9999</druckstufe>
            <netzEbene>1000</netzEbene>
        </BST_Gasleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Gasleitung gml:id="_bst_gasleitung.8">
            <title>BestandGas Nidda 1</title>
            <netzbetreiber>Open Grid Europe</netzbetreiber>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470642.090 5551004.533 470570.083 5551111.136 470492.824
                                5551229.975 470491.146 5551243.672 470483.688 5551293.847 470484.730
                                5551296.378 470519.854 5551340.016</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <leitungstyp>1000</leitungstyp>
            <nennweite>DN 300</nennweite>
            <gasArt>1000</gasArt>
            <druckstufe>3000</druckstufe>
            <netzEbene>1000</netzEbene>
        </BST_Gasleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Gasleitung gml:id="_bst_gasleitung.9">
            <title>BestandGas Oeserstraße</title>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>471025.073 5550936.454 470945.000 5550904.907</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470974.780 5550918.475 471024.039 5550938.135</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <leitungstyp>1000</leitungstyp>
            <nennweite>DN 300</nennweite>
            <gasArt>1000</gasArt>
            <druckstufe>9999</druckstufe>
            <netzEbene>1000</netzEbene>
        </BST_Gasleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Mast gml:id="_bst_mast.1">
            <title>Leuchten Westerbachstraße</title>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>470629.937 5552406.885</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470599.569 5552411.966</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470570.218 5552416.674</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470539.461 5552421.755</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470510.677 5552426.687</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470530.464 5552446.758</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470567.557 5552439.599</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
            <netzSparte>3000</netzSparte>
            <art>5000</art>
        </BST_Mast>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Telekommunikationsleitung gml:id="_bst_telekommunikationsleitung.1">
            <title>Leitung ITK Wilhelm-Fay-Straße</title>
            <netzbetreiber>Amt für Informations- und Kommunikationstechnik (ITK)</netzbetreiber>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470610.476 5552428.626 470605.788 5552430.145 470597.647
                                5552434.135 470595.383 5552435.676 470591.348 5552438.062 470590.337
                                5552438.535 470589.698 5552438.727 470589.071 5552438.868 470588.240
                                5552439.021 470587.063 5552439.124 470586.225 5552439.124 470584.882
                                5552439.034 470583.693 5552438.868 470583.066 5552438.740 470581.845
                                5552438.394 470581.154 5552438.305 470580.335 5552438.330 470562.275
                                5552441.605 470537.884 5552445.474 470531.859 5552444.630 470526.244
                                5552444.924 470517.112 5552446.970 470489.229 5552450.027</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470595.430 5552440.285 470596.440 5552438.148 470596.541
                                5552437.828 470596.557 5552437.558 470596.406 5552437.171 470595.383
                                5552435.676</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470545.256 5552449.441 470543.691 5552448.196 470540.526
                                5552446.210 470537.884 5552445.474</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470537.480 5552447.304 470537.884 5552445.474</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </BST_Telekommunikationsleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Telekommunikationsleitung gml:id="_bst_telekommunikationsleitung.2">
            <title>Leitung Schütenhausenweg</title>
            <netzbetreiber>Telekom/Unitymedia </netzbetreiber>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470947.098 5550906.814 471014.227 5550933.850 470999.621
                                5550945.344 470936.032 5550962.888 470930.880 5550968.562 470909.129
                                5550974.106 470878.150 5550978.280 470868.921 5550980.693 470832.496
                                5550990.541 470769.103 5551005.085 470750.711 5551008.150 470702.644
                                5551022.564 470671.404 5551033.064 470647.338 5551051.032 470607.521
                                5551075.392 470593.728 5551086.511 470581.010 5551104.594 470562.218
                                5551132.181 470553.805 5551134.456 470494.162 5551226.937 470486.792
                                5551237.177 470481.053 5551244.922 470475.639 5551247.873 470471.433
                                5551248.835 470465.987 5551248.639</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <leitungstyp>10005</leitungstyp>
            <nennweite></nennweite>
        </BST_Telekommunikationsleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Telekommunikationsleitung gml:id="_bst_telekommunikationsleitung.3">
            <title>Leitung Holler</title>
            <netzbetreiber>Telekom </netzbetreiber>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470414.496 5551355.799 470426.823 5551340.896 470450.090
                                5551314.515 470460.248 5551297.101 470461.113 5551289.316</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <leitungstyp>10001</leitungstyp>
        </BST_Telekommunikationsleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Telekommunikationsleitung gml:id="_bst_telekommunikationsleitung.4">
            <title>Leitung am Schwemmteich</title>
            <netzbetreiber>Telekom/Unitymedia </netzbetreiber>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470342.899 5551443.069 470331.812 5551479.608 470334.288
                                5551485.502 470397.351 5551492.931 470403.914 5551472.118 470413.567
                                5551474.498</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <nennweite>DN 100</nennweite>
        </BST_Telekommunikationsleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Telekommunikationsleitung gml:id="_bst_telekommunikationsleitung.5">
            <title>Leitung Oeserstraße Nord 1</title>
            <netzbetreiber>Telekom/Unitymedia </netzbetreiber>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>471025.873 5550948.294 470963.063 5550921.820</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <leitungstyp>10005</leitungstyp>
            <nennweite></nennweite>
            <leitungszoneBreite uom="m">0.4</leitungszoneBreite>
            <leitungszoneTiefe uom="m">0.5</leitungszoneTiefe>
        </BST_Telekommunikationsleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Telekommunikationsleitung gml:id="_bst_telekommunikationsleitung.6">
            <title>Leitung Oeserstraße Nord 2</title>
            <netzbetreiber>Telekom/Unitymedia </netzbetreiber>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>471030.455 5550948.794 471006.275 5550938.278 471006.232
                                5550937.502 470997.569 5550933.666 470995.500 5550934.226 470962.871
                                5550920.675</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <leitungstyp>10005</leitungstyp>
            <leitungszoneBreite uom="m">0.4</leitungszoneBreite>
            <leitungszoneTiefe uom="m">0.2</leitungszoneTiefe>
        </BST_Telekommunikationsleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Telekommunikationsleitung gml:id="_bst_telekommunikationsleitung.7">
            <title>Leitung Oeserstraße Süd</title>
            <netzbetreiber>Telekom/Initymedia</netzbetreiber>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>471034.872 5550941.557 471012.832 5550932.699 470969.605
                                5550915.325 470967.878 5550916.369 470938.914 5550904.214</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <nennweite>DN 100</nennweite>
        </BST_Telekommunikationsleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Telekommunikationsleitung gml:id="_bst_telekommunikationsleitung.8">
            <title>Leitung Nidda Nord</title>
            <netzbetreiber>Telekom/Unitymedia</netzbetreiber>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470570.961 5551310.225 470448.253 5551286.865</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <leitungstyp>10005</leitungstyp>
            <leitungszoneBreite uom="m">0.5</leitungszoneBreite>
        </BST_Telekommunikationsleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Telekommunikationsleitung gml:id="_bst_telekommunikationsleitung.9">
            <title>Leitung südlich A648</title>
            <netzbetreiber>Telekom </netzbetreiber>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470196.532 5551763.642 470232.047 5551766.832 470234.412
                                5551764.183</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470234.412 5551764.183 470242.804 5551785.096 470285.638
                                5551894.209 470291.204 5551919.050 470300.559 5551967.725 470299.168
                                5551970.972</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470340.339 5551442.311 470318.540 5551517.419 470299.724
                                5551582.052 470296.805 5551586.111 470290.153 5551587.693 470243.668
                                5551570.769 470241.804 5551576.894 470233.470 5551643.490 470230.847
                                5551662.527 470234.412 5551764.183</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470241.062 5552065.684 470261.179 5552037.101 470262.094
                                5552028.133</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470262.094 5552028.133 470263.920 5552023.742 470299.168
                                5551970.972</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470348.151 5551908.382 470336.731 5551922.687 470302.350
                                5551969.076 470299.168 5551970.972</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <leitungstyp>10005</leitungstyp>
            <leitungszoneBreite uom="m">0.3</leitungszoneBreite>
        </BST_Telekommunikationsleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Telekommunikationsleitung gml:id="_bst_telekommunikationsleitung.10">
            <title>Leitung Westerbachstraße Süd</title>
            <netzbetreiber>Telekom/Unitymedia </netzbetreiber>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470536.611 5552422.714 470507.679 5552427.638 470467.746
                                5552434.182 470389.940 5552446.549 470366.278 5552450.884 470348.504
                                5552453.870 470337.057 5552455.592 470348.504 5552453.870 470366.278
                                5552450.884 470389.940 5552446.549 470411.937 5552443.042 470419.451
                                5552441.826</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470347.358 5552447.351 470348.504 5552453.870</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470365.275 5552444.280 470366.278 5552450.884</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470389.044 5552440.038 470389.940 5552446.549</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470408.717 5552421.974 470411.937 5552443.042</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470416.103 5552420.877 470419.451 5552441.826</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470464.226 5552421.096 470465.542 5552420.804 470467.746
                                5552434.182</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470486.897 5552418.171 470488.649 5552430.733</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470506.643 5552420.072 470507.679 5552427.638</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470517.613 5552418.829 470518.414 5552425.805</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470535.494 5552415.977 470536.611 5552422.714</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470551.218 5552410.858 470552.023 5552415.831 470553.632
                                5552415.611</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470552.610 5552410.624 470551.218 5552410.858</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470576.342 5552407.041 470577.579 5552416.050</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470585.593 5552405.688 470586.654 5552405.578 470588.026
                                5552414.060</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470634.824 5552406.253 470536.611 5552422.714</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470650.793 5552403.593 470634.824 5552406.253</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470541.641 5552415.002 470542.811 5552421.694</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470554.293 5552419.719 470553.632 5552415.611</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470567.164 5552408.347 470568.627 5552417.342</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470596.345 5552401.134 470597.515 5552412.470</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470601.610 5552400.512 470602.780 5552411.592</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470607.644 5552399.854 470608.814 5552410.568</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </BST_Telekommunikationsleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Telekommunikationsleitung gml:id="_bst_telekommunikationsleitung.11">
            <title>Interroute entlang A648</title>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470354.379 5551909.599 470246.433 5552067.860</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </BST_Telekommunikationsleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Telekommunikationsleitung gml:id="_bst_telekommunikationsleitung.12">
            <title>Leitungen Westerbachstraße Nord</title>
            <netzbetreiber>Telekom/Unitymedia </netzbetreiber>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470533.727 5552442.909 470530.209 5552443.396 470524.582
                                5552444.342 470523.942 5552444.393 470520.617 5552443.626 470489.280
                                5552448.000 470435.435 5552454.081 470388.869 5552462.078 470380.651
                                5552466.236 470369.474 5552464.989 470339.440 5552469.880</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470583.776 5552446.414 470574.695 5552444.527 470554.345
                                5552447.744 470538.830 5552443.319 470533.727 5552442.909</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470533.727 5552442.909 470539.649 5552445.768 470546.313
                                5552449.426 470556.379 5552457.523 470560.037 5552465.574 470560.804
                                5552467.691 470563.772 5552477.278 470566.534 5552492.204</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470583.776 5552446.414 470581.806 5552449.727 470580.450
                                5552455.150 470579.785 5552459.384 470579.632 5552466.802 470580.425
                                5552490.458</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470582.676 5552506.836 470581.346 5552493.982 470580.143
                                5552481.780 470579.811 5552471.937 470580.425 5552465.939 470581.090
                                5552462.389 470582.471 5552457.222 470583.699 5552453.756 470583.929
                                5552453.040 470584.032 5552452.554 470585.849 5552448.420</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470585.849 5552448.420 470583.776 5552446.414</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470585.677 5552443.188 470583.776 5552446.414</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470585.677 5552443.188 470586.504 5552443.284 470588.382
                                5552444.556</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470585.677 5552443.187 470585.673 5552443.188 470585.677
                                5552443.188</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470585.677 5552443.187 470585.677 5552443.188</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470618.387 5552424.632 470608.231 5552427.433 470604.087
                                5552428.731 470595.876 5552432.971 470591.962 5552435.785 470589.890
                                5552437.314 470587.997 5552439.251 470585.677 5552443.187</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470585.677 5552443.187 470589.766 5552442.773</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470603.812 5552465.798 470585.849 5552448.420</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470585.849 5552448.420 470585.874 5552448.365 470588.022
                                5552445.020 470588.382 5552444.556</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470588.382 5552444.556 470588.891 5552444.901</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470588.382 5552444.556 470589.766 5552442.773</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470589.766 5552442.773 470591.655 5552440.339 470595.134
                                5552436.482 470597.973 5552434.129 470601.683 5552431.801 470606.223
                                5552430.074 470620.446 5552426.109 470634.541 5552423.737 470634.874
                                5552423.609 470635.181 5552423.302 470635.411 5552422.867 470635.462
                                5552422.330 470632.648 5552406.105 470633.672 5552405.542 470635.692
                                5552405.133 470636.332 5552405.593 470649.097 5552403.496</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470589.766 5552442.773 470589.880 5552442.762 470591.776
                                5552444.423</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
        </BST_Telekommunikationsleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Verteiler gml:id="_bst_verteiler.1">
            <title>Verteiler Wilhelm-Fay-Straße</title>
            <netzbetreiber>ITK</netzbetreiber>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>470595.383 5552435.676</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470537.480 5552447.304</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
            <netzSparte>1000</netzSparte>
        </BST_Verteiler>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Verteiler gml:id="_bst_verteiler.2">
            <title>Verteiler Wilhelm-Fay und Westerbachstraße</title>
            <netzbetreiber>Telekom</netzbetreiber>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>470588.891 5552444.901</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470634.824 5552406.253</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470591.776 5552444.423</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
            <netzSparte>1000</netzSparte>
        </BST_Verteiler>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Verteiler gml:id="_bst_verteiler.3">
            <title>Verteiler um Westerbachstraße</title>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>470522.848 5552448.403</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470525.432 5552448.147</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470528.502 5552447.891</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470538.427 5552449.401</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470626.873 5552427.084</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470563.063 5552413.963</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
            <netzSparte>3000</netzSparte>
        </BST_Verteiler>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Schacht gml:id="_bst_schacht.1">
            <title>Abwasserschächte</title>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>470505.453 5552431.325</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470525.240 5552428.843</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470572.910 5552420.811</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470575.699 5552419.199</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470578.513 5552418.662</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470616.001 5552413.802</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470641.263 5552408.366</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
            <netzSparte>5000</netzSparte>
        </BST_Schacht>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Schacht gml:id="_bst_schacht.2">
            <title>TK-Schächte Wilhelm-Fay-Straße</title>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiPoint srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos>470583.776 5552446.414</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                    <gml:pointMember>
                        <gml:Point>
                            <gml:pos> 470583.929 5552453.040</gml:pos>
                        </gml:Point>
                    </gml:pointMember>
                </gml:MultiPoint>
            </position>
            <netzSparte>1000</netzSparte>
        </BST_Schacht>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Wasserleitung gml:id="_bst_wasserleitung.1">
            <title>Wasser Oeserstraße</title>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>471023.843 5550996.365 471015.390 5550943.408</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>471020.169 5550932.571 471015.390 5550943.408</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470947.327 5550904.414 471031.943 5550937.251</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <nennweite>&lt; DN 300</nennweite>
        </BST_Wasserleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Wasserleitung gml:id="_bst_wasserleitung.2">
            <title>Wasser Westerbachstraße</title>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:MultiCurve srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470362.627 5552453.449 470368.816 5552452.385</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470372.638 5552475.696 470370.988 5552468.313 470369.837
                                5552462.797 470370.141 5552462.645 470368.816 5552452.385</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470368.816 5552452.385 470372.681 5552451.994</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470371.335 5552444.231 470372.681 5552451.994</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470372.681 5552451.994 470393.137 5552448.997</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470391.682 5552440.941 470391.748 5552445.414 470392.160
                                5552447.130 470392.790 5552446.978 470393.137 5552448.997</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470393.137 5552448.997 470400.911 5552447.955</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470404.603 5552465.371 470403.734 5552457.966 470402.627
                                5552457.857 470400.911 5552447.955</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470400.911 5552447.955 470401.997 5552447.694</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470401.823 5552445.154 470401.997 5552447.694</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470401.997 5552447.694 470405.689 5552447.173</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470405.254 5552443.634 470405.689 5552447.173</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470405.689 5552447.173 470422.757 5552443.959</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470421.541 5552435.382 470422.757 5552443.959</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470422.757 5552443.959 470430.575 5552442.569</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470430.097 5552438.726 470430.575 5552442.569</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470430.575 5552442.569 470460.629 5552437.575</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470459.326 5552419.139 470457.632 5552419.399 470460.629
                                5552437.575</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470497.841 5552430.815 470489.248 5552432.203 470460.629
                                5552437.575</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470497.841 5552430.815 470497.053 5552425.019</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470497.846 5552430.852 470497.841 5552430.815</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470509.162 5552428.987 470497.841 5552430.815</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470509.162 5552428.987 470507.759 5552420.338</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470509.166 5552429.010 470509.162 5552428.987</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470518.785 5552427.599 470511.427 5552428.621 470509.162
                                5552428.987</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470517.774 5552419.168 470518.785 5552427.599</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470518.785 5552427.599 470518.794 5552427.675</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470535.936 5552424.674 470518.794 5552427.598 470518.785
                                5552427.599</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470541.612 5552452.714 470540.730 5552447.591 470540.307
                                5552447.361 470535.936 5552424.674</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470555.081 5552421.588 470554.471 5552421.682 470552.855
                                5552421.766 470535.936 5552424.674</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470538.085 5552415.740 470538.270 5552416.884</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470538.270 5552416.884 470538.239 5552416.891</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470538.270 5552416.884 470539.432 5552424.073</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470539.569 5552416.609 470538.270 5552416.884</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470554.521 5552418.560 470555.081 5552421.588</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470555.081 5552421.588 470555.084 5552421.604</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470579.625 5552417.791 470555.081 5552421.588</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470556.107 5552415.362 470557.053 5552421.246</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470558.739 5552430.145 470560.192 5552429.922</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470559.020 5552421.025 470560.171 5552429.800 470560.192
                                5552429.922</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470560.192 5552429.922 470565.390 5552459.992 470564.622
                                5552470.927 470564.973 5552473.820</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470560.192 5552429.922 470588.105 5552425.643 470612.190
                                5552421.908 470649.436 5552415.346</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470564.973 5552473.820 470564.929 5552473.824</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470564.973 5552473.820 470567.104 5552491.367</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470574.010 5552472.827 470564.973 5552473.820</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470566.058 5552408.251 470567.874 5552419.532</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470578.310 5552408.057 470579.625 5552417.791</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470579.625 5552417.791 470579.625 5552417.797</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470602.854 5552413.949 470600.704 5552414.299 470592.480
                                5552415.693 470581.504 5552417.500 470579.625 5552417.791</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470581.899 5552408.679 470583.164 5552417.222</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470595.690 5552400.898 470597.356 5552414.856</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470601.970 5552400.131 470602.776 5552407.447 470602.085
                                5552407.562 470602.854 5552413.949</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470602.854 5552413.949 470602.856 5552413.967</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470636.282 5552408.210 470624.808 5552410.142 470608.219
                                5552413.074 470602.854 5552413.949</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470606.351 5552399.545 470608.206 5552413.011</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470635.699 5552404.505 470636.282 5552408.210</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470636.137 5552399.566 470637.362 5552406.449 470638.052
                                5552406.398 470638.580 5552407.823</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470636.282 5552408.210 470636.287 5552408.243</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470638.580 5552407.823 470636.282 5552408.210</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470638.580 5552407.823 470638.593 5552407.856</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                    <gml:curveMember>
                        <gml:LineString>
                            <gml:posList>470649.071 5552406.056 470638.580 5552407.823</gml:posList>
                        </gml:LineString>
                    </gml:curveMember>
                </gml:MultiCurve>
            </position>
            <nennweite>&lt; DN 300</nennweite>
        </BST_Wasserleitung>
    </sf:featureMember>
    <sf:featureMember>
        <BST_Wegekante gml:id="_bst_wegekante.1">
            <title>Beispiel</title>
            <gehoertZuPlan xlink:title="Trassenverbindung Oeserstraße mit Westerbachstraße"
                xlink:href="#_bra_ausbauplan.1" />
            <position>
                <gml:LineString srsName="http://www.opengis.net/def/crs/EPSG/0/25832">
                    <gml:posList>470542.414 5552448.857 470543.928 5552444.622 470545.854
                        5552445.158 470545.566 5552447.285 470547.406 5552448.205 470551.584
                        5552450.888 470555.455 5552453.840 470558.637 5552457.366 470560.630
                        5552461.506 470561.588 5552464.227 470562.470 5552467.754 470562.796
                        5552469.766 470563.409 5552474.768 470566.887 5552501.580</gml:posList>
                </gml:LineString>
            </position>
            <art>1000</art>
        </BST_Wegekante>
    </sf:featureMember>
</sf:FeatureCollection>