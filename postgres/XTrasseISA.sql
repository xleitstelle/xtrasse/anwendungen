CREATE EXTENSION postgis;

CREATE TABLE isa_abwasserleitung (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   foerderung_fkcl text NOT NULL, -- Das Kriterium der Förderung kennzeichnet einzelne Infrastrukturen, Leitungsabschnitte oder auch ganze Netzbereiche, die im Rahmen der Breitbandförderung finanziert wurden.
   verfuegbarkeit_fkcl text NOT NULL, -- Die tatsächliche Verfügbarkeit wird als Kapazitäts- bzw. Auslastungsangabe zu den Einrichtungen verstanden. Über vorgegebene Kategorien werden die tatsächlich vorhandenen Kapazitäten erfasst (ein Leer-/Schutzrohrabschnitt ist bspw. nur teilweise befüllt oder ein Bauwerk bietet als Technikraum noch Platz für TK-Infrastruktur und ist daher auf Anfrage verfügbar etc.).
   lagegenauigkeit_fkcl text, -- Lagegenauigkeit des Raumbezugs
   lagegenauigkeittext text, -- Textlich formulierte Lagegenauigkeit des Raumbezugs
   typ text, -- Es besteht die Möglichkeit zusätzlich nähere Spezifikationen zu den Infrastrukturen als TYP-Angaben in den ISA aufzunehmen. Diese sollten möglichst eindeutig benannt und den einzelnen Geometrien zugeordnet sein (s. Beispiele "TYP" in den einzelnen Objektarten).
   ausnahmeisa boolean, -- Das Objekt soll gemäß § 79 Abs. 3 TKG nicht im Infrastrukturatlas veröffentlicht werden. Das Objekt wird in einem separaten Datensatz „Ausnahme nach § 79 Abs. 3 TKG_Geodaten“ an die BNetzA geliefert.
   gehoertzuisa_fk bigint NOT NULL, -- Referenz auf den Infrastrukturatlas. zu dem das Objekt gehört
   position geometry(MULTILINESTRING,25832) NOT NULL, -- Raumbezug des Objektes
   verlegetiefe integer NOT NULL -- Positive Ganzzahl in cm (0 = Information liegt nicht vor). Die Verlegetiefe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte für die Erreichbarkeit der unterirdischen Einrichtungen und hilft bei der Koordinierung von Bauarbeiten.
);

CREATE TABLE isa_abwasserleitung_nutzung (

   _id bigserial NOT NULL PRIMARY KEY,
   isa_abwasserleitung_id bigint NOT NULL,
   isa_nutzungtyp_id text NOT NULL -- Die Angabe zur gegenwärtigen Nutzung enthält die Information, für welchen Zweck die gelieferten Einrichtungen tatsächlich genutzt werden (z. B. Nutzung des Schutz-/Leerrohrs für TK-Zwecke oder als Schutz-/Leerrohr für die Elektrizitätsversorgung oder Leitungen für Fernwärme etc.). Eine Zuordnung der vorgegebenen Kategorien soll möglichst bezogen auf jede einzelne Infrastruktureinrichtung vorgenommen werden. Eine Mehrfacheinordnung ist auch weiterhin möglich, damit Einrichtungen, die aktuell für mehrere Zwecke genutzt werden, entsprechend erfasst werden können.
);

CREATE TABLE isa_ampel (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   foerderung_fkcl text NOT NULL, -- Das Kriterium der Förderung kennzeichnet einzelne Infrastrukturen, Leitungsabschnitte oder auch ganze Netzbereiche, die im Rahmen der Breitbandförderung finanziert wurden.
   verfuegbarkeit_fkcl text NOT NULL, -- Die tatsächliche Verfügbarkeit wird als Kapazitäts- bzw. Auslastungsangabe zu den Einrichtungen verstanden. Über vorgegebene Kategorien werden die tatsächlich vorhandenen Kapazitäten erfasst (ein Leer-/Schutzrohrabschnitt ist bspw. nur teilweise befüllt oder ein Bauwerk bietet als Technikraum noch Platz für TK-Infrastruktur und ist daher auf Anfrage verfügbar etc.).
   lagegenauigkeit_fkcl text, -- Lagegenauigkeit des Raumbezugs
   lagegenauigkeittext text, -- Textlich formulierte Lagegenauigkeit des Raumbezugs
   typ text, -- Es besteht die Möglichkeit zusätzlich nähere Spezifikationen zu den Infrastrukturen als TYP-Angaben in den ISA aufzunehmen. Diese sollten möglichst eindeutig benannt und den einzelnen Geometrien zugeordnet sein (s. Beispiele "TYP" in den einzelnen Objektarten).
   ausnahmeisa boolean, -- Das Objekt soll gemäß § 79 Abs. 3 TKG nicht im Infrastrukturatlas veröffentlicht werden. Das Objekt wird in einem separaten Datensatz „Ausnahme nach § 79 Abs. 3 TKG_Geodaten“ an die BNetzA geliefert.
   gehoertzuisa_fk bigint NOT NULL, -- Referenz auf den Infrastrukturatlas. zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   tiefe_hoehe integer NOT NULL, -- Angabe der Verlegetiefe (für POP, HVt, KVz, Zugangspunkt) oder der Höhe als positive Ganzzahl in cm. 0 = Information liegt nicht vor. Die Verlegetiefe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte für die Erreichbarkeit der unterirdischen Einrichtungen und hilft bei der Koordinierung von Bauarbeiten. Die Höhe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte ob sich eine Trägerstruktur für die oberirdische Verlegung von Glasfaser aber auch für die Errichtung von Standorten für drahtlose Zugangspunkte mit geringer Reichweite eignet.
   strom_fkcl text NOT NULL -- Das Attribut der Stromversorgung gibt der an einer Mitnutzung interessierten Person Anhaltspunkte ob sich eine Trägerstruktur für die Errichtung von Standorten für drahtlose Zugangspunkte mit geringer Reichweite eignet. Ob die Stromversorgung nur temporär geschaltet ist, ist für eine Aufnahme der Einrichtung in den ISA nicht entscheidend.
);

CREATE TABLE isa_ampel_nutzung (

   _id bigserial NOT NULL PRIMARY KEY,
   isa_ampel_id bigint NOT NULL,
   isa_nutzungtyp_id text NOT NULL -- Die Angabe zur gegenwärtigen Nutzung enthält die Information, für welchen Zweck die gelieferten Einrichtungen tatsächlich genutzt werden (z. B. Nutzung des Schutz-/Leerrohrs für TK-Zwecke oder als Schutz-/Leerrohr für die Elektrizitätsversorgung oder Leitungen für Fernwärme etc.). Eine Zuordnung der vorgegebenen Kategorien soll möglichst bezogen auf jede einzelne Infrastruktureinrichtung vorgenommen werden. Eine Mehrfacheinordnung ist auch weiterhin möglich, damit Einrichtungen, die aktuell für mehrere Zwecke genutzt werden, entsprechend erfasst werden können.
);

CREATE TABLE isa_bauwerk (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   foerderung_fkcl text NOT NULL, -- Das Kriterium der Förderung kennzeichnet einzelne Infrastrukturen, Leitungsabschnitte oder auch ganze Netzbereiche, die im Rahmen der Breitbandförderung finanziert wurden.
   verfuegbarkeit_fkcl text NOT NULL, -- Die tatsächliche Verfügbarkeit wird als Kapazitäts- bzw. Auslastungsangabe zu den Einrichtungen verstanden. Über vorgegebene Kategorien werden die tatsächlich vorhandenen Kapazitäten erfasst (ein Leer-/Schutzrohrabschnitt ist bspw. nur teilweise befüllt oder ein Bauwerk bietet als Technikraum noch Platz für TK-Infrastruktur und ist daher auf Anfrage verfügbar etc.).
   lagegenauigkeit_fkcl text, -- Lagegenauigkeit des Raumbezugs
   lagegenauigkeittext text, -- Textlich formulierte Lagegenauigkeit des Raumbezugs
   typ text, -- Es besteht die Möglichkeit zusätzlich nähere Spezifikationen zu den Infrastrukturen als TYP-Angaben in den ISA aufzunehmen. Diese sollten möglichst eindeutig benannt und den einzelnen Geometrien zugeordnet sein (s. Beispiele "TYP" in den einzelnen Objektarten).
   ausnahmeisa boolean, -- Das Objekt soll gemäß § 79 Abs. 3 TKG nicht im Infrastrukturatlas veröffentlicht werden. Das Objekt wird in einem separaten Datensatz „Ausnahme nach § 79 Abs. 3 TKG_Geodaten“ an die BNetzA geliefert.
   gehoertzuisa_fk bigint NOT NULL, -- Referenz auf den Infrastrukturatlas. zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   tiefe_hoehe integer NOT NULL, -- Angabe der Verlegetiefe (für POP, HVt, KVz, Zugangspunkt) oder der Höhe als positive Ganzzahl in cm. 0 = Information liegt nicht vor. Die Verlegetiefe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte für die Erreichbarkeit der unterirdischen Einrichtungen und hilft bei der Koordinierung von Bauarbeiten. Die Höhe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte ob sich eine Trägerstruktur für die oberirdische Verlegung von Glasfaser aber auch für die Errichtung von Standorten für drahtlose Zugangspunkte mit geringer Reichweite eignet.
   strom_fkcl text NOT NULL -- Das Attribut der Stromversorgung gibt der an einer Mitnutzung interessierten Person Anhaltspunkte ob sich eine Trägerstruktur für die Errichtung von Standorten für drahtlose Zugangspunkte mit geringer Reichweite eignet. Ob die Stromversorgung nur temporär geschaltet ist, ist für eine Aufnahme der Einrichtung in den ISA nicht entscheidend.
);

CREATE TABLE isa_bauwerk_nutzung (

   _id bigserial NOT NULL PRIMARY KEY,
   isa_bauwerk_id bigint NOT NULL,
   isa_nutzungtyp_id text NOT NULL -- Die Angabe zur gegenwärtigen Nutzung enthält die Information, für welchen Zweck die gelieferten Einrichtungen tatsächlich genutzt werden (z. B. Nutzung des Schutz-/Leerrohrs für TK-Zwecke oder als Schutz-/Leerrohr für die Elektrizitätsversorgung oder Leitungen für Fernwärme etc.). Eine Zuordnung der vorgegebenen Kategorien soll möglichst bezogen auf jede einzelne Infrastruktureinrichtung vorgenommen werden. Eine Mehrfacheinordnung ist auch weiterhin möglich, damit Einrichtungen, die aktuell für mehrere Zwecke genutzt werden, entsprechend erfasst werden können.
);

CREATE TABLE isa_foerderung (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE isa_funkmast (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   foerderung_fkcl text NOT NULL, -- Das Kriterium der Förderung kennzeichnet einzelne Infrastrukturen, Leitungsabschnitte oder auch ganze Netzbereiche, die im Rahmen der Breitbandförderung finanziert wurden.
   verfuegbarkeit_fkcl text NOT NULL, -- Die tatsächliche Verfügbarkeit wird als Kapazitäts- bzw. Auslastungsangabe zu den Einrichtungen verstanden. Über vorgegebene Kategorien werden die tatsächlich vorhandenen Kapazitäten erfasst (ein Leer-/Schutzrohrabschnitt ist bspw. nur teilweise befüllt oder ein Bauwerk bietet als Technikraum noch Platz für TK-Infrastruktur und ist daher auf Anfrage verfügbar etc.).
   lagegenauigkeit_fkcl text, -- Lagegenauigkeit des Raumbezugs
   lagegenauigkeittext text, -- Textlich formulierte Lagegenauigkeit des Raumbezugs
   typ text, -- Es besteht die Möglichkeit zusätzlich nähere Spezifikationen zu den Infrastrukturen als TYP-Angaben in den ISA aufzunehmen. Diese sollten möglichst eindeutig benannt und den einzelnen Geometrien zugeordnet sein (s. Beispiele "TYP" in den einzelnen Objektarten).
   ausnahmeisa boolean, -- Das Objekt soll gemäß § 79 Abs. 3 TKG nicht im Infrastrukturatlas veröffentlicht werden. Das Objekt wird in einem separaten Datensatz „Ausnahme nach § 79 Abs. 3 TKG_Geodaten“ an die BNetzA geliefert.
   gehoertzuisa_fk bigint NOT NULL, -- Referenz auf den Infrastrukturatlas. zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   tiefe_hoehe integer NOT NULL, -- Angabe der Verlegetiefe (für POP, HVt, KVz, Zugangspunkt) oder der Höhe als positive Ganzzahl in cm. 0 = Information liegt nicht vor. Die Verlegetiefe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte für die Erreichbarkeit der unterirdischen Einrichtungen und hilft bei der Koordinierung von Bauarbeiten. Die Höhe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte ob sich eine Trägerstruktur für die oberirdische Verlegung von Glasfaser aber auch für die Errichtung von Standorten für drahtlose Zugangspunkte mit geringer Reichweite eignet.
   strom_fkcl text NOT NULL -- Das Attribut der Stromversorgung gibt der an einer Mitnutzung interessierten Person Anhaltspunkte ob sich eine Trägerstruktur für die Errichtung von Standorten für drahtlose Zugangspunkte mit geringer Reichweite eignet. Ob die Stromversorgung nur temporär geschaltet ist, ist für eine Aufnahme der Einrichtung in den ISA nicht entscheidend.
);

CREATE TABLE isa_funkmast_nutzung (

   _id bigserial NOT NULL PRIMARY KEY,
   isa_funkmast_id bigint NOT NULL,
   isa_nutzungtyp_id text NOT NULL -- Die Angabe zur gegenwärtigen Nutzung enthält die Information, für welchen Zweck die gelieferten Einrichtungen tatsächlich genutzt werden (z. B. Nutzung des Schutz-/Leerrohrs für TK-Zwecke oder als Schutz-/Leerrohr für die Elektrizitätsversorgung oder Leitungen für Fernwärme etc.). Eine Zuordnung der vorgegebenen Kategorien soll möglichst bezogen auf jede einzelne Infrastruktureinrichtung vorgenommen werden. Eine Mehrfacheinordnung ist auch weiterhin möglich, damit Einrichtungen, die aktuell für mehrere Zwecke genutzt werden, entsprechend erfasst werden können.
);

CREATE TABLE isa_glasfaser (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   foerderung_fkcl text NOT NULL, -- Das Kriterium der Förderung kennzeichnet einzelne Infrastrukturen, Leitungsabschnitte oder auch ganze Netzbereiche, die im Rahmen der Breitbandförderung finanziert wurden.
   verfuegbarkeit_fkcl text NOT NULL, -- Die tatsächliche Verfügbarkeit wird als Kapazitäts- bzw. Auslastungsangabe zu den Einrichtungen verstanden. Über vorgegebene Kategorien werden die tatsächlich vorhandenen Kapazitäten erfasst (ein Leer-/Schutzrohrabschnitt ist bspw. nur teilweise befüllt oder ein Bauwerk bietet als Technikraum noch Platz für TK-Infrastruktur und ist daher auf Anfrage verfügbar etc.).
   lagegenauigkeit_fkcl text, -- Lagegenauigkeit des Raumbezugs
   lagegenauigkeittext text, -- Textlich formulierte Lagegenauigkeit des Raumbezugs
   typ text, -- Es besteht die Möglichkeit zusätzlich nähere Spezifikationen zu den Infrastrukturen als TYP-Angaben in den ISA aufzunehmen. Diese sollten möglichst eindeutig benannt und den einzelnen Geometrien zugeordnet sein (s. Beispiele "TYP" in den einzelnen Objektarten).
   ausnahmeisa boolean, -- Das Objekt soll gemäß § 79 Abs. 3 TKG nicht im Infrastrukturatlas veröffentlicht werden. Das Objekt wird in einem separaten Datensatz „Ausnahme nach § 79 Abs. 3 TKG_Geodaten“ an die BNetzA geliefert.
   gehoertzuisa_fk bigint NOT NULL, -- Referenz auf den Infrastrukturatlas. zu dem das Objekt gehört
   position geometry(MULTILINESTRING,25832) NOT NULL, -- Raumbezug des Objektes
   verlegetiefe integer NOT NULL -- Positive Ganzzahl in cm (0 = Information liegt nicht vor). Die Verlegetiefe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte für die Erreichbarkeit der unterirdischen Einrichtungen und hilft bei der Koordinierung von Bauarbeiten.
);

CREATE TABLE isa_glasfaser_nutzung (

   _id bigserial NOT NULL PRIMARY KEY,
   isa_glasfaser_id bigint NOT NULL,
   isa_nutzungtyp_id text NOT NULL -- Die Angabe zur gegenwärtigen Nutzung enthält die Information, für welchen Zweck die gelieferten Einrichtungen tatsächlich genutzt werden (z. B. Nutzung des Schutz-/Leerrohrs für TK-Zwecke oder als Schutz-/Leerrohr für die Elektrizitätsversorgung oder Leitungen für Fernwärme etc.). Eine Zuordnung der vorgegebenen Kategorien soll möglichst bezogen auf jede einzelne Infrastruktureinrichtung vorgenommen werden. Eine Mehrfacheinordnung ist auch weiterhin möglich, damit Einrichtungen, die aktuell für mehrere Zwecke genutzt werden, entsprechend erfasst werden können.
);

CREATE TABLE isa_grundstueck_liegenschaft (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   foerderung_fkcl text NOT NULL, -- Das Kriterium der Förderung kennzeichnet einzelne Infrastrukturen, Leitungsabschnitte oder auch ganze Netzbereiche, die im Rahmen der Breitbandförderung finanziert wurden.
   verfuegbarkeit_fkcl text NOT NULL, -- Die tatsächliche Verfügbarkeit wird als Kapazitäts- bzw. Auslastungsangabe zu den Einrichtungen verstanden. Über vorgegebene Kategorien werden die tatsächlich vorhandenen Kapazitäten erfasst (ein Leer-/Schutzrohrabschnitt ist bspw. nur teilweise befüllt oder ein Bauwerk bietet als Technikraum noch Platz für TK-Infrastruktur und ist daher auf Anfrage verfügbar etc.).
   lagegenauigkeit_fkcl text, -- Lagegenauigkeit des Raumbezugs
   lagegenauigkeittext text, -- Textlich formulierte Lagegenauigkeit des Raumbezugs
   typ text, -- Es besteht die Möglichkeit zusätzlich nähere Spezifikationen zu den Infrastrukturen als TYP-Angaben in den ISA aufzunehmen. Diese sollten möglichst eindeutig benannt und den einzelnen Geometrien zugeordnet sein (s. Beispiele "TYP" in den einzelnen Objektarten).
   ausnahmeisa boolean, -- Das Objekt soll gemäß § 79 Abs. 3 TKG nicht im Infrastrukturatlas veröffentlicht werden. Das Objekt wird in einem separaten Datensatz „Ausnahme nach § 79 Abs. 3 TKG_Geodaten“ an die BNetzA geliefert.
   gehoertzuisa_fk bigint NOT NULL, -- Referenz auf den Infrastrukturatlas. zu dem das Objekt gehört
   position geometry(MULTIPOLYGON,25832) NOT NULL, -- Raumbezug des Objektes
   strom_fkcl text NOT NULL -- Das Attribut der Stromversorgung gibt der an einer Mitnutzung interessierten Person Anhaltspunkte ob sich eine Trägerstruktur für die Errichtung von Standorten für drahtlose Zugangspunkte mit geringer Reichweite eignet. Ob die Stromversorgung nur temporär geschaltet ist, ist für eine Aufnahme der Einrichtung in den ISA nicht entscheidend.
);

CREATE TABLE isa_grundstueck_liegenschaft_nutzung (

   _id bigserial NOT NULL PRIMARY KEY,
   isa_grundstueck_liegenschaft_id bigint NOT NULL,
   isa_nutzungtyp_id text NOT NULL -- Die Angabe zur gegenwärtigen Nutzung enthält die Information, für welchen Zweck die gelieferten Einrichtungen tatsächlich genutzt werden (z. B. Nutzung des Schutz-/Leerrohrs für TK-Zwecke oder als Schutz-/Leerrohr für die Elektrizitätsversorgung oder Leitungen für Fernwärme etc.). Eine Zuordnung der vorgegebenen Kategorien soll möglichst bezogen auf jede einzelne Infrastruktureinrichtung vorgenommen werden. Eine Mehrfacheinordnung ist auch weiterhin möglich, damit Einrichtungen, die aktuell für mehrere Zwecke genutzt werden, entsprechend erfasst werden können.
);

CREATE TABLE isa_haltestelle (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   foerderung_fkcl text NOT NULL, -- Das Kriterium der Förderung kennzeichnet einzelne Infrastrukturen, Leitungsabschnitte oder auch ganze Netzbereiche, die im Rahmen der Breitbandförderung finanziert wurden.
   verfuegbarkeit_fkcl text NOT NULL, -- Die tatsächliche Verfügbarkeit wird als Kapazitäts- bzw. Auslastungsangabe zu den Einrichtungen verstanden. Über vorgegebene Kategorien werden die tatsächlich vorhandenen Kapazitäten erfasst (ein Leer-/Schutzrohrabschnitt ist bspw. nur teilweise befüllt oder ein Bauwerk bietet als Technikraum noch Platz für TK-Infrastruktur und ist daher auf Anfrage verfügbar etc.).
   lagegenauigkeit_fkcl text, -- Lagegenauigkeit des Raumbezugs
   lagegenauigkeittext text, -- Textlich formulierte Lagegenauigkeit des Raumbezugs
   typ text, -- Es besteht die Möglichkeit zusätzlich nähere Spezifikationen zu den Infrastrukturen als TYP-Angaben in den ISA aufzunehmen. Diese sollten möglichst eindeutig benannt und den einzelnen Geometrien zugeordnet sein (s. Beispiele "TYP" in den einzelnen Objektarten).
   ausnahmeisa boolean, -- Das Objekt soll gemäß § 79 Abs. 3 TKG nicht im Infrastrukturatlas veröffentlicht werden. Das Objekt wird in einem separaten Datensatz „Ausnahme nach § 79 Abs. 3 TKG_Geodaten“ an die BNetzA geliefert.
   gehoertzuisa_fk bigint NOT NULL, -- Referenz auf den Infrastrukturatlas. zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   tiefe_hoehe integer NOT NULL, -- Angabe der Verlegetiefe (für POP, HVt, KVz, Zugangspunkt) oder der Höhe als positive Ganzzahl in cm. 0 = Information liegt nicht vor. Die Verlegetiefe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte für die Erreichbarkeit der unterirdischen Einrichtungen und hilft bei der Koordinierung von Bauarbeiten. Die Höhe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte ob sich eine Trägerstruktur für die oberirdische Verlegung von Glasfaser aber auch für die Errichtung von Standorten für drahtlose Zugangspunkte mit geringer Reichweite eignet.
   strom_fkcl text NOT NULL -- Das Attribut der Stromversorgung gibt der an einer Mitnutzung interessierten Person Anhaltspunkte ob sich eine Trägerstruktur für die Errichtung von Standorten für drahtlose Zugangspunkte mit geringer Reichweite eignet. Ob die Stromversorgung nur temporär geschaltet ist, ist für eine Aufnahme der Einrichtung in den ISA nicht entscheidend.
);

CREATE TABLE isa_haltestelle_nutzung (

   _id bigserial NOT NULL PRIMARY KEY,
   isa_haltestelle_id bigint NOT NULL,
   isa_nutzungtyp_id text NOT NULL -- Die Angabe zur gegenwärtigen Nutzung enthält die Information, für welchen Zweck die gelieferten Einrichtungen tatsächlich genutzt werden (z. B. Nutzung des Schutz-/Leerrohrs für TK-Zwecke oder als Schutz-/Leerrohr für die Elektrizitätsversorgung oder Leitungen für Fernwärme etc.). Eine Zuordnung der vorgegebenen Kategorien soll möglichst bezogen auf jede einzelne Infrastruktureinrichtung vorgenommen werden. Eine Mehrfacheinordnung ist auch weiterhin möglich, damit Einrichtungen, die aktuell für mehrere Zwecke genutzt werden, entsprechend erfasst werden können.
);

CREATE TABLE isa_hauptverteiler (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   foerderung_fkcl text NOT NULL, -- Das Kriterium der Förderung kennzeichnet einzelne Infrastrukturen, Leitungsabschnitte oder auch ganze Netzbereiche, die im Rahmen der Breitbandförderung finanziert wurden.
   verfuegbarkeit_fkcl text NOT NULL, -- Die tatsächliche Verfügbarkeit wird als Kapazitäts- bzw. Auslastungsangabe zu den Einrichtungen verstanden. Über vorgegebene Kategorien werden die tatsächlich vorhandenen Kapazitäten erfasst (ein Leer-/Schutzrohrabschnitt ist bspw. nur teilweise befüllt oder ein Bauwerk bietet als Technikraum noch Platz für TK-Infrastruktur und ist daher auf Anfrage verfügbar etc.).
   lagegenauigkeit_fkcl text, -- Lagegenauigkeit des Raumbezugs
   lagegenauigkeittext text, -- Textlich formulierte Lagegenauigkeit des Raumbezugs
   typ text, -- Es besteht die Möglichkeit zusätzlich nähere Spezifikationen zu den Infrastrukturen als TYP-Angaben in den ISA aufzunehmen. Diese sollten möglichst eindeutig benannt und den einzelnen Geometrien zugeordnet sein (s. Beispiele "TYP" in den einzelnen Objektarten).
   ausnahmeisa boolean, -- Das Objekt soll gemäß § 79 Abs. 3 TKG nicht im Infrastrukturatlas veröffentlicht werden. Das Objekt wird in einem separaten Datensatz „Ausnahme nach § 79 Abs. 3 TKG_Geodaten“ an die BNetzA geliefert.
   gehoertzuisa_fk bigint NOT NULL, -- Referenz auf den Infrastrukturatlas. zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   tiefe_hoehe integer NOT NULL -- Angabe der Verlegetiefe (für POP, HVt, KVz, Zugangspunkt) oder der Höhe als positive Ganzzahl in cm. 0 = Information liegt nicht vor. Die Verlegetiefe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte für die Erreichbarkeit der unterirdischen Einrichtungen und hilft bei der Koordinierung von Bauarbeiten. Die Höhe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte ob sich eine Trägerstruktur für die oberirdische Verlegung von Glasfaser aber auch für die Errichtung von Standorten für drahtlose Zugangspunkte mit geringer Reichweite eignet.
);

CREATE TABLE isa_hauptverteiler_nutzung (

   _id bigserial NOT NULL PRIMARY KEY,
   isa_hauptverteiler_id bigint NOT NULL,
   isa_nutzungtyp_id text NOT NULL -- Die Angabe zur gegenwärtigen Nutzung enthält die Information, für welchen Zweck die gelieferten Einrichtungen tatsächlich genutzt werden (z. B. Nutzung des Schutz-/Leerrohrs für TK-Zwecke oder als Schutz-/Leerrohr für die Elektrizitätsversorgung oder Leitungen für Fernwärme etc.). Eine Zuordnung der vorgegebenen Kategorien soll möglichst bezogen auf jede einzelne Infrastruktureinrichtung vorgenommen werden. Eine Mehrfacheinordnung ist auch weiterhin möglich, damit Einrichtungen, die aktuell für mehrere Zwecke genutzt werden, entsprechend erfasst werden können.
);

CREATE TABLE isa_holz_mast (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   foerderung_fkcl text NOT NULL, -- Das Kriterium der Förderung kennzeichnet einzelne Infrastrukturen, Leitungsabschnitte oder auch ganze Netzbereiche, die im Rahmen der Breitbandförderung finanziert wurden.
   verfuegbarkeit_fkcl text NOT NULL, -- Die tatsächliche Verfügbarkeit wird als Kapazitäts- bzw. Auslastungsangabe zu den Einrichtungen verstanden. Über vorgegebene Kategorien werden die tatsächlich vorhandenen Kapazitäten erfasst (ein Leer-/Schutzrohrabschnitt ist bspw. nur teilweise befüllt oder ein Bauwerk bietet als Technikraum noch Platz für TK-Infrastruktur und ist daher auf Anfrage verfügbar etc.).
   lagegenauigkeit_fkcl text, -- Lagegenauigkeit des Raumbezugs
   lagegenauigkeittext text, -- Textlich formulierte Lagegenauigkeit des Raumbezugs
   typ text, -- Es besteht die Möglichkeit zusätzlich nähere Spezifikationen zu den Infrastrukturen als TYP-Angaben in den ISA aufzunehmen. Diese sollten möglichst eindeutig benannt und den einzelnen Geometrien zugeordnet sein (s. Beispiele "TYP" in den einzelnen Objektarten).
   ausnahmeisa boolean, -- Das Objekt soll gemäß § 79 Abs. 3 TKG nicht im Infrastrukturatlas veröffentlicht werden. Das Objekt wird in einem separaten Datensatz „Ausnahme nach § 79 Abs. 3 TKG_Geodaten“ an die BNetzA geliefert.
   gehoertzuisa_fk bigint NOT NULL, -- Referenz auf den Infrastrukturatlas. zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   tiefe_hoehe integer NOT NULL, -- Angabe der Verlegetiefe (für POP, HVt, KVz, Zugangspunkt) oder der Höhe als positive Ganzzahl in cm. 0 = Information liegt nicht vor. Die Verlegetiefe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte für die Erreichbarkeit der unterirdischen Einrichtungen und hilft bei der Koordinierung von Bauarbeiten. Die Höhe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte ob sich eine Trägerstruktur für die oberirdische Verlegung von Glasfaser aber auch für die Errichtung von Standorten für drahtlose Zugangspunkte mit geringer Reichweite eignet.
   strom_fkcl text NOT NULL -- Das Attribut der Stromversorgung gibt der an einer Mitnutzung interessierten Person Anhaltspunkte ob sich eine Trägerstruktur für die Errichtung von Standorten für drahtlose Zugangspunkte mit geringer Reichweite eignet. Ob die Stromversorgung nur temporär geschaltet ist, ist für eine Aufnahme der Einrichtung in den ISA nicht entscheidend.
);

CREATE TABLE isa_holz_mast_nutzung (

   _id bigserial NOT NULL PRIMARY KEY,
   isa_holz_mast_id bigint NOT NULL,
   isa_nutzungtyp_id text NOT NULL -- Die Angabe zur gegenwärtigen Nutzung enthält die Information, für welchen Zweck die gelieferten Einrichtungen tatsächlich genutzt werden (z. B. Nutzung des Schutz-/Leerrohrs für TK-Zwecke oder als Schutz-/Leerrohr für die Elektrizitätsversorgung oder Leitungen für Fernwärme etc.). Eine Zuordnung der vorgegebenen Kategorien soll möglichst bezogen auf jede einzelne Infrastruktureinrichtung vorgenommen werden. Eine Mehrfacheinordnung ist auch weiterhin möglich, damit Einrichtungen, die aktuell für mehrere Zwecke genutzt werden, entsprechend erfasst werden können.
);

CREATE TABLE isa_inhabertyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE isa_kabelverzweiger (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   foerderung_fkcl text NOT NULL, -- Das Kriterium der Förderung kennzeichnet einzelne Infrastrukturen, Leitungsabschnitte oder auch ganze Netzbereiche, die im Rahmen der Breitbandförderung finanziert wurden.
   verfuegbarkeit_fkcl text NOT NULL, -- Die tatsächliche Verfügbarkeit wird als Kapazitäts- bzw. Auslastungsangabe zu den Einrichtungen verstanden. Über vorgegebene Kategorien werden die tatsächlich vorhandenen Kapazitäten erfasst (ein Leer-/Schutzrohrabschnitt ist bspw. nur teilweise befüllt oder ein Bauwerk bietet als Technikraum noch Platz für TK-Infrastruktur und ist daher auf Anfrage verfügbar etc.).
   lagegenauigkeit_fkcl text, -- Lagegenauigkeit des Raumbezugs
   lagegenauigkeittext text, -- Textlich formulierte Lagegenauigkeit des Raumbezugs
   typ text, -- Es besteht die Möglichkeit zusätzlich nähere Spezifikationen zu den Infrastrukturen als TYP-Angaben in den ISA aufzunehmen. Diese sollten möglichst eindeutig benannt und den einzelnen Geometrien zugeordnet sein (s. Beispiele "TYP" in den einzelnen Objektarten).
   ausnahmeisa boolean, -- Das Objekt soll gemäß § 79 Abs. 3 TKG nicht im Infrastrukturatlas veröffentlicht werden. Das Objekt wird in einem separaten Datensatz „Ausnahme nach § 79 Abs. 3 TKG_Geodaten“ an die BNetzA geliefert.
   gehoertzuisa_fk bigint NOT NULL, -- Referenz auf den Infrastrukturatlas. zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   tiefe_hoehe integer NOT NULL -- Angabe der Verlegetiefe (für POP, HVt, KVz, Zugangspunkt) oder der Höhe als positive Ganzzahl in cm. 0 = Information liegt nicht vor. Die Verlegetiefe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte für die Erreichbarkeit der unterirdischen Einrichtungen und hilft bei der Koordinierung von Bauarbeiten. Die Höhe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte ob sich eine Trägerstruktur für die oberirdische Verlegung von Glasfaser aber auch für die Errichtung von Standorten für drahtlose Zugangspunkte mit geringer Reichweite eignet.
);

CREATE TABLE isa_kabelverzweiger_nutzung (

   _id bigserial NOT NULL PRIMARY KEY,
   isa_kabelverzweiger_id bigint NOT NULL,
   isa_nutzungtyp_id text NOT NULL -- Die Angabe zur gegenwärtigen Nutzung enthält die Information, für welchen Zweck die gelieferten Einrichtungen tatsächlich genutzt werden (z. B. Nutzung des Schutz-/Leerrohrs für TK-Zwecke oder als Schutz-/Leerrohr für die Elektrizitätsversorgung oder Leitungen für Fernwärme etc.). Eine Zuordnung der vorgegebenen Kategorien soll möglichst bezogen auf jede einzelne Infrastruktureinrichtung vorgenommen werden. Eine Mehrfacheinordnung ist auch weiterhin möglich, damit Einrichtungen, die aktuell für mehrere Zwecke genutzt werden, entsprechend erfasst werden können.
);

CREATE TABLE isa_lagegenauigkeit (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE isa_lehrrohr (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   foerderung_fkcl text NOT NULL, -- Das Kriterium der Förderung kennzeichnet einzelne Infrastrukturen, Leitungsabschnitte oder auch ganze Netzbereiche, die im Rahmen der Breitbandförderung finanziert wurden.
   verfuegbarkeit_fkcl text NOT NULL, -- Die tatsächliche Verfügbarkeit wird als Kapazitäts- bzw. Auslastungsangabe zu den Einrichtungen verstanden. Über vorgegebene Kategorien werden die tatsächlich vorhandenen Kapazitäten erfasst (ein Leer-/Schutzrohrabschnitt ist bspw. nur teilweise befüllt oder ein Bauwerk bietet als Technikraum noch Platz für TK-Infrastruktur und ist daher auf Anfrage verfügbar etc.).
   lagegenauigkeit_fkcl text, -- Lagegenauigkeit des Raumbezugs
   lagegenauigkeittext text, -- Textlich formulierte Lagegenauigkeit des Raumbezugs
   typ text, -- Es besteht die Möglichkeit zusätzlich nähere Spezifikationen zu den Infrastrukturen als TYP-Angaben in den ISA aufzunehmen. Diese sollten möglichst eindeutig benannt und den einzelnen Geometrien zugeordnet sein (s. Beispiele "TYP" in den einzelnen Objektarten).
   ausnahmeisa boolean, -- Das Objekt soll gemäß § 79 Abs. 3 TKG nicht im Infrastrukturatlas veröffentlicht werden. Das Objekt wird in einem separaten Datensatz „Ausnahme nach § 79 Abs. 3 TKG_Geodaten“ an die BNetzA geliefert.
   gehoertzuisa_fk bigint NOT NULL, -- Referenz auf den Infrastrukturatlas. zu dem das Objekt gehört
   position geometry(MULTILINESTRING,25832) NOT NULL, -- Raumbezug des Objektes
   verlegetiefe integer NOT NULL -- Positive Ganzzahl in cm (0 = Information liegt nicht vor). Die Verlegetiefe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte für die Erreichbarkeit der unterirdischen Einrichtungen und hilft bei der Koordinierung von Bauarbeiten.
);

CREATE TABLE isa_lehrrohr_nutzung (

   _id bigserial NOT NULL PRIMARY KEY,
   isa_lehrrohr_id bigint NOT NULL,
   isa_nutzungtyp_id text NOT NULL -- Die Angabe zur gegenwärtigen Nutzung enthält die Information, für welchen Zweck die gelieferten Einrichtungen tatsächlich genutzt werden (z. B. Nutzung des Schutz-/Leerrohrs für TK-Zwecke oder als Schutz-/Leerrohr für die Elektrizitätsversorgung oder Leitungen für Fernwärme etc.). Eine Zuordnung der vorgegebenen Kategorien soll möglichst bezogen auf jede einzelne Infrastruktureinrichtung vorgenommen werden. Eine Mehrfacheinordnung ist auch weiterhin möglich, damit Einrichtungen, die aktuell für mehrere Zwecke genutzt werden, entsprechend erfasst werden können.
);

CREATE TABLE isa_nutzungtyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE isa_plan (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   name text NOT NULL, -- Name des Plans (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML und GML)
   nummer text, -- Nummer des Plans
   internalid text, -- Interner Identifikator des Plans
   beschreibung text, -- Kommentierende Beschreibung des Leitungsplans
   technischeplanerstellung text, -- Bezeichnung der Institution oder Firma, die den Plan technisch erstellt hat
   technherstelldatum date, -- Datum, an dem der Plan technisch ausgefertigt wurde
   erstellungsmassstab integer, -- Der bei der Erstellung des Plans benutzte Kartenmaßstab
   position geometry(POLYGON,25832) NOT NULL, -- Flächenhafter Raumbezug des Plans
   inhabertyp_fkcl text NOT NULL -- Es ist anzugeben, ob der Infrastrukturinhaber Eigentümer oder Betreiber der gelieferten Infrastrukturen ist. Eine Darstellung dieser Eigenschaft im Infrastrukturatlas erfolgt nicht. Die Erhebung erfolgt zur internen Verifizierung des Datenlieferanten.
);

CREATE TABLE isa_plan_externereferenz (

   _id bigserial NOT NULL PRIMARY KEY,
   referenzname text NOT NULL, -- Name des referierten Dokument innerhalb des Informationssystems
   referenzurl text NOT NULL, -- URI des referierten Dokuments, bzw. Datenbank-Schlüssel. Wenn der XTrasseGML Datensatz und das referierte Dokument in einem hierarchischen Ordnersystem gespeichert sind, kann die URI auch einen relativen Pfad vom XPlanGML-Datensatz zum Dokument enthalten.
   beschreibung text, -- Beschreibung des referierten Dokuments
   datum date, -- Datum des referierten Dokuments
   isa_plan_id bigint NOT NULL
);

CREATE TABLE isa_plan_gesetzlichegrundlage (

   _id bigserial NOT NULL PRIMARY KEY,
   name text, -- Name / Titel des Gesetzes
   detail text, -- Detaillierte Spezifikation der gesetzlichen Grundlage mit Angabe einer Paragraphennummer
   ausfertigungdatum date, -- Die Datumsangabe bezieht sich in der Regel auf das Datum der Ausfertigung des Gesetzes oder der Rechtsverordnung
   letztebekanntmdatum date, -- Ist das Gesetz oder die Verordnung nach mehreren Änderungen neu bekannt gemacht worden, kann anstelle des Ausfertigungsdatums das Datum der Bekanntmachung der Neufassung angegeben werden
   letzteaenderungdatum date, -- Ist ein Gesetz oder eine Rechtsverordnung nach der Veröffentlichung des amtlichen Volltextes geändert worden, kann hierauf hingewiesen werden
   isa_plan_id bigint NOT NULL
);

CREATE TABLE isa_plan_kontaktgis (

   _id bigserial NOT NULL PRIMARY KEY,
   anrede text, -- Anrede
   vorundzuname text NOT NULL, -- Vor- und Zuname
   telefon text NOT NULL, -- Telefonnummer
   email text NOT NULL, -- Email Adresse
   isa_plan_id bigint NOT NULL
);

CREATE TABLE isa_plan_kontaktmitnutzung (

   _id bigserial NOT NULL PRIMARY KEY,
   anrede text, -- Anrede
   vorundzuname text NOT NULL, -- Vor- und Zuname
   telefon text NOT NULL, -- Telefonnummer
   email text NOT NULL, -- Email Adresse
   isa_plan_id bigint NOT NULL
);

CREATE TABLE isa_pointofpresence (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   foerderung_fkcl text NOT NULL, -- Das Kriterium der Förderung kennzeichnet einzelne Infrastrukturen, Leitungsabschnitte oder auch ganze Netzbereiche, die im Rahmen der Breitbandförderung finanziert wurden.
   verfuegbarkeit_fkcl text NOT NULL, -- Die tatsächliche Verfügbarkeit wird als Kapazitäts- bzw. Auslastungsangabe zu den Einrichtungen verstanden. Über vorgegebene Kategorien werden die tatsächlich vorhandenen Kapazitäten erfasst (ein Leer-/Schutzrohrabschnitt ist bspw. nur teilweise befüllt oder ein Bauwerk bietet als Technikraum noch Platz für TK-Infrastruktur und ist daher auf Anfrage verfügbar etc.).
   lagegenauigkeit_fkcl text, -- Lagegenauigkeit des Raumbezugs
   lagegenauigkeittext text, -- Textlich formulierte Lagegenauigkeit des Raumbezugs
   typ text, -- Es besteht die Möglichkeit zusätzlich nähere Spezifikationen zu den Infrastrukturen als TYP-Angaben in den ISA aufzunehmen. Diese sollten möglichst eindeutig benannt und den einzelnen Geometrien zugeordnet sein (s. Beispiele "TYP" in den einzelnen Objektarten).
   ausnahmeisa boolean, -- Das Objekt soll gemäß § 79 Abs. 3 TKG nicht im Infrastrukturatlas veröffentlicht werden. Das Objekt wird in einem separaten Datensatz „Ausnahme nach § 79 Abs. 3 TKG_Geodaten“ an die BNetzA geliefert.
   gehoertzuisa_fk bigint NOT NULL, -- Referenz auf den Infrastrukturatlas. zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   tiefe_hoehe integer NOT NULL -- Angabe der Verlegetiefe (für POP, HVt, KVz, Zugangspunkt) oder der Höhe als positive Ganzzahl in cm. 0 = Information liegt nicht vor. Die Verlegetiefe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte für die Erreichbarkeit der unterirdischen Einrichtungen und hilft bei der Koordinierung von Bauarbeiten. Die Höhe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte ob sich eine Trägerstruktur für die oberirdische Verlegung von Glasfaser aber auch für die Errichtung von Standorten für drahtlose Zugangspunkte mit geringer Reichweite eignet.
);

CREATE TABLE isa_pointofpresence_nutzung (

   _id bigserial NOT NULL PRIMARY KEY,
   isa_pointofpresence_id bigint NOT NULL,
   isa_nutzungtyp_id text NOT NULL -- Die Angabe zur gegenwärtigen Nutzung enthält die Information, für welchen Zweck die gelieferten Einrichtungen tatsächlich genutzt werden (z. B. Nutzung des Schutz-/Leerrohrs für TK-Zwecke oder als Schutz-/Leerrohr für die Elektrizitätsversorgung oder Leitungen für Fernwärme etc.). Eine Zuordnung der vorgegebenen Kategorien soll möglichst bezogen auf jede einzelne Infrastruktureinrichtung vorgenommen werden. Eine Mehrfacheinordnung ist auch weiterhin möglich, damit Einrichtungen, die aktuell für mehrere Zwecke genutzt werden, entsprechend erfasst werden können.
);

CREATE TABLE isa_reklametafel_litfasssauele (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   foerderung_fkcl text NOT NULL, -- Das Kriterium der Förderung kennzeichnet einzelne Infrastrukturen, Leitungsabschnitte oder auch ganze Netzbereiche, die im Rahmen der Breitbandförderung finanziert wurden.
   verfuegbarkeit_fkcl text NOT NULL, -- Die tatsächliche Verfügbarkeit wird als Kapazitäts- bzw. Auslastungsangabe zu den Einrichtungen verstanden. Über vorgegebene Kategorien werden die tatsächlich vorhandenen Kapazitäten erfasst (ein Leer-/Schutzrohrabschnitt ist bspw. nur teilweise befüllt oder ein Bauwerk bietet als Technikraum noch Platz für TK-Infrastruktur und ist daher auf Anfrage verfügbar etc.).
   lagegenauigkeit_fkcl text, -- Lagegenauigkeit des Raumbezugs
   lagegenauigkeittext text, -- Textlich formulierte Lagegenauigkeit des Raumbezugs
   typ text, -- Es besteht die Möglichkeit zusätzlich nähere Spezifikationen zu den Infrastrukturen als TYP-Angaben in den ISA aufzunehmen. Diese sollten möglichst eindeutig benannt und den einzelnen Geometrien zugeordnet sein (s. Beispiele "TYP" in den einzelnen Objektarten).
   ausnahmeisa boolean, -- Das Objekt soll gemäß § 79 Abs. 3 TKG nicht im Infrastrukturatlas veröffentlicht werden. Das Objekt wird in einem separaten Datensatz „Ausnahme nach § 79 Abs. 3 TKG_Geodaten“ an die BNetzA geliefert.
   gehoertzuisa_fk bigint NOT NULL, -- Referenz auf den Infrastrukturatlas. zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   tiefe_hoehe integer NOT NULL, -- Angabe der Verlegetiefe (für POP, HVt, KVz, Zugangspunkt) oder der Höhe als positive Ganzzahl in cm. 0 = Information liegt nicht vor. Die Verlegetiefe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte für die Erreichbarkeit der unterirdischen Einrichtungen und hilft bei der Koordinierung von Bauarbeiten. Die Höhe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte ob sich eine Trägerstruktur für die oberirdische Verlegung von Glasfaser aber auch für die Errichtung von Standorten für drahtlose Zugangspunkte mit geringer Reichweite eignet.
   strom_fkcl text NOT NULL -- Das Attribut der Stromversorgung gibt der an einer Mitnutzung interessierten Person Anhaltspunkte ob sich eine Trägerstruktur für die Errichtung von Standorten für drahtlose Zugangspunkte mit geringer Reichweite eignet. Ob die Stromversorgung nur temporär geschaltet ist, ist für eine Aufnahme der Einrichtung in den ISA nicht entscheidend.
);

CREATE TABLE isa_reklametafel_litfasssauele_nutzung (

   _id bigserial NOT NULL PRIMARY KEY,
   isa_reklametafel_litfasssauele_id bigint NOT NULL,
   isa_nutzungtyp_id text NOT NULL -- Die Angabe zur gegenwärtigen Nutzung enthält die Information, für welchen Zweck die gelieferten Einrichtungen tatsächlich genutzt werden (z. B. Nutzung des Schutz-/Leerrohrs für TK-Zwecke oder als Schutz-/Leerrohr für die Elektrizitätsversorgung oder Leitungen für Fernwärme etc.). Eine Zuordnung der vorgegebenen Kategorien soll möglichst bezogen auf jede einzelne Infrastruktureinrichtung vorgenommen werden. Eine Mehrfacheinordnung ist auch weiterhin möglich, damit Einrichtungen, die aktuell für mehrere Zwecke genutzt werden, entsprechend erfasst werden können.
);

CREATE TABLE isa_richtfunkstrecke (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   foerderung_fkcl text NOT NULL, -- Das Kriterium der Förderung kennzeichnet einzelne Infrastrukturen, Leitungsabschnitte oder auch ganze Netzbereiche, die im Rahmen der Breitbandförderung finanziert wurden.
   verfuegbarkeit_fkcl text NOT NULL, -- Die tatsächliche Verfügbarkeit wird als Kapazitäts- bzw. Auslastungsangabe zu den Einrichtungen verstanden. Über vorgegebene Kategorien werden die tatsächlich vorhandenen Kapazitäten erfasst (ein Leer-/Schutzrohrabschnitt ist bspw. nur teilweise befüllt oder ein Bauwerk bietet als Technikraum noch Platz für TK-Infrastruktur und ist daher auf Anfrage verfügbar etc.).
   lagegenauigkeit_fkcl text, -- Lagegenauigkeit des Raumbezugs
   lagegenauigkeittext text, -- Textlich formulierte Lagegenauigkeit des Raumbezugs
   typ text, -- Es besteht die Möglichkeit zusätzlich nähere Spezifikationen zu den Infrastrukturen als TYP-Angaben in den ISA aufzunehmen. Diese sollten möglichst eindeutig benannt und den einzelnen Geometrien zugeordnet sein (s. Beispiele "TYP" in den einzelnen Objektarten).
   ausnahmeisa boolean, -- Das Objekt soll gemäß § 79 Abs. 3 TKG nicht im Infrastrukturatlas veröffentlicht werden. Das Objekt wird in einem separaten Datensatz „Ausnahme nach § 79 Abs. 3 TKG_Geodaten“ an die BNetzA geliefert.
   gehoertzuisa_fk bigint NOT NULL, -- Referenz auf den Infrastrukturatlas. zu dem das Objekt gehört
   position geometry(MULTILINESTRING,25832) NOT NULL -- Raumbezug des Objektes
);

CREATE TABLE isa_richtfunkstrecke_nutzung (

   _id bigserial NOT NULL PRIMARY KEY,
   isa_richtfunkstrecke_id bigint NOT NULL,
   isa_nutzungtyp_id text NOT NULL -- Die Angabe zur gegenwärtigen Nutzung enthält die Information, für welchen Zweck die gelieferten Einrichtungen tatsächlich genutzt werden (z. B. Nutzung des Schutz-/Leerrohrs für TK-Zwecke oder als Schutz-/Leerrohr für die Elektrizitätsversorgung oder Leitungen für Fernwärme etc.). Eine Zuordnung der vorgegebenen Kategorien soll möglichst bezogen auf jede einzelne Infrastruktureinrichtung vorgenommen werden. Eine Mehrfacheinordnung ist auch weiterhin möglich, damit Einrichtungen, die aktuell für mehrere Zwecke genutzt werden, entsprechend erfasst werden können.
);

CREATE TABLE isa_strassenlaterne (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   foerderung_fkcl text NOT NULL, -- Das Kriterium der Förderung kennzeichnet einzelne Infrastrukturen, Leitungsabschnitte oder auch ganze Netzbereiche, die im Rahmen der Breitbandförderung finanziert wurden.
   verfuegbarkeit_fkcl text NOT NULL, -- Die tatsächliche Verfügbarkeit wird als Kapazitäts- bzw. Auslastungsangabe zu den Einrichtungen verstanden. Über vorgegebene Kategorien werden die tatsächlich vorhandenen Kapazitäten erfasst (ein Leer-/Schutzrohrabschnitt ist bspw. nur teilweise befüllt oder ein Bauwerk bietet als Technikraum noch Platz für TK-Infrastruktur und ist daher auf Anfrage verfügbar etc.).
   lagegenauigkeit_fkcl text, -- Lagegenauigkeit des Raumbezugs
   lagegenauigkeittext text, -- Textlich formulierte Lagegenauigkeit des Raumbezugs
   typ text, -- Es besteht die Möglichkeit zusätzlich nähere Spezifikationen zu den Infrastrukturen als TYP-Angaben in den ISA aufzunehmen. Diese sollten möglichst eindeutig benannt und den einzelnen Geometrien zugeordnet sein (s. Beispiele "TYP" in den einzelnen Objektarten).
   ausnahmeisa boolean, -- Das Objekt soll gemäß § 79 Abs. 3 TKG nicht im Infrastrukturatlas veröffentlicht werden. Das Objekt wird in einem separaten Datensatz „Ausnahme nach § 79 Abs. 3 TKG_Geodaten“ an die BNetzA geliefert.
   gehoertzuisa_fk bigint NOT NULL, -- Referenz auf den Infrastrukturatlas. zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   tiefe_hoehe integer NOT NULL, -- Angabe der Verlegetiefe (für POP, HVt, KVz, Zugangspunkt) oder der Höhe als positive Ganzzahl in cm. 0 = Information liegt nicht vor. Die Verlegetiefe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte für die Erreichbarkeit der unterirdischen Einrichtungen und hilft bei der Koordinierung von Bauarbeiten. Die Höhe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte ob sich eine Trägerstruktur für die oberirdische Verlegung von Glasfaser aber auch für die Errichtung von Standorten für drahtlose Zugangspunkte mit geringer Reichweite eignet.
   strom_fkcl text NOT NULL -- Das Attribut der Stromversorgung gibt der an einer Mitnutzung interessierten Person Anhaltspunkte ob sich eine Trägerstruktur für die Errichtung von Standorten für drahtlose Zugangspunkte mit geringer Reichweite eignet. Ob die Stromversorgung nur temporär geschaltet ist, ist für eine Aufnahme der Einrichtung in den ISA nicht entscheidend.
);

CREATE TABLE isa_strassenlaterne_nutzung (

   _id bigserial NOT NULL PRIMARY KEY,
   isa_strassenlaterne_id bigint NOT NULL,
   isa_nutzungtyp_id text NOT NULL -- Die Angabe zur gegenwärtigen Nutzung enthält die Information, für welchen Zweck die gelieferten Einrichtungen tatsächlich genutzt werden (z. B. Nutzung des Schutz-/Leerrohrs für TK-Zwecke oder als Schutz-/Leerrohr für die Elektrizitätsversorgung oder Leitungen für Fernwärme etc.). Eine Zuordnung der vorgegebenen Kategorien soll möglichst bezogen auf jede einzelne Infrastruktureinrichtung vorgenommen werden. Eine Mehrfacheinordnung ist auch weiterhin möglich, damit Einrichtungen, die aktuell für mehrere Zwecke genutzt werden, entsprechend erfasst werden können.
);

CREATE TABLE isa_strassenmobiliar (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   foerderung_fkcl text NOT NULL, -- Das Kriterium der Förderung kennzeichnet einzelne Infrastrukturen, Leitungsabschnitte oder auch ganze Netzbereiche, die im Rahmen der Breitbandförderung finanziert wurden.
   verfuegbarkeit_fkcl text NOT NULL, -- Die tatsächliche Verfügbarkeit wird als Kapazitäts- bzw. Auslastungsangabe zu den Einrichtungen verstanden. Über vorgegebene Kategorien werden die tatsächlich vorhandenen Kapazitäten erfasst (ein Leer-/Schutzrohrabschnitt ist bspw. nur teilweise befüllt oder ein Bauwerk bietet als Technikraum noch Platz für TK-Infrastruktur und ist daher auf Anfrage verfügbar etc.).
   lagegenauigkeit_fkcl text, -- Lagegenauigkeit des Raumbezugs
   lagegenauigkeittext text, -- Textlich formulierte Lagegenauigkeit des Raumbezugs
   typ text, -- Es besteht die Möglichkeit zusätzlich nähere Spezifikationen zu den Infrastrukturen als TYP-Angaben in den ISA aufzunehmen. Diese sollten möglichst eindeutig benannt und den einzelnen Geometrien zugeordnet sein (s. Beispiele "TYP" in den einzelnen Objektarten).
   ausnahmeisa boolean, -- Das Objekt soll gemäß § 79 Abs. 3 TKG nicht im Infrastrukturatlas veröffentlicht werden. Das Objekt wird in einem separaten Datensatz „Ausnahme nach § 79 Abs. 3 TKG_Geodaten“ an die BNetzA geliefert.
   gehoertzuisa_fk bigint NOT NULL, -- Referenz auf den Infrastrukturatlas. zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   tiefe_hoehe integer NOT NULL, -- Angabe der Verlegetiefe (für POP, HVt, KVz, Zugangspunkt) oder der Höhe als positive Ganzzahl in cm. 0 = Information liegt nicht vor. Die Verlegetiefe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte für die Erreichbarkeit der unterirdischen Einrichtungen und hilft bei der Koordinierung von Bauarbeiten. Die Höhe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte ob sich eine Trägerstruktur für die oberirdische Verlegung von Glasfaser aber auch für die Errichtung von Standorten für drahtlose Zugangspunkte mit geringer Reichweite eignet.
   strom_fkcl text NOT NULL -- Das Attribut der Stromversorgung gibt der an einer Mitnutzung interessierten Person Anhaltspunkte ob sich eine Trägerstruktur für die Errichtung von Standorten für drahtlose Zugangspunkte mit geringer Reichweite eignet. Ob die Stromversorgung nur temporär geschaltet ist, ist für eine Aufnahme der Einrichtung in den ISA nicht entscheidend.
);

CREATE TABLE isa_strassenmobiliar_nutzung (

   _id bigserial NOT NULL PRIMARY KEY,
   isa_strassenmobiliar_id bigint NOT NULL,
   isa_nutzungtyp_id text NOT NULL -- Die Angabe zur gegenwärtigen Nutzung enthält die Information, für welchen Zweck die gelieferten Einrichtungen tatsächlich genutzt werden (z. B. Nutzung des Schutz-/Leerrohrs für TK-Zwecke oder als Schutz-/Leerrohr für die Elektrizitätsversorgung oder Leitungen für Fernwärme etc.). Eine Zuordnung der vorgegebenen Kategorien soll möglichst bezogen auf jede einzelne Infrastruktureinrichtung vorgenommen werden. Eine Mehrfacheinordnung ist auch weiterhin möglich, damit Einrichtungen, die aktuell für mehrere Zwecke genutzt werden, entsprechend erfasst werden können.
);

CREATE TABLE isa_stromversorgung (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE isa_verfuegbarkeittyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE isa_verkehrsschild (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   foerderung_fkcl text NOT NULL, -- Das Kriterium der Förderung kennzeichnet einzelne Infrastrukturen, Leitungsabschnitte oder auch ganze Netzbereiche, die im Rahmen der Breitbandförderung finanziert wurden.
   verfuegbarkeit_fkcl text NOT NULL, -- Die tatsächliche Verfügbarkeit wird als Kapazitäts- bzw. Auslastungsangabe zu den Einrichtungen verstanden. Über vorgegebene Kategorien werden die tatsächlich vorhandenen Kapazitäten erfasst (ein Leer-/Schutzrohrabschnitt ist bspw. nur teilweise befüllt oder ein Bauwerk bietet als Technikraum noch Platz für TK-Infrastruktur und ist daher auf Anfrage verfügbar etc.).
   lagegenauigkeit_fkcl text, -- Lagegenauigkeit des Raumbezugs
   lagegenauigkeittext text, -- Textlich formulierte Lagegenauigkeit des Raumbezugs
   typ text, -- Es besteht die Möglichkeit zusätzlich nähere Spezifikationen zu den Infrastrukturen als TYP-Angaben in den ISA aufzunehmen. Diese sollten möglichst eindeutig benannt und den einzelnen Geometrien zugeordnet sein (s. Beispiele "TYP" in den einzelnen Objektarten).
   ausnahmeisa boolean, -- Das Objekt soll gemäß § 79 Abs. 3 TKG nicht im Infrastrukturatlas veröffentlicht werden. Das Objekt wird in einem separaten Datensatz „Ausnahme nach § 79 Abs. 3 TKG_Geodaten“ an die BNetzA geliefert.
   gehoertzuisa_fk bigint NOT NULL, -- Referenz auf den Infrastrukturatlas. zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   tiefe_hoehe integer NOT NULL, -- Angabe der Verlegetiefe (für POP, HVt, KVz, Zugangspunkt) oder der Höhe als positive Ganzzahl in cm. 0 = Information liegt nicht vor. Die Verlegetiefe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte für die Erreichbarkeit der unterirdischen Einrichtungen und hilft bei der Koordinierung von Bauarbeiten. Die Höhe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte ob sich eine Trägerstruktur für die oberirdische Verlegung von Glasfaser aber auch für die Errichtung von Standorten für drahtlose Zugangspunkte mit geringer Reichweite eignet.
   strom_fkcl text NOT NULL -- Das Attribut der Stromversorgung gibt der an einer Mitnutzung interessierten Person Anhaltspunkte ob sich eine Trägerstruktur für die Errichtung von Standorten für drahtlose Zugangspunkte mit geringer Reichweite eignet. Ob die Stromversorgung nur temporär geschaltet ist, ist für eine Aufnahme der Einrichtung in den ISA nicht entscheidend.
);

CREATE TABLE isa_verkehrsschild_nutzung (

   _id bigserial NOT NULL PRIMARY KEY,
   isa_verkehrsschild_id bigint NOT NULL,
   isa_nutzungtyp_id text NOT NULL -- Die Angabe zur gegenwärtigen Nutzung enthält die Information, für welchen Zweck die gelieferten Einrichtungen tatsächlich genutzt werden (z. B. Nutzung des Schutz-/Leerrohrs für TK-Zwecke oder als Schutz-/Leerrohr für die Elektrizitätsversorgung oder Leitungen für Fernwärme etc.). Eine Zuordnung der vorgegebenen Kategorien soll möglichst bezogen auf jede einzelne Infrastruktureinrichtung vorgenommen werden. Eine Mehrfacheinordnung ist auch weiterhin möglich, damit Einrichtungen, die aktuell für mehrere Zwecke genutzt werden, entsprechend erfasst werden können.
);

CREATE TABLE isa_zugangspunkt (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   foerderung_fkcl text NOT NULL, -- Das Kriterium der Förderung kennzeichnet einzelne Infrastrukturen, Leitungsabschnitte oder auch ganze Netzbereiche, die im Rahmen der Breitbandförderung finanziert wurden.
   verfuegbarkeit_fkcl text NOT NULL, -- Die tatsächliche Verfügbarkeit wird als Kapazitäts- bzw. Auslastungsangabe zu den Einrichtungen verstanden. Über vorgegebene Kategorien werden die tatsächlich vorhandenen Kapazitäten erfasst (ein Leer-/Schutzrohrabschnitt ist bspw. nur teilweise befüllt oder ein Bauwerk bietet als Technikraum noch Platz für TK-Infrastruktur und ist daher auf Anfrage verfügbar etc.).
   lagegenauigkeit_fkcl text, -- Lagegenauigkeit des Raumbezugs
   lagegenauigkeittext text, -- Textlich formulierte Lagegenauigkeit des Raumbezugs
   typ text, -- Es besteht die Möglichkeit zusätzlich nähere Spezifikationen zu den Infrastrukturen als TYP-Angaben in den ISA aufzunehmen. Diese sollten möglichst eindeutig benannt und den einzelnen Geometrien zugeordnet sein (s. Beispiele "TYP" in den einzelnen Objektarten).
   ausnahmeisa boolean, -- Das Objekt soll gemäß § 79 Abs. 3 TKG nicht im Infrastrukturatlas veröffentlicht werden. Das Objekt wird in einem separaten Datensatz „Ausnahme nach § 79 Abs. 3 TKG_Geodaten“ an die BNetzA geliefert.
   gehoertzuisa_fk bigint NOT NULL, -- Referenz auf den Infrastrukturatlas. zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   tiefe_hoehe integer NOT NULL -- Angabe der Verlegetiefe (für POP, HVt, KVz, Zugangspunkt) oder der Höhe als positive Ganzzahl in cm. 0 = Information liegt nicht vor. Die Verlegetiefe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte für die Erreichbarkeit der unterirdischen Einrichtungen und hilft bei der Koordinierung von Bauarbeiten. Die Höhe gibt der an einer Mitnutzung interessierten Person Anhaltspunkte ob sich eine Trägerstruktur für die oberirdische Verlegung von Glasfaser aber auch für die Errichtung von Standorten für drahtlose Zugangspunkte mit geringer Reichweite eignet.
);

CREATE TABLE isa_zugangspunkt_nutzung (

   _id bigserial NOT NULL PRIMARY KEY,
   isa_zugangspunkt_id bigint NOT NULL,
   isa_nutzungtyp_id text NOT NULL -- Die Angabe zur gegenwärtigen Nutzung enthält die Information, für welchen Zweck die gelieferten Einrichtungen tatsächlich genutzt werden (z. B. Nutzung des Schutz-/Leerrohrs für TK-Zwecke oder als Schutz-/Leerrohr für die Elektrizitätsversorgung oder Leitungen für Fernwärme etc.). Eine Zuordnung der vorgegebenen Kategorien soll möglichst bezogen auf jede einzelne Infrastruktureinrichtung vorgenommen werden. Eine Mehrfacheinordnung ist auch weiterhin möglich, damit Einrichtungen, die aktuell für mehrere Zwecke genutzt werden, entsprechend erfasst werden können.
);

CREATE TABLE xp_armatureinsatzgebiet (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_armaturfunktion (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_baugrubetyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_bauweise (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_energiespeichertyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_gasdruckstufe (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_gastyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_gehaeusetyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_infrastrukturflaeche (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_kabeltyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_kraftwerktyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_legeverfahren (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_leitungtyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_planreferenz (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_isa_plan_fk bigint NOT NULL, -- Referenz auf den Netzplan, zu dem das Objekt gehört
   position geometry(POLYGON,25832) NOT NULL -- Raumbezug des Plans
);

CREATE TABLE xp_planreferenz_referenz (

   _id bigserial NOT NULL PRIMARY KEY,
   referenzname text NOT NULL, -- Name des referierten Dokument innerhalb des Informationssystems
   referenzurl text NOT NULL, -- URI des referierten Dokuments, bzw. Datenbank-Schlüssel. Wenn der XTrasseGML Datensatz und das referierte Dokument in einem hierarchischen Ordnersystem gespeichert sind, kann die URI auch einen relativen Pfad vom XPlanGML-Datensatz zum Dokument enthalten.
   beschreibung text, -- Beschreibung des referierten Dokuments
   datum date, -- Datum des referierten Dokuments
   xp_planreferenz_id bigint NOT NULL
);

CREATE TABLE xp_primaerenergietraeger (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_rohrleitungnetz (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_rolle (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_stationtyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_trassenquerschnitt (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_isa_plan_fk bigint NOT NULL, -- Referenz auf den Netzplan, zu dem das Objekt gehört
   position geometry(LINESTRING,25832) NOT NULL -- Verlauf des Trassenquerschnitts
);

CREATE TABLE xp_trassenquerschnitt_trassenquerschnitt (

   _id bigserial NOT NULL PRIMARY KEY,
   referenzname text NOT NULL, -- Name des referierten Dokument innerhalb des Informationssystems
   referenzurl text NOT NULL, -- URI des referierten Dokuments, bzw. Datenbank-Schlüssel. Wenn der XTrasseGML Datensatz und das referierte Dokument in einem hierarchischen Ordnersystem gespeichert sind, kann die URI auch einen relativen Pfad vom XPlanGML-Datensatz zum Dokument enthalten.
   beschreibung text, -- Beschreibung des referierten Dokuments
   datum date, -- Datum des referierten Dokuments
   xp_trassenquerschnitt_id bigint NOT NULL
);

CREATE TABLE xp_waermeleitungtyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_werkstoff (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);


ALTER TABLE isa_abwasserleitung ADD CONSTRAINT fk_isa_abwasserleitung_foerderung_fkcl FOREIGN KEY (foerderung_fkcl) REFERENCES isa_foerderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_abwasserleitung ADD CONSTRAINT fk_isa_abwasserleitung_gehoertzuisa_fk FOREIGN KEY (gehoertzuisa_fk) REFERENCES isa_plan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_abwasserleitung ADD CONSTRAINT fk_isa_abwasserleitung_lagegenauigkeit_fkcl FOREIGN KEY (lagegenauigkeit_fkcl) REFERENCES isa_lagegenauigkeit (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_abwasserleitung ADD CONSTRAINT fk_isa_abwasserleitung_verfuegbarkeit_fkcl FOREIGN KEY (verfuegbarkeit_fkcl) REFERENCES isa_verfuegbarkeittyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_abwasserleitung_nutzung ADD CONSTRAINT fk_isa_abwasserleitung_nutzung_isa_abwasserleitung_id FOREIGN KEY (isa_abwasserleitung_id) REFERENCES isa_abwasserleitung (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_abwasserleitung_nutzung ADD CONSTRAINT fk_isa_abwasserleitung_nutzung_isa_nutzungtyp_id FOREIGN KEY (isa_nutzungtyp_id) REFERENCES isa_nutzungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_ampel ADD CONSTRAINT fk_isa_ampel_foerderung_fkcl FOREIGN KEY (foerderung_fkcl) REFERENCES isa_foerderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_ampel ADD CONSTRAINT fk_isa_ampel_gehoertzuisa_fk FOREIGN KEY (gehoertzuisa_fk) REFERENCES isa_plan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_ampel ADD CONSTRAINT fk_isa_ampel_lagegenauigkeit_fkcl FOREIGN KEY (lagegenauigkeit_fkcl) REFERENCES isa_lagegenauigkeit (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_ampel ADD CONSTRAINT fk_isa_ampel_strom_fkcl FOREIGN KEY (strom_fkcl) REFERENCES isa_stromversorgung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_ampel ADD CONSTRAINT fk_isa_ampel_verfuegbarkeit_fkcl FOREIGN KEY (verfuegbarkeit_fkcl) REFERENCES isa_verfuegbarkeittyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_ampel_nutzung ADD CONSTRAINT fk_isa_ampel_nutzung_isa_ampel_id FOREIGN KEY (isa_ampel_id) REFERENCES isa_ampel (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_ampel_nutzung ADD CONSTRAINT fk_isa_ampel_nutzung_isa_nutzungtyp_id FOREIGN KEY (isa_nutzungtyp_id) REFERENCES isa_nutzungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_bauwerk ADD CONSTRAINT fk_isa_bauwerk_foerderung_fkcl FOREIGN KEY (foerderung_fkcl) REFERENCES isa_foerderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_bauwerk ADD CONSTRAINT fk_isa_bauwerk_gehoertzuisa_fk FOREIGN KEY (gehoertzuisa_fk) REFERENCES isa_plan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_bauwerk ADD CONSTRAINT fk_isa_bauwerk_lagegenauigkeit_fkcl FOREIGN KEY (lagegenauigkeit_fkcl) REFERENCES isa_lagegenauigkeit (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_bauwerk ADD CONSTRAINT fk_isa_bauwerk_strom_fkcl FOREIGN KEY (strom_fkcl) REFERENCES isa_stromversorgung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_bauwerk ADD CONSTRAINT fk_isa_bauwerk_verfuegbarkeit_fkcl FOREIGN KEY (verfuegbarkeit_fkcl) REFERENCES isa_verfuegbarkeittyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_bauwerk_nutzung ADD CONSTRAINT fk_isa_bauwerk_nutzung_isa_bauwerk_id FOREIGN KEY (isa_bauwerk_id) REFERENCES isa_bauwerk (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_bauwerk_nutzung ADD CONSTRAINT fk_isa_bauwerk_nutzung_isa_nutzungtyp_id FOREIGN KEY (isa_nutzungtyp_id) REFERENCES isa_nutzungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_funkmast ADD CONSTRAINT fk_isa_funkmast_foerderung_fkcl FOREIGN KEY (foerderung_fkcl) REFERENCES isa_foerderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_funkmast ADD CONSTRAINT fk_isa_funkmast_gehoertzuisa_fk FOREIGN KEY (gehoertzuisa_fk) REFERENCES isa_plan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_funkmast ADD CONSTRAINT fk_isa_funkmast_lagegenauigkeit_fkcl FOREIGN KEY (lagegenauigkeit_fkcl) REFERENCES isa_lagegenauigkeit (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_funkmast ADD CONSTRAINT fk_isa_funkmast_strom_fkcl FOREIGN KEY (strom_fkcl) REFERENCES isa_stromversorgung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_funkmast ADD CONSTRAINT fk_isa_funkmast_verfuegbarkeit_fkcl FOREIGN KEY (verfuegbarkeit_fkcl) REFERENCES isa_verfuegbarkeittyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_funkmast_nutzung ADD CONSTRAINT fk_isa_funkmast_nutzung_isa_funkmast_id FOREIGN KEY (isa_funkmast_id) REFERENCES isa_funkmast (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_funkmast_nutzung ADD CONSTRAINT fk_isa_funkmast_nutzung_isa_nutzungtyp_id FOREIGN KEY (isa_nutzungtyp_id) REFERENCES isa_nutzungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_glasfaser ADD CONSTRAINT fk_isa_glasfaser_foerderung_fkcl FOREIGN KEY (foerderung_fkcl) REFERENCES isa_foerderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_glasfaser ADD CONSTRAINT fk_isa_glasfaser_gehoertzuisa_fk FOREIGN KEY (gehoertzuisa_fk) REFERENCES isa_plan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_glasfaser ADD CONSTRAINT fk_isa_glasfaser_lagegenauigkeit_fkcl FOREIGN KEY (lagegenauigkeit_fkcl) REFERENCES isa_lagegenauigkeit (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_glasfaser ADD CONSTRAINT fk_isa_glasfaser_verfuegbarkeit_fkcl FOREIGN KEY (verfuegbarkeit_fkcl) REFERENCES isa_verfuegbarkeittyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_glasfaser_nutzung ADD CONSTRAINT fk_isa_glasfaser_nutzung_isa_glasfaser_id FOREIGN KEY (isa_glasfaser_id) REFERENCES isa_glasfaser (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_glasfaser_nutzung ADD CONSTRAINT fk_isa_glasfaser_nutzung_isa_nutzungtyp_id FOREIGN KEY (isa_nutzungtyp_id) REFERENCES isa_nutzungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_grundstueck_liegenschaft ADD CONSTRAINT fk_isa_grundstueck_liegenschaft_foerderung_fkcl FOREIGN KEY (foerderung_fkcl) REFERENCES isa_foerderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_grundstueck_liegenschaft ADD CONSTRAINT fk_isa_grundstueck_liegenschaft_gehoertzuisa_fk FOREIGN KEY (gehoertzuisa_fk) REFERENCES isa_plan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_grundstueck_liegenschaft ADD CONSTRAINT fk_isa_grundstueck_liegenschaft_lagegenauigkeit_fkcl FOREIGN KEY (lagegenauigkeit_fkcl) REFERENCES isa_lagegenauigkeit (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_grundstueck_liegenschaft ADD CONSTRAINT fk_isa_grundstueck_liegenschaft_strom_fkcl FOREIGN KEY (strom_fkcl) REFERENCES isa_stromversorgung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_grundstueck_liegenschaft ADD CONSTRAINT fk_isa_grundstueck_liegenschaft_verfuegbarkeit_fkcl FOREIGN KEY (verfuegbarkeit_fkcl) REFERENCES isa_verfuegbarkeittyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_grundstueck_liegenschaft_nutzung ADD CONSTRAINT fk_isa_grundstueck_liegenschaft_nutzung_isa_grundstueck_liegens FOREIGN KEY (isa_grundstueck_liegenschaft_id) REFERENCES isa_grundstueck_liegenschaft (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_grundstueck_liegenschaft_nutzung ADD CONSTRAINT fk_isa_grundstueck_liegenschaft_nutzung_isa_nutzungtyp_id FOREIGN KEY (isa_nutzungtyp_id) REFERENCES isa_nutzungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_haltestelle ADD CONSTRAINT fk_isa_haltestelle_foerderung_fkcl FOREIGN KEY (foerderung_fkcl) REFERENCES isa_foerderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_haltestelle ADD CONSTRAINT fk_isa_haltestelle_gehoertzuisa_fk FOREIGN KEY (gehoertzuisa_fk) REFERENCES isa_plan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_haltestelle ADD CONSTRAINT fk_isa_haltestelle_lagegenauigkeit_fkcl FOREIGN KEY (lagegenauigkeit_fkcl) REFERENCES isa_lagegenauigkeit (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_haltestelle ADD CONSTRAINT fk_isa_haltestelle_strom_fkcl FOREIGN KEY (strom_fkcl) REFERENCES isa_stromversorgung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_haltestelle ADD CONSTRAINT fk_isa_haltestelle_verfuegbarkeit_fkcl FOREIGN KEY (verfuegbarkeit_fkcl) REFERENCES isa_verfuegbarkeittyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_haltestelle_nutzung ADD CONSTRAINT fk_isa_haltestelle_nutzung_isa_haltestelle_id FOREIGN KEY (isa_haltestelle_id) REFERENCES isa_haltestelle (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_haltestelle_nutzung ADD CONSTRAINT fk_isa_haltestelle_nutzung_isa_nutzungtyp_id FOREIGN KEY (isa_nutzungtyp_id) REFERENCES isa_nutzungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_hauptverteiler ADD CONSTRAINT fk_isa_hauptverteiler_foerderung_fkcl FOREIGN KEY (foerderung_fkcl) REFERENCES isa_foerderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_hauptverteiler ADD CONSTRAINT fk_isa_hauptverteiler_gehoertzuisa_fk FOREIGN KEY (gehoertzuisa_fk) REFERENCES isa_plan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_hauptverteiler ADD CONSTRAINT fk_isa_hauptverteiler_lagegenauigkeit_fkcl FOREIGN KEY (lagegenauigkeit_fkcl) REFERENCES isa_lagegenauigkeit (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_hauptverteiler ADD CONSTRAINT fk_isa_hauptverteiler_verfuegbarkeit_fkcl FOREIGN KEY (verfuegbarkeit_fkcl) REFERENCES isa_verfuegbarkeittyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_hauptverteiler_nutzung ADD CONSTRAINT fk_isa_hauptverteiler_nutzung_isa_hauptverteiler_id FOREIGN KEY (isa_hauptverteiler_id) REFERENCES isa_hauptverteiler (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_hauptverteiler_nutzung ADD CONSTRAINT fk_isa_hauptverteiler_nutzung_isa_nutzungtyp_id FOREIGN KEY (isa_nutzungtyp_id) REFERENCES isa_nutzungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_holz_mast ADD CONSTRAINT fk_isa_holz_mast_foerderung_fkcl FOREIGN KEY (foerderung_fkcl) REFERENCES isa_foerderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_holz_mast ADD CONSTRAINT fk_isa_holz_mast_gehoertzuisa_fk FOREIGN KEY (gehoertzuisa_fk) REFERENCES isa_plan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_holz_mast ADD CONSTRAINT fk_isa_holz_mast_lagegenauigkeit_fkcl FOREIGN KEY (lagegenauigkeit_fkcl) REFERENCES isa_lagegenauigkeit (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_holz_mast ADD CONSTRAINT fk_isa_holz_mast_strom_fkcl FOREIGN KEY (strom_fkcl) REFERENCES isa_stromversorgung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_holz_mast ADD CONSTRAINT fk_isa_holz_mast_verfuegbarkeit_fkcl FOREIGN KEY (verfuegbarkeit_fkcl) REFERENCES isa_verfuegbarkeittyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_holz_mast_nutzung ADD CONSTRAINT fk_isa_holz_mast_nutzung_isa_holz_mast_id FOREIGN KEY (isa_holz_mast_id) REFERENCES isa_holz_mast (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_holz_mast_nutzung ADD CONSTRAINT fk_isa_holz_mast_nutzung_isa_nutzungtyp_id FOREIGN KEY (isa_nutzungtyp_id) REFERENCES isa_nutzungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_kabelverzweiger ADD CONSTRAINT fk_isa_kabelverzweiger_foerderung_fkcl FOREIGN KEY (foerderung_fkcl) REFERENCES isa_foerderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_kabelverzweiger ADD CONSTRAINT fk_isa_kabelverzweiger_gehoertzuisa_fk FOREIGN KEY (gehoertzuisa_fk) REFERENCES isa_plan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_kabelverzweiger ADD CONSTRAINT fk_isa_kabelverzweiger_lagegenauigkeit_fkcl FOREIGN KEY (lagegenauigkeit_fkcl) REFERENCES isa_lagegenauigkeit (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_kabelverzweiger ADD CONSTRAINT fk_isa_kabelverzweiger_verfuegbarkeit_fkcl FOREIGN KEY (verfuegbarkeit_fkcl) REFERENCES isa_verfuegbarkeittyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_kabelverzweiger_nutzung ADD CONSTRAINT fk_isa_kabelverzweiger_nutzung_isa_kabelverzweiger_id FOREIGN KEY (isa_kabelverzweiger_id) REFERENCES isa_kabelverzweiger (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_kabelverzweiger_nutzung ADD CONSTRAINT fk_isa_kabelverzweiger_nutzung_isa_nutzungtyp_id FOREIGN KEY (isa_nutzungtyp_id) REFERENCES isa_nutzungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_lehrrohr ADD CONSTRAINT fk_isa_lehrrohr_foerderung_fkcl FOREIGN KEY (foerderung_fkcl) REFERENCES isa_foerderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_lehrrohr ADD CONSTRAINT fk_isa_lehrrohr_gehoertzuisa_fk FOREIGN KEY (gehoertzuisa_fk) REFERENCES isa_plan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_lehrrohr ADD CONSTRAINT fk_isa_lehrrohr_lagegenauigkeit_fkcl FOREIGN KEY (lagegenauigkeit_fkcl) REFERENCES isa_lagegenauigkeit (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_lehrrohr ADD CONSTRAINT fk_isa_lehrrohr_verfuegbarkeit_fkcl FOREIGN KEY (verfuegbarkeit_fkcl) REFERENCES isa_verfuegbarkeittyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_lehrrohr_nutzung ADD CONSTRAINT fk_isa_lehrrohr_nutzung_isa_lehrrohr_id FOREIGN KEY (isa_lehrrohr_id) REFERENCES isa_lehrrohr (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_lehrrohr_nutzung ADD CONSTRAINT fk_isa_lehrrohr_nutzung_isa_nutzungtyp_id FOREIGN KEY (isa_nutzungtyp_id) REFERENCES isa_nutzungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_plan ADD CONSTRAINT fk_isa_plan_inhabertyp_fkcl FOREIGN KEY (inhabertyp_fkcl) REFERENCES isa_inhabertyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_plan_externereferenz ADD CONSTRAINT fk_isa_plan_externereferenz_isa_plan_id FOREIGN KEY (isa_plan_id) REFERENCES isa_plan (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_plan_gesetzlichegrundlage ADD CONSTRAINT fk_isa_plan_gesetzlichegrundlage_isa_plan_id FOREIGN KEY (isa_plan_id) REFERENCES isa_plan (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_plan_kontaktgis ADD CONSTRAINT fk_isa_plan_kontaktgis_isa_plan_id FOREIGN KEY (isa_plan_id) REFERENCES isa_plan (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_plan_kontaktmitnutzung ADD CONSTRAINT fk_isa_plan_kontaktmitnutzung_isa_plan_id FOREIGN KEY (isa_plan_id) REFERENCES isa_plan (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_pointofpresence ADD CONSTRAINT fk_isa_pointofpresence_foerderung_fkcl FOREIGN KEY (foerderung_fkcl) REFERENCES isa_foerderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_pointofpresence ADD CONSTRAINT fk_isa_pointofpresence_gehoertzuisa_fk FOREIGN KEY (gehoertzuisa_fk) REFERENCES isa_plan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_pointofpresence ADD CONSTRAINT fk_isa_pointofpresence_lagegenauigkeit_fkcl FOREIGN KEY (lagegenauigkeit_fkcl) REFERENCES isa_lagegenauigkeit (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_pointofpresence ADD CONSTRAINT fk_isa_pointofpresence_verfuegbarkeit_fkcl FOREIGN KEY (verfuegbarkeit_fkcl) REFERENCES isa_verfuegbarkeittyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_pointofpresence_nutzung ADD CONSTRAINT fk_isa_pointofpresence_nutzung_isa_nutzungtyp_id FOREIGN KEY (isa_nutzungtyp_id) REFERENCES isa_nutzungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_pointofpresence_nutzung ADD CONSTRAINT fk_isa_pointofpresence_nutzung_isa_pointofpresence_id FOREIGN KEY (isa_pointofpresence_id) REFERENCES isa_pointofpresence (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_reklametafel_litfasssauele ADD CONSTRAINT fk_isa_reklametafel_litfasssauele_foerderung_fkcl FOREIGN KEY (foerderung_fkcl) REFERENCES isa_foerderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_reklametafel_litfasssauele ADD CONSTRAINT fk_isa_reklametafel_litfasssauele_gehoertzuisa_fk FOREIGN KEY (gehoertzuisa_fk) REFERENCES isa_plan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_reklametafel_litfasssauele ADD CONSTRAINT fk_isa_reklametafel_litfasssauele_lagegenauigkeit_fkcl FOREIGN KEY (lagegenauigkeit_fkcl) REFERENCES isa_lagegenauigkeit (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_reklametafel_litfasssauele ADD CONSTRAINT fk_isa_reklametafel_litfasssauele_strom_fkcl FOREIGN KEY (strom_fkcl) REFERENCES isa_stromversorgung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_reklametafel_litfasssauele ADD CONSTRAINT fk_isa_reklametafel_litfasssauele_verfuegbarkeit_fkcl FOREIGN KEY (verfuegbarkeit_fkcl) REFERENCES isa_verfuegbarkeittyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_reklametafel_litfasssauele_nutzung ADD CONSTRAINT fk_isa_reklametafel_litfasssauele_nutzung_isa_nutzungtyp_id FOREIGN KEY (isa_nutzungtyp_id) REFERENCES isa_nutzungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_reklametafel_litfasssauele_nutzung ADD CONSTRAINT fk_isa_reklametafel_litfasssauele_nutzung_isa_reklametafel_litf FOREIGN KEY (isa_reklametafel_litfasssauele_id) REFERENCES isa_reklametafel_litfasssauele (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_richtfunkstrecke ADD CONSTRAINT fk_isa_richtfunkstrecke_foerderung_fkcl FOREIGN KEY (foerderung_fkcl) REFERENCES isa_foerderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_richtfunkstrecke ADD CONSTRAINT fk_isa_richtfunkstrecke_gehoertzuisa_fk FOREIGN KEY (gehoertzuisa_fk) REFERENCES isa_plan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_richtfunkstrecke ADD CONSTRAINT fk_isa_richtfunkstrecke_lagegenauigkeit_fkcl FOREIGN KEY (lagegenauigkeit_fkcl) REFERENCES isa_lagegenauigkeit (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_richtfunkstrecke ADD CONSTRAINT fk_isa_richtfunkstrecke_verfuegbarkeit_fkcl FOREIGN KEY (verfuegbarkeit_fkcl) REFERENCES isa_verfuegbarkeittyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_richtfunkstrecke_nutzung ADD CONSTRAINT fk_isa_richtfunkstrecke_nutzung_isa_nutzungtyp_id FOREIGN KEY (isa_nutzungtyp_id) REFERENCES isa_nutzungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_richtfunkstrecke_nutzung ADD CONSTRAINT fk_isa_richtfunkstrecke_nutzung_isa_richtfunkstrecke_id FOREIGN KEY (isa_richtfunkstrecke_id) REFERENCES isa_richtfunkstrecke (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_strassenlaterne ADD CONSTRAINT fk_isa_strassenlaterne_foerderung_fkcl FOREIGN KEY (foerderung_fkcl) REFERENCES isa_foerderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_strassenlaterne ADD CONSTRAINT fk_isa_strassenlaterne_gehoertzuisa_fk FOREIGN KEY (gehoertzuisa_fk) REFERENCES isa_plan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_strassenlaterne ADD CONSTRAINT fk_isa_strassenlaterne_lagegenauigkeit_fkcl FOREIGN KEY (lagegenauigkeit_fkcl) REFERENCES isa_lagegenauigkeit (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_strassenlaterne ADD CONSTRAINT fk_isa_strassenlaterne_strom_fkcl FOREIGN KEY (strom_fkcl) REFERENCES isa_stromversorgung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_strassenlaterne ADD CONSTRAINT fk_isa_strassenlaterne_verfuegbarkeit_fkcl FOREIGN KEY (verfuegbarkeit_fkcl) REFERENCES isa_verfuegbarkeittyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_strassenlaterne_nutzung ADD CONSTRAINT fk_isa_strassenlaterne_nutzung_isa_nutzungtyp_id FOREIGN KEY (isa_nutzungtyp_id) REFERENCES isa_nutzungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_strassenlaterne_nutzung ADD CONSTRAINT fk_isa_strassenlaterne_nutzung_isa_strassenlaterne_id FOREIGN KEY (isa_strassenlaterne_id) REFERENCES isa_strassenlaterne (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_strassenmobiliar ADD CONSTRAINT fk_isa_strassenmobiliar_foerderung_fkcl FOREIGN KEY (foerderung_fkcl) REFERENCES isa_foerderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_strassenmobiliar ADD CONSTRAINT fk_isa_strassenmobiliar_gehoertzuisa_fk FOREIGN KEY (gehoertzuisa_fk) REFERENCES isa_plan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_strassenmobiliar ADD CONSTRAINT fk_isa_strassenmobiliar_lagegenauigkeit_fkcl FOREIGN KEY (lagegenauigkeit_fkcl) REFERENCES isa_lagegenauigkeit (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_strassenmobiliar ADD CONSTRAINT fk_isa_strassenmobiliar_strom_fkcl FOREIGN KEY (strom_fkcl) REFERENCES isa_stromversorgung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_strassenmobiliar ADD CONSTRAINT fk_isa_strassenmobiliar_verfuegbarkeit_fkcl FOREIGN KEY (verfuegbarkeit_fkcl) REFERENCES isa_verfuegbarkeittyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_strassenmobiliar_nutzung ADD CONSTRAINT fk_isa_strassenmobiliar_nutzung_isa_nutzungtyp_id FOREIGN KEY (isa_nutzungtyp_id) REFERENCES isa_nutzungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_strassenmobiliar_nutzung ADD CONSTRAINT fk_isa_strassenmobiliar_nutzung_isa_strassenmobiliar_id FOREIGN KEY (isa_strassenmobiliar_id) REFERENCES isa_strassenmobiliar (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_verkehrsschild ADD CONSTRAINT fk_isa_verkehrsschild_foerderung_fkcl FOREIGN KEY (foerderung_fkcl) REFERENCES isa_foerderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_verkehrsschild ADD CONSTRAINT fk_isa_verkehrsschild_gehoertzuisa_fk FOREIGN KEY (gehoertzuisa_fk) REFERENCES isa_plan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_verkehrsschild ADD CONSTRAINT fk_isa_verkehrsschild_lagegenauigkeit_fkcl FOREIGN KEY (lagegenauigkeit_fkcl) REFERENCES isa_lagegenauigkeit (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_verkehrsschild ADD CONSTRAINT fk_isa_verkehrsschild_strom_fkcl FOREIGN KEY (strom_fkcl) REFERENCES isa_stromversorgung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_verkehrsschild ADD CONSTRAINT fk_isa_verkehrsschild_verfuegbarkeit_fkcl FOREIGN KEY (verfuegbarkeit_fkcl) REFERENCES isa_verfuegbarkeittyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_verkehrsschild_nutzung ADD CONSTRAINT fk_isa_verkehrsschild_nutzung_isa_nutzungtyp_id FOREIGN KEY (isa_nutzungtyp_id) REFERENCES isa_nutzungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_verkehrsschild_nutzung ADD CONSTRAINT fk_isa_verkehrsschild_nutzung_isa_verkehrsschild_id FOREIGN KEY (isa_verkehrsschild_id) REFERENCES isa_verkehrsschild (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_zugangspunkt ADD CONSTRAINT fk_isa_zugangspunkt_foerderung_fkcl FOREIGN KEY (foerderung_fkcl) REFERENCES isa_foerderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_zugangspunkt ADD CONSTRAINT fk_isa_zugangspunkt_gehoertzuisa_fk FOREIGN KEY (gehoertzuisa_fk) REFERENCES isa_plan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_zugangspunkt ADD CONSTRAINT fk_isa_zugangspunkt_lagegenauigkeit_fkcl FOREIGN KEY (lagegenauigkeit_fkcl) REFERENCES isa_lagegenauigkeit (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_zugangspunkt ADD CONSTRAINT fk_isa_zugangspunkt_verfuegbarkeit_fkcl FOREIGN KEY (verfuegbarkeit_fkcl) REFERENCES isa_verfuegbarkeittyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_zugangspunkt_nutzung ADD CONSTRAINT fk_isa_zugangspunkt_nutzung_isa_nutzungtyp_id FOREIGN KEY (isa_nutzungtyp_id) REFERENCES isa_nutzungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE isa_zugangspunkt_nutzung ADD CONSTRAINT fk_isa_zugangspunkt_nutzung_isa_zugangspunkt_id FOREIGN KEY (isa_zugangspunkt_id) REFERENCES isa_zugangspunkt (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE xp_planreferenz ADD CONSTRAINT fk_xp_planreferenz_gehoertzuplan_isa_plan_fk FOREIGN KEY (gehoertzuplan_isa_plan_fk) REFERENCES isa_plan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE xp_planreferenz_referenz ADD CONSTRAINT fk_xp_planreferenz_referenz_xp_planreferenz_id FOREIGN KEY (xp_planreferenz_id) REFERENCES xp_planreferenz (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE xp_trassenquerschnitt ADD CONSTRAINT fk_xp_trassenquerschnitt_gehoertzuplan_isa_plan_fk FOREIGN KEY (gehoertzuplan_isa_plan_fk) REFERENCES isa_plan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE xp_trassenquerschnitt_trassenquerschnitt ADD CONSTRAINT fk_xp_trassenquerschnitt_trassenquerschnitt_xp_trassenquerschni FOREIGN KEY (xp_trassenquerschnitt_id) REFERENCES xp_trassenquerschnitt (_id) DEFERRABLE INITIALLY DEFERRED;

INSERT INTO isa_foerderung (code, model, documentation, description) VALUES ('1000', 'Gefoerdert', 'gefördert im Rahmen des Breitbandausbaus', 'gefördert');
INSERT INTO isa_foerderung (code, model, documentation, description) VALUES ('2000', 'TeilweiseGefoerdert', 'teilweise gefördert im Rahmen des Breitbandausbaus', 'teilweise gefördert');
INSERT INTO isa_foerderung (code, model, documentation, description) VALUES ('3000', 'NichtGefoerdert', 'nicht gefördert', 'nicht gefördert');
INSERT INTO isa_inhabertyp (code, model, documentation, description) VALUES ('1000', 'Eigentuemer', 'Eigentümer', 'Eigentümer');
INSERT INTO isa_inhabertyp (code, model, documentation, description) VALUES ('2000', 'Betreiber', 'Betreiber', 'Betreiber');
INSERT INTO isa_lagegenauigkeit (code, model, documentation, description) VALUES ('1000', 'bis_10_CM', 'Lagegenauigkeit bis 10 cm', 'bis 10 cm ');
INSERT INTO isa_lagegenauigkeit (code, model, documentation, description) VALUES ('2000', 'bis_1_M', 'Lagegenauigkeit bis zu 1 m', 'bis 1 m');
INSERT INTO isa_lagegenauigkeit (code, model, documentation, description) VALUES ('3000', 'bis_10_M', 'Lagegenauigkeit bis zu 10 m', 'bis 10 m');
INSERT INTO isa_lagegenauigkeit (code, model, documentation, description) VALUES ('4000', 'ueber_10_M', 'Lagegenauigkeit schlechter als 10 m', 'über 10 m');
INSERT INTO isa_nutzungtyp (code, model, documentation, description) VALUES ('1000', 'Telekommunikation', 'Telekommunikation', 'Telekommunikation');
INSERT INTO isa_nutzungtyp (code, model, documentation, description) VALUES ('2000', 'Gas', 'Gasversorgung', 'Gas');
INSERT INTO isa_nutzungtyp (code, model, documentation, description) VALUES ('3000', 'Elektrizitaet', 'Stromversorgung', 'Elektrizität');
INSERT INTO isa_nutzungtyp (code, model, documentation, description) VALUES ('4000', 'Fernwaerme', 'Versorgung mit Fernwärme', 'Fernwärme');
INSERT INTO isa_nutzungtyp (code, model, documentation, description) VALUES ('5000', 'Wasser_Abwasser', 'Trinkwasserversorgung  und Abwasserentsorgung', 'Trinkwasserversorgung und Abwasserentsorgung');
INSERT INTO isa_nutzungtyp (code, model, documentation, description) VALUES ('6000', 'Verkehr', 'Verkehr', 'Verkehr');
INSERT INTO isa_nutzungtyp (code, model, documentation, description) VALUES ('9999', 'Sonstige', 'Die Kategorie „Sonstige“ dient der Aufnahme von Einrichtungen, die zum Zeitpunkt der Datenlieferung (noch) keiner gegenwärtigen Nutzung zugeordnet werden können. Darunter fallen z.B. öffentliche Gebäude/Grundstücke oder Leerrohre, die nur als Reserve mitverlegt wurden.', 'Sonstiges Netz');
INSERT INTO isa_stromversorgung (code, model, documentation, description) VALUES ('1000', 'StromVorhanden', 'Stromversorgung ist vorhanden', 'Strom vorhanden');
INSERT INTO isa_stromversorgung (code, model, documentation, description) VALUES ('2000', 'KeinStrom', 'Keine Stromversorgung vorhanden', 'kein Strom');
INSERT INTO isa_stromversorgung (code, model, documentation, description) VALUES ('9999', 'keineAngabe', 'Informationen liegen nicht vor', 'keine Angabe');
INSERT INTO isa_verfuegbarkeittyp (code, model, documentation, description) VALUES ('1000', 'NichtVerfuegbar_Belegt', 'belegt', 'nicht verfügbar - belegt');
INSERT INTO isa_verfuegbarkeittyp (code, model, documentation, description) VALUES ('2000', 'NichtVerfuegbar_Reserviert', 'für eigene Planung reserviert - nicht verfügbar', 'nicht verfügbar - für eigene Planung reserviert');
INSERT INTO isa_verfuegbarkeittyp (code, model, documentation, description) VALUES ('3000', 'Verfuegbar_Teilweise', 'teilweise verfügbar', 'verfügbar - verfügbar');
INSERT INTO isa_verfuegbarkeittyp (code, model, documentation, description) VALUES ('4000', 'Verfuegbar_AufAnfrage', 'auf Anfrage verfügbar', 'verfügbar - auf Anfrage');
INSERT INTO isa_verfuegbarkeittyp (code, model, documentation, description) VALUES ('5000', 'Verfuegbar_ZurMitnutzungAngeboten', 'Kapazitäten werden zur Mitnutzung angeboten', 'verfuegbar - zur Mitnutzung angeboten');
INSERT INTO xp_armatureinsatzgebiet (code, model, documentation, description) VALUES ('1000', 'Streckenarmatur', 'Armaturen in Abständen entlang einer Leitung', 'Streckenarmatur');
INSERT INTO xp_armatureinsatzgebiet (code, model, documentation, description) VALUES ('2000', 'Ausblasearmatur', 'Dient dem kontrollierten Ableiten von Gasen und Gas-Luftgemischen innerhalb eines Rohrnetzes', 'Ausblasearmatur');
INSERT INTO xp_armatureinsatzgebiet (code, model, documentation, description) VALUES ('3000', 'Hauptabsperreinrichtung', 'Hauptabsperreinrichtung', 'Hauptabsperreinrichtung');
INSERT INTO xp_armatureinsatzgebiet (code, model, documentation, description) VALUES ('4000', 'Ein_Ausgangsarmatur', 'Eingangs- und Ausgangsarmaturen im Rohrnetz', 'Ein-/ Ausgangsarmatur');
INSERT INTO xp_armatureinsatzgebiet (code, model, documentation, description) VALUES ('5000', 'Hydrant', 'Hydrant', 'Hydrant');
INSERT INTO xp_armatureinsatzgebiet (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'sonstiges Einsatzgebiet', 'sonstiges Einsatzgebiet');
INSERT INTO xp_armaturfunktion (code, model, documentation, description) VALUES ('1000', 'Absperrarmatur', 'Absperrung von Stoffströmen durch Hähne und Klappen', 'Absperramatur');
INSERT INTO xp_armaturfunktion (code, model, documentation, description) VALUES ('2000', 'Regulierarmatur', 'Regulierung des Volumenstroms mittels Schieber und Ventilen', 'Regulierarmatur');
INSERT INTO xp_armaturfunktion (code, model, documentation, description) VALUES ('3000', 'Entlueftungsarmatur', 'Dient dem Enfernen von Gasen, insbesondere Luft, aus einer flüssigkeitsführenden Anlage', 'Entlüftungsarmatur');
INSERT INTO xp_armaturfunktion (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'sonstige Funktion', 'sonstige Funktion');
INSERT INTO xp_baugrubetyp (code, model, documentation, description) VALUES ('1000', 'Startgrube', 'Startgrube', 'Startgrube');
INSERT INTO xp_baugrubetyp (code, model, documentation, description) VALUES ('2000', 'Zielgrube', 'Zielgrube', 'Zielgrube');
INSERT INTO xp_bauweise (code, model, documentation, description) VALUES ('1000', 'OffeneBauweise', 'offene Bauweise', 'offene Bauweise');
INSERT INTO xp_bauweise (code, model, documentation, description) VALUES ('2000', 'GeschlosseneBauweise', 'geschlossene Bauweise', 'geschlossene Bauweise');
INSERT INTO xp_bauweise (code, model, documentation, description) VALUES ('3000', 'Oberirdisch', 'oberirdische Verlegung', 'oberirdische Verlegung');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('1000', 'Gasspeicher', 'Oberirdische Nieder- und Mitteldruckbehälter (Gastürme, Gasometer) sowie Hochdruckbehälter (Röhrenspeicher, Kugelspeicher) zur Aufbewahrung von Gasen aller Art', 'Gasspeicher');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('2000', 'Untergrundspeicher', 'Ein Untergrundspeicher (auch Untertagespeicher) ist ein Speicher in natürlichen oder künstlichen Hohlräumen unter der Erdoberfläche. - Untergrundspeicher gemäß Bundesberggesetz (BBergG) § 126', 'Untergrundspeicher');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('20001', 'Kavernenspeicher', 'Große, künstlich angelegte Hohlräume in mächtigen unterirdischen Salzformationen, wie z.B. Salzstöcken. Kavernenspeicher werden durch einen Solprozess bergmännisch angelegt.', 'Kavernenspeicher');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('20002', 'Porenspeicher', 'Natürliche Lagerstätten, die sich durch ihre geologische Formation zur Speicherung von Gas eignen. Sie befinden sich in porösem Gestein, in dem das Gas ähnlich einem stabilen Schwamm aufgenommen und eingelagert wird.', 'Porenspeicher');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('3000', 'Stromspeicher', 'Großspeicheranlagen im Stromnetz', 'Stromspeicher');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('30001', 'Batteriespeicher', 'Großbatteriespeicher (z.B. an einer PV-Anlage)', 'Batteriespeicher');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('30002', 'Pumpspeicherkraftwerk', 'Ein Pumpspeicherkraftwerk (PSW) speichert elektrische Energie in Form von potentieller Energie (Lageenergie) in einem Stausee', 'Pumpspeicherkraftwerk');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('4000', 'Fernwaermespeicher', 'Zumeist drucklose, mit Wasser gefüllte Behälter, die Schwankungen im Wärmebedarf des Fernwärmenetzes bei gleicher Erzeugungsleistung der Fernheizwerke ausgleichen sollen', 'Fernwärmespeicher');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstige Speicher', 'sonstige Speicher');
INSERT INTO xp_gasdruckstufe (code, model, documentation, description) VALUES ('1000', 'Niederdruck', 'Niederdruck', 'Niederdruck');
INSERT INTO xp_gasdruckstufe (code, model, documentation, description) VALUES ('2000', 'Mitteldruck', 'Mitteldruck', 'Mitteldruck');
INSERT INTO xp_gasdruckstufe (code, model, documentation, description) VALUES ('3000', 'Hochdruck', 'Hochdruck', 'Hochdruck');
INSERT INTO xp_gasdruckstufe (code, model, documentation, description) VALUES ('9999', 'UnbekannterDruck', 'Unbekannter Druck', 'Unbekannter Druck');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('1000', 'Erdgas', 'Erdgas', 'Erdgas');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('10001', 'L_Gas', 'L-Gas (low calorific gas)', 'L-Gas');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('10002', 'H_Gas', 'H-Gas (high calorific gas)', 'H-Gas');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('2000', 'Wasserstoff', 'Wasserstoff (H2)', 'Wasserstoff');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('20001', 'GruenerWasserstoff', 'Durch die Elektrolyse von Wasser hergestellter Wasserstoff unter Verwendung von Strom aus erneuerbaren Energiequellen', 'grüner Wasserstoff');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('20002', 'BlauerWasserstoff', 'Grauer Wasserstoff, bei dessen Entstehung das CO2 jedoch teilweise abgeschieden und im Erdboden gespeichert wird (CCS, Carbon Capture and Storage). Maximal 90 Prozent des CO₂ sind speicherbar.', 'blauer Wasserstoff');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('20003', 'OrangenerWasserstoff', 'Auf Basis von Abfall und Reststoffen produzierter Wasserstoff, der als CO2-frei gilt', 'orangener Wasserstoff');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('20004', 'GrauerWasserstoff', 'Mittels Dampfreformierung meist aus fossilem Erdgas hergestellter Wasserstoff. Dabei entstehen rund 10 Tonnen CO₂ pro Tonne Wasserstoff. Das CO2 wird in die Atmosphäre abgegeben.', 'grauer Wasserstoff');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('3000', 'Erdgas_H2_Gemisch', 'Erdgas-Wasserstoff-Gemisch', 'Erdgas-Wasserstoff-Gemisch');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('4000', 'Biogas', 'Biogas', 'Biogas');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('5000', 'Fluessiggas', 'Flüssiggas', 'Flüssiggas');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('6000', 'SynthetischesMethan', 'Wird durch wasserelektrolytisch erzeugten Wasserstoff und anschließende Methanisierung hergestellt', 'synthetisch erzeugtes Methan');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'sonstiges Gas', 'sonstiges Gas');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('1000', 'TK_Verteiler', 'Verteilerschränke der Telekommunikation', 'TK-Verteiler');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('10001', 'Multifunktionsgehaeuse', 'Multifunktionsgehäuse', 'Multifunktionsgehäuse');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('10002', 'GlasfaserNetzverteiler', 'Glasfaser-Netzverteiler (Gf-NVt)', 'Glasfaser-Netzverteiler (Gf- NVt)');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('10003', 'Kabelverzweiger_KVz', 'Kabelverzweiger (KVz) - (Telekom AG)', 'Kabelverzweiger ( KVz) - (Telekom  AG)');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('2000', 'Strom_Schrank', 'Schränke für die Stromversorgung, öffentliche Beleuchtung, Verkehrstechnik u.a.', 'Strom-Schrank');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('20001', 'Schaltschrank', 'Schaltschrank', 'Schaltschrank');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('20002', 'Kabelverteilerschrank', 'Kabelverteilerschrank', 'Kabelverteilerschrank');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('20003', 'Steuerschrank', 'Steuerschrank', 'Steuerschrank');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('20004', 'Trennschrank', 'Trennschrank', 'Trennschrank');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'sonstiger Schrank', 'sonstiger Schrank');
INSERT INTO xp_infrastrukturflaeche (code, model, documentation, description) VALUES ('1000', 'Betriebsgelaende', 'gesamtes Betriebsgelände bzw. Grundstücksfläche', 'Betriebsgelände');
INSERT INTO xp_infrastrukturflaeche (code, model, documentation, description) VALUES ('2000', 'EingezaeunteFlaeche', 'eingezäuntes Gelände der Infrastrukturgebäude (ohne Parkplätze und Nebengebäude)', 'eingezäunte Fläche');
INSERT INTO xp_infrastrukturflaeche (code, model, documentation, description) VALUES ('3000', 'Gebaeudeflaeche', 'Fläche eines Gebäudes, das technische Anlagen enthält', 'Gebäudefläche');
INSERT INTO xp_kabeltyp (code, model, documentation, description) VALUES ('1000', 'Glasfaserkabel', 'Glasfaserkabel', 'Glasfaserkabel');
INSERT INTO xp_kabeltyp (code, model, documentation, description) VALUES ('2000', 'Kupferkabel', 'Kupferkabel', 'Kupferkabel');
INSERT INTO xp_kabeltyp (code, model, documentation, description) VALUES ('3000', 'Hybridkabel', 'Hybridkabel', 'Hybridkabel');
INSERT INTO xp_kabeltyp (code, model, documentation, description) VALUES ('4000', 'Koaxialkabel', 'Koaxial-(TV)-Kabel', 'Koaxial-(TV)-Kabel');
INSERT INTO xp_kraftwerktyp (code, model, documentation, description) VALUES ('1000', 'ThermischeTurbine', 'Thermische arbeitende Dampfturbinen- und Gasturbinen-Kraftwerke oder Gas-und-Dampf-Kombikraftwerke', 'Thermisch arbeitende Turbine');
INSERT INTO xp_kraftwerktyp (code, model, documentation, description) VALUES ('2000', 'Windkraft', 'Eine Windkraftanlage (WKA) oder Windenergieanlage (WEA) wandelt Bewegungsenergie des Windes in elektrische Energie um und speist sie in ein Stromnetz ein. Sie werden an Land (onshore) und in Offshore-Windparks im Küstenvorfeld der Meere installiert. Eine Gruppe von Windkraftanlagen wird Windpark genannt.', 'Windkraftanlage');
INSERT INTO xp_kraftwerktyp (code, model, documentation, description) VALUES ('3000', 'Photovoltaik', 'Eine Photovoltaikanlage, auch PV-Anlage (bzw. PVA) wandelt mittels Solarzellen ein Teil der Sonnenstrahlung in elektrische Energie um.  Die Photovoltaik-Freiflächenanlage (auch Solarpark) wird auf einer freien Fläche als fest montiertes System aufgestellt, bei dem mittels einer Unterkonstruktion die Photovoltaikmodule in einem optimalen Winkel zur Sonne (Azimut) ausgerichtet sind.', 'Photovoltaik-Freinflächenanlage');
INSERT INTO xp_kraftwerktyp (code, model, documentation, description) VALUES ('4000', 'Wasserkraft', 'Ein Wasserkraftwerk wandelt die potentielle Energie des Wassers in der Regel über Turbinen in mechanische bzw. elektrische Energie um. Dies kann an Fließgewässern oder Stauseen erfolgen oder durch Strömungs- und Gezeitenkraftwerke auf dem Meer (Pumpspeicherkraftwerke s. PFS_Energiespeicheer)', 'Wasserkraftwerk');
INSERT INTO xp_kraftwerktyp (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstiges Kraftwerk', 'sonstiges Kraftwerk');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('1000', 'Konventionell_offenerGraben', 'Ausschachtung mit Schaufel, Bagger, Fräse', 'Konventionelle Verlegung im offenen Graben');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('2000', 'Pressbohrverfahren', 'Unterirdische Verlegetechnik, die in verschiedenen Varianten zur Anwendung kommt (statisch, dynamisch, ungesteuert, gesteuert) und von Herstellern spezifisch bezeichnet wird ("Modifiziertes Direct-Pipe-Verfahren").  Im Breitbandausbau auch als Erdraketentechnik bekannt. Im Rohrleitungsbau können durch hydraulische oder pneumatische Presseinrichtungen Produktenrohrkreuzungen DN 1000 bis zu 100 m grabenlos verlegt werden.', 'Pressbohrverfahren');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('3000', 'HorizontalSpuelbohrverfahren', 'Richtbohrtechnik für Horizontalbohrungen („Horizontal Directional Drilling“, HDD), die eine grabenlose Verlegung von Produkt- oder Leerrohren ermöglicht.  Die Bohrung ist anfangs meist schräg nach unten in das Erdreich gerichtet und verläuft dann in leichtem Bogen zum Ziel, wo sie schräg nach oben wieder zutage tritt.', 'Horizontal-Spülbohrverfahren');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('4000', 'Pflugverfahren', 'Erstellung eines Leitungsgrabens (Breite > 30cm) oder Schlitzes mit einem Pflugschwert durch Verdrängung der Schicht(en) und gleichzeitigem Einbringen der Glasfasermedien. Der Einsatz des Pflugverfahrens ist ausschließlich in unbefestigten Oberflächen zulässig.', 'Pflugverfahren');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('5000', 'Fraesverfahren_ungebundeOberfl', 'Fräsverfahren in ungebunden Oberflächen (Schlitzbreite: 15 bis 30 cm, Schlitztiefe: 40 bis 120 cm)', 'Fräsverfahren in ungebundenen Oberflächen');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('6000', 'Trenching', 'Erstellung eines Schlitzes (< 30 cm) in gebundenen Verkehrsflächen in verschiedenen Verfahren durch rotierende, senkrecht stehende Werkzeuge, wobei die Schicht(en) gelöst, zerkleinert und gefördert wird (werden)', 'Trenching');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('60001', 'Schleif_Saegeverfahren', 'Erstellung eines Schlitzes eine durch eine Schneideeinheit (Schlitzbreite: 1,5 bis 11 cm, Schlitztiefe: 7 bis 45 cm)', 'Schleif-/Sägeverfahren');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('60002', 'Fraesverfahren', 'Erstellung eines Schlitzes durch ein Fräswerkzeug (Kette, Rad), (Schlitzbreite: 5 bis 15 cm, Schlitztiefe: 30 bis 60 cm)', 'Fräsverfahren');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('7000', 'Rammverfahren', 'Vortriebsverfahren, welches durch hydraulisches oder pneumatisches Vibrationsrammen das Rohr unter dem Hindernis hindurch schlägt. Mit dem Rammverfahren können Produkten- oder Mantelrohrkreuzungen bis zu 100 m Vortriebslänge grabenlos verlegt werden.', 'Rammverfahren');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('8000', 'Microtunneling', 'Für den grabenlosen Vortrieb werden in dem steuerbaren Verfahren zunächst Stahlbetonrohre mit großem Nenndurchmesser verlegt,  in denen nach Durchführung der Unterquerung das eigentliche Produktenrohr eingebracht/eingezogen wird. Es kommt nur bei schwierigen Kreuzungen zur Anwendung.', 'Microtunneling');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('9000', 'oberirdischeVerlegung', 'oberirdische Verlegung mittels Holzmasten', 'oberirdische Verlegung');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstiges Verfahren', 'sonstiges Verfahren');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('1000', 'Erdverlegt', 'Oberkategorie für erdverlegte (Rohr-)Leitungen', 'erdverlegte (Rohr-)Leitungen');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('10001', 'Erdkabel', 'Ein Erdkabel ist ein im Erdboden verlegtes elektrisch genutztes Kabel mit einer besonders robusten Isolierung nach außen, dem Kabelmantel, der eine Zerstörung derselben durch chemische Einflüsse im Erdreich bzw. im Boden lebender Kleintiere verhindert.', 'Erdkabel');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('10002', 'Seekabel', 'Ein Seekabel (auch Unterseekabel, Unterwasserkabel) ist ein im Wesentlichen in einem Gewässer verlegtes Kabel zur Datenübertragung oder die Übertragung elektrischer Energie.', 'Seekabel');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('10003', 'Schutzrohr', 'Im Schutzrohr verlegte oder zu verlegende Kabel/Leitungen. - Schutzrohre schützen erdverlegte Leitungen vor mechanischen Einflüssen und Feuchtigkeit.', 'Schutzrohr');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('10004', 'Leerrohr', 'Über die Baumaßnahme hinaus unbelegtes Schutzrohr', 'Leerrohr (unbelegtes Schutzrohr)');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('10005', 'Leitungsbuendel', 'Bündel von Kabeln und/oder Schutzrohren in den Sparten Sparten Strom und Telekommunikation im Bestand', 'Leitungsbündel');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('10006', 'Dueker', 'Druckleitung zur Unterquerung von Straßen, Flüssen, Bahngleisen etc. Im Düker kann die Flüssigkeit das Hindernis überwinden, ohne dass Pumpen eingesetzt werden müssen.', 'Düker');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('2000', 'Oberirdisch', 'Oberirdisch verlegte Leitungen und Rohre', 'oberirdischer Verlauf');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('20001', 'Freileitung', 'Elektrische Leitung, deren spannungsführende Leiter im Freien durch die Luft geführt und meist auch nur durch die umgebende Luft voneinander und vom Erdboden isoliert sind. In der Regel werden die Leiterseile von Freileitungsmasten getragen, an denen sie mit Isolatoren befestigt sind.', 'Freileitung');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('20002', 'Heberleitung', 'Leitung zur Überquerung von Straßen oder zur Verbindung von Behältern (Gegenstück zu einem Düker)', 'Heberleitung');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('20003', 'Rohrbruecke', 'Eine Rohrbrücke oder Rohrleitungsbrücke dient dazu, einzelne oder mehrere Rohrleitungen oberirdisch über größere Entfernungen zu führen.', 'Rohrbrücke');
INSERT INTO xp_primaerenergietraeger (code, model, documentation, description) VALUES ('1000', 'FossilerBrennstoff', 'Fossile Energie wird aus Brennstoffen gewonnen, die in geologischer Vorzeit aus Abbauprodukten von toten Pflanzen und Tieren entstanden sind. Dazu gehören Braunkohle, Steinkohle, Torf, Erdgas und Erdöl.', 'fossiler Brennstoff');
INSERT INTO xp_primaerenergietraeger (code, model, documentation, description) VALUES ('2000', 'Ersatzbrennstoff', 'Ersatzbrennstoffe (EBS) bzw. Sekundärbrennstoffe (SBS) sind Brennstoffe, die aus Abfällen gewonnen werden. Dabei kann es sich sowohl um feste, flüssige oder gasförmige Abfälle aus Haushalten, Industrie oder Gewerbe handeln.', 'Ersatzbrennstoff');
INSERT INTO xp_primaerenergietraeger (code, model, documentation, description) VALUES ('3000', 'Biomasse', 'Der energietechnische Biomasse-Begriff umfasst tierische und pflanzliche Erzeugnisse, die zur Gewinnung von Heizenergie, von elektrischer Energie und als Kraftstoffe verwendet werden können (u.a. Holzpellets, Hackschnitzel, Stroh, Getreide, Altholz, Biogas). Energietechnisch relevante Biomasse kann in gasförmiger, flüssiger und fester Form vorliegen.', 'Biomasse');
INSERT INTO xp_primaerenergietraeger (code, model, documentation, description) VALUES ('4000', 'Erdwaerme', 'Geothermie bezeichnet die in den oberen Schichten der Erdkruste gespeicherte Wärme und deren Ausbeutung zur Wärme- oder Stromerzeugung. In der Energiegewinnung wird zwischen tiefer und oberflächennaher Geothermie unterschieden. Die tiefe Geothermie wird von Kraftwerken zur Stromerzeugung genutzt.', 'Erdwärme');
INSERT INTO xp_primaerenergietraeger (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstige Energieträger', 'sonstiger Energieträger');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('1000', 'Fernleitung', 'Fernleitung gemäß Umweltverträglichkeitsprüfung (UVPG), Anlage 1 und ENWG § 3, Nr. 19d/20; Leitungen der Fernleitungsnetzbetreiber', 'Fernleitung');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('2000', 'Verteilnetzleitung', 'Leitung eines Verteil(er)netzes; Leitungen der Versorgungsunternehmen', 'Verteilnetzleitung');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('3000', 'Hauptleitung', 'Hauptleitung, oberste Leitungskategorie in einem Trinkwasser und Wärmenetz', 'Hauptleitung');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('4000', 'Versorgungsleitung', 'Versorgungsleitung, auch Ortsleitung (z.B Wasserleitungen innerhalb des Versorgungsgebietes im bebauten Bereich)', 'Versorgungsleitung');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('5000', 'Zubringerleitung', 'Zubringerleitung (z.B. Wasserleitungen zwischen Wassergewinnungs- und Versorgungsgebieten)', 'Zubringerleitung');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('6000', 'Anschlussleitung', 'Anschlussleitung, Hausanschluss (z.B. Wasserleitungen von der Abzweigstelle der Versorgungsleitung bis zur Übergabestelle/Hauptabsperreinrichtung)', 'Hausanschlussleitung');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('7000', 'Verbindungsleitung', 'Verbindungsleitung (z.B. Wasserleitungen außerhalb der Versorgungsgebiete, die Versorgungsgebiete (Orte) miteinander verbinden), in der Wärmeversorung auch Transportleitung genannt (die eine Wärmeerzeuugungsinfrastruktur mit einem entfernten Versorgungsgebiet verbindet)', 'Verbindungsleitung');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('8000', 'Strassenablaufleitung', 'Straßenablaufleitung (in der Abwasserentsorgung)', 'Straßenablaufleitung');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstige Leitung', 'sonstige Leitung');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('1000', 'Antragstellung', 'Antragsteller', 'Antragstellung');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('2000', 'BevollmaechtigtPlanung', 'Bevollmächtigt und Ersteller der Planung', 'Bevollmächtigt für Planung');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('3000', 'Bevollmaechtigt', 'Bevollmächtigtes Unternehmen', 'Bevollmächtigt');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('4000', 'Planung', 'Planendes Büro', 'Planung');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('5000', 'Bauunternehmen', 'Unternehmen, das Tiefbaumaßnahmen durchführt', 'Bauunternehmen');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('6000', 'Vorhabentraeger', 'Träger eines Vorhabens im Planfeststellungs- oder Raumordnungsverfahren', 'Vorhabenträger');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('7100', 'Planfeststellungsbehoerde', 'Zuständige Behörde eines Planfeststellungsverfahrens', 'Planfeststellungsbehörde');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('7200', 'Anhoerungsbehoerde', 'Behörde, die Anhörungsverfahren im Rahmen eines Planfeststellungsverfahrens durchführt', 'Anhörungsbehörde');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('8000', 'Raumordnungsbehoerde', 'Zuständige Behörde einer Raumverträglichkeitsprüfung', 'Zuständig für Raumverträglichkeitsprüfung');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('1000', 'StationGas', 'Station für Medium Gas (Wasserstoff/Erdgas)', 'Station Gas');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('10001', 'Schieberstation', 'Über eine Schieberstation (Abzweigstation?)  kann mit Hilfe der dort installierten Kugelhähne der Gasfluss gestoppt bzw. umgelenkt werden', 'Schieberstation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('10002', 'Verdichterstation', 'Eine Verdichterstation (Kompressorstation) ist eine Anlage in einer Transportleitung, bei der ein Kompressor das Gas wieder komprimiert, um Rohr-Druckverluste auszugleichen und den Volumenstrom zu regeln', 'Verdichterstation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('10003', 'Regel_Messstation', 'Eine Gas-Druckregelanlage (GDRA) ist eine Anlage zur ein- oder mehrstufigen Gas-Druckreduzierung. Bei einer Gas-Druckregel- und Messanlage (GDRMA) wird zusätzlich noch die Gas-Mengenmessung vorgenommen. (Anmerkung: Einspeise- und Übergabestationen können separat erfasst werden)', 'Regel- und Messstation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('10004', 'Armaturstation', 'Kombination von Armaturengruppen wie Absperr- und und Abgangsarmaturengruppen', 'Armaturstation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('10005', 'Einspeisestation', 'Die Einspeisungs- oder Empfangsstation leitet Erdgas oder Wasserstoff in ein Transportleitungsnetz. Die Einspeisung erfolgt z.B. aus einer Produktions- oder Speicheranlage oder über ein LNG-Terminal nach der Regasifizierung.', 'Einspeisestation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('10006', 'Uebergabestation', 'Gas-Übergabestationen (auch Übernahme- oder Entnahmestation) dienen i.d.R. der Verteilung von Gas aus Transportleitungen in die Verbrauchernetze. Dafür muss das ankommende Gas heruntergeregelt werden. Wird Wasserstoff in ein Erdgasleitungsnetz übergeben, muss zusätzlich ein Mischer an der Übernahmestelle gewährleisten, dass sich Wasserstoff und Erdgas gleichmäßig durchmischen. 
Eine weitere Variante ist die Übergabe von Gas an ein Kraftwerk.', 'Übergabe-/Entnahmestation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('10007', 'Molchstation', 'Station um Molchungen zur Prüfung der Integrität der Fernleitung während der Betriebsphase durchzuführen.  
Der Molch füllt den Leitungsquerschnitt aus und wandert entweder einfach mit dem Produktstrom durch die Leitung (meist bei Öl) oder wird durch Druck durch die Leitung gepresst. Im Rahmen der Molchtechnik werden neben dem Molch noch ins System eingebaute Schleusen benötigt, durch die der Molch in die Leitungen eingesetzt bzw. herausgenommen und von hinten mit Druck belegt werden kann.', 'Molchstation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('2000', 'StationStrom', 'Station für Medium Strom', 'Station Strom');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('20001', 'Transformatorenstation', 'In einer Transformatorenstation (Umspannstation, Netzstation, Ortsnetzstation oder kurz Trafostation) wird die elektrische Energie aus dem Mittelspannungsnetz mit einer elektrischen Spannung von 10 kV bis 36 kV auf die in Niederspannungsnetzen (Ortsnetzen) verwendeten 400/230 V zur allgemeinen Versorgung transformiert', 'Transformatorenstation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('20002', 'Konverterstation', 'Ein Konverter steht an den Verbindungspunkten von Gleich- und Wechselstromleitungen. Er verwandelt Wechsel- in Gleichstrom und kann ebenso Gleichstrom wieder zurück in Wechselstrom umwandeln und diesen ins Übertragungsnetz einspeisen.', 'Konverterstation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('20003', 'Phasenschieber', 'Phasenschiebertransformatoren (PST), auch Querregler genannt, werden zur Steuerung der Stromflüsse zwischen Übertragungsnetzen eingesetzt. Der Phasenschiebertransformator speist einen Ausgleichsstrom in das System ein, der den Laststrom in der Leitung entweder verringert oder erhöht. Sinkt der Stromfluss in einer Leitung, werden die Stromflüsse im gesamten Verbundsystem neu verteilt.', 'Phasenschieber');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('3000', 'StationWaerme', 'Station im (Fern-)Wärmenetz', 'Station (Fern-)Wärme');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstige Station', 'sonstige Station');
INSERT INTO xp_waermeleitungtyp (code, model, documentation, description) VALUES ('1000', 'Strang', 'Schematische Darstellung als Strang (mit Vor- und Rücklauf)', 'Strang');
INSERT INTO xp_waermeleitungtyp (code, model, documentation, description) VALUES ('2000', 'Vorlauf', 'Vorlaufrohr', 'Vorlauf');
INSERT INTO xp_waermeleitungtyp (code, model, documentation, description) VALUES ('3000', 'Ruecklauf', 'Rücklaufrohr', 'Rücklauf');
INSERT INTO xp_waermeleitungtyp (code, model, documentation, description) VALUES ('4000', 'Doppelrohr', 'Vor- und Rücklauf in einem Doppelrohr', 'Doppelrohr');
INSERT INTO xp_waermeleitungtyp (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstiger Typ', 'sonstiger Typ');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('1000', 'Kunststoff', 'Kunststoff', 'Kunststoff');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('10001', 'Polyethylen_PE', 'Polyethylen (PE)', 'Polyethylen ( PE)');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('10002', 'Polyethylen_PE_HD', 'High-Density Polyethylen', 'High-Density Polyethylen');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('10003', 'Polypropylen_PP', 'Polypropylen (PP)', 'Polypropylen ( PP)');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('10004', 'Polycarbonat_PC', 'Polycarbonat (PC)', 'Polycarbonat ( PC)');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('10005', 'Polyvinylchlorid_PVC_U', 'Polyvinylchlorid (PVC-U)', 'Polyvinylchlorid ( PVC- U)');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('2000', 'Stahl', 'Stahl', 'Stahl');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('20001', 'StahlVerzinkt', 'Stahl verzinkt', 'Stahl verzinkt');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('20002', 'Stahlgitter', 'Stahlfachwerkskonstruktion (z.B. Freileitungsmast als Gittermast)', 'Stahlgitter');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('20003', 'Stahlrohr', 'Rohrförmiger Profilstahl, dessen Wand aus Stahl besteht. Stahlrohre dienen der Durchleitung von flüssigen, gasförmigen oder festen Stoffen, oder werden als statische oder konstruktive Elemente verwendet (z.B. Freileitungsmast als Stahlrohrmast)', 'Stahlrohr');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('2500', 'Stahlverbundrohr', 'Stahlverbundrohre im Rohrleitungsbau', 'Stahlverbundrohr');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('25001', 'St_PE', 'Stahlrohr mit  Kunststoffumhüllung auf PE-Basis', 'Stahlrohr mit Standard-Kunststoffumhüllung (PE)');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('25002', 'St_PP', 'Stahlrohr mit  Kunststoffumhüllung auf PP-Basis für höhere Temperatur- und Härte-Anforderungen', 'Stahlrohr mit Kunstoffumhüllung (PP)');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('25003', 'St_FZM', 'Stahlrohr mit mit Kunststoff-Umhüllung und zusätzlichem Außenschutz durch Faserzementmörtel-Ummantelung (FZM)', 'Stahlrohr mit FZM-Ummantelung');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('25004', 'St_GFK', 'Stahlrohr mit mit Kunststoff-Umhüllung und zusätzlichem Außenschutz aus glasfaserverstärktem Kunststoff (GFK) für höchste mechanische Abriebfestigkeit bei grabenlosem Rohrvortrieb', 'Stahlrohr mit GFK-Ummantelung');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('25005', 'St_ZM_PE', 'Stahlrohr mit Zementmörtelauskleidung und PE-Außenschutz (z.B. Abwasserohr)', 'Stahl-Verbundrohr (ZM-PE)');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('3000', 'Gusseisen', 'Gusseisen', 'Gusseisen');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('30001', 'GGG_ZM', 'duktiles Gussrohr mit Zementmörtelauskleidung (z.B Abwasserrohr)', 'duktiles Gussrohr mit ZM-Auskleidung');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('30002', 'GGG_ZM_PE', 'duktiles Gussrohr mit Zementmörtelauskleidung und PE-Außenschutz (z.B. Abwasserrohr)', 'duktiles Guss-Verbundrohr (ZM-PE)');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('4000', 'Beton', 'Beton (z.B. Schacht)', 'Beton');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('5000', 'Holz', 'Holz (z.B. Holzmast)', 'Holz');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstiger Werkstoff', 'sonstiger Werkstoff');

CREATE INDEX idx_isa_abwasserleitung_position ON isa_abwasserleitung USING GIST (position);
CREATE INDEX idx_isa_ampel_position ON isa_ampel USING GIST (position);
CREATE INDEX idx_isa_bauwerk_position ON isa_bauwerk USING GIST (position);
CREATE INDEX idx_isa_funkmast_position ON isa_funkmast USING GIST (position);
CREATE INDEX idx_isa_glasfaser_position ON isa_glasfaser USING GIST (position);
CREATE INDEX idx_isa_grundstueck_liegenschaft_position ON isa_grundstueck_liegenschaft USING GIST (position);
CREATE INDEX idx_isa_haltestelle_position ON isa_haltestelle USING GIST (position);
CREATE INDEX idx_isa_hauptverteiler_position ON isa_hauptverteiler USING GIST (position);
CREATE INDEX idx_isa_holz_mast_position ON isa_holz_mast USING GIST (position);
CREATE INDEX idx_isa_kabelverzweiger_position ON isa_kabelverzweiger USING GIST (position);
CREATE INDEX idx_isa_lehrrohr_position ON isa_lehrrohr USING GIST (position);
CREATE INDEX idx_isa_plan_position ON isa_plan USING GIST (position);
CREATE INDEX idx_isa_pointofpresence_position ON isa_pointofpresence USING GIST (position);
CREATE INDEX idx_isa_reklametafel_litfasssauele_position ON isa_reklametafel_litfasssauele USING GIST (position);
CREATE INDEX idx_isa_richtfunkstrecke_position ON isa_richtfunkstrecke USING GIST (position);
CREATE INDEX idx_isa_strassenlaterne_position ON isa_strassenlaterne USING GIST (position);
CREATE INDEX idx_isa_strassenmobiliar_position ON isa_strassenmobiliar USING GIST (position);
CREATE INDEX idx_isa_verkehrsschild_position ON isa_verkehrsschild USING GIST (position);
CREATE INDEX idx_isa_zugangspunkt_position ON isa_zugangspunkt USING GIST (position);
CREATE INDEX idx_xp_planreferenz_position ON xp_planreferenz USING GIST (position);
CREATE INDEX idx_xp_trassenquerschnitt_position ON xp_trassenquerschnitt USING GIST (position);
