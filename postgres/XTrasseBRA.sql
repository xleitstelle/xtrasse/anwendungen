CREATE EXTENSION postgis;

CREATE TABLE bra_ausbauplan (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   name text NOT NULL, -- Name des Plans (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML und GML)
   nummer text, -- Nummer des Plans
   internalid text, -- Interner Identifikator des Plans
   beschreibung text, -- Kommentierende Beschreibung des Leitungsplans
   technischeplanerstellung text, -- Bezeichnung der Institution oder Firma, die den Plan technisch erstellt hat
   technherstelldatum date, -- Datum, an dem der Plan technisch ausgefertigt wurde
   erstellungsmassstab integer, -- Der bei der Erstellung des Plans benutzte Kartenmaßstab
   position geometry(POLYGON,25832) NOT NULL, -- Flächenhafter Raumbezug des Plans
   baubeginn date, -- Datum des geplanten Baubeginns
   bauende date, -- Datum des geplanten Abschlusses der Baumaßnahme
   status_fkcl text -- Angabe zum Status des Plans im Kontext des TKG und der behördlichen Verfahren
);

CREATE TABLE bra_ausbauplan_beteiligte (

   _id bigserial NOT NULL PRIMARY KEY,
   nameorganisation text, -- Name der Organisation
   nameperson text, -- Name der Person
   strassehausnr text, -- Straße und Hausnummer
   postfach text, -- Postfach
   postleitzahl text, -- Postleitzahl
   ort text, -- Ort
   telefon text, -- Telefonnummer
   mail text, -- Mail-Adresse
   rolle_fkcl text, -- Rolle der Person/Organisation
   bra_ausbauplan_id bigint NOT NULL
);

CREATE TABLE bra_ausbauplan_externereferenz (

   _id bigserial NOT NULL PRIMARY KEY,
   referenzname text NOT NULL, -- Name des referierten Dokument innerhalb des Informationssystems
   referenzurl text NOT NULL, -- URI des referierten Dokuments, bzw. Datenbank-Schlüssel. Wenn der XTrasseGML Datensatz und das referierte Dokument in einem hierarchischen Ordnersystem gespeichert sind, kann die URI auch einen relativen Pfad vom XPlanGML-Datensatz zum Dokument enthalten.
   beschreibung text, -- Beschreibung des referierten Dokuments
   datum date, -- Datum des referierten Dokuments
   bra_ausbauplan_id bigint NOT NULL
);

CREATE TABLE bra_ausbauplan_gesetzlichegrundlage (

   _id bigserial NOT NULL PRIMARY KEY,
   name text, -- Name / Titel des Gesetzes
   detail text, -- Detaillierte Spezifikation der gesetzlichen Grundlage mit Angabe einer Paragraphennummer
   ausfertigungdatum date, -- Die Datumsangabe bezieht sich in der Regel auf das Datum der Ausfertigung des Gesetzes oder der Rechtsverordnung
   letztebekanntmdatum date, -- Ist das Gesetz oder die Verordnung nach mehreren Änderungen neu bekannt gemacht worden, kann anstelle des Ausfertigungsdatums das Datum der Bekanntmachung der Neufassung angegeben werden
   letzteaenderungdatum date, -- Ist ein Gesetz oder eine Rechtsverordnung nach der Veröffentlichung des amtlichen Volltextes geändert worden, kann hierauf hingewiesen werden
   bra_ausbauplan_id bigint NOT NULL
);

CREATE TABLE bra_baugrube (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzubra_fk bigint NOT NULL, -- Referenz auf den Breitband-Ausbauplan, zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   art_fkcl text, -- Art der Baugrube (Start oder Ziel)
   startdatum date, -- Geplante Errichtung der Baugrube
   enddatum date -- Geplanter Abbau der Baugrube
);

CREATE TABLE bra_baugrubetyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE bra_baugrundtyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE bra_baustelle (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzubra_fk bigint NOT NULL, -- Referenz auf den Breitband-Ausbauplan, zu dem das Objekt gehört
   position geometry(MULTIPOLYGON,25832) NOT NULL, -- Raumbezug des Objektes
   art_fkcl text NOT NULL, -- Auswahl der darzustellenden Baustellenfläche
   startdatum date, -- Geplanter Beginn der Baustelle
   enddatum date -- Geplantes Ende der Baustelle
);

CREATE TABLE bra_baustelletyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE bra_breitbandtrasseabschnitt (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzubra_fk bigint NOT NULL, -- Referenz auf den Breitband-Ausbauplan, zu dem das Objekt gehört
   position geometry(LINESTRING,25832) NOT NULL, -- Raumbezug des Objektes
   leitungstyp_fkcl text, -- Auswahl des Leitungstyps
   nennweite text, -- Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser numeric, -- Außendurchmesser in m. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   ueberdeckung numeric, -- Mindestüberdeckung (DIN) ist Abstand zwischen Oberkante der Verkehrsfläche und Oberkante des Rohres/Kabels in m. Die "Verlegetiefe" wird dagegen bis zur Grabensohle gemessen (s. Attribut grabentiefe). Gilt nur für erdverlegte Linienobjekte. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   pufferzone3d numeric, -- Die Pufferzone definiert in einem 3D Modell einen rechteckigen Körper, in dem die Höhenlage einer Leitung variieren kann. Die obere Grenze des Puffers wird durch das Attribut Überdeckung definiert. Das hier einzutragende Maß ist die Distanz zur unteren Grenze des Puffers. Die Breite ergibt sich aus dem Attribut Nennweite oder Außendurchmesser.
   schutzzone3d numeric, -- Die Schutzzone definiert in einem 3D Modell einen quadratischen Körper um die Leitung. Der hier einzutragende Wert ist die Länge, die von vier Kreistangenten ausgehend den Abstand zu den waage- und senkrechten Kanten des Quadrats darstellt.
   bauweise_fkcl text, -- Angabe der Bauweise
   legeverfahren_fkcl text, -- Auswahl der konventionellen oder alternativen Legeverfahren/Verlegemethoden
   verfuellmethode_fkcl text, -- Auswahl Verfüllmethode beim Trenching-Verfahren
   grabenbreite numeric, -- Breite des Leitungsgrabens in m. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   grabentiefe numeric, -- Tiefe des Leitungsgrabens in m. Entspricht der "Verlegetiefe". (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   strassenart_fkcl text, -- Kategorie der Straße, die von der Verlegung betroffen ist.
   baugrund_fkcl text, -- Art der Wegefläche, in der die Verlegung erfolgt.
   istortsdurchfahrt boolean, -- Trasse betrifft eine Ortsdurchfahrt = true
   istueberfuehrungsbauwerk boolean, -- Trasse verläuft entlang einer Brücke = true
   istgruenflaeche boolean, -- Trasse verläuft (teilweise) in einer Grünfläche = true
   kreuztstrasse boolean -- Trasse kreuzt im Verlauf dieses Abschnitts eine Straße = true
);

CREATE TABLE bra_farbe (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE bra_hausanschluss (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzubra_fk bigint NOT NULL, -- Referenz auf den Breitband-Ausbauplan, zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   technik_fkcl text -- Auswahl der aktiven oder passiven Netztechnik
);

CREATE TABLE bra_kabel (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzubra_fk bigint NOT NULL, -- Referenz auf den Breitband-Ausbauplan, zu dem das Objekt gehört
   position geometry(LINESTRING,25832) NOT NULL, -- Raumbezug des Objektes
   leitungstyp_fkcl text, -- Auswahl des Leitungstyps
   nennweite text, -- Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser numeric, -- Außendurchmesser in m. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   ueberdeckung numeric, -- Mindestüberdeckung (DIN) ist Abstand zwischen Oberkante der Verkehrsfläche und Oberkante des Rohres/Kabels in m. Die "Verlegetiefe" wird dagegen bis zur Grabensohle gemessen (s. Attribut grabentiefe). Gilt nur für erdverlegte Linienobjekte. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   pufferzone3d numeric, -- Die Pufferzone definiert in einem 3D Modell einen rechteckigen Körper, in dem die Höhenlage einer Leitung variieren kann. Die obere Grenze des Puffers wird durch das Attribut Überdeckung definiert. Das hier einzutragende Maß ist die Distanz zur unteren Grenze des Puffers. Die Breite ergibt sich aus dem Attribut Nennweite oder Außendurchmesser.
   schutzzone3d numeric, -- Die Schutzzone definiert in einem 3D Modell einen quadratischen Körper um die Leitung. Der hier einzutragende Wert ist die Länge, die von vier Kreistangenten ausgehend den Abstand zu den waage- und senkrechten Kanten des Quadrats darstellt.
   art_fkcl text, -- Art des Kabels
   mikrorohrverbund_fk bigint, -- Referenz auf den Mikrorohrverbund, in dem das Kabel liegt bzw. verlegt wird
   mikrorohr_fk bigint, -- Referenz auf das Mikrorohr, in dem das Kabel liegt bzw. verlegt wird
   schutzrohr_fk bigint -- Referenz auf das Schutzrohr, in dem das Kabel liegt bzw. verlegt wird (sofern kein weiteres Rohr genutzt wird)
);

CREATE TABLE bra_kompaktstation (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzubra_fk bigint NOT NULL, -- Referenz auf den Breitband-Ausbauplan, zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   technik_fkcl text, -- Auswahl der aktiven oder passiven Netztechnik
   werkstoff_fkcl text -- Werkstoff der Kompaktstation
);

CREATE TABLE bra_mast (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzubra_fk bigint NOT NULL, -- Referenz auf den Breitband-Ausbauplan, zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL -- Raumbezug des Objektes
);

CREATE TABLE bra_mikrorohr (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzubra_fk bigint NOT NULL, -- Referenz auf den Breitband-Ausbauplan, zu dem das Objekt gehört
   position geometry(LINESTRING,25832) NOT NULL, -- Raumbezug des Objektes
   leitungstyp_fkcl text, -- Auswahl des Leitungstyps
   nennweite text, -- Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser numeric, -- Außendurchmesser in m. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   ueberdeckung numeric, -- Mindestüberdeckung (DIN) ist Abstand zwischen Oberkante der Verkehrsfläche und Oberkante des Rohres/Kabels in m. Die "Verlegetiefe" wird dagegen bis zur Grabensohle gemessen (s. Attribut grabentiefe). Gilt nur für erdverlegte Linienobjekte. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   pufferzone3d numeric, -- Die Pufferzone definiert in einem 3D Modell einen rechteckigen Körper, in dem die Höhenlage einer Leitung variieren kann. Die obere Grenze des Puffers wird durch das Attribut Überdeckung definiert. Das hier einzutragende Maß ist die Distanz zur unteren Grenze des Puffers. Die Breite ergibt sich aus dem Attribut Nennweite oder Außendurchmesser.
   schutzzone3d numeric, -- Die Schutzzone definiert in einem 3D Modell einen quadratischen Körper um die Leitung. Der hier einzutragende Wert ist die Länge, die von vier Kreistangenten ausgehend den Abstand zu den waage- und senkrechten Kanten des Quadrats darstellt.
   farbe_fkcl text, -- Auswahl der Farbe
   istreserverohr boolean, -- Rohr ist ein Reserverohr und bleibt nach der Baumaßnahme unbelegt = true
   werkstoff_fkcl text, -- Werkstoff des Mikrorohrs
   rohrtyp_fkcl text, -- Art des Mikrorohrs in Bezug auf Durchmesser (DN) und Wandstärke
   mikrorohrverbund_fk bigint, -- Referenz auf den Verbund, zu dem das Mikrorohr gehört
   schutzrohr_fk bigint -- Referenz auf das Schutzrohr, in dem sich das Mikrorohr befindet bzw. verlegt wird
);

CREATE TABLE bra_mikrorohrtyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE bra_mikrorohrverbund (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzubra_fk bigint NOT NULL, -- Referenz auf den Breitband-Ausbauplan, zu dem das Objekt gehört
   position geometry(LINESTRING,25832) NOT NULL, -- Raumbezug des Objektes
   leitungstyp_fkcl text, -- Auswahl des Leitungstyps
   nennweite text, -- Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser numeric, -- Außendurchmesser in m. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   ueberdeckung numeric, -- Mindestüberdeckung (DIN) ist Abstand zwischen Oberkante der Verkehrsfläche und Oberkante des Rohres/Kabels in m. Die "Verlegetiefe" wird dagegen bis zur Grabensohle gemessen (s. Attribut grabentiefe). Gilt nur für erdverlegte Linienobjekte. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   pufferzone3d numeric, -- Die Pufferzone definiert in einem 3D Modell einen rechteckigen Körper, in dem die Höhenlage einer Leitung variieren kann. Die obere Grenze des Puffers wird durch das Attribut Überdeckung definiert. Das hier einzutragende Maß ist die Distanz zur unteren Grenze des Puffers. Die Breite ergibt sich aus dem Attribut Nennweite oder Außendurchmesser.
   schutzzone3d numeric, -- Die Schutzzone definiert in einem 3D Modell einen quadratischen Körper um die Leitung. Der hier einzutragende Wert ist die Länge, die von vier Kreistangenten ausgehend den Abstand zu den waage- und senkrechten Kanten des Quadrats darstellt.
   anzahlmikrorohr1 integer, -- Anzahl der Mikrorohre im Rohrverbund (gleicher Größe)
   anzahlmikrorohr2 integer, -- Anzahl der Mikrorohre im Rohrverbund (gleicher Größe), wenn der Verbund zwei unterschiedliche Größen umfasst.
   davonreserverohre integer, -- Anzahl der Rohre, die als Reserve eingeplant werden. (Nach der Verlegung werden keine Kabel eingeblasen).
   artmikrorohr1_fkcl text, -- Auswahl Mikrorohr1 in Bezug auf Außendurchmesser und Wandstärke
   artmikrorohr2_fkcl text, -- Auswahl Mikrorohr2 in Bezug auf Außendurchmesser und Wandstärke
   farbe_fkcl text, -- Auswahl der Farbe des äußeren Hüllrohrs
   werkstoff_fkcl text, -- Werkstoff des Mikrorohrverbunds
   master boolean, -- Verbund ist äußeres Rohr = true (wird nicht im Schutzrohr verlegt)
   schutzrohr_fk bigint -- Referenz auf das Schutzrohr, in dem sich der Mikrorohrverbund befindet bzw. verlegt wird
);

CREATE TABLE bra_netztechnik (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE bra_planstatus (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE bra_rohrmuffe (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzubra_fk bigint NOT NULL, -- Referenz auf den Breitband-Ausbauplan, zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   art_fkcl text -- Auswahl der Rohrverbindung
);

CREATE TABLE bra_rohrmuffetyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE bra_schacht (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzubra_fk bigint NOT NULL, -- Referenz auf den Breitband-Ausbauplan, zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL -- Raumbezug des Objektes
);

CREATE TABLE bra_schutzrohr (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzubra_fk bigint NOT NULL, -- Referenz auf den Breitband-Ausbauplan, zu dem das Objekt gehört
   position geometry(LINESTRING,25832) NOT NULL, -- Raumbezug des Objektes
   leitungstyp_fkcl text, -- Auswahl des Leitungstyps
   nennweite text, -- Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser numeric, -- Außendurchmesser in m. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   ueberdeckung numeric, -- Mindestüberdeckung (DIN) ist Abstand zwischen Oberkante der Verkehrsfläche und Oberkante des Rohres/Kabels in m. Die "Verlegetiefe" wird dagegen bis zur Grabensohle gemessen (s. Attribut grabentiefe). Gilt nur für erdverlegte Linienobjekte. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   pufferzone3d numeric, -- Die Pufferzone definiert in einem 3D Modell einen rechteckigen Körper, in dem die Höhenlage einer Leitung variieren kann. Die obere Grenze des Puffers wird durch das Attribut Überdeckung definiert. Das hier einzutragende Maß ist die Distanz zur unteren Grenze des Puffers. Die Breite ergibt sich aus dem Attribut Nennweite oder Außendurchmesser.
   schutzzone3d numeric, -- Die Schutzzone definiert in einem 3D Modell einen quadratischen Körper um die Leitung. Der hier einzutragende Wert ist die Länge, die von vier Kreistangenten ausgehend den Abstand zu den waage- und senkrechten Kanten des Quadrats darstellt.
   istreserverohr boolean, -- Rohr ist ein Reserverohr und bleibt nach der Baumaßnahme unbelegt = true
   master boolean, -- Schutzrohr ist äußerstes Rohr im Rohrsystem = true
   werkstoff_fkcl text, -- Werkstoff des Schutzrohres
   rohrtyp_fkcl text, -- Rohrtyp in Bezug auf Durchmesser (DN) und Wandstärke
   farbe_fkcl text -- Auswahl der Farbe
);

CREATE TABLE bra_schutzrohrtyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE bra_strassetyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE bra_umfeld (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzubra_fk bigint NOT NULL, -- Referenz auf den Breitband-Ausbauplan, zu dem das Objekt gehört
   position geometry(MULTIPOLYGON,25832) NOT NULL, -- Raumbezug des Objektes
   art_fkcl text NOT NULL -- Auswahl der darzustellenden Flächenart
);

CREATE TABLE bra_umfeldtyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE bra_verfuellmethode (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE bra_verteiler (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzubra_fk bigint NOT NULL, -- Referenz auf den Breitband-Ausbauplan, zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   art_fkcl text, -- Auswahl der Gehäuse und Bauten
   technik_fkcl text, -- Auswahl der aktiven oder passiven Netztechnik
   werkstoff_fkcl text -- Werkstoff des Verteilers
);

CREATE TABLE bst_abwasserleitung (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_bra_ausbauplan_fk bigint NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   position geometry(MULTILINESTRING,25832) NOT NULL, -- Raumbezug des Objektes
   leitungstyp_fkcl text, -- Auswahl des Leitungstyps
   statusaktuell_fkcl text, -- aktueller Status
   statusaenderung_fkcl text, -- Statusveränderung im Rahmen einer Baumaßnahme
   nennweite text, -- Nennweite einer einzelnen Leitung. Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser numeric, -- Außendurchmesser einer einzelnen Leitung in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   ueberdeckung numeric, -- Mindestüberdeckung (DIN): Mindestabstand zwischen Oberkante der Verkehrsfläche und Oberkante der Leitung in m. Bei Leitungsbündeln bezieht sich der Wert auf die oberste Leitung bzw. die Oberkante des Bündels. Die "Verlegetiefe" einer Leitung wird dagegen bis zur Grabensohle gemessen. Gilt nur für erdverlegte Linienobjekte. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   lagegenauigkeit numeric, -- Statistisches Maß der maximalen Abweichung des realen Verlaufs der Leitung von der Liniengeometrie in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   pufferzone3d numeric, -- Die Pufferzone definiert in einem 3D Modell einen rechteckigen Körper, in dem die Höhenlage einer Leitung (oder eines Leitungsbündels) variieren kann. Die obere Grenze des Puffers wird durch das Attribut Überdeckung definiert. Das hier einzutragende Maß ist die Distanz zur unteren Grenze des Puffers. Die Breite ergibt sich bei einzelnen Leitungen aus dem Attribut Nennweite oder Außendurchmesser, bei Leitungsbündeln aus der Breite der Leitungszone.
   schutzzone3d numeric, -- Die Schutzzone definiert in einem 3D Modell einen quadratischen Körper um die Leitung. Der hier einzutragende Wert ist die Länge, die von vier Kreistangenten ausgehend den Abstand zu den waage- und senkrechten Kanten des Quadrats darstellt.
   art_fkcl text, -- Auswahl des Kanaltyps bezogen auf die Art der Entwässerung
   netzebene_fkcl text, -- Leitungsart innerhalb des Abwassernetzes
   werkstoff_fkcl text -- Werkstoff der Leitung
);

CREATE TABLE bst_armatur (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_bra_ausbauplan_fk bigint NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   netzsparte_fkcl text, -- Leitungssparte eines Punktobjektes
   statusaktuell_fkcl text, -- aktueller Status
   statusaenderung_fkcl text, -- Statusveränderung im Rahmen einer Baumaßnahme
   funktion_fkcl text, -- Funktion der Armatur
   einsatzgebiet_fkcl text -- Einsatzgebiet der Armatur
);

CREATE TABLE bst_baum (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_bra_ausbauplan_fk bigint NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   netzsparte_fkcl text, -- Leitungssparte eines Punktobjektes
   statusaktuell_fkcl text, -- aktueller Status
   statusaenderung_fkcl text, -- Statusveränderung im Rahmen einer Baumaßnahme
   nrbaumkataster text, -- Nummer des Baumes im kommunalen Straßenbaumkataster
   stammumfang numeric, -- Umfang des Stammes
   kronendurchmesser numeric -- Durchmesser der Baumkrone (Kronentraufbereich)
);

CREATE TABLE bst_energiespeicher (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_bra_ausbauplan_fk bigint NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   position geometry(MULTIPOLYGON,25832) NOT NULL, -- Raumbezug des Objektes
   netzsparte_fkcl text, -- Leitungssparte eines Punktobjektes
   statusaktuell_fkcl text, -- aktueller Status
   statusaenderung_fkcl text, -- Statusveränderung im Rahmen einer Baumaßnahme
   begrenzung_fkcl text, -- Bestimmung der dargestellten Fläche
   art_fkcl text, -- Art des Energiespeichers
   gasart_fkcl text, -- Art des gespeicherten Gases
   gasdruckstufe_fkcl text -- Druckstufe des Gases
);

CREATE TABLE bst_gasleitung (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_bra_ausbauplan_fk bigint NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   position geometry(MULTILINESTRING,25832) NOT NULL, -- Raumbezug des Objektes
   leitungstyp_fkcl text, -- Auswahl des Leitungstyps
   statusaktuell_fkcl text, -- aktueller Status
   statusaenderung_fkcl text, -- Statusveränderung im Rahmen einer Baumaßnahme
   nennweite text, -- Nennweite einer einzelnen Leitung. Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser numeric, -- Außendurchmesser einer einzelnen Leitung in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   ueberdeckung numeric, -- Mindestüberdeckung (DIN): Mindestabstand zwischen Oberkante der Verkehrsfläche und Oberkante der Leitung in m. Bei Leitungsbündeln bezieht sich der Wert auf die oberste Leitung bzw. die Oberkante des Bündels. Die "Verlegetiefe" einer Leitung wird dagegen bis zur Grabensohle gemessen. Gilt nur für erdverlegte Linienobjekte. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   lagegenauigkeit numeric, -- Statistisches Maß der maximalen Abweichung des realen Verlaufs der Leitung von der Liniengeometrie in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   pufferzone3d numeric, -- Die Pufferzone definiert in einem 3D Modell einen rechteckigen Körper, in dem die Höhenlage einer Leitung (oder eines Leitungsbündels) variieren kann. Die obere Grenze des Puffers wird durch das Attribut Überdeckung definiert. Das hier einzutragende Maß ist die Distanz zur unteren Grenze des Puffers. Die Breite ergibt sich bei einzelnen Leitungen aus dem Attribut Nennweite oder Außendurchmesser, bei Leitungsbündeln aus der Breite der Leitungszone.
   schutzzone3d numeric, -- Die Schutzzone definiert in einem 3D Modell einen quadratischen Körper um die Leitung. Der hier einzutragende Wert ist die Länge, die von vier Kreistangenten ausgehend den Abstand zu den waage- und senkrechten Kanten des Quadrats darstellt.
   gasart_fkcl text NOT NULL, -- Art des transportierten Gases
   druckstufe_fkcl text, -- Angabe der Druckstufe
   netzebene_fkcl text, -- Leitungsart innerhalb des Gasnetzes
   werkstoff_fkcl text -- Werkstoff der Leitung
);

CREATE TABLE bst_hausanschluss (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_bra_ausbauplan_fk bigint NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   netzsparte_fkcl text, -- Leitungssparte eines Punktobjektes
   statusaktuell_fkcl text, -- aktueller Status
   statusaenderung_fkcl text -- Statusveränderung im Rahmen einer Baumaßnahme
);

CREATE TABLE bst_kanaltyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE bst_kraftwerk (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_bra_ausbauplan_fk bigint NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   position geometry(MULTIPOLYGON,25832) NOT NULL, -- Raumbezug des Objektes
   netzsparte_fkcl text, -- Leitungssparte eines Punktobjektes
   statusaktuell_fkcl text, -- aktueller Status
   statusaenderung_fkcl text, -- Statusveränderung im Rahmen einer Baumaßnahme
   begrenzung_fkcl text, -- Bestimmung der dargestellten Fläche
   art_fkcl text, -- Art des Kraftwerks
   primaerenergie_fkcl text, -- Energieträger, der in Dampf- und Gasturbinenkraftwerken in Sekundärenergie gewandelt wird
   kraftwaermekopplung boolean -- Kraft-Wärme-Kopplung (KWK) ist die gleichzeitige Gewinnung von mechanischer Energie und nutzbarer Wärme, die in einem gemeinsamen thermodynamischen Prozess entstehen. Die mechanische Energie wird in der Regel unmittelbar in elektrischen Strom umgewandelt. Die Wärme wird für Heizzwecke als Nah- oder Fernwärme oder für Produktionsprozesse als Prozesswärme genutzt.
);

CREATE TABLE bst_mast (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_bra_ausbauplan_fk bigint NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   netzsparte_fkcl text, -- Leitungssparte eines Punktobjektes
   statusaktuell_fkcl text, -- aktueller Status
   statusaenderung_fkcl text, -- Statusveränderung im Rahmen einer Baumaßnahme
   art_fkcl text, -- Typ des Mastes
   werkstoff_fkcl text -- Werkstoff des Masts
);

CREATE TABLE bst_masttyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE bst_netzsparte (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE bst_richtfunkstrecke (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_bra_ausbauplan_fk bigint NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   position geometry(MULTILINESTRING,25832) NOT NULL, -- Raumbezug des Objektes
   leitungstyp_fkcl text, -- Auswahl des Leitungstyps
   statusaktuell_fkcl text, -- aktueller Status
   statusaenderung_fkcl text, -- Statusveränderung im Rahmen einer Baumaßnahme
   nennweite text, -- Nennweite einer einzelnen Leitung. Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser numeric, -- Außendurchmesser einer einzelnen Leitung in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   ueberdeckung numeric, -- Mindestüberdeckung (DIN): Mindestabstand zwischen Oberkante der Verkehrsfläche und Oberkante der Leitung in m. Bei Leitungsbündeln bezieht sich der Wert auf die oberste Leitung bzw. die Oberkante des Bündels. Die "Verlegetiefe" einer Leitung wird dagegen bis zur Grabensohle gemessen. Gilt nur für erdverlegte Linienobjekte. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   lagegenauigkeit numeric, -- Statistisches Maß der maximalen Abweichung des realen Verlaufs der Leitung von der Liniengeometrie in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   pufferzone3d numeric, -- Die Pufferzone definiert in einem 3D Modell einen rechteckigen Körper, in dem die Höhenlage einer Leitung (oder eines Leitungsbündels) variieren kann. Die obere Grenze des Puffers wird durch das Attribut Überdeckung definiert. Das hier einzutragende Maß ist die Distanz zur unteren Grenze des Puffers. Die Breite ergibt sich bei einzelnen Leitungen aus dem Attribut Nennweite oder Außendurchmesser, bei Leitungsbündeln aus der Breite der Leitungszone.
   schutzzone3d numeric -- Die Schutzzone definiert in einem 3D Modell einen quadratischen Körper um die Leitung. Der hier einzutragende Wert ist die Länge, die von vier Kreistangenten ausgehend den Abstand zu den waage- und senkrechten Kanten des Quadrats darstellt.
);

CREATE TABLE bst_schacht (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_bra_ausbauplan_fk bigint NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   netzsparte_fkcl text, -- Leitungssparte eines Punktobjektes
   statusaktuell_fkcl text, -- aktueller Status
   statusaenderung_fkcl text, -- Statusveränderung im Rahmen einer Baumaßnahme
   schachttiefe numeric -- Schachttiefe (= Deckelhöhe - Sohlhöhe)
);

CREATE TABLE bst_sonsteinrichtunglinie (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_bra_ausbauplan_fk bigint NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   position geometry(MULTILINESTRING,25832) NOT NULL, -- Raumbezug des Objektes
   leitungstyp_fkcl text, -- Auswahl des Leitungstyps
   statusaktuell_fkcl text, -- aktueller Status
   statusaenderung_fkcl text, -- Statusveränderung im Rahmen einer Baumaßnahme
   nennweite text, -- Nennweite einer einzelnen Leitung. Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser numeric, -- Außendurchmesser einer einzelnen Leitung in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   ueberdeckung numeric, -- Mindestüberdeckung (DIN): Mindestabstand zwischen Oberkante der Verkehrsfläche und Oberkante der Leitung in m. Bei Leitungsbündeln bezieht sich der Wert auf die oberste Leitung bzw. die Oberkante des Bündels. Die "Verlegetiefe" einer Leitung wird dagegen bis zur Grabensohle gemessen. Gilt nur für erdverlegte Linienobjekte. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   lagegenauigkeit numeric, -- Statistisches Maß der maximalen Abweichung des realen Verlaufs der Leitung von der Liniengeometrie in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   pufferzone3d numeric, -- Die Pufferzone definiert in einem 3D Modell einen rechteckigen Körper, in dem die Höhenlage einer Leitung (oder eines Leitungsbündels) variieren kann. Die obere Grenze des Puffers wird durch das Attribut Überdeckung definiert. Das hier einzutragende Maß ist die Distanz zur unteren Grenze des Puffers. Die Breite ergibt sich bei einzelnen Leitungen aus dem Attribut Nennweite oder Außendurchmesser, bei Leitungsbündeln aus der Breite der Leitungszone.
   schutzzone3d numeric -- Die Schutzzone definiert in einem 3D Modell einen quadratischen Körper um die Leitung. Der hier einzutragende Wert ist die Länge, die von vier Kreistangenten ausgehend den Abstand zu den waage- und senkrechten Kanten des Quadrats darstellt.
);

CREATE TABLE bst_spannung (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE bst_station (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_bra_ausbauplan_fk bigint NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   netzsparte_fkcl text, -- Leitungssparte eines Punktobjektes
   statusaktuell_fkcl text, -- aktueller Status
   statusaenderung_fkcl text, -- Statusveränderung im Rahmen einer Baumaßnahme
   art_fkcl text -- Art der Station
);

CREATE TABLE bst_stationflaeche (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_bra_ausbauplan_fk bigint NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   position geometry(MULTIPOLYGON,25832) NOT NULL, -- Raumbezug des Objektes
   netzsparte_fkcl text, -- Leitungssparte eines Punktobjektes
   statusaktuell_fkcl text, -- aktueller Status
   statusaenderung_fkcl text, -- Statusveränderung im Rahmen einer Baumaßnahme
   begrenzung_fkcl text, -- Bestimmung der dargestellten Fläche
   art_fkcl text -- Art der Station
);

CREATE TABLE bst_statusaenderung (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE bst_statusaktuell (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE bst_strassenablauf (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_bra_ausbauplan_fk bigint NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   netzsparte_fkcl text, -- Leitungssparte eines Punktobjektes
   statusaktuell_fkcl text, -- aktueller Status
   statusaenderung_fkcl text -- Statusveränderung im Rahmen einer Baumaßnahme
);

CREATE TABLE bst_strassenbeleuchtung (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_bra_ausbauplan_fk bigint NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   position geometry(MULTILINESTRING,25832) NOT NULL, -- Raumbezug des Objektes
   leitungstyp_fkcl text, -- Auswahl des Leitungstyps
   statusaktuell_fkcl text, -- aktueller Status
   statusaenderung_fkcl text, -- Statusveränderung im Rahmen einer Baumaßnahme
   nennweite text, -- Nennweite einer einzelnen Leitung. Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser numeric, -- Außendurchmesser einer einzelnen Leitung in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   ueberdeckung numeric, -- Mindestüberdeckung (DIN): Mindestabstand zwischen Oberkante der Verkehrsfläche und Oberkante der Leitung in m. Bei Leitungsbündeln bezieht sich der Wert auf die oberste Leitung bzw. die Oberkante des Bündels. Die "Verlegetiefe" einer Leitung wird dagegen bis zur Grabensohle gemessen. Gilt nur für erdverlegte Linienobjekte. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   lagegenauigkeit numeric, -- Statistisches Maß der maximalen Abweichung des realen Verlaufs der Leitung von der Liniengeometrie in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   pufferzone3d numeric, -- Die Pufferzone definiert in einem 3D Modell einen rechteckigen Körper, in dem die Höhenlage einer Leitung (oder eines Leitungsbündels) variieren kann. Die obere Grenze des Puffers wird durch das Attribut Überdeckung definiert. Das hier einzutragende Maß ist die Distanz zur unteren Grenze des Puffers. Die Breite ergibt sich bei einzelnen Leitungen aus dem Attribut Nennweite oder Außendurchmesser, bei Leitungsbündeln aus der Breite der Leitungszone.
   schutzzone3d numeric, -- Die Schutzzone definiert in einem 3D Modell einen quadratischen Körper um die Leitung. Der hier einzutragende Wert ist die Länge, die von vier Kreistangenten ausgehend den Abstand zu den waage- und senkrechten Kanten des Quadrats darstellt.
   spannung_fkcl text, -- Angabe der Spannung einer Leitung. Bei Leitungsbündeln kann das Textattribut "beschreibung" zur Differenzierung der Spannungsarten genutzt werden.
   leitungszonebreite numeric, -- Ein Bündel an Leitungen wird über deren Gesamtbreite und -tiefe in Metern spezifiziert. Eine weitere Differenzierung zwischen Kabeln mit und ohne Schutzrohr sowie deren jeweiligem Durchmesser erfolgt nicht.
   leitungszonetiefe numeric -- Ein Bündel an Leitungen wird über deren Gesamtbreite und -tiefe in Metern spezifiziert. Die Tiefe bezieht sich auf den Abstand zwischen der Oberkante der obersten und der Unterkante der untersten Lage der Leitungen.
);

CREATE TABLE bst_stromleitung (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_bra_ausbauplan_fk bigint NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   position geometry(MULTILINESTRING,25832) NOT NULL, -- Raumbezug des Objektes
   leitungstyp_fkcl text, -- Auswahl des Leitungstyps
   statusaktuell_fkcl text, -- aktueller Status
   statusaenderung_fkcl text, -- Statusveränderung im Rahmen einer Baumaßnahme
   nennweite text, -- Nennweite einer einzelnen Leitung. Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser numeric, -- Außendurchmesser einer einzelnen Leitung in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   ueberdeckung numeric, -- Mindestüberdeckung (DIN): Mindestabstand zwischen Oberkante der Verkehrsfläche und Oberkante der Leitung in m. Bei Leitungsbündeln bezieht sich der Wert auf die oberste Leitung bzw. die Oberkante des Bündels. Die "Verlegetiefe" einer Leitung wird dagegen bis zur Grabensohle gemessen. Gilt nur für erdverlegte Linienobjekte. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   lagegenauigkeit numeric, -- Statistisches Maß der maximalen Abweichung des realen Verlaufs der Leitung von der Liniengeometrie in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   pufferzone3d numeric, -- Die Pufferzone definiert in einem 3D Modell einen rechteckigen Körper, in dem die Höhenlage einer Leitung (oder eines Leitungsbündels) variieren kann. Die obere Grenze des Puffers wird durch das Attribut Überdeckung definiert. Das hier einzutragende Maß ist die Distanz zur unteren Grenze des Puffers. Die Breite ergibt sich bei einzelnen Leitungen aus dem Attribut Nennweite oder Außendurchmesser, bei Leitungsbündeln aus der Breite der Leitungszone.
   schutzzone3d numeric, -- Die Schutzzone definiert in einem 3D Modell einen quadratischen Körper um die Leitung. Der hier einzutragende Wert ist die Länge, die von vier Kreistangenten ausgehend den Abstand zu den waage- und senkrechten Kanten des Quadrats darstellt.
   spannung_fkcl text, -- Angabe der Spannung einer Leitung. Bei Leitungsbündeln kann das Textattribut "beschreibung" zur Differenzierung der Spannungsarten genutzt werden.
   leitungszonebreite numeric, -- Ein Bündel an Leitungen wird über deren Gesamtbreite und -tiefe in Metern spezifiziert. Eine weitere Differenzierung zwischen Kabeln mit und ohne Schutzrohr sowie deren jeweiligem Durchmesser erfolgt nicht.
   leitungszonetiefe numeric -- Ein Bündel an Leitungen wird über deren Gesamtbreite und -tiefe in Metern spezifiziert. Die Tiefe bezieht sich auf den Abstand zwischen der Oberkante der obersten und der Unterkante der untersten Lage der Leitungen.
);

CREATE TABLE bst_telekommunikationsleitung (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_bra_ausbauplan_fk bigint NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   position geometry(MULTILINESTRING,25832) NOT NULL, -- Raumbezug des Objektes
   leitungstyp_fkcl text, -- Auswahl des Leitungstyps
   statusaktuell_fkcl text, -- aktueller Status
   statusaenderung_fkcl text, -- Statusveränderung im Rahmen einer Baumaßnahme
   nennweite text, -- Nennweite einer einzelnen Leitung. Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser numeric, -- Außendurchmesser einer einzelnen Leitung in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   ueberdeckung numeric, -- Mindestüberdeckung (DIN): Mindestabstand zwischen Oberkante der Verkehrsfläche und Oberkante der Leitung in m. Bei Leitungsbündeln bezieht sich der Wert auf die oberste Leitung bzw. die Oberkante des Bündels. Die "Verlegetiefe" einer Leitung wird dagegen bis zur Grabensohle gemessen. Gilt nur für erdverlegte Linienobjekte. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   lagegenauigkeit numeric, -- Statistisches Maß der maximalen Abweichung des realen Verlaufs der Leitung von der Liniengeometrie in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   pufferzone3d numeric, -- Die Pufferzone definiert in einem 3D Modell einen rechteckigen Körper, in dem die Höhenlage einer Leitung (oder eines Leitungsbündels) variieren kann. Die obere Grenze des Puffers wird durch das Attribut Überdeckung definiert. Das hier einzutragende Maß ist die Distanz zur unteren Grenze des Puffers. Die Breite ergibt sich bei einzelnen Leitungen aus dem Attribut Nennweite oder Außendurchmesser, bei Leitungsbündeln aus der Breite der Leitungszone.
   schutzzone3d numeric, -- Die Schutzzone definiert in einem 3D Modell einen quadratischen Körper um die Leitung. Der hier einzutragende Wert ist die Länge, die von vier Kreistangenten ausgehend den Abstand zu den waage- und senkrechten Kanten des Quadrats darstellt.
   art_fkcl text, -- Auswahl des Kabeltyps. Bei Leitungsbündeln kann das Textattribut "beschreibung" zur Differenzierung der Kabel genutzt werden.
   leitungszonebreite numeric, -- Ein Bündel an Leitungen wird über deren Gesamtbreite und -tiefe in Metern spezifiziert. Eine weitere Differenzierung zwischen Kabeln mit und ohne Schutzrohr sowie deren jeweiligem Durchmesser erfolgt nicht.
   leitungszonetiefe numeric -- Ein Bündel an Leitungen wird über deren Gesamtbreite und -tiefe in Metern spezifiziert. Die Tiefe bezieht sich auf den Abstand zwischen der Oberkante der obersten und der Unterkante der untersten Lage der Leitungen.
);

CREATE TABLE bst_umspannwerk (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_bra_ausbauplan_fk bigint NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   position geometry(MULTIPOLYGON,25832) NOT NULL, -- Raumbezug des Objektes
   netzsparte_fkcl text, -- Leitungssparte eines Punktobjektes
   statusaktuell_fkcl text, -- aktueller Status
   statusaenderung_fkcl text, -- Statusveränderung im Rahmen einer Baumaßnahme
   begrenzung_fkcl text -- Bestimmung der dargestellten Fläche
);

CREATE TABLE bst_verteiler (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_bra_ausbauplan_fk bigint NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   position geometry(MULTIPOINT,25832) NOT NULL, -- Raumbezug des Objektes
   netzsparte_fkcl text, -- Leitungssparte eines Punktobjektes
   statusaktuell_fkcl text, -- aktueller Status
   statusaenderung_fkcl text, -- Statusveränderung im Rahmen einer Baumaßnahme
   art_fkcl text -- Typ des Gehäuses bzw. der Funktion
);

CREATE TABLE bst_waermeleitung (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_bra_ausbauplan_fk bigint NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   position geometry(MULTILINESTRING,25832) NOT NULL, -- Raumbezug des Objektes
   leitungstyp_fkcl text, -- Auswahl des Leitungstyps
   statusaktuell_fkcl text, -- aktueller Status
   statusaenderung_fkcl text, -- Statusveränderung im Rahmen einer Baumaßnahme
   nennweite text, -- Nennweite einer einzelnen Leitung. Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser numeric, -- Außendurchmesser einer einzelnen Leitung in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   ueberdeckung numeric, -- Mindestüberdeckung (DIN): Mindestabstand zwischen Oberkante der Verkehrsfläche und Oberkante der Leitung in m. Bei Leitungsbündeln bezieht sich der Wert auf die oberste Leitung bzw. die Oberkante des Bündels. Die "Verlegetiefe" einer Leitung wird dagegen bis zur Grabensohle gemessen. Gilt nur für erdverlegte Linienobjekte. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   lagegenauigkeit numeric, -- Statistisches Maß der maximalen Abweichung des realen Verlaufs der Leitung von der Liniengeometrie in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   pufferzone3d numeric, -- Die Pufferzone definiert in einem 3D Modell einen rechteckigen Körper, in dem die Höhenlage einer Leitung (oder eines Leitungsbündels) variieren kann. Die obere Grenze des Puffers wird durch das Attribut Überdeckung definiert. Das hier einzutragende Maß ist die Distanz zur unteren Grenze des Puffers. Die Breite ergibt sich bei einzelnen Leitungen aus dem Attribut Nennweite oder Außendurchmesser, bei Leitungsbündeln aus der Breite der Leitungszone.
   schutzzone3d numeric, -- Die Schutzzone definiert in einem 3D Modell einen quadratischen Körper um die Leitung. Der hier einzutragende Wert ist die Länge, die von vier Kreistangenten ausgehend den Abstand zu den waage- und senkrechten Kanten des Quadrats darstellt.
   art_fkcl text, -- Art der Wärmeleitung
   netzebene_fkcl text, -- Leitungsart innerhalb des Wärmenetzes
   werkstoff_fkcl text -- Werkstoff der Leitung
);

CREATE TABLE bst_wasserleitung (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_bra_ausbauplan_fk bigint NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   position geometry(MULTILINESTRING,25832) NOT NULL, -- Raumbezug des Objektes
   leitungstyp_fkcl text, -- Auswahl des Leitungstyps
   statusaktuell_fkcl text, -- aktueller Status
   statusaenderung_fkcl text, -- Statusveränderung im Rahmen einer Baumaßnahme
   nennweite text, -- Nennweite einer einzelnen Leitung. Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser numeric, -- Außendurchmesser einer einzelnen Leitung in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   ueberdeckung numeric, -- Mindestüberdeckung (DIN): Mindestabstand zwischen Oberkante der Verkehrsfläche und Oberkante der Leitung in m. Bei Leitungsbündeln bezieht sich der Wert auf die oberste Leitung bzw. die Oberkante des Bündels. Die "Verlegetiefe" einer Leitung wird dagegen bis zur Grabensohle gemessen. Gilt nur für erdverlegte Linienobjekte. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   lagegenauigkeit numeric, -- Statistisches Maß der maximalen Abweichung des realen Verlaufs der Leitung von der Liniengeometrie in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   pufferzone3d numeric, -- Die Pufferzone definiert in einem 3D Modell einen rechteckigen Körper, in dem die Höhenlage einer Leitung (oder eines Leitungsbündels) variieren kann. Die obere Grenze des Puffers wird durch das Attribut Überdeckung definiert. Das hier einzutragende Maß ist die Distanz zur unteren Grenze des Puffers. Die Breite ergibt sich bei einzelnen Leitungen aus dem Attribut Nennweite oder Außendurchmesser, bei Leitungsbündeln aus der Breite der Leitungszone.
   schutzzone3d numeric, -- Die Schutzzone definiert in einem 3D Modell einen quadratischen Körper um die Leitung. Der hier einzutragende Wert ist die Länge, die von vier Kreistangenten ausgehend den Abstand zu den waage- und senkrechten Kanten des Quadrats darstellt.
   netzebene_fkcl text, -- Leitungsart innerhalb des Wassernetzes
   werkstoff_fkcl text -- Werkstoff der Leitung
);

CREATE TABLE bst_wegekante (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_bra_ausbauplan_fk bigint NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   position geometry(LINESTRING,25832) NOT NULL, -- Raumbezug des Objektes
   art_fkcl text -- Art der Wegekante
);

CREATE TABLE bst_wegekantetyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_armatureinsatzgebiet (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_armaturfunktion (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_baugrubetyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_bauweise (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_energiespeichertyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_gasdruckstufe (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_gastyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_gehaeusetyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_infrastrukturflaeche (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_kabeltyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_kraftwerktyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_legeverfahren (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_leitungtyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_planreferenz (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_bra_ausbauplan_fk bigint NOT NULL, -- Referenz auf den Netzplan, zu dem das Objekt gehört
   position geometry(POLYGON,25832) NOT NULL -- Raumbezug des Plans
);

CREATE TABLE xp_planreferenz_referenz (

   _id bigserial NOT NULL PRIMARY KEY,
   referenzname text NOT NULL, -- Name des referierten Dokument innerhalb des Informationssystems
   referenzurl text NOT NULL, -- URI des referierten Dokuments, bzw. Datenbank-Schlüssel. Wenn der XTrasseGML Datensatz und das referierte Dokument in einem hierarchischen Ordnersystem gespeichert sind, kann die URI auch einen relativen Pfad vom XPlanGML-Datensatz zum Dokument enthalten.
   beschreibung text, -- Beschreibung des referierten Dokuments
   datum date, -- Datum des referierten Dokuments
   xp_planreferenz_id bigint NOT NULL
);

CREATE TABLE xp_primaerenergietraeger (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_rohrleitungnetz (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_rolle (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_stationtyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_trassenquerschnitt (

   _id bigserial NOT NULL PRIMARY KEY,
   uuid text, -- Eindeutiger Identifier des Objektes
   title text NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung text, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift text, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber text, -- Angabe des Leitungsbetreibers
   gehoertzuplan_bra_ausbauplan_fk bigint NOT NULL, -- Referenz auf den Netzplan, zu dem das Objekt gehört
   position geometry(LINESTRING,25832) NOT NULL -- Verlauf des Trassenquerschnitts
);

CREATE TABLE xp_trassenquerschnitt_trassenquerschnitt (

   _id bigserial NOT NULL PRIMARY KEY,
   referenzname text NOT NULL, -- Name des referierten Dokument innerhalb des Informationssystems
   referenzurl text NOT NULL, -- URI des referierten Dokuments, bzw. Datenbank-Schlüssel. Wenn der XTrasseGML Datensatz und das referierte Dokument in einem hierarchischen Ordnersystem gespeichert sind, kann die URI auch einen relativen Pfad vom XPlanGML-Datensatz zum Dokument enthalten.
   beschreibung text, -- Beschreibung des referierten Dokuments
   datum date, -- Datum des referierten Dokuments
   xp_trassenquerschnitt_id bigint NOT NULL
);

CREATE TABLE xp_waermeleitungtyp (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);

CREATE TABLE xp_werkstoff (

   code text NOT NULL PRIMARY KEY,
   model text,
   documentation text,
   description text
);


ALTER TABLE bra_ausbauplan ADD CONSTRAINT fk_bra_ausbauplan_status_fkcl FOREIGN KEY (status_fkcl) REFERENCES bra_planstatus (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_ausbauplan_beteiligte ADD CONSTRAINT fk_bra_ausbauplan_beteiligte_bra_ausbauplan_id FOREIGN KEY (bra_ausbauplan_id) REFERENCES bra_ausbauplan (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_ausbauplan_beteiligte ADD CONSTRAINT fk_bra_ausbauplan_beteiligte_rolle_fkcl FOREIGN KEY (rolle_fkcl) REFERENCES xp_rolle (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_ausbauplan_externereferenz ADD CONSTRAINT fk_bra_ausbauplan_externereferenz_bra_ausbauplan_id FOREIGN KEY (bra_ausbauplan_id) REFERENCES bra_ausbauplan (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_ausbauplan_gesetzlichegrundlage ADD CONSTRAINT fk_bra_ausbauplan_gesetzlichegrundlage_bra_ausbauplan_id FOREIGN KEY (bra_ausbauplan_id) REFERENCES bra_ausbauplan (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_baugrube ADD CONSTRAINT fk_bra_baugrube_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES xp_baugrubetyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_baugrube ADD CONSTRAINT fk_bra_baugrube_gehoertzubra_fk FOREIGN KEY (gehoertzubra_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_baustelle ADD CONSTRAINT fk_bra_baustelle_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES bra_baustelletyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_baustelle ADD CONSTRAINT fk_bra_baustelle_gehoertzubra_fk FOREIGN KEY (gehoertzubra_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_breitbandtrasseabschnitt ADD CONSTRAINT fk_bra_breitbandtrasseabschnitt_baugrund_fkcl FOREIGN KEY (baugrund_fkcl) REFERENCES bra_baugrundtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_breitbandtrasseabschnitt ADD CONSTRAINT fk_bra_breitbandtrasseabschnitt_bauweise_fkcl FOREIGN KEY (bauweise_fkcl) REFERENCES xp_bauweise (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_breitbandtrasseabschnitt ADD CONSTRAINT fk_bra_breitbandtrasseabschnitt_gehoertzubra_fk FOREIGN KEY (gehoertzubra_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_breitbandtrasseabschnitt ADD CONSTRAINT fk_bra_breitbandtrasseabschnitt_legeverfahren_fkcl FOREIGN KEY (legeverfahren_fkcl) REFERENCES xp_legeverfahren (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_breitbandtrasseabschnitt ADD CONSTRAINT fk_bra_breitbandtrasseabschnitt_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_breitbandtrasseabschnitt ADD CONSTRAINT fk_bra_breitbandtrasseabschnitt_strassenart_fkcl FOREIGN KEY (strassenart_fkcl) REFERENCES bra_strassetyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_breitbandtrasseabschnitt ADD CONSTRAINT fk_bra_breitbandtrasseabschnitt_verfuellmethode_fkcl FOREIGN KEY (verfuellmethode_fkcl) REFERENCES bra_verfuellmethode (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_hausanschluss ADD CONSTRAINT fk_bra_hausanschluss_gehoertzubra_fk FOREIGN KEY (gehoertzubra_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_hausanschluss ADD CONSTRAINT fk_bra_hausanschluss_technik_fkcl FOREIGN KEY (technik_fkcl) REFERENCES bra_netztechnik (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_kabel ADD CONSTRAINT fk_bra_kabel_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES xp_kabeltyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_kabel ADD CONSTRAINT fk_bra_kabel_gehoertzubra_fk FOREIGN KEY (gehoertzubra_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_kabel ADD CONSTRAINT fk_bra_kabel_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_kabel ADD CONSTRAINT fk_bra_kabel_mikrorohr_fk FOREIGN KEY (mikrorohr_fk) REFERENCES bra_mikrorohr (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_kabel ADD CONSTRAINT fk_bra_kabel_mikrorohrverbund_fk FOREIGN KEY (mikrorohrverbund_fk) REFERENCES bra_mikrorohrverbund (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_kabel ADD CONSTRAINT fk_bra_kabel_schutzrohr_fk FOREIGN KEY (schutzrohr_fk) REFERENCES bra_schutzrohr (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_kompaktstation ADD CONSTRAINT fk_bra_kompaktstation_gehoertzubra_fk FOREIGN KEY (gehoertzubra_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_kompaktstation ADD CONSTRAINT fk_bra_kompaktstation_technik_fkcl FOREIGN KEY (technik_fkcl) REFERENCES bra_netztechnik (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_kompaktstation ADD CONSTRAINT fk_bra_kompaktstation_werkstoff_fkcl FOREIGN KEY (werkstoff_fkcl) REFERENCES xp_werkstoff (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_mast ADD CONSTRAINT fk_bra_mast_gehoertzubra_fk FOREIGN KEY (gehoertzubra_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_mikrorohr ADD CONSTRAINT fk_bra_mikrorohr_farbe_fkcl FOREIGN KEY (farbe_fkcl) REFERENCES bra_farbe (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_mikrorohr ADD CONSTRAINT fk_bra_mikrorohr_gehoertzubra_fk FOREIGN KEY (gehoertzubra_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_mikrorohr ADD CONSTRAINT fk_bra_mikrorohr_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_mikrorohr ADD CONSTRAINT fk_bra_mikrorohr_mikrorohrverbund_fk FOREIGN KEY (mikrorohrverbund_fk) REFERENCES bra_mikrorohrverbund (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_mikrorohr ADD CONSTRAINT fk_bra_mikrorohr_rohrtyp_fkcl FOREIGN KEY (rohrtyp_fkcl) REFERENCES bra_mikrorohrtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_mikrorohr ADD CONSTRAINT fk_bra_mikrorohr_schutzrohr_fk FOREIGN KEY (schutzrohr_fk) REFERENCES bra_schutzrohr (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_mikrorohr ADD CONSTRAINT fk_bra_mikrorohr_werkstoff_fkcl FOREIGN KEY (werkstoff_fkcl) REFERENCES xp_werkstoff (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_mikrorohrverbund ADD CONSTRAINT fk_bra_mikrorohrverbund_artmikrorohr1_fkcl FOREIGN KEY (artmikrorohr1_fkcl) REFERENCES bra_mikrorohrtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_mikrorohrverbund ADD CONSTRAINT fk_bra_mikrorohrverbund_artmikrorohr2_fkcl FOREIGN KEY (artmikrorohr2_fkcl) REFERENCES bra_mikrorohrtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_mikrorohrverbund ADD CONSTRAINT fk_bra_mikrorohrverbund_farbe_fkcl FOREIGN KEY (farbe_fkcl) REFERENCES bra_farbe (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_mikrorohrverbund ADD CONSTRAINT fk_bra_mikrorohrverbund_gehoertzubra_fk FOREIGN KEY (gehoertzubra_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_mikrorohrverbund ADD CONSTRAINT fk_bra_mikrorohrverbund_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_mikrorohrverbund ADD CONSTRAINT fk_bra_mikrorohrverbund_schutzrohr_fk FOREIGN KEY (schutzrohr_fk) REFERENCES bra_schutzrohr (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_mikrorohrverbund ADD CONSTRAINT fk_bra_mikrorohrverbund_werkstoff_fkcl FOREIGN KEY (werkstoff_fkcl) REFERENCES xp_werkstoff (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_rohrmuffe ADD CONSTRAINT fk_bra_rohrmuffe_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES bra_rohrmuffetyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_rohrmuffe ADD CONSTRAINT fk_bra_rohrmuffe_gehoertzubra_fk FOREIGN KEY (gehoertzubra_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_schacht ADD CONSTRAINT fk_bra_schacht_gehoertzubra_fk FOREIGN KEY (gehoertzubra_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_schutzrohr ADD CONSTRAINT fk_bra_schutzrohr_farbe_fkcl FOREIGN KEY (farbe_fkcl) REFERENCES bra_farbe (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_schutzrohr ADD CONSTRAINT fk_bra_schutzrohr_gehoertzubra_fk FOREIGN KEY (gehoertzubra_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_schutzrohr ADD CONSTRAINT fk_bra_schutzrohr_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_schutzrohr ADD CONSTRAINT fk_bra_schutzrohr_rohrtyp_fkcl FOREIGN KEY (rohrtyp_fkcl) REFERENCES bra_schutzrohrtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_schutzrohr ADD CONSTRAINT fk_bra_schutzrohr_werkstoff_fkcl FOREIGN KEY (werkstoff_fkcl) REFERENCES xp_werkstoff (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_umfeld ADD CONSTRAINT fk_bra_umfeld_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES bra_umfeldtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_umfeld ADD CONSTRAINT fk_bra_umfeld_gehoertzubra_fk FOREIGN KEY (gehoertzubra_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_verteiler ADD CONSTRAINT fk_bra_verteiler_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES xp_gehaeusetyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_verteiler ADD CONSTRAINT fk_bra_verteiler_gehoertzubra_fk FOREIGN KEY (gehoertzubra_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_verteiler ADD CONSTRAINT fk_bra_verteiler_technik_fkcl FOREIGN KEY (technik_fkcl) REFERENCES bra_netztechnik (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bra_verteiler ADD CONSTRAINT fk_bra_verteiler_werkstoff_fkcl FOREIGN KEY (werkstoff_fkcl) REFERENCES xp_werkstoff (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_abwasserleitung ADD CONSTRAINT fk_bst_abwasserleitung_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES bst_kanaltyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_abwasserleitung ADD CONSTRAINT fk_bst_abwasserleitung_gehoertzuplan_bra_ausbauplan_fk FOREIGN KEY (gehoertzuplan_bra_ausbauplan_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_abwasserleitung ADD CONSTRAINT fk_bst_abwasserleitung_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_abwasserleitung ADD CONSTRAINT fk_bst_abwasserleitung_netzebene_fkcl FOREIGN KEY (netzebene_fkcl) REFERENCES xp_rohrleitungnetz (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_abwasserleitung ADD CONSTRAINT fk_bst_abwasserleitung_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_abwasserleitung ADD CONSTRAINT fk_bst_abwasserleitung_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_abwasserleitung ADD CONSTRAINT fk_bst_abwasserleitung_werkstoff_fkcl FOREIGN KEY (werkstoff_fkcl) REFERENCES xp_werkstoff (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_armatur ADD CONSTRAINT fk_bst_armatur_einsatzgebiet_fkcl FOREIGN KEY (einsatzgebiet_fkcl) REFERENCES xp_armatureinsatzgebiet (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_armatur ADD CONSTRAINT fk_bst_armatur_funktion_fkcl FOREIGN KEY (funktion_fkcl) REFERENCES xp_armaturfunktion (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_armatur ADD CONSTRAINT fk_bst_armatur_gehoertzuplan_bra_ausbauplan_fk FOREIGN KEY (gehoertzuplan_bra_ausbauplan_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_armatur ADD CONSTRAINT fk_bst_armatur_netzsparte_fkcl FOREIGN KEY (netzsparte_fkcl) REFERENCES bst_netzsparte (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_armatur ADD CONSTRAINT fk_bst_armatur_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_armatur ADD CONSTRAINT fk_bst_armatur_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_baum ADD CONSTRAINT fk_bst_baum_gehoertzuplan_bra_ausbauplan_fk FOREIGN KEY (gehoertzuplan_bra_ausbauplan_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_baum ADD CONSTRAINT fk_bst_baum_netzsparte_fkcl FOREIGN KEY (netzsparte_fkcl) REFERENCES bst_netzsparte (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_baum ADD CONSTRAINT fk_bst_baum_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_baum ADD CONSTRAINT fk_bst_baum_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_energiespeicher ADD CONSTRAINT fk_bst_energiespeicher_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES xp_energiespeichertyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_energiespeicher ADD CONSTRAINT fk_bst_energiespeicher_begrenzung_fkcl FOREIGN KEY (begrenzung_fkcl) REFERENCES xp_infrastrukturflaeche (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_energiespeicher ADD CONSTRAINT fk_bst_energiespeicher_gasart_fkcl FOREIGN KEY (gasart_fkcl) REFERENCES xp_gastyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_energiespeicher ADD CONSTRAINT fk_bst_energiespeicher_gasdruckstufe_fkcl FOREIGN KEY (gasdruckstufe_fkcl) REFERENCES xp_gasdruckstufe (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_energiespeicher ADD CONSTRAINT fk_bst_energiespeicher_gehoertzuplan_bra_ausbauplan_fk FOREIGN KEY (gehoertzuplan_bra_ausbauplan_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_energiespeicher ADD CONSTRAINT fk_bst_energiespeicher_netzsparte_fkcl FOREIGN KEY (netzsparte_fkcl) REFERENCES bst_netzsparte (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_energiespeicher ADD CONSTRAINT fk_bst_energiespeicher_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_energiespeicher ADD CONSTRAINT fk_bst_energiespeicher_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_gasleitung ADD CONSTRAINT fk_bst_gasleitung_druckstufe_fkcl FOREIGN KEY (druckstufe_fkcl) REFERENCES xp_gasdruckstufe (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_gasleitung ADD CONSTRAINT fk_bst_gasleitung_gasart_fkcl FOREIGN KEY (gasart_fkcl) REFERENCES xp_gastyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_gasleitung ADD CONSTRAINT fk_bst_gasleitung_gehoertzuplan_bra_ausbauplan_fk FOREIGN KEY (gehoertzuplan_bra_ausbauplan_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_gasleitung ADD CONSTRAINT fk_bst_gasleitung_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_gasleitung ADD CONSTRAINT fk_bst_gasleitung_netzebene_fkcl FOREIGN KEY (netzebene_fkcl) REFERENCES xp_rohrleitungnetz (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_gasleitung ADD CONSTRAINT fk_bst_gasleitung_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_gasleitung ADD CONSTRAINT fk_bst_gasleitung_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_gasleitung ADD CONSTRAINT fk_bst_gasleitung_werkstoff_fkcl FOREIGN KEY (werkstoff_fkcl) REFERENCES xp_werkstoff (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_hausanschluss ADD CONSTRAINT fk_bst_hausanschluss_gehoertzuplan_bra_ausbauplan_fk FOREIGN KEY (gehoertzuplan_bra_ausbauplan_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_hausanschluss ADD CONSTRAINT fk_bst_hausanschluss_netzsparte_fkcl FOREIGN KEY (netzsparte_fkcl) REFERENCES bst_netzsparte (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_hausanschluss ADD CONSTRAINT fk_bst_hausanschluss_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_hausanschluss ADD CONSTRAINT fk_bst_hausanschluss_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_kraftwerk ADD CONSTRAINT fk_bst_kraftwerk_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES xp_kraftwerktyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_kraftwerk ADD CONSTRAINT fk_bst_kraftwerk_begrenzung_fkcl FOREIGN KEY (begrenzung_fkcl) REFERENCES xp_infrastrukturflaeche (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_kraftwerk ADD CONSTRAINT fk_bst_kraftwerk_gehoertzuplan_bra_ausbauplan_fk FOREIGN KEY (gehoertzuplan_bra_ausbauplan_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_kraftwerk ADD CONSTRAINT fk_bst_kraftwerk_netzsparte_fkcl FOREIGN KEY (netzsparte_fkcl) REFERENCES bst_netzsparte (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_kraftwerk ADD CONSTRAINT fk_bst_kraftwerk_primaerenergie_fkcl FOREIGN KEY (primaerenergie_fkcl) REFERENCES xp_primaerenergietraeger (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_kraftwerk ADD CONSTRAINT fk_bst_kraftwerk_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_kraftwerk ADD CONSTRAINT fk_bst_kraftwerk_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_mast ADD CONSTRAINT fk_bst_mast_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES bst_masttyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_mast ADD CONSTRAINT fk_bst_mast_gehoertzuplan_bra_ausbauplan_fk FOREIGN KEY (gehoertzuplan_bra_ausbauplan_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_mast ADD CONSTRAINT fk_bst_mast_netzsparte_fkcl FOREIGN KEY (netzsparte_fkcl) REFERENCES bst_netzsparte (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_mast ADD CONSTRAINT fk_bst_mast_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_mast ADD CONSTRAINT fk_bst_mast_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_mast ADD CONSTRAINT fk_bst_mast_werkstoff_fkcl FOREIGN KEY (werkstoff_fkcl) REFERENCES xp_werkstoff (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_richtfunkstrecke ADD CONSTRAINT fk_bst_richtfunkstrecke_gehoertzuplan_bra_ausbauplan_fk FOREIGN KEY (gehoertzuplan_bra_ausbauplan_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_richtfunkstrecke ADD CONSTRAINT fk_bst_richtfunkstrecke_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_richtfunkstrecke ADD CONSTRAINT fk_bst_richtfunkstrecke_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_richtfunkstrecke ADD CONSTRAINT fk_bst_richtfunkstrecke_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_schacht ADD CONSTRAINT fk_bst_schacht_gehoertzuplan_bra_ausbauplan_fk FOREIGN KEY (gehoertzuplan_bra_ausbauplan_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_schacht ADD CONSTRAINT fk_bst_schacht_netzsparte_fkcl FOREIGN KEY (netzsparte_fkcl) REFERENCES bst_netzsparte (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_schacht ADD CONSTRAINT fk_bst_schacht_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_schacht ADD CONSTRAINT fk_bst_schacht_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_sonsteinrichtunglinie ADD CONSTRAINT fk_bst_sonsteinrichtunglinie_gehoertzuplan_bra_ausbauplan_fk FOREIGN KEY (gehoertzuplan_bra_ausbauplan_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_sonsteinrichtunglinie ADD CONSTRAINT fk_bst_sonsteinrichtunglinie_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_sonsteinrichtunglinie ADD CONSTRAINT fk_bst_sonsteinrichtunglinie_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_sonsteinrichtunglinie ADD CONSTRAINT fk_bst_sonsteinrichtunglinie_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_station ADD CONSTRAINT fk_bst_station_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES xp_stationtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_station ADD CONSTRAINT fk_bst_station_gehoertzuplan_bra_ausbauplan_fk FOREIGN KEY (gehoertzuplan_bra_ausbauplan_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_station ADD CONSTRAINT fk_bst_station_netzsparte_fkcl FOREIGN KEY (netzsparte_fkcl) REFERENCES bst_netzsparte (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_station ADD CONSTRAINT fk_bst_station_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_station ADD CONSTRAINT fk_bst_station_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_stationflaeche ADD CONSTRAINT fk_bst_stationflaeche_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES xp_stationtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_stationflaeche ADD CONSTRAINT fk_bst_stationflaeche_begrenzung_fkcl FOREIGN KEY (begrenzung_fkcl) REFERENCES xp_infrastrukturflaeche (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_stationflaeche ADD CONSTRAINT fk_bst_stationflaeche_gehoertzuplan_bra_ausbauplan_fk FOREIGN KEY (gehoertzuplan_bra_ausbauplan_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_stationflaeche ADD CONSTRAINT fk_bst_stationflaeche_netzsparte_fkcl FOREIGN KEY (netzsparte_fkcl) REFERENCES bst_netzsparte (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_stationflaeche ADD CONSTRAINT fk_bst_stationflaeche_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_stationflaeche ADD CONSTRAINT fk_bst_stationflaeche_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_strassenablauf ADD CONSTRAINT fk_bst_strassenablauf_gehoertzuplan_bra_ausbauplan_fk FOREIGN KEY (gehoertzuplan_bra_ausbauplan_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_strassenablauf ADD CONSTRAINT fk_bst_strassenablauf_netzsparte_fkcl FOREIGN KEY (netzsparte_fkcl) REFERENCES bst_netzsparte (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_strassenablauf ADD CONSTRAINT fk_bst_strassenablauf_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_strassenablauf ADD CONSTRAINT fk_bst_strassenablauf_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_strassenbeleuchtung ADD CONSTRAINT fk_bst_strassenbeleuchtung_gehoertzuplan_bra_ausbauplan_fk FOREIGN KEY (gehoertzuplan_bra_ausbauplan_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_strassenbeleuchtung ADD CONSTRAINT fk_bst_strassenbeleuchtung_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_strassenbeleuchtung ADD CONSTRAINT fk_bst_strassenbeleuchtung_spannung_fkcl FOREIGN KEY (spannung_fkcl) REFERENCES bst_spannung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_strassenbeleuchtung ADD CONSTRAINT fk_bst_strassenbeleuchtung_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_strassenbeleuchtung ADD CONSTRAINT fk_bst_strassenbeleuchtung_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_stromleitung ADD CONSTRAINT fk_bst_stromleitung_gehoertzuplan_bra_ausbauplan_fk FOREIGN KEY (gehoertzuplan_bra_ausbauplan_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_stromleitung ADD CONSTRAINT fk_bst_stromleitung_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_stromleitung ADD CONSTRAINT fk_bst_stromleitung_spannung_fkcl FOREIGN KEY (spannung_fkcl) REFERENCES bst_spannung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_stromleitung ADD CONSTRAINT fk_bst_stromleitung_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_stromleitung ADD CONSTRAINT fk_bst_stromleitung_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_telekommunikationsleitung ADD CONSTRAINT fk_bst_telekommunikationsleitung_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES xp_kabeltyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_telekommunikationsleitung ADD CONSTRAINT fk_bst_telekommunikationsleitung_gehoertzuplan_bra_ausbauplan_f FOREIGN KEY (gehoertzuplan_bra_ausbauplan_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_telekommunikationsleitung ADD CONSTRAINT fk_bst_telekommunikationsleitung_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_telekommunikationsleitung ADD CONSTRAINT fk_bst_telekommunikationsleitung_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_telekommunikationsleitung ADD CONSTRAINT fk_bst_telekommunikationsleitung_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_umspannwerk ADD CONSTRAINT fk_bst_umspannwerk_begrenzung_fkcl FOREIGN KEY (begrenzung_fkcl) REFERENCES xp_infrastrukturflaeche (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_umspannwerk ADD CONSTRAINT fk_bst_umspannwerk_gehoertzuplan_bra_ausbauplan_fk FOREIGN KEY (gehoertzuplan_bra_ausbauplan_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_umspannwerk ADD CONSTRAINT fk_bst_umspannwerk_netzsparte_fkcl FOREIGN KEY (netzsparte_fkcl) REFERENCES bst_netzsparte (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_umspannwerk ADD CONSTRAINT fk_bst_umspannwerk_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_umspannwerk ADD CONSTRAINT fk_bst_umspannwerk_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_verteiler ADD CONSTRAINT fk_bst_verteiler_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES xp_gehaeusetyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_verteiler ADD CONSTRAINT fk_bst_verteiler_gehoertzuplan_bra_ausbauplan_fk FOREIGN KEY (gehoertzuplan_bra_ausbauplan_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_verteiler ADD CONSTRAINT fk_bst_verteiler_netzsparte_fkcl FOREIGN KEY (netzsparte_fkcl) REFERENCES bst_netzsparte (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_verteiler ADD CONSTRAINT fk_bst_verteiler_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_verteiler ADD CONSTRAINT fk_bst_verteiler_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_waermeleitung ADD CONSTRAINT fk_bst_waermeleitung_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES xp_waermeleitungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_waermeleitung ADD CONSTRAINT fk_bst_waermeleitung_gehoertzuplan_bra_ausbauplan_fk FOREIGN KEY (gehoertzuplan_bra_ausbauplan_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_waermeleitung ADD CONSTRAINT fk_bst_waermeleitung_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_waermeleitung ADD CONSTRAINT fk_bst_waermeleitung_netzebene_fkcl FOREIGN KEY (netzebene_fkcl) REFERENCES xp_rohrleitungnetz (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_waermeleitung ADD CONSTRAINT fk_bst_waermeleitung_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_waermeleitung ADD CONSTRAINT fk_bst_waermeleitung_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_waermeleitung ADD CONSTRAINT fk_bst_waermeleitung_werkstoff_fkcl FOREIGN KEY (werkstoff_fkcl) REFERENCES xp_werkstoff (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_wasserleitung ADD CONSTRAINT fk_bst_wasserleitung_gehoertzuplan_bra_ausbauplan_fk FOREIGN KEY (gehoertzuplan_bra_ausbauplan_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_wasserleitung ADD CONSTRAINT fk_bst_wasserleitung_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_wasserleitung ADD CONSTRAINT fk_bst_wasserleitung_netzebene_fkcl FOREIGN KEY (netzebene_fkcl) REFERENCES xp_rohrleitungnetz (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_wasserleitung ADD CONSTRAINT fk_bst_wasserleitung_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_wasserleitung ADD CONSTRAINT fk_bst_wasserleitung_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_wasserleitung ADD CONSTRAINT fk_bst_wasserleitung_werkstoff_fkcl FOREIGN KEY (werkstoff_fkcl) REFERENCES xp_werkstoff (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_wegekante ADD CONSTRAINT fk_bst_wegekante_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES bst_wegekantetyp (code) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE bst_wegekante ADD CONSTRAINT fk_bst_wegekante_gehoertzuplan_bra_ausbauplan_fk FOREIGN KEY (gehoertzuplan_bra_ausbauplan_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE xp_planreferenz ADD CONSTRAINT fk_xp_planreferenz_gehoertzuplan_bra_ausbauplan_fk FOREIGN KEY (gehoertzuplan_bra_ausbauplan_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE xp_planreferenz_referenz ADD CONSTRAINT fk_xp_planreferenz_referenz_xp_planreferenz_id FOREIGN KEY (xp_planreferenz_id) REFERENCES xp_planreferenz (_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE xp_trassenquerschnitt ADD CONSTRAINT fk_xp_trassenquerschnitt_gehoertzuplan_bra_ausbauplan_fk FOREIGN KEY (gehoertzuplan_bra_ausbauplan_fk) REFERENCES bra_ausbauplan (_id) ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE xp_trassenquerschnitt_trassenquerschnitt ADD CONSTRAINT fk_xp_trassenquerschnitt_trassenquerschnitt_xp_trassenquerschni FOREIGN KEY (xp_trassenquerschnitt_id) REFERENCES xp_trassenquerschnitt (_id) DEFERRABLE INITIALLY DEFERRED;

INSERT INTO bra_baugrubetyp (code, model, documentation, description) VALUES ('1000', 'Startgrube', 'Startgrube für alternative Verlegemethoden', 'Startgrube');
INSERT INTO bra_baugrubetyp (code, model, documentation, description) VALUES ('2000', 'Zielgrube', 'Zielgrube für alternative Verlegemethoden', 'Zielgrube');
INSERT INTO bra_baugrundtyp (code, model, documentation, description) VALUES ('1000', 'Fahrbahn', 'Fahrbahn', 'Fahrbahn');
INSERT INTO bra_baugrundtyp (code, model, documentation, description) VALUES ('2100', 'Bankett_Sicherheitstrennstreifen', 'Bankett / Sicherheitstrennstreifen', 'Bankett / Sicherheitstrennstreifen');
INSERT INTO bra_baugrundtyp (code, model, documentation, description) VALUES ('21001', 'Bankett_Fraesbereich', 'Bankett - zulässiger Fräsbereich nach DIN 18220', 'Bankett  - zulässiger Fräsbereich');
INSERT INTO bra_baugrundtyp (code, model, documentation, description) VALUES ('21002', 'Bankett', 'Bankett  - außerhalb zulässigem Fräsbereich nach DIN 18220', 'Bankett - außerhalb Fräsbereich');
INSERT INTO bra_baugrundtyp (code, model, documentation, description) VALUES ('21003', 'Sicherheitstrennstreifen_Fraesbereich', 'Sicherheitstrennstreifen - zulässiger Fräsbereich nach DIN 18220', 'Sicherheitstrennstreifen  - zulässiger Fräsbereich');
INSERT INTO bra_baugrundtyp (code, model, documentation, description) VALUES ('21004', 'Sicherheitstrennstreifen', 'Sicherheitstrennstreifen - außerhalb zulässigem Fräsbereich nach DIN 18220', 'Sicherheitstrennstreifen - außerhalb Fräsbereich');
INSERT INTO bra_baugrundtyp (code, model, documentation, description) VALUES ('2200', 'Entwaesserungsgraben_Boeschung', 'Entwässerungsgraben / Böschung', 'Entwässerungsgraben / Böschung');
INSERT INTO bra_baugrundtyp (code, model, documentation, description) VALUES ('22001', 'Entwaesserungsgraben', 'Entwässerungsgraben / Mulde (ohne Entwässerungsleitung)', 'Entwässerungsgraben');
INSERT INTO bra_baugrundtyp (code, model, documentation, description) VALUES ('22002', 'Mulde', 'Mulde (mit Entwässerungsleitung)', 'Mulde (mit Entwässerungsleitung)');
INSERT INTO bra_baugrundtyp (code, model, documentation, description) VALUES ('22003', 'StrassenseitigeGrabenboeschung', 'straßenseitige Grabenböschung', 'straßenseitige Grabenböschung');
INSERT INTO bra_baugrundtyp (code, model, documentation, description) VALUES ('22004', 'FeldseitigeBoeschung', 'feldseitige Böschung', 'feldseitige Böschung');
INSERT INTO bra_baugrundtyp (code, model, documentation, description) VALUES ('22005', 'Gelaende', 'Gelände', 'Gelände');
INSERT INTO bra_baugrundtyp (code, model, documentation, description) VALUES ('2300', 'Gehweg_Radweg_strassenbegleitend', 'straßenbegleitender Gehweg / Radweg (außerorts, gebundene Deckschicht)', 'straßenbegleitender Gehweg / Radweg');
INSERT INTO bra_baugrundtyp (code, model, documentation, description) VALUES ('3000', 'Weg_nichtStrassenbegleitend', 'nicht straßenbegleitender Weg (außerorts, Deckschicht ohne Bindemittel)', 'nicht straßenbegleitender Weg');
INSERT INTO bra_baugrundtyp (code, model, documentation, description) VALUES ('30001', 'Gehweg_Radweg', 'Gehweg / Radweg', 'Gehweg / Radweg');
INSERT INTO bra_baugrundtyp (code, model, documentation, description) VALUES ('30002', 'Feldweg_Waldweg', 'öffentlicher Feldweg / Waldweg', 'Feldweg / Waldweg');
INSERT INTO bra_baugrundtyp (code, model, documentation, description) VALUES ('30003', 'Wirtschaftsweg', 'Wirtschaftsweg (nicht straßenbegleitend, Deckschicht ohne Bindemittel)', 'Wirtschaftsweg');
INSERT INTO bra_baugrundtyp (code, model, documentation, description) VALUES ('4000', 'Gehweg_Radweg_innerorts', 'Gehweg / Radweg (innerorts)', 'Gehweg / Radweg (innerorts)');
INSERT INTO bra_baugrundtyp (code, model, documentation, description) VALUES ('40001', 'Gehweg', 'Gehweg', 'Gehweg');
INSERT INTO bra_baugrundtyp (code, model, documentation, description) VALUES ('40002', 'Radweg', 'Radweg', 'Radweg');
INSERT INTO bra_baugrundtyp (code, model, documentation, description) VALUES ('5000', 'Gruenstreifen', 'Grünstreifen / Straßenbegleitgrün (innerorts)', 'Grünstreifen / Straßenbegleitgrün (innerorts)');
INSERT INTO bra_baugrundtyp (code, model, documentation, description) VALUES ('6000', 'Parkplatz_Parkstreifen', 'Parkplatz / Parkstreifen', 'Parkplatz / Parkstreifen');
INSERT INTO bra_baugrundtyp (code, model, documentation, description) VALUES ('7000', 'Strassengrundstueck', 'Straßengrundstück', 'Straßengrundstück');
INSERT INTO bra_baugrundtyp (code, model, documentation, description) VALUES ('9999', 'SonstigeWegeflaechen', 'sonstige Wegeflächen', 'sonstige Wegeflächen');
INSERT INTO bra_baustelletyp (code, model, documentation, description) VALUES ('1000', 'Baustelleneinrichtung', 'Produktions-, Transport-, Lager- und sonstige Einrichtungen, die zur Errichtung eines Bauwerks auf der Baustelle benötigt werden.', 'Fläche für Baustelleneinrichtung');
INSERT INTO bra_baustelletyp (code, model, documentation, description) VALUES ('2000', 'Graben_Grube', 'Zur Verlegung von Leitungen auszuhebende Gräben und Gruben', 'Graben oder Grube');
INSERT INTO bra_baustelletyp (code, model, documentation, description) VALUES ('3000', 'Bauabschnitt', 'Räumliche Ausdehnung eines oder mehrerer Bauabschnitte einer Breitbandtrasse', 'Ausdehnung des Bauabschnitts');
INSERT INTO bra_farbe (code, model, documentation, description) VALUES ('1000', 'Rot', 'Rot', 'Rot');
INSERT INTO bra_farbe (code, model, documentation, description) VALUES ('1100', 'Gruen', 'Grün', 'Grün');
INSERT INTO bra_farbe (code, model, documentation, description) VALUES ('1200', 'Blau', 'Blau', 'Blau');
INSERT INTO bra_farbe (code, model, documentation, description) VALUES ('1300', 'Gelb', 'Gelb', 'Gelb');
INSERT INTO bra_farbe (code, model, documentation, description) VALUES ('1400', 'Weiss', 'Weiß', 'Weiß');
INSERT INTO bra_farbe (code, model, documentation, description) VALUES ('1500', 'Grau', 'Grau', 'Grau');
INSERT INTO bra_farbe (code, model, documentation, description) VALUES ('1600', 'Braun', 'Braun', 'Braun');
INSERT INTO bra_farbe (code, model, documentation, description) VALUES ('1700', 'Violett', 'Violett', 'Violett');
INSERT INTO bra_farbe (code, model, documentation, description) VALUES ('1800', 'Tuerkis', 'Türkis', 'Türkis');
INSERT INTO bra_farbe (code, model, documentation, description) VALUES ('1900', 'Schwarz', 'Schwarz', 'Schwarz');
INSERT INTO bra_farbe (code, model, documentation, description) VALUES ('2000', 'Orange', 'Orange', 'Orange');
INSERT INTO bra_farbe (code, model, documentation, description) VALUES ('2100', 'Pink', 'Pink', 'Pink');
INSERT INTO bra_mikrorohrtyp (code, model, documentation, description) VALUES ('1100', '7x0,75', '7x0,75', '7x0,75');
INSERT INTO bra_mikrorohrtyp (code, model, documentation, description) VALUES ('1200', '7x1,5', '7x1,5', '7x1,5');
INSERT INTO bra_mikrorohrtyp (code, model, documentation, description) VALUES ('2100', '10x1,0', '10x1,0', '10x1,0');
INSERT INTO bra_mikrorohrtyp (code, model, documentation, description) VALUES ('2200', '10x2,0', '10x2,0', '10x2,0');
INSERT INTO bra_mikrorohrtyp (code, model, documentation, description) VALUES ('3100', '12x1,1', '12x1,1', '12x1,1');
INSERT INTO bra_mikrorohrtyp (code, model, documentation, description) VALUES ('3200', '12x2,0', '12x2,0', '12x2,0');
INSERT INTO bra_mikrorohrtyp (code, model, documentation, description) VALUES ('4100', '14x1,3', '14x1,3', '14x1,3');
INSERT INTO bra_mikrorohrtyp (code, model, documentation, description) VALUES ('4200', '14x2,0', '14x2,0', '14x2,0');
INSERT INTO bra_mikrorohrtyp (code, model, documentation, description) VALUES ('5100', '16x1,5', '16x1,5', '16x1,5');
INSERT INTO bra_mikrorohrtyp (code, model, documentation, description) VALUES ('5200', '16x2,0', '16x2,0', '16x2,0');
INSERT INTO bra_mikrorohrtyp (code, model, documentation, description) VALUES ('6100', '20x2.0', '20x2.0', '20x2.0');
INSERT INTO bra_mikrorohrtyp (code, model, documentation, description) VALUES ('6200', '20x2,5', '20x2,5', '20x2,5');
INSERT INTO bra_mikrorohrtyp (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstiges', 'Sonstiges');
INSERT INTO bra_netztechnik (code, model, documentation, description) VALUES ('1000', 'Hauptverteiler_HVt', 'Hauptverteiler (HVt) - konventionell', 'Hauptverteiler ( HVt) - konventionell');
INSERT INTO bra_netztechnik (code, model, documentation, description) VALUES ('2000', 'GlasfaserHVt_PoP', 'Glasfaser-HVt/ PoP', 'Glasfaser-HVt/ PoP');
INSERT INTO bra_netztechnik (code, model, documentation, description) VALUES ('3000', 'DSLAM_MSAN', 'DSLAM/MSAN', 'DSLAM/ MSAN');
INSERT INTO bra_netztechnik (code, model, documentation, description) VALUES ('4000', 'GlasfaserVerteiler', 'Glasfaser-Verteiler', 'Glasfaser-Verteiler');
INSERT INTO bra_netztechnik (code, model, documentation, description) VALUES ('5000', 'Kabelmuffe', 'Kabelmuffe', 'Kabelmuffe');
INSERT INTO bra_netztechnik (code, model, documentation, description) VALUES ('6000', 'Hausuebergabepunkt_APL', 'Hausübergabepunkt/ APL', 'Hausübergabepunkt/ APL');
INSERT INTO bra_netztechnik (code, model, documentation, description) VALUES ('7000', 'UebergabepunktBackbone', 'Übergabepunkt Backbone', 'Übergabepunkt Backbone');
INSERT INTO bra_netztechnik (code, model, documentation, description) VALUES ('8000', 'OpticalLineTermination_OLT', 'Optical Line Termination (OLT)', 'Optical Line Termination (OLT)');
INSERT INTO bra_netztechnik (code, model, documentation, description) VALUES ('9000', 'OptischerSplitter', 'Optischer Splitter', 'Optischer  Splitter');
INSERT INTO bra_netztechnik (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstiges', 'Sonstiges');
INSERT INTO bra_planstatus (code, model, documentation, description) VALUES ('1000', 'VerlegungTKG', 'Verlegung einer neuen Trasse/TK-Linie gemäß § 127 Abs. 1 TKG', 'Verlegung TK-Linie nach TKG');
INSERT INTO bra_planstatus (code, model, documentation, description) VALUES ('2000', 'AenderungTKG', 'Änderung einer bestehenden Trasse/TK-Linie gemäß § 127 Abs. 1 TKG', 'Änderung TK-Linie nach TKG');
INSERT INTO bra_planstatus (code, model, documentation, description) VALUES ('3000', 'AnzeigeTKG', 'Geringfügige bauliche Maßnahme gemäß § 127 Abs. 4 TKG', 'Anzeige gemäß TKG ');
INSERT INTO bra_planstatus (code, model, documentation, description) VALUES ('4000', 'AnzeigeRahmenvertrag', 'Anzuzeigende Maßnahme gemäß Rahmenvertrag', 'Anzeige gemäß Rahmenvertrag');
INSERT INTO bra_rohrmuffetyp (code, model, documentation, description) VALUES ('1000', 'Abzweigemuffe', 'Abzweigemuffe', 'Abzweigemuffe');
INSERT INTO bra_rohrmuffetyp (code, model, documentation, description) VALUES ('2000', 'Verbindungsmuffe', 'Verbindungsmuffe', 'Verbindungsmuffe');
INSERT INTO bra_rohrmuffetyp (code, model, documentation, description) VALUES ('3000', 'Endmuffe', 'Endmuffe', 'Endmuffe');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('03218', '32x1,8', '32x1,8', '32x1,8');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('03229', '32x2,9', '32x2,9', '32x2,9');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('03240', '32x4,0', '32x4,0', '32x4,0');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('04019', '40x1,9', '40x1,9', '40x1,9');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('04037', '40x3,7', '40x3,7', '40x3,7');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('04040', '40x4,0', '40x4,0', '40x4,0');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('05018', '50x1,8', '50x1,8', '50x1,8');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('05024', '50x2,4', '50x2,4', '50x2,4');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('05040', '50x4,0', '50x4,0', '50x4,0');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('05046', '50x4,6', '50x4,6', '50x4,6');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('06319', '63x1,9', '63x1,9', '63x1,9');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('06330', '63x3,0', '63x3,0', '63x3,0');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('06347', '63x4,7', '63x4,7', '63x4,7');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('07522', '75x2,2', '75x2,2', '75x2,2');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('07536', '75x3,6', '75x3,6', '75x3,6');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('08525', '85x2,5', '85x2,5', '85x2,5');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('09027', '90x2,7', '90x2,7', '90x2,7');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('09043', '90x4,3', '90x4,3', '90x4,3');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('11032', '110x3,2', '110x3,2', '110x3,2');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('11034', '110x3,4', '110x3,4', '110x3,4');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('11037', '110x3,7', '110x3,7', '110x3,7');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('11042', '110x4,2', '110x4,2', '110x4,2');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('11053', '110x5,3', '110x5,3', '110x5,3');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('11063', '110x6,3', '110x6,3', '110x6,3');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('12537', '125x3,7', '125x3,7', '125x3,7');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('12539', '125x3,9', '125x3,9', '125x3,9');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('12548', '125x4,8', '125x4,8', '125x4,8');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('12560', '125x6,0', '125x6,0', '125x6,0');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('12571', '125x7,1', '125x7,1', '125x7,1');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('14041', '140x4,1', '140x4,1', '140x4,1');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('14067', '140x6,7', '140x6,7', '140x6,7');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('16047', '160x4,7', '160x4,7', '160x4,7');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('16049', '160x4,9', '160x4,9', '160x4,9');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('16062', '160x6,2', '160x6,2', '160x6,2');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('16077', '160x7,7', '160x7,7', '160x7,7');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('16091', '160x9,1', '160x9,1', '160x9,1');
INSERT INTO bra_schutzrohrtyp (code, model, documentation, description) VALUES ('99999', 'Sonstiges', 'Sonstiges', 'Sonstiges');
INSERT INTO bra_strassetyp (code, model, documentation, description) VALUES ('1000', 'Bundesautobahn', 'Bundesautobahn', 'Bundesautobahn');
INSERT INTO bra_strassetyp (code, model, documentation, description) VALUES ('1100', 'Bundesstrasse', 'Bundesstraße', 'Bundesstraße');
INSERT INTO bra_strassetyp (code, model, documentation, description) VALUES ('2000', 'Landesstrasse', 'Landesstraße', 'Landesstraße');
INSERT INTO bra_strassetyp (code, model, documentation, description) VALUES ('2100', 'Staatsstrasse', 'Staatsstraße (Landesstraße in Bayern)', 'Staatsstraße');
INSERT INTO bra_strassetyp (code, model, documentation, description) VALUES ('3000', 'Hauptverkehrsstrasse', 'Hauptverkehrsstraße (in Hamburg)', 'Hauptverkehrsstraße');
INSERT INTO bra_strassetyp (code, model, documentation, description) VALUES ('4000', 'Kreisstrasse', 'Kreisstraße', 'Kreisstraße');
INSERT INTO bra_strassetyp (code, model, documentation, description) VALUES ('5000', 'Gemeindestrasse', 'Gemeindestraße', 'Gemeindestraße');
INSERT INTO bra_strassetyp (code, model, documentation, description) VALUES ('5100', 'BSGB', 'Bezirksstraße mit Gesamtstädtischer Bedeutung (BSGB)', 'Bezirksstraße mit Gesamtstädtischer Bedeutung ');
INSERT INTO bra_strassetyp (code, model, documentation, description) VALUES ('5200', 'Bezirksstrasse', 'Bezirksstraße', 'Bezirksstraße');
INSERT INTO bra_strassetyp (code, model, documentation, description) VALUES ('9999', 'SonstigeStrasse', 'sonstige Straße', 'sonstige Straße');
INSERT INTO bra_umfeldtyp (code, model, documentation, description) VALUES ('1000', 'Abstandsflaeche', 'Fläche zur Markierung von Abständen zwischen bestimmten Objekten', 'Abstandsfläche');
INSERT INTO bra_umfeldtyp (code, model, documentation, description) VALUES ('2000', 'Verkehsflaeche', 'Fläche zur Markierung einer vorhandenen Verkehrsfläche', 'Verkehsfläche');
INSERT INTO bra_umfeldtyp (code, model, documentation, description) VALUES ('9999', 'sonstigeFlaeche', 'Markierung einer sonstigen Fläche', 'sonstige Fläche');
INSERT INTO bra_verfuellmethode (code, model, documentation, description) VALUES ('1000', 'Konventionell', 'konventionelle Verfüllung, z.B. mit Aushub', 'konventionell');
INSERT INTO bra_verfuellmethode (code, model, documentation, description) VALUES ('2000', 'Fluessigboden', 'z.B. Flüssigasphalt', 'Flüssigboden');
INSERT INTO bra_verfuellmethode (code, model, documentation, description) VALUES ('3000', 'SonstigeVerfuellung', 'Sonstiges', 'sonstige Verfüllung');
INSERT INTO bst_kanaltyp (code, model, documentation, description) VALUES ('1000', 'Schmutzwasser', 'Schmutzwasser', 'Schmutzwasser');
INSERT INTO bst_kanaltyp (code, model, documentation, description) VALUES ('2000', 'Regenwasser', 'Regenwasser', 'Regenwasser');
INSERT INTO bst_kanaltyp (code, model, documentation, description) VALUES ('3000', 'Mischwasser', 'Mischwasser', 'Mischwasser');
INSERT INTO bst_masttyp (code, model, documentation, description) VALUES ('1000', 'Funkmast', 'ortsfester Funkanlagenstandort', 'Funkmast');
INSERT INTO bst_masttyp (code, model, documentation, description) VALUES ('2000', 'Sendemast', 'Zumeist Konstruktion aus Stahlfachwerk oder Stahlrohr, die zur Aufnahme von Antennen für Sendezwecke bzw. zur unmittelbaren Verwendung als Sendeantenne dient (Für digitalen Datenfunk ist häufig die Nutzung vorhandener hoher Bauwerke ausreichend)', 'Sendemast');
INSERT INTO bst_masttyp (code, model, documentation, description) VALUES ('3000', 'Telefonmast', 'Ein Telefonmast (Telegrafenmast) trägt eine oberirdisch gezogene Fernsprechleitung', 'Telefonmast');
INSERT INTO bst_masttyp (code, model, documentation, description) VALUES ('4000', 'Freileitungsmast', 'Der Freileitungsmast (Strommast) ist eine Konstruktion für die Aufhängung einer elektrischen Freileitung. Je nach Funktion lässt sich zwischen Trag-, Abspann-, Abzweig-, Kabelend- und Endabspannmasten unterscheiden. Je nach der elektrischen Spannung der Freileitung werden unterschiedliche Freileitungsmasten aus verschiedenen Materialen verwendet (Masten zur Nachrichtenübermittlung werden separat als Telefonmasten erfasst)', 'Freileitungsmast');
INSERT INTO bst_masttyp (code, model, documentation, description) VALUES ('5000', 'Strassenleuchte', 'Trägersystem der Straßenbeleuchtung. Die Leuchte wird an der Spitze eines Holz-, Stahl-, Aluminium- oder Betonmastes montiert, wobei ein Ausleger über die Straße ragt. Teilweise werden Straßenleuchten in dicht bebauten Gebieten an Seilen hängend über der Straße (Überspannungsanlage) oder an Hauswänden angebracht.', 'Straßenleuchte');
INSERT INTO bst_masttyp (code, model, documentation, description) VALUES ('6000', 'Ampel', 'Signalgeber einer Lichtsignalanlage (LSA) oder Lichtzeichenanlage (LZA). Sie dient der Steuerung des Straßen- und Schienenverkehrs.', 'Ampel');
INSERT INTO bst_netzsparte (code, model, documentation, description) VALUES ('1000', 'Telekommunikation', 'Telekommunikation', 'Telekommunikation');
INSERT INTO bst_netzsparte (code, model, documentation, description) VALUES ('2000', 'Gas', 'Gasversorgung', 'Gas');
INSERT INTO bst_netzsparte (code, model, documentation, description) VALUES ('3000', 'Elektrizitaet', 'Stromversorgung', 'Elektrizität');
INSERT INTO bst_netzsparte (code, model, documentation, description) VALUES ('4000', 'Waermeversorgung', 'Versorgung mit Fern- oder Nahwärme', 'Wärmeversorgung');
INSERT INTO bst_netzsparte (code, model, documentation, description) VALUES ('5000', 'Abwasserentsorgung', 'Abwasserentsorgung', 'Abwasserentsorgung');
INSERT INTO bst_netzsparte (code, model, documentation, description) VALUES ('6000', 'Wasserversorgung', 'Trinkwasserversorgung', 'Wasserversorgung');
INSERT INTO bst_netzsparte (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstiges Ver- bzw. Entsorgungsnetz', 'Sonstiges Netz');
INSERT INTO bst_spannung (code, model, documentation, description) VALUES ('1000', 'Niedrigspannung', 'Niedrigspannung (< 1 kV)', 'Niedrigspannung');
INSERT INTO bst_spannung (code, model, documentation, description) VALUES ('2000', 'Mittelspannung', 'Der Begriff Mittelspannung ist nicht genormt bzw. in den Grenzen nicht exakt definiert. Die oberen Grenze wird häufig  mit 30 oder 50 kV angegeben.', 'Mittelspannung');
INSERT INTO bst_spannung (code, model, documentation, description) VALUES ('3000', 'Hochspannung', 'Hochspannung', 'Hochspannung');
INSERT INTO bst_spannung (code, model, documentation, description) VALUES ('30001', 'Hochspannung_110 kV', 'Hochspannung 110 kV', 'Hochspannung 110 kV');
INSERT INTO bst_spannung (code, model, documentation, description) VALUES ('30002', 'Hoechstspannung_220 kV', 'Höchstspannung 220 kV', 'Höchstspannung 220 kV');
INSERT INTO bst_spannung (code, model, documentation, description) VALUES ('30003', 'Hoechstspannung_380 kV', 'Höchstspannung 380 kV', 'Höchstspannung 380 kV');
INSERT INTO bst_spannung (code, model, documentation, description) VALUES ('9999', 'UnbekannteSpannung', 'Unbekannte Spannung', 'Unbekannte Spannung');
INSERT INTO bst_statusaenderung (code, model, documentation, description) VALUES ('1000', 'Wiederinbetriebnahme', 'Wiederinbetriebnahme einer Leitung; Wiederinbetriebnahme eines Infrastrukturobjektes', 'Wiederinbetriebnahme');
INSERT INTO bst_statusaenderung (code, model, documentation, description) VALUES ('2100', 'Ausserbetriebnahme', 'Betriebszustand einer Leitung, in der aktuell kein Medientransport erfolgt, die Anlage jedoch für diesen Zweck weiterhin vorgehalten wird (Eine Gasleitung wird weiterhin überwacht und betriebsbereit instandgehalten, sie ist ebenso in den Korrosionsschutz eingebunden)', 'Außerbetriebnahme');
INSERT INTO bst_statusaenderung (code, model, documentation, description) VALUES ('2200', 'Stilllegung', 'Endgültige Einstellung des Betriebs ohne dass ein vollständiger Rückbau der Leitung vorgesehen ist. Die Anlage wird nach endgültiger Stilllegung so gesichert, dass von ihr keine Gefahr ausgeht.', 'Stilllegung');
INSERT INTO bst_statusaenderung (code, model, documentation, description) VALUES ('3000', 'Rueckbau', 'Rückbau einer Leitung nach endgültiger Stilllegung; Rückbau eines Infrastrukturobjektes', 'Rückbau');
INSERT INTO bst_statusaenderung (code, model, documentation, description) VALUES ('4000', 'Sanierung', 'Sanierung oder Instandsetzung bestehender Leitungen', 'Sanierung');
INSERT INTO bst_statusaenderung (code, model, documentation, description) VALUES ('40001', 'Umstellung_H2', 'Umstellung von Leitungen und Speichern für Transport und Speicherung von Wasserstoff', 'Umstellung H2');
INSERT INTO bst_statusaenderung (code, model, documentation, description) VALUES ('5000', 'AenderungErweiterung', 'NABEG § 3, Nr.1: Änderung oder  Ausbau einer Leitung in einer Bestandstrasse, wobei die bestehende Leitung grundsätzlich fortbestehen soll', 'Änderung/Erweiterung');
INSERT INTO bst_statusaenderung (code, model, documentation, description) VALUES ('6100', 'Ersatzneubau', 'NABEG § 3, Nr. 4: Errichtung einer neuen Leitung in oder unmittelbar neben einer Bestandstrasse, wobei die bestehende Leitung innerhalb von drei Jahren ersetzt wird; die Errichtung erfolgt in der Bestandstrasse, wenn sich bei Freileitungen die Mastfundamente und bei Erdkabeln die Kabel in der Bestandstrasse befinden; die Errichtung erfolgt unmittelbar neben der Bestandstrasse, wenn ein Abstand von 200 Metern zwischen den Trassenachsen nicht überschritten wird.', 'Ersatzneubau');
INSERT INTO bst_statusaenderung (code, model, documentation, description) VALUES ('6200', 'Parallelneubau', 'NABEG § 3, Nr.5: Errichtung einer neuen Leitung unmittelbar neben einer Bestandstrasse, wobei die bestehende Leitung fortbestehen soll; die Errichtung erfolgt unmittelbar neben der Bestandstrasse, wenn ein Abstand von 200 Metern zwischen den Trassenachsen nicht überschritten wird.', 'Parallelneubau');
INSERT INTO bst_statusaktuell (code, model, documentation, description) VALUES ('1000', 'InBetrieb', 'Bestandsobjekt ist in Betrieb', 'in Betrieb');
INSERT INTO bst_statusaktuell (code, model, documentation, description) VALUES ('2100', 'AusserBetriebGenommen', 'Bestandsobjekt ist temporär oder dauerhaft außer Betrieb genommen aber nicht stillgelegt', 'außer Betrieb genommen');
INSERT INTO bst_statusaktuell (code, model, documentation, description) VALUES ('2200', 'Stillgelegt', 'Bestandsobjekt ist dauerhaft stillgelegt und steht nicht mehr für eine Wiederinbetriebnahme zur Verfügung', 'stillgelegt');
INSERT INTO bst_statusaktuell (code, model, documentation, description) VALUES ('3000', 'ImRueckbau', 'Bestandsobjekt ist aktuell im Rückbau', 'im Rückbau');
INSERT INTO bst_statusaktuell (code, model, documentation, description) VALUES ('4000', 'InSanierung', 'Bestandsobjekt ist nicht in Betrieb, da Instandsetzungs- oder Sanierungsarbeiten erfolgen', 'in Sanierung');
INSERT INTO bst_statusaktuell (code, model, documentation, description) VALUES ('5000', 'InAenderung', 'Bestandsobjekt wird zurzeit geändert oder erweitertert (gemäß NABEG § 3, Nr.1)', 'in Änderung/Erweiterung');
INSERT INTO bst_statusaktuell (code, model, documentation, description) VALUES ('6000', 'InErsetzung', 'Bestandsobjekt wird zurzeit durch einen Neubau ersetzt (Ersatzneubau nach NABEG § 3, Nr. 4)', 'in Ersetzung');
INSERT INTO bst_statusaktuell (code, model, documentation, description) VALUES ('9999', 'UnbekannterStatus', 'aktueller Status des Bestandsobjektes ist unbekannt', 'unbekannter Status');
INSERT INTO bst_wegekantetyp (code, model, documentation, description) VALUES ('1000', 'Strassenkante', 'Straßenkante', 'Straßenkante');
INSERT INTO bst_wegekantetyp (code, model, documentation, description) VALUES ('2000', 'KanteFahrradweg', 'Kante Fahrradweg', 'Kante Fahrradweg');
INSERT INTO bst_wegekantetyp (code, model, documentation, description) VALUES ('3000', 'KanteGehweg', 'Kante Gehweg', 'Kante Gehweg');
INSERT INTO xp_armatureinsatzgebiet (code, model, documentation, description) VALUES ('1000', 'Streckenarmatur', 'Armaturen in Abständen entlang einer Leitung', 'Streckenarmatur');
INSERT INTO xp_armatureinsatzgebiet (code, model, documentation, description) VALUES ('2000', 'Ausblasearmatur', 'Dient dem kontrollierten Ableiten von Gasen und Gas-Luftgemischen innerhalb eines Rohrnetzes', 'Ausblasearmatur');
INSERT INTO xp_armatureinsatzgebiet (code, model, documentation, description) VALUES ('3000', 'Hauptabsperreinrichtung', 'Hauptabsperreinrichtung', 'Hauptabsperreinrichtung');
INSERT INTO xp_armatureinsatzgebiet (code, model, documentation, description) VALUES ('4000', 'Ein_Ausgangsarmatur', 'Eingangs- und Ausgangsarmaturen im Rohrnetz', 'Ein-/ Ausgangsarmatur');
INSERT INTO xp_armatureinsatzgebiet (code, model, documentation, description) VALUES ('5000', 'Hydrant', 'Hydrant', 'Hydrant');
INSERT INTO xp_armatureinsatzgebiet (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'sonstiges Einsatzgebiet', 'sonstiges Einsatzgebiet');
INSERT INTO xp_armaturfunktion (code, model, documentation, description) VALUES ('1000', 'Absperrarmatur', 'Absperrung von Stoffströmen durch Hähne und Klappen', 'Absperramatur');
INSERT INTO xp_armaturfunktion (code, model, documentation, description) VALUES ('2000', 'Regulierarmatur', 'Regulierung des Volumenstroms mittels Schieber und Ventilen', 'Regulierarmatur');
INSERT INTO xp_armaturfunktion (code, model, documentation, description) VALUES ('3000', 'Entlueftungsarmatur', 'Dient dem Enfernen von Gasen, insbesondere Luft, aus einer flüssigkeitsführenden Anlage', 'Entlüftungsarmatur');
INSERT INTO xp_armaturfunktion (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'sonstige Funktion', 'sonstige Funktion');
INSERT INTO xp_baugrubetyp (code, model, documentation, description) VALUES ('1000', 'Startgrube', 'Startgrube', 'Startgrube');
INSERT INTO xp_baugrubetyp (code, model, documentation, description) VALUES ('2000', 'Zielgrube', 'Zielgrube', 'Zielgrube');
INSERT INTO xp_bauweise (code, model, documentation, description) VALUES ('1000', 'OffeneBauweise', 'offene Bauweise', 'offene Bauweise');
INSERT INTO xp_bauweise (code, model, documentation, description) VALUES ('2000', 'GeschlosseneBauweise', 'geschlossene Bauweise', 'geschlossene Bauweise');
INSERT INTO xp_bauweise (code, model, documentation, description) VALUES ('3000', 'Oberirdisch', 'oberirdische Verlegung', 'oberirdische Verlegung');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('1000', 'Gasspeicher', 'Oberirdische Nieder- und Mitteldruckbehälter (Gastürme, Gasometer) sowie Hochdruckbehälter (Röhrenspeicher, Kugelspeicher) zur Aufbewahrung von Gasen aller Art', 'Gasspeicher');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('2000', 'Untergrundspeicher', 'Ein Untergrundspeicher (auch Untertagespeicher) ist ein Speicher in natürlichen oder künstlichen Hohlräumen unter der Erdoberfläche. - Untergrundspeicher gemäß Bundesberggesetz (BBergG) § 126', 'Untergrundspeicher');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('20001', 'Kavernenspeicher', 'Große, künstlich angelegte Hohlräume in mächtigen unterirdischen Salzformationen, wie z.B. Salzstöcken. Kavernenspeicher werden durch einen Solprozess bergmännisch angelegt.', 'Kavernenspeicher');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('20002', 'Porenspeicher', 'Natürliche Lagerstätten, die sich durch ihre geologische Formation zur Speicherung von Gas eignen. Sie befinden sich in porösem Gestein, in dem das Gas ähnlich einem stabilen Schwamm aufgenommen und eingelagert wird.', 'Porenspeicher');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('3000', 'Stromspeicher', 'Großspeicheranlagen im Stromnetz', 'Stromspeicher');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('30001', 'Batteriespeicher', 'Großbatteriespeicher (z.B. an einer PV-Anlage)', 'Batteriespeicher');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('30002', 'Pumpspeicherkraftwerk', 'Ein Pumpspeicherkraftwerk (PSW) speichert elektrische Energie in Form von potentieller Energie (Lageenergie) in einem Stausee', 'Pumpspeicherkraftwerk');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('4000', 'Fernwaermespeicher', 'Zumeist drucklose, mit Wasser gefüllte Behälter, die Schwankungen im Wärmebedarf des Fernwärmenetzes bei gleicher Erzeugungsleistung der Fernheizwerke ausgleichen sollen', 'Fernwärmespeicher');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstige Speicher', 'sonstige Speicher');
INSERT INTO xp_gasdruckstufe (code, model, documentation, description) VALUES ('1000', 'Niederdruck', 'Niederdruck', 'Niederdruck');
INSERT INTO xp_gasdruckstufe (code, model, documentation, description) VALUES ('2000', 'Mitteldruck', 'Mitteldruck', 'Mitteldruck');
INSERT INTO xp_gasdruckstufe (code, model, documentation, description) VALUES ('3000', 'Hochdruck', 'Hochdruck', 'Hochdruck');
INSERT INTO xp_gasdruckstufe (code, model, documentation, description) VALUES ('9999', 'UnbekannterDruck', 'Unbekannter Druck', 'Unbekannter Druck');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('1000', 'Erdgas', 'Erdgas', 'Erdgas');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('10001', 'L_Gas', 'L-Gas (low calorific gas)', 'L-Gas');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('10002', 'H_Gas', 'H-Gas (high calorific gas)', 'H-Gas');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('2000', 'Wasserstoff', 'Wasserstoff (H2)', 'Wasserstoff');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('20001', 'GruenerWasserstoff', 'Durch die Elektrolyse von Wasser hergestellter Wasserstoff unter Verwendung von Strom aus erneuerbaren Energiequellen', 'grüner Wasserstoff');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('20002', 'BlauerWasserstoff', 'Grauer Wasserstoff, bei dessen Entstehung das CO2 jedoch teilweise abgeschieden und im Erdboden gespeichert wird (CCS, Carbon Capture and Storage). Maximal 90 Prozent des CO₂ sind speicherbar.', 'blauer Wasserstoff');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('20003', 'OrangenerWasserstoff', 'Auf Basis von Abfall und Reststoffen produzierter Wasserstoff, der als CO2-frei gilt', 'orangener Wasserstoff');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('20004', 'GrauerWasserstoff', 'Mittels Dampfreformierung meist aus fossilem Erdgas hergestellter Wasserstoff. Dabei entstehen rund 10 Tonnen CO₂ pro Tonne Wasserstoff. Das CO2 wird in die Atmosphäre abgegeben.', 'grauer Wasserstoff');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('3000', 'Erdgas_H2_Gemisch', 'Erdgas-Wasserstoff-Gemisch', 'Erdgas-Wasserstoff-Gemisch');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('4000', 'Biogas', 'Biogas', 'Biogas');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('5000', 'Fluessiggas', 'Flüssiggas', 'Flüssiggas');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('6000', 'SynthetischesMethan', 'Wird durch wasserelektrolytisch erzeugten Wasserstoff und anschließende Methanisierung hergestellt', 'synthetisch erzeugtes Methan');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'sonstiges Gas', 'sonstiges Gas');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('1000', 'TK_Verteiler', 'Verteilerschränke der Telekommunikation', 'TK-Verteiler');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('10001', 'Multifunktionsgehaeuse', 'Multifunktionsgehäuse', 'Multifunktionsgehäuse');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('10002', 'GlasfaserNetzverteiler', 'Glasfaser-Netzverteiler (Gf-NVt)', 'Glasfaser-Netzverteiler (Gf- NVt)');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('10003', 'Kabelverzweiger_KVz', 'Kabelverzweiger (KVz) - (Telekom AG)', 'Kabelverzweiger ( KVz) - (Telekom  AG)');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('2000', 'Strom_Schrank', 'Schränke für die Stromversorgung, öffentliche Beleuchtung, Verkehrstechnik u.a.', 'Strom-Schrank');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('20001', 'Schaltschrank', 'Schaltschrank', 'Schaltschrank');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('20002', 'Kabelverteilerschrank', 'Kabelverteilerschrank', 'Kabelverteilerschrank');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('20003', 'Steuerschrank', 'Steuerschrank', 'Steuerschrank');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('20004', 'Trennschrank', 'Trennschrank', 'Trennschrank');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'sonstiger Schrank', 'sonstiger Schrank');
INSERT INTO xp_infrastrukturflaeche (code, model, documentation, description) VALUES ('1000', 'Betriebsgelaende', 'gesamtes Betriebsgelände bzw. Grundstücksfläche', 'Betriebsgelände');
INSERT INTO xp_infrastrukturflaeche (code, model, documentation, description) VALUES ('2000', 'EingezaeunteFlaeche', 'eingezäuntes Gelände der Infrastrukturgebäude (ohne Parkplätze und Nebengebäude)', 'eingezäunte Fläche');
INSERT INTO xp_infrastrukturflaeche (code, model, documentation, description) VALUES ('3000', 'Gebaeudeflaeche', 'Fläche eines Gebäudes, das technische Anlagen enthält', 'Gebäudefläche');
INSERT INTO xp_kabeltyp (code, model, documentation, description) VALUES ('1000', 'Glasfaserkabel', 'Glasfaserkabel', 'Glasfaserkabel');
INSERT INTO xp_kabeltyp (code, model, documentation, description) VALUES ('2000', 'Kupferkabel', 'Kupferkabel', 'Kupferkabel');
INSERT INTO xp_kabeltyp (code, model, documentation, description) VALUES ('3000', 'Hybridkabel', 'Hybridkabel', 'Hybridkabel');
INSERT INTO xp_kabeltyp (code, model, documentation, description) VALUES ('4000', 'Koaxialkabel', 'Koaxial-(TV)-Kabel', 'Koaxial-(TV)-Kabel');
INSERT INTO xp_kraftwerktyp (code, model, documentation, description) VALUES ('1000', 'ThermischeTurbine', 'Thermische arbeitende Dampfturbinen- und Gasturbinen-Kraftwerke oder Gas-und-Dampf-Kombikraftwerke', 'Thermisch arbeitende Turbine');
INSERT INTO xp_kraftwerktyp (code, model, documentation, description) VALUES ('2000', 'Windkraft', 'Eine Windkraftanlage (WKA) oder Windenergieanlage (WEA) wandelt Bewegungsenergie des Windes in elektrische Energie um und speist sie in ein Stromnetz ein. Sie werden an Land (onshore) und in Offshore-Windparks im Küstenvorfeld der Meere installiert. Eine Gruppe von Windkraftanlagen wird Windpark genannt.', 'Windkraftanlage');
INSERT INTO xp_kraftwerktyp (code, model, documentation, description) VALUES ('3000', 'Photovoltaik', 'Eine Photovoltaikanlage, auch PV-Anlage (bzw. PVA) wandelt mittels Solarzellen ein Teil der Sonnenstrahlung in elektrische Energie um.  Die Photovoltaik-Freiflächenanlage (auch Solarpark) wird auf einer freien Fläche als fest montiertes System aufgestellt, bei dem mittels einer Unterkonstruktion die Photovoltaikmodule in einem optimalen Winkel zur Sonne (Azimut) ausgerichtet sind.', 'Photovoltaik-Freinflächenanlage');
INSERT INTO xp_kraftwerktyp (code, model, documentation, description) VALUES ('4000', 'Wasserkraft', 'Ein Wasserkraftwerk wandelt die potentielle Energie des Wassers in der Regel über Turbinen in mechanische bzw. elektrische Energie um. Dies kann an Fließgewässern oder Stauseen erfolgen oder durch Strömungs- und Gezeitenkraftwerke auf dem Meer (Pumpspeicherkraftwerke s. PFS_Energiespeicheer)', 'Wasserkraftwerk');
INSERT INTO xp_kraftwerktyp (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstiges Kraftwerk', 'sonstiges Kraftwerk');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('1000', 'Konventionell_offenerGraben', 'Ausschachtung mit Schaufel, Bagger, Fräse', 'Konventionelle Verlegung im offenen Graben');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('2000', 'Pressbohrverfahren', 'Unterirdische Verlegetechnik, die in verschiedenen Varianten zur Anwendung kommt (statisch, dynamisch, ungesteuert, gesteuert) und von Herstellern spezifisch bezeichnet wird ("Modifiziertes Direct-Pipe-Verfahren").  Im Breitbandausbau auch als Erdraketentechnik bekannt. Im Rohrleitungsbau können durch hydraulische oder pneumatische Presseinrichtungen Produktenrohrkreuzungen DN 1000 bis zu 100 m grabenlos verlegt werden.', 'Pressbohrverfahren');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('3000', 'HorizontalSpuelbohrverfahren', 'Richtbohrtechnik für Horizontalbohrungen („Horizontal Directional Drilling“, HDD), die eine grabenlose Verlegung von Produkt- oder Leerrohren ermöglicht.  Die Bohrung ist anfangs meist schräg nach unten in das Erdreich gerichtet und verläuft dann in leichtem Bogen zum Ziel, wo sie schräg nach oben wieder zutage tritt.', 'Horizontal-Spülbohrverfahren');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('4000', 'Pflugverfahren', 'Erstellung eines Leitungsgrabens (Breite > 30cm) oder Schlitzes mit einem Pflugschwert durch Verdrängung der Schicht(en) und gleichzeitigem Einbringen der Glasfasermedien. Der Einsatz des Pflugverfahrens ist ausschließlich in unbefestigten Oberflächen zulässig.', 'Pflugverfahren');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('5000', 'Fraesverfahren_ungebundeOberfl', 'Fräsverfahren in ungebunden Oberflächen (Schlitzbreite: 15 bis 30 cm, Schlitztiefe: 40 bis 120 cm)', 'Fräsverfahren in ungebundenen Oberflächen');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('6000', 'Trenching', 'Erstellung eines Schlitzes (< 30 cm) in gebundenen Verkehrsflächen in verschiedenen Verfahren durch rotierende, senkrecht stehende Werkzeuge, wobei die Schicht(en) gelöst, zerkleinert und gefördert wird (werden)', 'Trenching');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('60001', 'Schleif_Saegeverfahren', 'Erstellung eines Schlitzes eine durch eine Schneideeinheit (Schlitzbreite: 1,5 bis 11 cm, Schlitztiefe: 7 bis 45 cm)', 'Schleif-/Sägeverfahren');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('60002', 'Fraesverfahren', 'Erstellung eines Schlitzes durch ein Fräswerkzeug (Kette, Rad), (Schlitzbreite: 5 bis 15 cm, Schlitztiefe: 30 bis 60 cm)', 'Fräsverfahren');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('7000', 'Rammverfahren', 'Vortriebsverfahren, welches durch hydraulisches oder pneumatisches Vibrationsrammen das Rohr unter dem Hindernis hindurch schlägt. Mit dem Rammverfahren können Produkten- oder Mantelrohrkreuzungen bis zu 100 m Vortriebslänge grabenlos verlegt werden.', 'Rammverfahren');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('8000', 'Microtunneling', 'Für den grabenlosen Vortrieb werden in dem steuerbaren Verfahren zunächst Stahlbetonrohre mit großem Nenndurchmesser verlegt,  in denen nach Durchführung der Unterquerung das eigentliche Produktenrohr eingebracht/eingezogen wird. Es kommt nur bei schwierigen Kreuzungen zur Anwendung.', 'Microtunneling');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('9000', 'oberirdischeVerlegung', 'oberirdische Verlegung mittels Holzmasten', 'oberirdische Verlegung');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstiges Verfahren', 'sonstiges Verfahren');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('1000', 'Erdverlegt', 'Oberkategorie für erdverlegte (Rohr-)Leitungen', 'erdverlegte (Rohr-)Leitungen');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('10001', 'Erdkabel', 'Ein Erdkabel ist ein im Erdboden verlegtes elektrisch genutztes Kabel mit einer besonders robusten Isolierung nach außen, dem Kabelmantel, der eine Zerstörung derselben durch chemische Einflüsse im Erdreich bzw. im Boden lebender Kleintiere verhindert.', 'Erdkabel');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('10002', 'Seekabel', 'Ein Seekabel (auch Unterseekabel, Unterwasserkabel) ist ein im Wesentlichen in einem Gewässer verlegtes Kabel zur Datenübertragung oder die Übertragung elektrischer Energie.', 'Seekabel');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('10003', 'Schutzrohr', 'Im Schutzrohr verlegte oder zu verlegende Kabel/Leitungen. - Schutzrohre schützen erdverlegte Leitungen vor mechanischen Einflüssen und Feuchtigkeit.', 'Schutzrohr');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('10004', 'Leerrohr', 'Über die Baumaßnahme hinaus unbelegtes Schutzrohr', 'Leerrohr (unbelegtes Schutzrohr)');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('10005', 'Leitungsbuendel', 'Bündel von Kabeln und/oder Schutzrohren in den Sparten Sparten Strom und Telekommunikation im Bestand', 'Leitungsbündel');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('10006', 'Dueker', 'Druckleitung zur Unterquerung von Straßen, Flüssen, Bahngleisen etc. Im Düker kann die Flüssigkeit das Hindernis überwinden, ohne dass Pumpen eingesetzt werden müssen.', 'Düker');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('2000', 'Oberirdisch', 'Oberirdisch verlegte Leitungen und Rohre', 'oberirdischer Verlauf');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('20001', 'Freileitung', 'Elektrische Leitung, deren spannungsführende Leiter im Freien durch die Luft geführt und meist auch nur durch die umgebende Luft voneinander und vom Erdboden isoliert sind. In der Regel werden die Leiterseile von Freileitungsmasten getragen, an denen sie mit Isolatoren befestigt sind.', 'Freileitung');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('20002', 'Heberleitung', 'Leitung zur Überquerung von Straßen oder zur Verbindung von Behältern (Gegenstück zu einem Düker)', 'Heberleitung');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('20003', 'Rohrbruecke', 'Eine Rohrbrücke oder Rohrleitungsbrücke dient dazu, einzelne oder mehrere Rohrleitungen oberirdisch über größere Entfernungen zu führen.', 'Rohrbrücke');
INSERT INTO xp_primaerenergietraeger (code, model, documentation, description) VALUES ('1000', 'FossilerBrennstoff', 'Fossile Energie wird aus Brennstoffen gewonnen, die in geologischer Vorzeit aus Abbauprodukten von toten Pflanzen und Tieren entstanden sind. Dazu gehören Braunkohle, Steinkohle, Torf, Erdgas und Erdöl.', 'fossiler Brennstoff');
INSERT INTO xp_primaerenergietraeger (code, model, documentation, description) VALUES ('2000', 'Ersatzbrennstoff', 'Ersatzbrennstoffe (EBS) bzw. Sekundärbrennstoffe (SBS) sind Brennstoffe, die aus Abfällen gewonnen werden. Dabei kann es sich sowohl um feste, flüssige oder gasförmige Abfälle aus Haushalten, Industrie oder Gewerbe handeln.', 'Ersatzbrennstoff');
INSERT INTO xp_primaerenergietraeger (code, model, documentation, description) VALUES ('3000', 'Biomasse', 'Der energietechnische Biomasse-Begriff umfasst tierische und pflanzliche Erzeugnisse, die zur Gewinnung von Heizenergie, von elektrischer Energie und als Kraftstoffe verwendet werden können (u.a. Holzpellets, Hackschnitzel, Stroh, Getreide, Altholz, Biogas). Energietechnisch relevante Biomasse kann in gasförmiger, flüssiger und fester Form vorliegen.', 'Biomasse');
INSERT INTO xp_primaerenergietraeger (code, model, documentation, description) VALUES ('4000', 'Erdwaerme', 'Geothermie bezeichnet die in den oberen Schichten der Erdkruste gespeicherte Wärme und deren Ausbeutung zur Wärme- oder Stromerzeugung. In der Energiegewinnung wird zwischen tiefer und oberflächennaher Geothermie unterschieden. Die tiefe Geothermie wird von Kraftwerken zur Stromerzeugung genutzt.', 'Erdwärme');
INSERT INTO xp_primaerenergietraeger (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstige Energieträger', 'sonstiger Energieträger');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('1000', 'Fernleitung', 'Fernleitung gemäß Umweltverträglichkeitsprüfung (UVPG), Anlage 1 und ENWG § 3, Nr. 19d/20; Leitungen der Fernleitungsnetzbetreiber', 'Fernleitung');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('2000', 'Verteilnetzleitung', 'Leitung eines Verteil(er)netzes; Leitungen der Versorgungsunternehmen', 'Verteilnetzleitung');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('3000', 'Hauptleitung', 'Hauptleitung, oberste Leitungskategorie in einem Trinkwasser und Wärmenetz', 'Hauptleitung');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('4000', 'Versorgungsleitung', 'Versorgungsleitung, auch Ortsleitung (z.B Wasserleitungen innerhalb des Versorgungsgebietes im bebauten Bereich)', 'Versorgungsleitung');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('5000', 'Zubringerleitung', 'Zubringerleitung (z.B. Wasserleitungen zwischen Wassergewinnungs- und Versorgungsgebieten)', 'Zubringerleitung');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('6000', 'Anschlussleitung', 'Anschlussleitung, Hausanschluss (z.B. Wasserleitungen von der Abzweigstelle der Versorgungsleitung bis zur Übergabestelle/Hauptabsperreinrichtung)', 'Hausanschlussleitung');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('7000', 'Verbindungsleitung', 'Verbindungsleitung (z.B. Wasserleitungen außerhalb der Versorgungsgebiete, die Versorgungsgebiete (Orte) miteinander verbinden), in der Wärmeversorung auch Transportleitung genannt (die eine Wärmeerzeuugungsinfrastruktur mit einem entfernten Versorgungsgebiet verbindet)', 'Verbindungsleitung');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('8000', 'Strassenablaufleitung', 'Straßenablaufleitung (in der Abwasserentsorgung)', 'Straßenablaufleitung');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstige Leitung', 'sonstige Leitung');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('1000', 'Antragstellung', 'Antragsteller', 'Antragstellung');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('2000', 'BevollmaechtigtPlanung', 'Bevollmächtigt und Ersteller der Planung', 'Bevollmächtigt für Planung');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('3000', 'Bevollmaechtigt', 'Bevollmächtigtes Unternehmen', 'Bevollmächtigt');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('4000', 'Planung', 'Planendes Büro', 'Planung');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('5000', 'Bauunternehmen', 'Unternehmen, das Tiefbaumaßnahmen durchführt', 'Bauunternehmen');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('6000', 'Vorhabentraeger', 'Träger eines Vorhabens im Planfeststellungs- oder Raumordnungsverfahren', 'Vorhabenträger');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('7100', 'Planfeststellungsbehoerde', 'Zuständige Behörde eines Planfeststellungsverfahrens', 'Planfeststellungsbehörde');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('7200', 'Anhoerungsbehoerde', 'Behörde, die Anhörungsverfahren im Rahmen eines Planfeststellungsverfahrens durchführt', 'Anhörungsbehörde');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('8000', 'Raumordnungsbehoerde', 'Zuständige Behörde einer Raumverträglichkeitsprüfung', 'Zuständig für Raumverträglichkeitsprüfung');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('1000', 'StationGas', 'Station für Medium Gas (Wasserstoff/Erdgas)', 'Station Gas');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('10001', 'Schieberstation', 'Über eine Schieberstation (Abzweigstation?)  kann mit Hilfe der dort installierten Kugelhähne der Gasfluss gestoppt bzw. umgelenkt werden', 'Schieberstation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('10002', 'Verdichterstation', 'Eine Verdichterstation (Kompressorstation) ist eine Anlage in einer Transportleitung, bei der ein Kompressor das Gas wieder komprimiert, um Rohr-Druckverluste auszugleichen und den Volumenstrom zu regeln', 'Verdichterstation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('10003', 'Regel_Messstation', 'Eine Gas-Druckregelanlage (GDRA) ist eine Anlage zur ein- oder mehrstufigen Gas-Druckreduzierung. Bei einer Gas-Druckregel- und Messanlage (GDRMA) wird zusätzlich noch die Gas-Mengenmessung vorgenommen. (Anmerkung: Einspeise- und Übergabestationen können separat erfasst werden)', 'Regel- und Messstation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('10004', 'Armaturstation', 'Kombination von Armaturengruppen wie Absperr- und und Abgangsarmaturengruppen', 'Armaturstation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('10005', 'Einspeisestation', 'Die Einspeisungs- oder Empfangsstation leitet Erdgas oder Wasserstoff in ein Transportleitungsnetz. Die Einspeisung erfolgt z.B. aus einer Produktions- oder Speicheranlage oder über ein LNG-Terminal nach der Regasifizierung.', 'Einspeisestation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('10006', 'Uebergabestation', 'Gas-Übergabestationen (auch Übernahme- oder Entnahmestation) dienen i.d.R. der Verteilung von Gas aus Transportleitungen in die Verbrauchernetze. Dafür muss das ankommende Gas heruntergeregelt werden. Wird Wasserstoff in ein Erdgasleitungsnetz übergeben, muss zusätzlich ein Mischer an der Übernahmestelle gewährleisten, dass sich Wasserstoff und Erdgas gleichmäßig durchmischen. 
Eine weitere Variante ist die Übergabe von Gas an ein Kraftwerk.', 'Übergabe-/Entnahmestation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('10007', 'Molchstation', 'Station um Molchungen zur Prüfung der Integrität der Fernleitung während der Betriebsphase durchzuführen.  
Der Molch füllt den Leitungsquerschnitt aus und wandert entweder einfach mit dem Produktstrom durch die Leitung (meist bei Öl) oder wird durch Druck durch die Leitung gepresst. Im Rahmen der Molchtechnik werden neben dem Molch noch ins System eingebaute Schleusen benötigt, durch die der Molch in die Leitungen eingesetzt bzw. herausgenommen und von hinten mit Druck belegt werden kann.', 'Molchstation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('2000', 'StationStrom', 'Station für Medium Strom', 'Station Strom');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('20001', 'Transformatorenstation', 'In einer Transformatorenstation (Umspannstation, Netzstation, Ortsnetzstation oder kurz Trafostation) wird die elektrische Energie aus dem Mittelspannungsnetz mit einer elektrischen Spannung von 10 kV bis 36 kV auf die in Niederspannungsnetzen (Ortsnetzen) verwendeten 400/230 V zur allgemeinen Versorgung transformiert', 'Transformatorenstation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('20002', 'Konverterstation', 'Ein Konverter steht an den Verbindungspunkten von Gleich- und Wechselstromleitungen. Er verwandelt Wechsel- in Gleichstrom und kann ebenso Gleichstrom wieder zurück in Wechselstrom umwandeln und diesen ins Übertragungsnetz einspeisen.', 'Konverterstation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('20003', 'Phasenschieber', 'Phasenschiebertransformatoren (PST), auch Querregler genannt, werden zur Steuerung der Stromflüsse zwischen Übertragungsnetzen eingesetzt. Der Phasenschiebertransformator speist einen Ausgleichsstrom in das System ein, der den Laststrom in der Leitung entweder verringert oder erhöht. Sinkt der Stromfluss in einer Leitung, werden die Stromflüsse im gesamten Verbundsystem neu verteilt.', 'Phasenschieber');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('3000', 'StationWaerme', 'Station im (Fern-)Wärmenetz', 'Station (Fern-)Wärme');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstige Station', 'sonstige Station');
INSERT INTO xp_waermeleitungtyp (code, model, documentation, description) VALUES ('1000', 'Strang', 'Schematische Darstellung als Strang (mit Vor- und Rücklauf)', 'Strang');
INSERT INTO xp_waermeleitungtyp (code, model, documentation, description) VALUES ('2000', 'Vorlauf', 'Vorlaufrohr', 'Vorlauf');
INSERT INTO xp_waermeleitungtyp (code, model, documentation, description) VALUES ('3000', 'Ruecklauf', 'Rücklaufrohr', 'Rücklauf');
INSERT INTO xp_waermeleitungtyp (code, model, documentation, description) VALUES ('4000', 'Doppelrohr', 'Vor- und Rücklauf in einem Doppelrohr', 'Doppelrohr');
INSERT INTO xp_waermeleitungtyp (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstiger Typ', 'sonstiger Typ');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('1000', 'Kunststoff', 'Kunststoff', 'Kunststoff');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('10001', 'Polyethylen_PE', 'Polyethylen (PE)', 'Polyethylen ( PE)');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('10002', 'Polyethylen_PE_HD', 'High-Density Polyethylen', 'High-Density Polyethylen');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('10003', 'Polypropylen_PP', 'Polypropylen (PP)', 'Polypropylen ( PP)');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('10004', 'Polycarbonat_PC', 'Polycarbonat (PC)', 'Polycarbonat ( PC)');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('10005', 'Polyvinylchlorid_PVC_U', 'Polyvinylchlorid (PVC-U)', 'Polyvinylchlorid ( PVC- U)');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('2000', 'Stahl', 'Stahl', 'Stahl');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('20001', 'StahlVerzinkt', 'Stahl verzinkt', 'Stahl verzinkt');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('20002', 'Stahlgitter', 'Stahlfachwerkskonstruktion (z.B. Freileitungsmast als Gittermast)', 'Stahlgitter');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('20003', 'Stahlrohr', 'Rohrförmiger Profilstahl, dessen Wand aus Stahl besteht. Stahlrohre dienen der Durchleitung von flüssigen, gasförmigen oder festen Stoffen, oder werden als statische oder konstruktive Elemente verwendet (z.B. Freileitungsmast als Stahlrohrmast)', 'Stahlrohr');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('2500', 'Stahlverbundrohr', 'Stahlverbundrohre im Rohrleitungsbau', 'Stahlverbundrohr');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('25001', 'St_PE', 'Stahlrohr mit  Kunststoffumhüllung auf PE-Basis', 'Stahlrohr mit Standard-Kunststoffumhüllung (PE)');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('25002', 'St_PP', 'Stahlrohr mit  Kunststoffumhüllung auf PP-Basis für höhere Temperatur- und Härte-Anforderungen', 'Stahlrohr mit Kunstoffumhüllung (PP)');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('25003', 'St_FZM', 'Stahlrohr mit mit Kunststoff-Umhüllung und zusätzlichem Außenschutz durch Faserzementmörtel-Ummantelung (FZM)', 'Stahlrohr mit FZM-Ummantelung');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('25004', 'St_GFK', 'Stahlrohr mit mit Kunststoff-Umhüllung und zusätzlichem Außenschutz aus glasfaserverstärktem Kunststoff (GFK) für höchste mechanische Abriebfestigkeit bei grabenlosem Rohrvortrieb', 'Stahlrohr mit GFK-Ummantelung');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('25005', 'St_ZM_PE', 'Stahlrohr mit Zementmörtelauskleidung und PE-Außenschutz (z.B. Abwasserohr)', 'Stahl-Verbundrohr (ZM-PE)');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('3000', 'Gusseisen', 'Gusseisen', 'Gusseisen');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('30001', 'GGG_ZM', 'duktiles Gussrohr mit Zementmörtelauskleidung (z.B Abwasserrohr)', 'duktiles Gussrohr mit ZM-Auskleidung');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('30002', 'GGG_ZM_PE', 'duktiles Gussrohr mit Zementmörtelauskleidung und PE-Außenschutz (z.B. Abwasserrohr)', 'duktiles Guss-Verbundrohr (ZM-PE)');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('4000', 'Beton', 'Beton (z.B. Schacht)', 'Beton');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('5000', 'Holz', 'Holz (z.B. Holzmast)', 'Holz');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstiger Werkstoff', 'sonstiger Werkstoff');

CREATE INDEX idx_bra_ausbauplan_position ON bra_ausbauplan USING GIST (position);
CREATE INDEX idx_bra_baugrube_position ON bra_baugrube USING GIST (position);
CREATE INDEX idx_bra_baustelle_position ON bra_baustelle USING GIST (position);
CREATE INDEX idx_bra_breitbandtrasseabschnitt_position ON bra_breitbandtrasseabschnitt USING GIST (position);
CREATE INDEX idx_bra_hausanschluss_position ON bra_hausanschluss USING GIST (position);
CREATE INDEX idx_bra_kabel_position ON bra_kabel USING GIST (position);
CREATE INDEX idx_bra_kompaktstation_position ON bra_kompaktstation USING GIST (position);
CREATE INDEX idx_bra_mast_position ON bra_mast USING GIST (position);
CREATE INDEX idx_bra_mikrorohr_position ON bra_mikrorohr USING GIST (position);
CREATE INDEX idx_bra_mikrorohrverbund_position ON bra_mikrorohrverbund USING GIST (position);
CREATE INDEX idx_bra_rohrmuffe_position ON bra_rohrmuffe USING GIST (position);
CREATE INDEX idx_bra_schacht_position ON bra_schacht USING GIST (position);
CREATE INDEX idx_bra_schutzrohr_position ON bra_schutzrohr USING GIST (position);
CREATE INDEX idx_bra_umfeld_position ON bra_umfeld USING GIST (position);
CREATE INDEX idx_bra_verteiler_position ON bra_verteiler USING GIST (position);
CREATE INDEX idx_bst_abwasserleitung_position ON bst_abwasserleitung USING GIST (position);
CREATE INDEX idx_bst_armatur_position ON bst_armatur USING GIST (position);
CREATE INDEX idx_bst_baum_position ON bst_baum USING GIST (position);
CREATE INDEX idx_bst_energiespeicher_position ON bst_energiespeicher USING GIST (position);
CREATE INDEX idx_bst_gasleitung_position ON bst_gasleitung USING GIST (position);
CREATE INDEX idx_bst_hausanschluss_position ON bst_hausanschluss USING GIST (position);
CREATE INDEX idx_bst_kraftwerk_position ON bst_kraftwerk USING GIST (position);
CREATE INDEX idx_bst_mast_position ON bst_mast USING GIST (position);
CREATE INDEX idx_bst_richtfunkstrecke_position ON bst_richtfunkstrecke USING GIST (position);
CREATE INDEX idx_bst_schacht_position ON bst_schacht USING GIST (position);
CREATE INDEX idx_bst_sonsteinrichtunglinie_position ON bst_sonsteinrichtunglinie USING GIST (position);
CREATE INDEX idx_bst_station_position ON bst_station USING GIST (position);
CREATE INDEX idx_bst_stationflaeche_position ON bst_stationflaeche USING GIST (position);
CREATE INDEX idx_bst_strassenablauf_position ON bst_strassenablauf USING GIST (position);
CREATE INDEX idx_bst_strassenbeleuchtung_position ON bst_strassenbeleuchtung USING GIST (position);
CREATE INDEX idx_bst_stromleitung_position ON bst_stromleitung USING GIST (position);
CREATE INDEX idx_bst_telekommunikationsleitung_position ON bst_telekommunikationsleitung USING GIST (position);
CREATE INDEX idx_bst_umspannwerk_position ON bst_umspannwerk USING GIST (position);
CREATE INDEX idx_bst_verteiler_position ON bst_verteiler USING GIST (position);
CREATE INDEX idx_bst_waermeleitung_position ON bst_waermeleitung USING GIST (position);
CREATE INDEX idx_bst_wasserleitung_position ON bst_wasserleitung USING GIST (position);
CREATE INDEX idx_bst_wegekante_position ON bst_wegekante USING GIST (position);
CREATE INDEX idx_xp_planreferenz_position ON xp_planreferenz USING GIST (position);
CREATE INDEX idx_xp_trassenquerschnitt_position ON xp_trassenquerschnitt USING GIST (position);
