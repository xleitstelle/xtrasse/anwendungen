## Voraussetzungen

Installation der Version 4.1 (oder *latest*) von [ldproxy](https://github.com/interactive-instruments/ldproxy).

ldproxy benötigt Zugriff auf eine [PostgreSQL-Datenbank](https://gitlab.opencode.de/xleitstelle/xtrasse/anwendungen/-/tree/main/postgres), die einen XTrasse-Plan enthält. 

Passend zum XTrasse-Profil der Datenbank müssen die hier verfügbaren ldproxy-Konfigurationsdateien in den Datenordner der lokalen Installation kopiert werden. 

## Konfigurationsdateien

Die mit ShapeChange erzeugten Dateien für ldproxy umfassen:

- Konfiguration der SQL-Feature-Provider (/entities/instances/providers/xtrasse.yml)

- Grundkonfiguration der API (/entities/instances/services/xtrasse.yml)

- Zum Profil zugehörige Codelisten (/values/codelists/\*.*)

Für die Profile **BRA**, **PFS** und **RVP** sind darüber hinaus verfügbar: 

- Beispiele für Mapbox-Styles zur Darstellung eines Trassenplans im Kartenclient des ldproxy (/values/maplibre-styles/xtrasse/default.mbs)

- Beispiel einer *stored query*, um den Trassenplan in XTrasseGML und XTrasseJSON aus der Datenbank zu exportieren (/values/queries/xtrasse/*ausbauplan-ueber-id.json*, *planfeststellung-ueber-id.json*, *raumvertraeglichkeit-ueber-id.json*)

- Beispielhafte Konfiguration von Mapbox Vector Tiles (/entities/instances/providers/xtrasse-tiles.yml) für das Modul zur Generierung von Geodaten als Kacheln (buildingBlock: TILES in der API-Konfiguration). 



Weiterer Hinweis zur Konfiguration:

- Der lokale Zugang zur PostgreSQL-Datenbank erfolgt entweder im SQL-Feature-Provider oder es wird eine Datei angelegt, die die dortigen Einstellungen überschreibt (/store/overrides/providers/xtrasse.yml).


## Beispiele zur Ansicht

Mit der Produktionsumgebung erzeugte Testdaten sind [hier](https://xtrasse.ldproxy.develop.diplanung.de/) mit ldproxy veröffentlicht.


