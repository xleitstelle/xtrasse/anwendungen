## Anwendungen XTrasse

Die Produktionsumgebung von XTrasse ermöglicht es, Instanzen/Planwerke zu erstellen, auszutauschen und zu publizieren. Die erforderlichen Software-Komponenten (QGIS, PostgreSQL/SQLite, ldproxy) sind alle unter OpenSource-Lizenzen nutzbar. Hierfür werden Skripte und Konfigurationen zur Verfügung gestellt, die aus dem UML-Modell abgeleitet werden. Dies erfolgt in Form von Profilen für die sechs Anwendungsfälle von XTrasse 2.0:

- **BRA**: Trassenpläne für den Breitbandausbau,
- **RVP**: Trassenkorridore für Raumverträglichkeitsprüfungen,
- **IGP**: Infrastrukturgebiete für Infrastrukturgebietspläne
- **PFS**: Trassenpläne für Planfeststellungsverfahren,
- **BST**: Bestandspläne im Leitungsbau,
- **ISA**: Meldungen an den Infrastrukturatlas der Bundesnetzagentur.

Die sechs Profile beinhalten die jeweils spezifische *Plan*-Klasse und die zu dem Profil passenden *Objekt*-Klassen des UML-Modells:

- Das Profil **BRA** hat die Planklasse "BRA_Ausbauplan", auf den die zugehörenden BRA-Objekte referenzieren. Darüber hinaus umfasst das Profil die BST-Objekte (die gemäß Modell auf alle Planklassen referenzieren).
- Analog dazu gehören zu den Profilen **RVP** und **IGP** die Planklasse "RVP_Plan", RVP- und BST-Objekte bzw. "IGP_Plan", IGP- und BST-Objekte.
- Das Profil **PFS** beinhaltet die Planklasse "PFS_Plan", PFS- und BST-Objekte sowie zusätzlich RVP-Objekte und IGP-Objekte. 
- Das Profil **BST** umfasst die Planklasse "BST_Netzplan" und BST-Objekte.
- Vergleichbar dazu enthält das Profil **ISA** die Planklasse "ISA_Plan" und ISA-Objekte.

Während in den Auslieferungsgegenständen des Standards die Einheit des XTrasse-Modells gewahrt bleibt, erfolgt hier die Ausdifferenzierung in sechs unabhängig voneinander nutzbare Profile.  

Für die Profile Breitbandausbau, Planfeststellung und Raumverträglichkeitsprüfung stehen weiterhin Testdaten zur Verfügung. 

Das Repository umfasst folgende Anwendungen von XTrasse:

- **Datenbank 1** (Ordner postgres): Mit den fünf SQL-Skripten wird für jedes Profil eine PostgreSQL/PostGIS-Datenbank erzeugt, in der das Datenmodell von XTrasse in Tabellenstrukturen einer relationalen Datenbank übersetzt ist, inkl. der dafür notwendigen "Abflachung" der Beziehungen von abstrakten Ober- zu den Objektklassen. Diese Datenbanken lassen sich einfach in QGIS einbinden und stellen Layer zur Verfügung, mit der XTrasse-konforme Pläne gezeichnet und gespeichert werden können.

- **Datenbank 2** (Ordner sqlite): Mit identischer Funktionalität der PostgreSQL-DB lassen sich mit den sechs SQL-Skripten SQLite/SpatiaLite-Datenbanken erzeugen. Für die Profile BRA, RVP und PFS stehen Datenbanken und QGIS-Projekte zur sofortigen Nutzung bereit. 

- **OGC-API** (Ordner ldproxy): Mit den Konfigurationsdateien für das Programm [ldproxy](https://github.com/interactive-instruments/ldproxy) werden *OGC API Features* für die sechs Profile eingerichtet, die Daten aus den jeweils zugehörigen PostgreSQL-Datenbanken über das Internet veröffentlichen und weiteren Softwareanwendungen zugänglich machen (z.B. QGIS, GIS-Portale). Eine weitere Funktionalität der ldproxy-API ist Möglichkeit, die Pläne als **XTrasseGML** und **XTrasseJSON** über einen Download aus der Datenbank zu exportieren.

- **GFS-Template**: Die sechs Templates erweitert die Darstellung von XTrasse-Instanzen im Zusammenspiel mit dem GDAL-Treiber. 

- **Testdaten**: Die GML- und JSON-Instanzen werden gegen XML- und JSON-Schemata von XTrasse validiert. 



## Releases und Versionierung

Mit dem Programm [ShapeChange](https://shapechange.github.io/ShapeChange/) werden von der XLeitstelle sowohl die normativen Bestandteile des Standards XTrasse (s. [Spezifikation](https://gitlab.opencode.de/xleitstelle/xtrasse/spezifikation)) als auch die hier vorgestellten Profile und Anwendungen abgeleitet. Daher stehen die Releases der Anwendungen in direkter Abhängigkeit zu denen des Standards: Geänderte und erweiterte Versionen der Produktionsumgebung werden jeweils für eine spezifische XTrasse-Version veröffentlicht.

![Shapechange-Workflow](shapechange_grafik.png)

Solange der Standard XTrasse 2.0 Entwurfsstatus besitzt, haben die Anwendungen 1.0 für XTrasse 2.0 ebenfalls Entwurfsstatus. Das bislang veröffentlichte Anwendungspaket zu XTrasse 1.0 ist [hier](https://gitlab.opencode.de/xleitstelle/xtrasse/anwendungen/-/releases) verfügbar.





