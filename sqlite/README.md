## Erzeugung einer SQLite/SpatiaLite-Datenbank

Die Installation von SQLite und der Erweiterung SpatiaLite ist nicht nötig, wenn QGIS auf dem System installiert ist. Über die mit der OGIS-Installation bereitgestellte **OSGeo4W Shell** werden SQLite-Datenbanken mit dem hier bereitgestellten XTrasse-Datenbankschema erzeugt.   
Der Befehl hat die Struktur:    

	sqlite3 {db-Dateiname} < {db-Schema-Dateiname}

Wird der Befehl im Ordner des DB-Schemas ausgeführt, kann auf Pfadangaben verzichtet werden. Der Befehl kann z.B. lauten: 

	sqlite3 xtrasseBRA.sqlite < XTrasseBRA.sql  

Die erzeugte Datenbank lässt sich mit gängigen DB-Browsern (z.B. DBeaver) überprüfen und bearbeiten. 
Hinweis: Der EPSG-Code 25832 muss evtl. vor dem Ausführen des Scripts ersetzt werden.  

## Empfehlung zur QGIS-Version

Es sollte wenn möglich die aktuellste QGIS-Version genutzt werden. Für den reibungslosen Umgang mit Codelisten ist die 
Version 3.40 notwendig (s. 2b). 


## Nutzung der SQLite-DB mit QGIS-Projekt

Die hier abgelegte Dateien xtrasseBRA.sqlite, xtrassePFS.sqlite, xtrasseRVP.sqlite und xtrasseBST_Z.sqlite sind mit der OSGeo4W Shell erzeugte Datenbanken.

In den QGIS-Projekten _BRA, _PFS, _RVP und BST_Z sind jeweils diese SQLite-Datenbanken eingebunden. Die Projekte stellen einen strukturierten Layerbaum zur Verfügung.


## Erzeugung eines SQLite-Templates

Der über das QGIS-Projekt erzeugte Layerbaum kann unter "Projekt --> Speichern als --> Vorlagen" als ein Template gespeichert werden. Zur Erzeugung neuer Pläne kann jeweils unter "Projekt --> Neu aus Vorlage" das entsprechende QGIS-Projekt ausgewählt werden.  
   

## Einstellungen in QGIS 

### 1. Laden der Beziehungen aus der Datenbank 

a) SQLite

QGIS erkennt die Beziehungen zwischen den Features, zu den Datentabellen (und ebenso die Pflichtattribute), wenn die SQLite-DB per *Drag & Drop* in das Karten- oder Layerfenster gezogen wird. Die Einbindung der Datenbank über die Datenquellenverwaltung oder den Browser wird zurzeit nicht empfohlen. 

b) PostgreSQL

Wenn mit dem SQL-Script eine Datenbank erzeugt wird, müssen die *foreign keys* in QGIS eingelesen werden. Folgende Reihenfolge wird empfohlen: 

1. Datenbank in QGIS einbinden (Datenquellenverwaltung) 
2. Tabellen der Datenbank in das QGIS-Projekt importieren (auch geometrielose Tabellen). 
3. Beziehungen zwischen den Tabellen aus der Datenbank in das QGIS-Projekt laden.

![QGIS-Bedienfeld](BeziehungenLaden.jpg)

 (erreichbar unter: Projekt --> Beziehungen --> Beziehungen laden).

Werden (Daten)Tabellen zu einem späteren Zeitpunkt in das QGIS-Projekt geholt, müssen anschließend die Beziehungen **erneut** geladen werden. 

### 2. Einbindung der Codelisten 

a) Falls bei der Erzeugung eines neuen Objektes Codelisten-Attribute automatisch "befüllt" werden, sollte die folgende Einstellung überprüft werden:

![QGIS-Bedienfeld](DatenquellenOptionen.jpg)

(erreichbar unter: Einstellungen --> Optionen --> Datenquellen - Datenquellenbehandlung)  

b) QGIS erkennt bis einschließlich Version 3.38 bei *foreign keys* auf Codelistentabellen nicht, ob es sich bei diesen Datenbankeinträgen um Pflichtattribute handelt (s. dazu diesen mittlerweile gefixten [Bug](https://github.com/qgis/QGIS/issues/52219)).   
Falls die Version 3.40 nicht genutzt werden kann, muss bei optionalen Codelisten-Attributen, die nicht belegt werden sollen, in dem entsprechenden Formularfeld **ein Null-Wert** zugelassen werden:

![QGIS-Bedienfeld](Layereigenschaften.jpg)

(erreichbar unter: Layereigenschaften --> Attributformular - im Formularlayout Auswahl der Codeliste mit der Endung _fkcl - Bedienelementtyp)

c) Änderungen an den Einstellungen des Attributformulars (s.u.) können zur Folge haben, dass Codelistenwerte nicht mehr angezeigt werden. In diesem Fall sollte geprüft werden, ob unter "Bedienelementtyp" anstatt "Beziehungsreferenz" fälschlicherweise "Texteditor" ausgewählt ist. 

### 3. Attributformulare konfigurieren 

Die im Datenbankschema enthaltenen *foreign keys* führen bei den Layern der Planklassen zu Performance-Einbußen beim Aufrufen der Attributtabelle. Abhilfe schafft eine Konfiguration der "Formularansicht" der Attributtabelle (erreichbar über "Layereigenschaften --> Attributformular"). Anstelle der automatischen Generierung des Attributformulars, bei der alle eingelesenen Relationen berücksichtigt und beim Öffnen aktualisiert werden, kann die Auswahl gesteuert werden über "Mit Drag and Drop zusammenstellen".   

![QGIS-Bedienfeld](LayoutAttributformular.jpg)

In der hier abgebildeten Konfiguration für den "PFS_Plan" sind die Datenfelder der Tabelle und die Relationen dieser Planklasse auf zwei Tabs verteilt. Die übrigen in der linken Spalte dargestellten Beziehungen sind Referenzen der Planobjekte (bzw. Features) auf den Plan. Diese werden hier nicht in das Attributformular übernommen. 

### 4. Umgang mit verknüpften Tabellen im Attributformular

Die mit einem Layer über *foreign keys* verbundenen Datentabellen lassen sich in der Formularansicht der Attributtabelle bearbeiten. Die ID des Layers wird von QGIS automatisch in die verknüpfte Tabelle eingetragen (sichtbar durch grüne Häkchen). Dabei sollte Bearbeitungsreihenfolge beachtet werden: Wird für einen Layer ein neues Features angelegt, muss dies gespeichert werden, ***bevor*** ein Eintrag in einer Datentabelle erfolgt, die mit diesem Features verknüpft ist (das Features muss eine ID für die assoziative Tabelle besitzen). Der Workflow ist wie folgt: Feature zeichnen und Pflichtattribute eintragen --> Attributformular mit "ok" schließen --> Attributtabelle des Layers in der Formularansicht öffnen --> "Änderungen speichern" --> in der gleichen Ansicht die verknüpfte Datentabelle in "Bearbeitungsmodus" schalten --> Eintrag vornehmen. 

Zur besseren Übersicht sollte auch darauf geachtet werden, dass die linke Spalte in der  Formularansicht nicht leer bleibt. Ein Klick auf "Ausdruck" macht "Spaltenvoransicht" sichtbar, in der belegte Felder wie "_id" oder "name" ausgewählt werden. 

![QGIS-Bedienfeld](Attributformular.jpg)

