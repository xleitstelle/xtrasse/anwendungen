SELECT load_extension('mod_spatialite');

SELECT InitSpatialMetaData();

PRAGMA foreign_keys = ON;

CREATE TABLE bst_abwasserleitung (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuplan_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   leitungstyp_fkcl TEXT, -- Auswahl des Leitungstyps
   statusaktuell_fkcl TEXT, -- aktueller Status
   statusaenderung_fkcl TEXT, -- Statusveränderung im Rahmen einer Baumaßnahme
   nennweite TEXT, -- Nennweite einer einzelnen Leitung. Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser REAL, -- Außendurchmesser einer einzelnen Leitung in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   ueberdeckung REAL, -- Mindestüberdeckung (DIN): Mindestabstand zwischen Oberkante der Verkehrsfläche und Oberkante der Leitung in m. Bei Leitungsbündeln bezieht sich der Wert auf die oberste Leitung bzw. die Oberkante des Bündels. Die "Verlegetiefe" einer Leitung wird dagegen bis zur Grabensohle gemessen. Gilt nur für erdverlegte Linienobjekte. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   lagegenauigkeit REAL, -- Statistisches Maß der maximalen Abweichung des realen Verlaufs der Leitung von der Liniengeometrie in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   pufferzone3d REAL, -- Die Pufferzone definiert in einem 3D Modell einen rechteckigen Körper, in dem die Höhenlage einer Leitung (oder eines Leitungsbündels) variieren kann. Die obere Grenze des Puffers wird durch das Attribut Überdeckung definiert. Das hier einzutragende Maß ist die Distanz zur unteren Grenze des Puffers. Die Breite ergibt sich bei einzelnen Leitungen aus dem Attribut Nennweite oder Außendurchmesser, bei Leitungsbündeln aus der Breite der Leitungszone.
   schutzzone3d REAL, -- Die Schutzzone definiert in einem 3D Modell einen quadratischen Körper um die Leitung. Der hier einzutragende Wert ist die Länge, die von vier Kreistangenten ausgehend den Abstand zu den waage- und senkrechten Kanten des Quadrats darstellt.
   art_fkcl TEXT, -- Auswahl des Kanaltyps bezogen auf die Art der Entwässerung
   netzebene_fkcl TEXT, -- Leitungsart innerhalb des Abwassernetzes
   werkstoff_fkcl TEXT, -- Werkstoff der Leitung
   CONSTRAINT fk_bst_abwasserleitung_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES bst_kanaltyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_abwasserleitung_gehoertzuplan_pfs_plan_fk FOREIGN KEY (gehoertzuplan_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_abwasserleitung_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_abwasserleitung_netzebene_fkcl FOREIGN KEY (netzebene_fkcl) REFERENCES xp_rohrleitungnetz (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_abwasserleitung_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_abwasserleitung_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_abwasserleitung_werkstoff_fkcl FOREIGN KEY (werkstoff_fkcl) REFERENCES xp_werkstoff (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE bst_armatur (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuplan_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   netzsparte_fkcl TEXT, -- Leitungssparte eines Punktobjektes
   statusaktuell_fkcl TEXT, -- aktueller Status
   statusaenderung_fkcl TEXT, -- Statusveränderung im Rahmen einer Baumaßnahme
   funktion_fkcl TEXT, -- Funktion der Armatur
   einsatzgebiet_fkcl TEXT, -- Einsatzgebiet der Armatur
   CONSTRAINT fk_bst_armatur_einsatzgebiet_fkcl FOREIGN KEY (einsatzgebiet_fkcl) REFERENCES xp_armatureinsatzgebiet (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_armatur_funktion_fkcl FOREIGN KEY (funktion_fkcl) REFERENCES xp_armaturfunktion (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_armatur_gehoertzuplan_pfs_plan_fk FOREIGN KEY (gehoertzuplan_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_armatur_netzsparte_fkcl FOREIGN KEY (netzsparte_fkcl) REFERENCES bst_netzsparte (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_armatur_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_armatur_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE bst_baum (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuplan_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   netzsparte_fkcl TEXT, -- Leitungssparte eines Punktobjektes
   statusaktuell_fkcl TEXT, -- aktueller Status
   statusaenderung_fkcl TEXT, -- Statusveränderung im Rahmen einer Baumaßnahme
   nrbaumkataster TEXT, -- Nummer des Baumes im kommunalen Straßenbaumkataster
   stammumfang REAL, -- Umfang des Stammes
   kronendurchmesser REAL, -- Durchmesser der Baumkrone (Kronentraufbereich)
   CONSTRAINT fk_bst_baum_gehoertzuplan_pfs_plan_fk FOREIGN KEY (gehoertzuplan_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_baum_netzsparte_fkcl FOREIGN KEY (netzsparte_fkcl) REFERENCES bst_netzsparte (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_baum_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_baum_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE bst_energiespeicher (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuplan_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   netzsparte_fkcl TEXT, -- Leitungssparte eines Punktobjektes
   statusaktuell_fkcl TEXT, -- aktueller Status
   statusaenderung_fkcl TEXT, -- Statusveränderung im Rahmen einer Baumaßnahme
   begrenzung_fkcl TEXT, -- Bestimmung der dargestellten Fläche
   art_fkcl TEXT, -- Art des Energiespeichers
   gasart_fkcl TEXT, -- Art des gespeicherten Gases
   gasdruckstufe_fkcl TEXT, -- Druckstufe des Gases
   CONSTRAINT fk_bst_energiespeicher_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES xp_energiespeichertyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_energiespeicher_begrenzung_fkcl FOREIGN KEY (begrenzung_fkcl) REFERENCES xp_infrastrukturflaeche (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_energiespeicher_gasart_fkcl FOREIGN KEY (gasart_fkcl) REFERENCES xp_gastyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_energiespeicher_gasdruckstufe_fkcl FOREIGN KEY (gasdruckstufe_fkcl) REFERENCES xp_gasdruckstufe (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_energiespeicher_gehoertzuplan_pfs_plan_fk FOREIGN KEY (gehoertzuplan_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_energiespeicher_netzsparte_fkcl FOREIGN KEY (netzsparte_fkcl) REFERENCES bst_netzsparte (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_energiespeicher_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_energiespeicher_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE bst_gasleitung (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuplan_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   leitungstyp_fkcl TEXT, -- Auswahl des Leitungstyps
   statusaktuell_fkcl TEXT, -- aktueller Status
   statusaenderung_fkcl TEXT, -- Statusveränderung im Rahmen einer Baumaßnahme
   nennweite TEXT, -- Nennweite einer einzelnen Leitung. Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser REAL, -- Außendurchmesser einer einzelnen Leitung in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   ueberdeckung REAL, -- Mindestüberdeckung (DIN): Mindestabstand zwischen Oberkante der Verkehrsfläche und Oberkante der Leitung in m. Bei Leitungsbündeln bezieht sich der Wert auf die oberste Leitung bzw. die Oberkante des Bündels. Die "Verlegetiefe" einer Leitung wird dagegen bis zur Grabensohle gemessen. Gilt nur für erdverlegte Linienobjekte. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   lagegenauigkeit REAL, -- Statistisches Maß der maximalen Abweichung des realen Verlaufs der Leitung von der Liniengeometrie in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   pufferzone3d REAL, -- Die Pufferzone definiert in einem 3D Modell einen rechteckigen Körper, in dem die Höhenlage einer Leitung (oder eines Leitungsbündels) variieren kann. Die obere Grenze des Puffers wird durch das Attribut Überdeckung definiert. Das hier einzutragende Maß ist die Distanz zur unteren Grenze des Puffers. Die Breite ergibt sich bei einzelnen Leitungen aus dem Attribut Nennweite oder Außendurchmesser, bei Leitungsbündeln aus der Breite der Leitungszone.
   schutzzone3d REAL, -- Die Schutzzone definiert in einem 3D Modell einen quadratischen Körper um die Leitung. Der hier einzutragende Wert ist die Länge, die von vier Kreistangenten ausgehend den Abstand zu den waage- und senkrechten Kanten des Quadrats darstellt.
   gasart_fkcl TEXT NOT NULL, -- Art des transportierten Gases
   druckstufe_fkcl TEXT, -- Angabe der Druckstufe
   netzebene_fkcl TEXT, -- Leitungsart innerhalb des Gasnetzes
   werkstoff_fkcl TEXT, -- Werkstoff der Leitung
   CONSTRAINT fk_bst_gasleitung_druckstufe_fkcl FOREIGN KEY (druckstufe_fkcl) REFERENCES xp_gasdruckstufe (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_gasleitung_gasart_fkcl FOREIGN KEY (gasart_fkcl) REFERENCES xp_gastyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_gasleitung_gehoertzuplan_pfs_plan_fk FOREIGN KEY (gehoertzuplan_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_gasleitung_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_gasleitung_netzebene_fkcl FOREIGN KEY (netzebene_fkcl) REFERENCES xp_rohrleitungnetz (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_gasleitung_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_gasleitung_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_gasleitung_werkstoff_fkcl FOREIGN KEY (werkstoff_fkcl) REFERENCES xp_werkstoff (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE bst_hausanschluss (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuplan_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   netzsparte_fkcl TEXT, -- Leitungssparte eines Punktobjektes
   statusaktuell_fkcl TEXT, -- aktueller Status
   statusaenderung_fkcl TEXT, -- Statusveränderung im Rahmen einer Baumaßnahme
   CONSTRAINT fk_bst_hausanschluss_gehoertzuplan_pfs_plan_fk FOREIGN KEY (gehoertzuplan_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_hausanschluss_netzsparte_fkcl FOREIGN KEY (netzsparte_fkcl) REFERENCES bst_netzsparte (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_hausanschluss_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_hausanschluss_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE bst_kanaltyp (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE bst_kraftwerk (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuplan_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   netzsparte_fkcl TEXT, -- Leitungssparte eines Punktobjektes
   statusaktuell_fkcl TEXT, -- aktueller Status
   statusaenderung_fkcl TEXT, -- Statusveränderung im Rahmen einer Baumaßnahme
   begrenzung_fkcl TEXT, -- Bestimmung der dargestellten Fläche
   art_fkcl TEXT, -- Art des Kraftwerks
   primaerenergie_fkcl TEXT, -- Energieträger, der in Dampf- und Gasturbinenkraftwerken in Sekundärenergie gewandelt wird
   kraftwaermekopplung INTEGER, -- Kraft-Wärme-Kopplung (KWK) ist die gleichzeitige Gewinnung von mechanischer Energie und nutzbarer Wärme, die in einem gemeinsamen thermodynamischen Prozess entstehen. Die mechanische Energie wird in der Regel unmittelbar in elektrischen Strom umgewandelt. Die Wärme wird für Heizzwecke als Nah- oder Fernwärme oder für Produktionsprozesse als Prozesswärme genutzt.
   CONSTRAINT fk_bst_kraftwerk_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES xp_kraftwerktyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_kraftwerk_begrenzung_fkcl FOREIGN KEY (begrenzung_fkcl) REFERENCES xp_infrastrukturflaeche (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_kraftwerk_gehoertzuplan_pfs_plan_fk FOREIGN KEY (gehoertzuplan_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_kraftwerk_netzsparte_fkcl FOREIGN KEY (netzsparte_fkcl) REFERENCES bst_netzsparte (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_kraftwerk_primaerenergie_fkcl FOREIGN KEY (primaerenergie_fkcl) REFERENCES xp_primaerenergietraeger (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_kraftwerk_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_kraftwerk_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE bst_mast (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuplan_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   netzsparte_fkcl TEXT, -- Leitungssparte eines Punktobjektes
   statusaktuell_fkcl TEXT, -- aktueller Status
   statusaenderung_fkcl TEXT, -- Statusveränderung im Rahmen einer Baumaßnahme
   art_fkcl TEXT, -- Typ des Mastes
   werkstoff_fkcl TEXT, -- Werkstoff des Masts
   CONSTRAINT fk_bst_mast_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES bst_masttyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_mast_gehoertzuplan_pfs_plan_fk FOREIGN KEY (gehoertzuplan_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_mast_netzsparte_fkcl FOREIGN KEY (netzsparte_fkcl) REFERENCES bst_netzsparte (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_mast_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_mast_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_mast_werkstoff_fkcl FOREIGN KEY (werkstoff_fkcl) REFERENCES xp_werkstoff (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE bst_masttyp (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE bst_netzsparte (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE bst_richtfunkstrecke (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuplan_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   leitungstyp_fkcl TEXT, -- Auswahl des Leitungstyps
   statusaktuell_fkcl TEXT, -- aktueller Status
   statusaenderung_fkcl TEXT, -- Statusveränderung im Rahmen einer Baumaßnahme
   nennweite TEXT, -- Nennweite einer einzelnen Leitung. Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser REAL, -- Außendurchmesser einer einzelnen Leitung in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   ueberdeckung REAL, -- Mindestüberdeckung (DIN): Mindestabstand zwischen Oberkante der Verkehrsfläche und Oberkante der Leitung in m. Bei Leitungsbündeln bezieht sich der Wert auf die oberste Leitung bzw. die Oberkante des Bündels. Die "Verlegetiefe" einer Leitung wird dagegen bis zur Grabensohle gemessen. Gilt nur für erdverlegte Linienobjekte. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   lagegenauigkeit REAL, -- Statistisches Maß der maximalen Abweichung des realen Verlaufs der Leitung von der Liniengeometrie in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   pufferzone3d REAL, -- Die Pufferzone definiert in einem 3D Modell einen rechteckigen Körper, in dem die Höhenlage einer Leitung (oder eines Leitungsbündels) variieren kann. Die obere Grenze des Puffers wird durch das Attribut Überdeckung definiert. Das hier einzutragende Maß ist die Distanz zur unteren Grenze des Puffers. Die Breite ergibt sich bei einzelnen Leitungen aus dem Attribut Nennweite oder Außendurchmesser, bei Leitungsbündeln aus der Breite der Leitungszone.
   schutzzone3d REAL, -- Die Schutzzone definiert in einem 3D Modell einen quadratischen Körper um die Leitung. Der hier einzutragende Wert ist die Länge, die von vier Kreistangenten ausgehend den Abstand zu den waage- und senkrechten Kanten des Quadrats darstellt.
   CONSTRAINT fk_bst_richtfunkstrecke_gehoertzuplan_pfs_plan_fk FOREIGN KEY (gehoertzuplan_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_richtfunkstrecke_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_richtfunkstrecke_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_richtfunkstrecke_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE bst_schacht (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuplan_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   netzsparte_fkcl TEXT, -- Leitungssparte eines Punktobjektes
   statusaktuell_fkcl TEXT, -- aktueller Status
   statusaenderung_fkcl TEXT, -- Statusveränderung im Rahmen einer Baumaßnahme
   schachttiefe REAL, -- Schachttiefe (= Deckelhöhe - Sohlhöhe)
   CONSTRAINT fk_bst_schacht_gehoertzuplan_pfs_plan_fk FOREIGN KEY (gehoertzuplan_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_schacht_netzsparte_fkcl FOREIGN KEY (netzsparte_fkcl) REFERENCES bst_netzsparte (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_schacht_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_schacht_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE bst_sonsteinrichtunglinie (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuplan_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   leitungstyp_fkcl TEXT, -- Auswahl des Leitungstyps
   statusaktuell_fkcl TEXT, -- aktueller Status
   statusaenderung_fkcl TEXT, -- Statusveränderung im Rahmen einer Baumaßnahme
   nennweite TEXT, -- Nennweite einer einzelnen Leitung. Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser REAL, -- Außendurchmesser einer einzelnen Leitung in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   ueberdeckung REAL, -- Mindestüberdeckung (DIN): Mindestabstand zwischen Oberkante der Verkehrsfläche und Oberkante der Leitung in m. Bei Leitungsbündeln bezieht sich der Wert auf die oberste Leitung bzw. die Oberkante des Bündels. Die "Verlegetiefe" einer Leitung wird dagegen bis zur Grabensohle gemessen. Gilt nur für erdverlegte Linienobjekte. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   lagegenauigkeit REAL, -- Statistisches Maß der maximalen Abweichung des realen Verlaufs der Leitung von der Liniengeometrie in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   pufferzone3d REAL, -- Die Pufferzone definiert in einem 3D Modell einen rechteckigen Körper, in dem die Höhenlage einer Leitung (oder eines Leitungsbündels) variieren kann. Die obere Grenze des Puffers wird durch das Attribut Überdeckung definiert. Das hier einzutragende Maß ist die Distanz zur unteren Grenze des Puffers. Die Breite ergibt sich bei einzelnen Leitungen aus dem Attribut Nennweite oder Außendurchmesser, bei Leitungsbündeln aus der Breite der Leitungszone.
   schutzzone3d REAL, -- Die Schutzzone definiert in einem 3D Modell einen quadratischen Körper um die Leitung. Der hier einzutragende Wert ist die Länge, die von vier Kreistangenten ausgehend den Abstand zu den waage- und senkrechten Kanten des Quadrats darstellt.
   CONSTRAINT fk_bst_sonsteinrichtunglinie_gehoertzuplan_pfs_plan_fk FOREIGN KEY (gehoertzuplan_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_sonsteinrichtunglinie_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_sonsteinrichtunglinie_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_sonsteinrichtunglinie_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE bst_spannung (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE bst_station (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuplan_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   netzsparte_fkcl TEXT, -- Leitungssparte eines Punktobjektes
   statusaktuell_fkcl TEXT, -- aktueller Status
   statusaenderung_fkcl TEXT, -- Statusveränderung im Rahmen einer Baumaßnahme
   art_fkcl TEXT, -- Art der Station
   CONSTRAINT fk_bst_station_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES xp_stationtyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_station_gehoertzuplan_pfs_plan_fk FOREIGN KEY (gehoertzuplan_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_station_netzsparte_fkcl FOREIGN KEY (netzsparte_fkcl) REFERENCES bst_netzsparte (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_station_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_station_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE bst_stationflaeche (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuplan_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   netzsparte_fkcl TEXT, -- Leitungssparte eines Punktobjektes
   statusaktuell_fkcl TEXT, -- aktueller Status
   statusaenderung_fkcl TEXT, -- Statusveränderung im Rahmen einer Baumaßnahme
   begrenzung_fkcl TEXT, -- Bestimmung der dargestellten Fläche
   art_fkcl TEXT, -- Art der Station
   CONSTRAINT fk_bst_stationflaeche_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES xp_stationtyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_stationflaeche_begrenzung_fkcl FOREIGN KEY (begrenzung_fkcl) REFERENCES xp_infrastrukturflaeche (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_stationflaeche_gehoertzuplan_pfs_plan_fk FOREIGN KEY (gehoertzuplan_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_stationflaeche_netzsparte_fkcl FOREIGN KEY (netzsparte_fkcl) REFERENCES bst_netzsparte (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_stationflaeche_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_stationflaeche_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE bst_statusaenderung (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE bst_statusaktuell (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE bst_strassenablauf (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuplan_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   netzsparte_fkcl TEXT, -- Leitungssparte eines Punktobjektes
   statusaktuell_fkcl TEXT, -- aktueller Status
   statusaenderung_fkcl TEXT, -- Statusveränderung im Rahmen einer Baumaßnahme
   CONSTRAINT fk_bst_strassenablauf_gehoertzuplan_pfs_plan_fk FOREIGN KEY (gehoertzuplan_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_strassenablauf_netzsparte_fkcl FOREIGN KEY (netzsparte_fkcl) REFERENCES bst_netzsparte (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_strassenablauf_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_strassenablauf_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE bst_strassenbeleuchtung (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuplan_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   leitungstyp_fkcl TEXT, -- Auswahl des Leitungstyps
   statusaktuell_fkcl TEXT, -- aktueller Status
   statusaenderung_fkcl TEXT, -- Statusveränderung im Rahmen einer Baumaßnahme
   nennweite TEXT, -- Nennweite einer einzelnen Leitung. Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser REAL, -- Außendurchmesser einer einzelnen Leitung in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   ueberdeckung REAL, -- Mindestüberdeckung (DIN): Mindestabstand zwischen Oberkante der Verkehrsfläche und Oberkante der Leitung in m. Bei Leitungsbündeln bezieht sich der Wert auf die oberste Leitung bzw. die Oberkante des Bündels. Die "Verlegetiefe" einer Leitung wird dagegen bis zur Grabensohle gemessen. Gilt nur für erdverlegte Linienobjekte. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   lagegenauigkeit REAL, -- Statistisches Maß der maximalen Abweichung des realen Verlaufs der Leitung von der Liniengeometrie in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   pufferzone3d REAL, -- Die Pufferzone definiert in einem 3D Modell einen rechteckigen Körper, in dem die Höhenlage einer Leitung (oder eines Leitungsbündels) variieren kann. Die obere Grenze des Puffers wird durch das Attribut Überdeckung definiert. Das hier einzutragende Maß ist die Distanz zur unteren Grenze des Puffers. Die Breite ergibt sich bei einzelnen Leitungen aus dem Attribut Nennweite oder Außendurchmesser, bei Leitungsbündeln aus der Breite der Leitungszone.
   schutzzone3d REAL, -- Die Schutzzone definiert in einem 3D Modell einen quadratischen Körper um die Leitung. Der hier einzutragende Wert ist die Länge, die von vier Kreistangenten ausgehend den Abstand zu den waage- und senkrechten Kanten des Quadrats darstellt.
   spannung_fkcl TEXT, -- Angabe der Spannung einer Leitung. Bei Leitungsbündeln kann das Textattribut "beschreibung" zur Differenzierung der Spannungsarten genutzt werden.
   leitungszonebreite REAL, -- Ein Bündel an Leitungen wird über deren Gesamtbreite und -tiefe in Metern spezifiziert. Eine weitere Differenzierung zwischen Kabeln mit und ohne Schutzrohr sowie deren jeweiligem Durchmesser erfolgt nicht.
   leitungszonetiefe REAL, -- Ein Bündel an Leitungen wird über deren Gesamtbreite und -tiefe in Metern spezifiziert. Die Tiefe bezieht sich auf den Abstand zwischen der Oberkante der obersten und der Unterkante der untersten Lage der Leitungen.
   CONSTRAINT fk_bst_strassenbeleuchtung_gehoertzuplan_pfs_plan_fk FOREIGN KEY (gehoertzuplan_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_strassenbeleuchtung_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_strassenbeleuchtung_spannung_fkcl FOREIGN KEY (spannung_fkcl) REFERENCES bst_spannung (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_strassenbeleuchtung_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_strassenbeleuchtung_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE bst_stromleitung (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuplan_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   leitungstyp_fkcl TEXT, -- Auswahl des Leitungstyps
   statusaktuell_fkcl TEXT, -- aktueller Status
   statusaenderung_fkcl TEXT, -- Statusveränderung im Rahmen einer Baumaßnahme
   nennweite TEXT, -- Nennweite einer einzelnen Leitung. Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser REAL, -- Außendurchmesser einer einzelnen Leitung in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   ueberdeckung REAL, -- Mindestüberdeckung (DIN): Mindestabstand zwischen Oberkante der Verkehrsfläche und Oberkante der Leitung in m. Bei Leitungsbündeln bezieht sich der Wert auf die oberste Leitung bzw. die Oberkante des Bündels. Die "Verlegetiefe" einer Leitung wird dagegen bis zur Grabensohle gemessen. Gilt nur für erdverlegte Linienobjekte. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   lagegenauigkeit REAL, -- Statistisches Maß der maximalen Abweichung des realen Verlaufs der Leitung von der Liniengeometrie in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   pufferzone3d REAL, -- Die Pufferzone definiert in einem 3D Modell einen rechteckigen Körper, in dem die Höhenlage einer Leitung (oder eines Leitungsbündels) variieren kann. Die obere Grenze des Puffers wird durch das Attribut Überdeckung definiert. Das hier einzutragende Maß ist die Distanz zur unteren Grenze des Puffers. Die Breite ergibt sich bei einzelnen Leitungen aus dem Attribut Nennweite oder Außendurchmesser, bei Leitungsbündeln aus der Breite der Leitungszone.
   schutzzone3d REAL, -- Die Schutzzone definiert in einem 3D Modell einen quadratischen Körper um die Leitung. Der hier einzutragende Wert ist die Länge, die von vier Kreistangenten ausgehend den Abstand zu den waage- und senkrechten Kanten des Quadrats darstellt.
   spannung_fkcl TEXT, -- Angabe der Spannung einer Leitung. Bei Leitungsbündeln kann das Textattribut "beschreibung" zur Differenzierung der Spannungsarten genutzt werden.
   leitungszonebreite REAL, -- Ein Bündel an Leitungen wird über deren Gesamtbreite und -tiefe in Metern spezifiziert. Eine weitere Differenzierung zwischen Kabeln mit und ohne Schutzrohr sowie deren jeweiligem Durchmesser erfolgt nicht.
   leitungszonetiefe REAL, -- Ein Bündel an Leitungen wird über deren Gesamtbreite und -tiefe in Metern spezifiziert. Die Tiefe bezieht sich auf den Abstand zwischen der Oberkante der obersten und der Unterkante der untersten Lage der Leitungen.
   CONSTRAINT fk_bst_stromleitung_gehoertzuplan_pfs_plan_fk FOREIGN KEY (gehoertzuplan_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_stromleitung_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_stromleitung_spannung_fkcl FOREIGN KEY (spannung_fkcl) REFERENCES bst_spannung (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_stromleitung_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_stromleitung_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE bst_telekommunikationsleitung (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuplan_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   leitungstyp_fkcl TEXT, -- Auswahl des Leitungstyps
   statusaktuell_fkcl TEXT, -- aktueller Status
   statusaenderung_fkcl TEXT, -- Statusveränderung im Rahmen einer Baumaßnahme
   nennweite TEXT, -- Nennweite einer einzelnen Leitung. Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser REAL, -- Außendurchmesser einer einzelnen Leitung in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   ueberdeckung REAL, -- Mindestüberdeckung (DIN): Mindestabstand zwischen Oberkante der Verkehrsfläche und Oberkante der Leitung in m. Bei Leitungsbündeln bezieht sich der Wert auf die oberste Leitung bzw. die Oberkante des Bündels. Die "Verlegetiefe" einer Leitung wird dagegen bis zur Grabensohle gemessen. Gilt nur für erdverlegte Linienobjekte. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   lagegenauigkeit REAL, -- Statistisches Maß der maximalen Abweichung des realen Verlaufs der Leitung von der Liniengeometrie in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   pufferzone3d REAL, -- Die Pufferzone definiert in einem 3D Modell einen rechteckigen Körper, in dem die Höhenlage einer Leitung (oder eines Leitungsbündels) variieren kann. Die obere Grenze des Puffers wird durch das Attribut Überdeckung definiert. Das hier einzutragende Maß ist die Distanz zur unteren Grenze des Puffers. Die Breite ergibt sich bei einzelnen Leitungen aus dem Attribut Nennweite oder Außendurchmesser, bei Leitungsbündeln aus der Breite der Leitungszone.
   schutzzone3d REAL, -- Die Schutzzone definiert in einem 3D Modell einen quadratischen Körper um die Leitung. Der hier einzutragende Wert ist die Länge, die von vier Kreistangenten ausgehend den Abstand zu den waage- und senkrechten Kanten des Quadrats darstellt.
   art_fkcl TEXT, -- Auswahl des Kabeltyps. Bei Leitungsbündeln kann das Textattribut "beschreibung" zur Differenzierung der Kabel genutzt werden.
   leitungszonebreite REAL, -- Ein Bündel an Leitungen wird über deren Gesamtbreite und -tiefe in Metern spezifiziert. Eine weitere Differenzierung zwischen Kabeln mit und ohne Schutzrohr sowie deren jeweiligem Durchmesser erfolgt nicht.
   leitungszonetiefe REAL, -- Ein Bündel an Leitungen wird über deren Gesamtbreite und -tiefe in Metern spezifiziert. Die Tiefe bezieht sich auf den Abstand zwischen der Oberkante der obersten und der Unterkante der untersten Lage der Leitungen.
   CONSTRAINT fk_bst_telekommunikationsleitung_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES xp_kabeltyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_telekommunikationsleitung_gehoertzuplan_pfs_plan_fk FOREIGN KEY (gehoertzuplan_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_telekommunikationsleitung_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_telekommunikationsleitung_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_telekommunikationsleitung_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE bst_umspannwerk (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuplan_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   netzsparte_fkcl TEXT, -- Leitungssparte eines Punktobjektes
   statusaktuell_fkcl TEXT, -- aktueller Status
   statusaenderung_fkcl TEXT, -- Statusveränderung im Rahmen einer Baumaßnahme
   begrenzung_fkcl TEXT, -- Bestimmung der dargestellten Fläche
   CONSTRAINT fk_bst_umspannwerk_begrenzung_fkcl FOREIGN KEY (begrenzung_fkcl) REFERENCES xp_infrastrukturflaeche (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_umspannwerk_gehoertzuplan_pfs_plan_fk FOREIGN KEY (gehoertzuplan_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_umspannwerk_netzsparte_fkcl FOREIGN KEY (netzsparte_fkcl) REFERENCES bst_netzsparte (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_umspannwerk_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_umspannwerk_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE bst_verteiler (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuplan_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   netzsparte_fkcl TEXT, -- Leitungssparte eines Punktobjektes
   statusaktuell_fkcl TEXT, -- aktueller Status
   statusaenderung_fkcl TEXT, -- Statusveränderung im Rahmen einer Baumaßnahme
   art_fkcl TEXT, -- Typ des Gehäuses bzw. der Funktion
   CONSTRAINT fk_bst_verteiler_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES xp_gehaeusetyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_verteiler_gehoertzuplan_pfs_plan_fk FOREIGN KEY (gehoertzuplan_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_verteiler_netzsparte_fkcl FOREIGN KEY (netzsparte_fkcl) REFERENCES bst_netzsparte (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_verteiler_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_verteiler_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE bst_waermeleitung (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuplan_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   leitungstyp_fkcl TEXT, -- Auswahl des Leitungstyps
   statusaktuell_fkcl TEXT, -- aktueller Status
   statusaenderung_fkcl TEXT, -- Statusveränderung im Rahmen einer Baumaßnahme
   nennweite TEXT, -- Nennweite einer einzelnen Leitung. Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser REAL, -- Außendurchmesser einer einzelnen Leitung in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   ueberdeckung REAL, -- Mindestüberdeckung (DIN): Mindestabstand zwischen Oberkante der Verkehrsfläche und Oberkante der Leitung in m. Bei Leitungsbündeln bezieht sich der Wert auf die oberste Leitung bzw. die Oberkante des Bündels. Die "Verlegetiefe" einer Leitung wird dagegen bis zur Grabensohle gemessen. Gilt nur für erdverlegte Linienobjekte. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   lagegenauigkeit REAL, -- Statistisches Maß der maximalen Abweichung des realen Verlaufs der Leitung von der Liniengeometrie in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   pufferzone3d REAL, -- Die Pufferzone definiert in einem 3D Modell einen rechteckigen Körper, in dem die Höhenlage einer Leitung (oder eines Leitungsbündels) variieren kann. Die obere Grenze des Puffers wird durch das Attribut Überdeckung definiert. Das hier einzutragende Maß ist die Distanz zur unteren Grenze des Puffers. Die Breite ergibt sich bei einzelnen Leitungen aus dem Attribut Nennweite oder Außendurchmesser, bei Leitungsbündeln aus der Breite der Leitungszone.
   schutzzone3d REAL, -- Die Schutzzone definiert in einem 3D Modell einen quadratischen Körper um die Leitung. Der hier einzutragende Wert ist die Länge, die von vier Kreistangenten ausgehend den Abstand zu den waage- und senkrechten Kanten des Quadrats darstellt.
   art_fkcl TEXT, -- Art der Wärmeleitung
   netzebene_fkcl TEXT, -- Leitungsart innerhalb des Wärmenetzes
   werkstoff_fkcl TEXT, -- Werkstoff der Leitung
   CONSTRAINT fk_bst_waermeleitung_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES xp_waermeleitungtyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_waermeleitung_gehoertzuplan_pfs_plan_fk FOREIGN KEY (gehoertzuplan_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_waermeleitung_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_waermeleitung_netzebene_fkcl FOREIGN KEY (netzebene_fkcl) REFERENCES xp_rohrleitungnetz (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_waermeleitung_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_waermeleitung_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_waermeleitung_werkstoff_fkcl FOREIGN KEY (werkstoff_fkcl) REFERENCES xp_werkstoff (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE bst_wasserleitung (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuplan_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   leitungstyp_fkcl TEXT, -- Auswahl des Leitungstyps
   statusaktuell_fkcl TEXT, -- aktueller Status
   statusaenderung_fkcl TEXT, -- Statusveränderung im Rahmen einer Baumaßnahme
   nennweite TEXT, -- Nennweite einer einzelnen Leitung. Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser REAL, -- Außendurchmesser einer einzelnen Leitung in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   ueberdeckung REAL, -- Mindestüberdeckung (DIN): Mindestabstand zwischen Oberkante der Verkehrsfläche und Oberkante der Leitung in m. Bei Leitungsbündeln bezieht sich der Wert auf die oberste Leitung bzw. die Oberkante des Bündels. Die "Verlegetiefe" einer Leitung wird dagegen bis zur Grabensohle gemessen. Gilt nur für erdverlegte Linienobjekte. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   lagegenauigkeit REAL, -- Statistisches Maß der maximalen Abweichung des realen Verlaufs der Leitung von der Liniengeometrie in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   pufferzone3d REAL, -- Die Pufferzone definiert in einem 3D Modell einen rechteckigen Körper, in dem die Höhenlage einer Leitung (oder eines Leitungsbündels) variieren kann. Die obere Grenze des Puffers wird durch das Attribut Überdeckung definiert. Das hier einzutragende Maß ist die Distanz zur unteren Grenze des Puffers. Die Breite ergibt sich bei einzelnen Leitungen aus dem Attribut Nennweite oder Außendurchmesser, bei Leitungsbündeln aus der Breite der Leitungszone.
   schutzzone3d REAL, -- Die Schutzzone definiert in einem 3D Modell einen quadratischen Körper um die Leitung. Der hier einzutragende Wert ist die Länge, die von vier Kreistangenten ausgehend den Abstand zu den waage- und senkrechten Kanten des Quadrats darstellt.
   netzebene_fkcl TEXT, -- Leitungsart innerhalb des Wassernetzes
   werkstoff_fkcl TEXT, -- Werkstoff der Leitung
   CONSTRAINT fk_bst_wasserleitung_gehoertzuplan_pfs_plan_fk FOREIGN KEY (gehoertzuplan_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_wasserleitung_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_wasserleitung_netzebene_fkcl FOREIGN KEY (netzebene_fkcl) REFERENCES xp_rohrleitungnetz (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_wasserleitung_statusaenderung_fkcl FOREIGN KEY (statusaenderung_fkcl) REFERENCES bst_statusaenderung (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_wasserleitung_statusaktuell_fkcl FOREIGN KEY (statusaktuell_fkcl) REFERENCES bst_statusaktuell (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_wasserleitung_werkstoff_fkcl FOREIGN KEY (werkstoff_fkcl) REFERENCES xp_werkstoff (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE bst_wegekante (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuplan_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf einen Netzplan, zu dem das Objekt gehört
   art_fkcl TEXT, -- Art der Wegekante
   CONSTRAINT fk_bst_wegekante_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES bst_wegekantetyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_bst_wegekante_gehoertzuplan_pfs_plan_fk FOREIGN KEY (gehoertzuplan_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE bst_wegekantetyp (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE ip_gelenkpunkt (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuip_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf den Infrastrukturplan, zu dem das Objekt gehört
   CONSTRAINT fk_ip_gelenkpunkt_gehoertzuip_pfs_plan_fk FOREIGN KEY (gehoertzuip_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE ip_netzkopplungspunkt (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuip_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf den Infrastrukturplan, zu dem das Objekt gehört
   CONSTRAINT fk_ip_netzkopplungspunkt_gehoertzuip_pfs_plan_fk FOREIGN KEY (gehoertzuip_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE ip_netzkopplungspunkt_einausspeisung (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   einspeiseleistung REAL, -- Einspeiseleistung in MWh/hth
   einspeisemengeprojahr REAL, -- Einspeisemenge pro Jahr in MWhth
   ausspeiseleistung REAL, -- Ausspeiseleistung in
   ausspeisemengeprojahr REAL, -- Ausspeiseleistung pro Jahr in MWhth
   ip_netzkopplungspunkt_id INTEGER NOT NULL,
   CONSTRAINT fk_ip_netzkopplungspunkt_einausspeisung_ip_netzkopplungspunkt_id FOREIGN KEY (ip_netzkopplungspunkt_id) REFERENCES ip_netzkopplungspunkt (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE ip_netzverknuepfungspunkt (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuip_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf den Infrastrukturplan, zu dem das Objekt gehört
   CONSTRAINT fk_ip_netzverknuepfungspunkt_gehoertzuip_pfs_plan_fk FOREIGN KEY (gehoertzuip_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE ip_stationierungspunkt (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuip_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf den Infrastrukturplan, zu dem das Objekt gehört
   laenge REAL, -- Angabe der Streckenkilometer in m
   CONSTRAINT fk_ip_stationierungspunkt_gehoertzuip_pfs_plan_fk FOREIGN KEY (gehoertzuip_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE ip_webservicetyp (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE pfs_alternativetrasseabschnitt (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzupfs_fk INTEGER NOT NULL, -- Referenz auf den Plan, zu dem das Objekt gehört
   nachrichtluebernahme INTEGER DEFAULT 0, -- Nachrichtliche Übernahme = true: Objekt ist nicht Bestandteil dieses Planfeststellungsverfahrens. Default = false.
   planergaenzaenderung INTEGER DEFAULT 0, -- Objekt ist Bestandtteil eines Planergänzungs- oder -äenderungsverfahrens = true (s. PFS_PlanStatus). Default = false.
   leitungstyp_fkcl TEXT, -- Geplanter Leitungstyp
   variante TEXT, -- Bezeichnung der Variante
   CONSTRAINT fk_pfs_alternativetrasseabschnitt_gehoertzupfs_fk FOREIGN KEY (gehoertzupfs_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_alternativetrasseabschnitt_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_armaturengruppe (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzupfs_fk INTEGER NOT NULL, -- Referenz auf den Plan, zu dem das Objekt gehört
   nachrichtluebernahme INTEGER DEFAULT 0, -- Nachrichtliche Übernahme = true: Objekt ist nicht Bestandteil dieses Planfeststellungsverfahrens. Default = false.
   planergaenzaenderung INTEGER DEFAULT 0, -- Objekt ist Bestandtteil eines Planergänzungs- oder -äenderungsverfahrens = true (s. PFS_PlanStatus). Default = false.
   CONSTRAINT fk_pfs_armaturengruppe_gehoertzupfs_fk FOREIGN KEY (gehoertzupfs_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_armaturengruppe_einsatzgebiet (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   pfs_armaturengruppe_id INTEGER NOT NULL,
   xp_armatureinsatzgebiet_id TEXT NOT NULL, -- Einsatzgebiet(e) der Armaturengruppe
   CONSTRAINT fk_pfs_armaturengruppe_einsatzgebiet_pfs_armaturengruppe_id FOREIGN KEY (pfs_armaturengruppe_id) REFERENCES pfs_armaturengruppe (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_armaturengruppe_einsatzgebiet_xp_armatureinsatzgebiet_id FOREIGN KEY (xp_armatureinsatzgebiet_id) REFERENCES xp_armatureinsatzgebiet (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_armaturengruppe_funktion (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   pfs_armaturengruppe_id INTEGER NOT NULL,
   xp_armaturfunktion_id TEXT NOT NULL, -- Funktion(en) der Armaturengruppe.
   CONSTRAINT fk_pfs_armaturengruppe_funktion_pfs_armaturengruppe_id FOREIGN KEY (pfs_armaturengruppe_id) REFERENCES pfs_armaturengruppe (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_armaturengruppe_funktion_xp_armaturfunktion_id FOREIGN KEY (xp_armaturfunktion_id) REFERENCES xp_armaturfunktion (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_baugrube (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzupfs_fk INTEGER NOT NULL, -- Referenz auf den Plan, zu dem das Objekt gehört
   nachrichtluebernahme INTEGER DEFAULT 0, -- Nachrichtliche Übernahme = true: Objekt ist nicht Bestandteil dieses Planfeststellungsverfahrens. Default = false.
   planergaenzaenderung INTEGER DEFAULT 0, -- Objekt ist Bestandtteil eines Planergänzungs- oder -äenderungsverfahrens = true (s. PFS_PlanStatus). Default = false.
   art_fkcl TEXT, -- Auswahl der Start- und Zielgrube
   CONSTRAINT fk_pfs_baugrube_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES xp_baugrubetyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_baugrube_gehoertzupfs_fk FOREIGN KEY (gehoertzupfs_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_baugrubeflaeche (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzupfs_fk INTEGER NOT NULL, -- Referenz auf den Plan, zu dem das Objekt gehört
   nachrichtluebernahme INTEGER DEFAULT 0, -- Nachrichtliche Übernahme = true: Objekt ist nicht Bestandteil dieses Planfeststellungsverfahrens. Default = false.
   planergaenzaenderung INTEGER DEFAULT 0, -- Objekt ist Bestandtteil eines Planergänzungs- oder -äenderungsverfahrens = true (s. PFS_PlanStatus). Default = false.
   art_fkcl TEXT, -- Art der Baustelle
   CONSTRAINT fk_pfs_baugrubeflaeche_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES xp_baugrubetyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_baugrubeflaeche_gehoertzupfs_fk FOREIGN KEY (gehoertzupfs_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_baustelle (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzupfs_fk INTEGER NOT NULL, -- Referenz auf den Plan, zu dem das Objekt gehört
   nachrichtluebernahme INTEGER DEFAULT 0, -- Nachrichtliche Übernahme = true: Objekt ist nicht Bestandteil dieses Planfeststellungsverfahrens. Default = false.
   planergaenzaenderung INTEGER DEFAULT 0, -- Objekt ist Bestandtteil eines Planergänzungs- oder -äenderungsverfahrens = true (s. PFS_PlanStatus). Default = false.
   art_fkcl TEXT, -- Art der Baustelle
   CONSTRAINT fk_pfs_baustelle_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES pfs_baustelletyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_baustelle_gehoertzupfs_fk FOREIGN KEY (gehoertzupfs_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_baustelletyp (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE pfs_energiekopplungsanlage (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzupfs_fk INTEGER NOT NULL, -- Referenz auf den Plan, zu dem das Objekt gehört
   nachrichtluebernahme INTEGER DEFAULT 0, -- Nachrichtliche Übernahme = true: Objekt ist nicht Bestandteil dieses Planfeststellungsverfahrens. Default = false.
   planergaenzaenderung INTEGER DEFAULT 0, -- Objekt ist Bestandtteil eines Planergänzungs- oder -äenderungsverfahrens = true (s. PFS_PlanStatus). Default = false.
   elektrolyseleistung REAL, -- Elektrolyseleistung in MWh/hel
   begrenzung_fkcl TEXT, -- Bestimmung der dargestellten Fläche
   CONSTRAINT fk_pfs_energiekopplungsanlage_begrenzung_fkcl FOREIGN KEY (begrenzung_fkcl) REFERENCES xp_infrastrukturflaeche (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_energiekopplungsanlage_gehoertzupfs_fk FOREIGN KEY (gehoertzupfs_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_energiespeicher (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzupfs_fk INTEGER NOT NULL, -- Referenz auf den Plan, zu dem das Objekt gehört
   nachrichtluebernahme INTEGER DEFAULT 0, -- Nachrichtliche Übernahme = true: Objekt ist nicht Bestandteil dieses Planfeststellungsverfahrens. Default = false.
   planergaenzaenderung INTEGER DEFAULT 0, -- Objekt ist Bestandtteil eines Planergänzungs- oder -äenderungsverfahrens = true (s. PFS_PlanStatus). Default = false.
   art_fkcl TEXT, -- Art des Energiespeichers
   gasart_fkcl TEXT, -- Art des Gases
   gasdruckstufe_fkcl TEXT, -- Druckstufe des Gases
   begrenzung_fkcl TEXT, -- Bestimmung der dargestellten Fläche
   CONSTRAINT fk_pfs_energiespeicher_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES xp_energiespeichertyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_energiespeicher_begrenzung_fkcl FOREIGN KEY (begrenzung_fkcl) REFERENCES xp_infrastrukturflaeche (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_energiespeicher_gasart_fkcl FOREIGN KEY (gasart_fkcl) REFERENCES xp_gastyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_energiespeicher_gasdruckstufe_fkcl FOREIGN KEY (gasdruckstufe_fkcl) REFERENCES xp_gasdruckstufe (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_energiespeicher_gehoertzupfs_fk FOREIGN KEY (gehoertzupfs_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_gasversorgungsleitungabschnitt (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzupfs_fk INTEGER NOT NULL, -- Referenz auf den Plan, zu dem das Objekt gehört
   nachrichtluebernahme INTEGER DEFAULT 0, -- Nachrichtliche Übernahme = true: Objekt ist nicht Bestandteil dieses Planfeststellungsverfahrens. Default = false.
   planergaenzaenderung INTEGER DEFAULT 0, -- Objekt ist Bestandtteil eines Planergänzungs- oder -äenderungsverfahrens = true (s. PFS_PlanStatus). Default = false.
   leitungstyp_fkcl TEXT, -- Geplanter Leitungstyp
   bauweise_fkcl TEXT, -- Bauweise im Trassenabschnitt
   legeverfahren_fkcl TEXT, -- Legeverfahren/Verlegemethode im Trassenabschnitt
   gasart_fkcl TEXT NOT NULL, -- Art des transportierten Gases
   netzebene_fkcl TEXT, -- Leitungsart im Gasnetz
   regelueberdeckung REAL, -- Mindestabstand zwischen Oberkante des Weges und Oberkante des Rohres in m. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   nennweite TEXT, -- Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser REAL, -- Außendurchmesser in m (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   werkstoff_fkcl TEXT, -- Werkstoff der Leitung
   CONSTRAINT fk_pfs_gasversorgungsleitungabschnitt_bauweise_fkcl FOREIGN KEY (bauweise_fkcl) REFERENCES xp_bauweise (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_gasversorgungsleitungabschnitt_gasart_fkcl FOREIGN KEY (gasart_fkcl) REFERENCES xp_gastyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_gasversorgungsleitungabschnitt_gehoertzupfs_fk FOREIGN KEY (gehoertzupfs_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_gasversorgungsleitungabschnitt_legeverfahren_fkcl FOREIGN KEY (legeverfahren_fkcl) REFERENCES xp_legeverfahren (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_gasversorgungsleitungabschnitt_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_gasversorgungsleitungabschnitt_netzebene_fkcl FOREIGN KEY (netzebene_fkcl) REFERENCES xp_rohrleitungnetz (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_gasversorgungsleitungabschnitt_werkstoff_fkcl FOREIGN KEY (werkstoff_fkcl) REFERENCES xp_werkstoff (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_gasversorgungsleitungabschnitt_arbeitsstreifen (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   arbeitsstreifenflur REAL, -- Zur Bauausführung wird ein Regelarbeitsstreifen auf freier Feldflur in Anspruch genommen, Gesamtbreite in m (zur Darstellung im Web-GIS kann auch die Klasse PFS_Baustelle genutzt werden)
   arbeitsstreifenwald REAL, -- Im Wald wird ein schmalerer Arbeitsstreifen beansprucht, Gesamtbreite in m (zur Darstellung im Web-GIS kann auch die Klasse PFS_Baustelle genutzt werden)
   arbeitsstreifenwiese REAL, -- Auf feuchten Wiesen wird ein schmalerer Arbeitsstreifen beansprucht, Gesamtbreite in m (zur Darstellung im Web-GIS kann auch die Klasse PFS_Baustelle genutzt werden)
   pfs_gasversorgungsleitungabschnitt_id INTEGER NOT NULL,
   CONSTRAINT fk_pfs_gasversorgungsleitungabschnitt_arbeitsstreifen_pfs_gasversorgungsleitungabschnitt_id FOREIGN KEY (pfs_gasversorgungsleitungabschnitt_id) REFERENCES pfs_gasversorgungsleitungabschnitt (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_gasversorgungsleitungabschnitt_rohrgraben (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   breitekrone REAL, -- Ungefähre Breite der Grabenkrone in m
   breitesohle REAL, -- Ungefähre Breite der Grabensohle in m
   tiefe REAL, -- Ungefähre Tiefe des Grabens bis zur Grabensohle in m
   pfs_gasversorgungsleitungabschnitt_id INTEGER NOT NULL,
   CONSTRAINT fk_pfs_gasversorgungsleitungabschnitt_rohrgraben_pfs_gasversorgungsleitungabschnitt_id FOREIGN KEY (pfs_gasversorgungsleitungabschnitt_id) REFERENCES pfs_gasversorgungsleitungabschnitt (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_gasversorgungsleitungabschnitt_schutzrohr (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   regelueberdeckung REAL, -- Mindestabstand zwischen Oberkante des Weges und Oberkante des Rohres in m. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   nennweite TEXT, -- Nennweite (DN)
   aussendurchmesser REAL, -- Aussendurchmesser (DA) in m
   anzahl INTEGER, -- Anzahl der Schutzrohre
   werkstoff_fkcl TEXT, -- Werkstoff des Rohres
   zweck TEXT, -- Zweck der Verlegung (z.B. Mitverlegung Leerrohr, Steuerungskabel)
   pfs_gasversorgungsleitungabschnitt_id INTEGER NOT NULL,
   CONSTRAINT fk_pfs_gasversorgungsleitungabschnitt_schutzrohr_pfs_gasversorgungsleitungabschnitt_id FOREIGN KEY (pfs_gasversorgungsleitungabschnitt_id) REFERENCES pfs_gasversorgungsleitungabschnitt (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_gasversorgungsleitungabschnitt_schutzrohr_werkstoff_fkcl FOREIGN KEY (werkstoff_fkcl) REFERENCES xp_werkstoff (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_gasversorgungsleitungabschnitt_schutzstreifen (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   schutzstreifen REAL, -- Dinglich zu sichernder Schutzstreifen einer (Frei-)Leitung. Angabe der Gesamtbreite in m. Bei Hochspannungsleitungen erfolgt für die parabolische Form die Angabe der maximalen Breite. (Zur Darstellung im Web-GIS kann auch der FeatureType PFS_Schutzstreifen genutzt werden)
   schutzstreifengehoelzfrei REAL, -- Breite des Schutzstreifens, der dauerhaft von Gehölzen freizuhalten ist, in m
   schutzstreifenwald REAL, -- Nur für Hochspannungsleitungen: In bewaldeten Leitungsabschnitten verläuft der Schutzstreifen parallel zur Leitungsachse und nicht in parabolischer Form. Maßgebend für die Gesamtbreite ist eine sog. Baumfallkurve, welche zur Sicherung der äußeren Leiterseile vor umstürzenden Bäumen dient.
   pfs_gasversorgungsleitungabschnitt_id INTEGER NOT NULL,
   CONSTRAINT fk_pfs_gasversorgungsleitungabschnitt_schutzstreifen_pfs_gasversorgungsleitungabschnitt_id FOREIGN KEY (pfs_gasversorgungsleitungabschnitt_id) REFERENCES pfs_gasversorgungsleitungabschnitt (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_gleis (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzupfs_fk INTEGER NOT NULL, -- Referenz auf den Plan, zu dem das Objekt gehört
   nachrichtluebernahme INTEGER DEFAULT 0, -- Nachrichtliche Übernahme = true: Objekt ist nicht Bestandteil dieses Planfeststellungsverfahrens. Default = false.
   planergaenzaenderung INTEGER DEFAULT 0, -- Objekt ist Bestandtteil eines Planergänzungs- oder -äenderungsverfahrens = true (s. PFS_PlanStatus). Default = false.
   breite REAL, -- Angabe in m.
   CONSTRAINT fk_pfs_gleis_gehoertzupfs_fk FOREIGN KEY (gehoertzupfs_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_hochspannung (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE pfs_hochspannungsleitungabschnitt (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzupfs_fk INTEGER NOT NULL, -- Referenz auf den Plan, zu dem das Objekt gehört
   nachrichtluebernahme INTEGER DEFAULT 0, -- Nachrichtliche Übernahme = true: Objekt ist nicht Bestandteil dieses Planfeststellungsverfahrens. Default = false.
   planergaenzaenderung INTEGER DEFAULT 0, -- Objekt ist Bestandtteil eines Planergänzungs- oder -äenderungsverfahrens = true (s. PFS_PlanStatus). Default = false.
   leitungstyp_fkcl TEXT, -- Geplanter Leitungstyp
   bauweise_fkcl TEXT, -- Bauweise im Trassenabschnitt
   legeverfahren_fkcl TEXT, -- Legeverfahren/Verlegemethode im Trassenabschnitt
   spannung_fkcl TEXT, -- Spannung in Kilovolt
   CONSTRAINT fk_pfs_hochspannungsleitungabschnitt_bauweise_fkcl FOREIGN KEY (bauweise_fkcl) REFERENCES xp_bauweise (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_hochspannungsleitungabschnitt_gehoertzupfs_fk FOREIGN KEY (gehoertzupfs_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_hochspannungsleitungabschnitt_legeverfahren_fkcl FOREIGN KEY (legeverfahren_fkcl) REFERENCES xp_legeverfahren (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_hochspannungsleitungabschnitt_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_hochspannungsleitungabschnitt_spannung_fkcl FOREIGN KEY (spannung_fkcl) REFERENCES pfs_hochspannung (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_hochspannungsleitungabschnitt_arbeitsstreifen (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   arbeitsstreifenflur REAL, -- Zur Bauausführung wird ein Regelarbeitsstreifen auf freier Feldflur in Anspruch genommen, Gesamtbreite in m (zur Darstellung im Web-GIS kann auch die Klasse PFS_Baustelle genutzt werden)
   arbeitsstreifenwald REAL, -- Im Wald wird ein schmalerer Arbeitsstreifen beansprucht, Gesamtbreite in m (zur Darstellung im Web-GIS kann auch die Klasse PFS_Baustelle genutzt werden)
   arbeitsstreifenwiese REAL, -- Auf feuchten Wiesen wird ein schmalerer Arbeitsstreifen beansprucht, Gesamtbreite in m (zur Darstellung im Web-GIS kann auch die Klasse PFS_Baustelle genutzt werden)
   pfs_hochspannungsleitungabschnitt_id INTEGER NOT NULL,
   CONSTRAINT fk_pfs_hochspannungsleitungabschnitt_arbeitsstreifen_pfs_hochspannungsleitungabschnitt_id FOREIGN KEY (pfs_hochspannungsleitungabschnitt_id) REFERENCES pfs_hochspannungsleitungabschnitt (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_hochspannungsleitungabschnitt_erdkabel (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   regelueberdeckung REAL, -- Mindestabstand zwischen Oberkante des Weges und Oberkante des Rohres in m. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   aussendurchmesser REAL, -- Aussendurchmesser (DA) der Kabel in m
   anzahl INTEGER, -- Anzahl der Erdkabel
   leitungszonebreite REAL, -- Gesamtbreite des Streifens der Erdkabel in m
   pfs_hochspannungsleitungabschnitt_id INTEGER NOT NULL,
   CONSTRAINT fk_pfs_hochspannungsleitungabschnitt_erdkabel_pfs_hochspannungsleitungabschnitt_id FOREIGN KEY (pfs_hochspannungsleitungabschnitt_id) REFERENCES pfs_hochspannungsleitungabschnitt (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_hochspannungsleitungabschnitt_schutzrohr (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   regelueberdeckung REAL, -- Mindestabstand zwischen Oberkante des Weges und Oberkante des Rohres in m. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   nennweite TEXT, -- Nennweite (DN)
   aussendurchmesser REAL, -- Aussendurchmesser (DA) in m
   anzahl INTEGER, -- Anzahl der Schutzrohre
   werkstoff_fkcl TEXT, -- Werkstoff des Rohres
   zweck TEXT, -- Zweck der Verlegung (z.B. Mitverlegung Leerrohr, Steuerungskabel)
   pfs_hochspannungsleitungabschnitt_id INTEGER NOT NULL,
   CONSTRAINT fk_pfs_hochspannungsleitungabschnitt_schutzrohr_pfs_hochspannungsleitungabschnitt_id FOREIGN KEY (pfs_hochspannungsleitungabschnitt_id) REFERENCES pfs_hochspannungsleitungabschnitt (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_hochspannungsleitungabschnitt_schutzrohr_werkstoff_fkcl FOREIGN KEY (werkstoff_fkcl) REFERENCES xp_werkstoff (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_hochspannungsleitungabschnitt_schutzstreifen (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   schutzstreifen REAL, -- Dinglich zu sichernder Schutzstreifen einer (Frei-)Leitung. Angabe der Gesamtbreite in m. Bei Hochspannungsleitungen erfolgt für die parabolische Form die Angabe der maximalen Breite. (Zur Darstellung im Web-GIS kann auch der FeatureType PFS_Schutzstreifen genutzt werden)
   schutzstreifengehoelzfrei REAL, -- Breite des Schutzstreifens, der dauerhaft von Gehölzen freizuhalten ist, in m
   schutzstreifenwald REAL, -- Nur für Hochspannungsleitungen: In bewaldeten Leitungsabschnitten verläuft der Schutzstreifen parallel zur Leitungsachse und nicht in parabolischer Form. Maßgebend für die Gesamtbreite ist eine sog. Baumfallkurve, welche zur Sicherung der äußeren Leiterseile vor umstürzenden Bäumen dient.
   pfs_hochspannungsleitungabschnitt_id INTEGER NOT NULL,
   CONSTRAINT fk_pfs_hochspannungsleitungabschnitt_schutzstreifen_pfs_hochspannungsleitungabschnitt_id FOREIGN KEY (pfs_hochspannungsleitungabschnitt_id) REFERENCES pfs_hochspannungsleitungabschnitt (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_hochspannungsmast (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzupfs_fk INTEGER NOT NULL, -- Referenz auf den Plan, zu dem das Objekt gehört
   nachrichtluebernahme INTEGER DEFAULT 0, -- Nachrichtliche Übernahme = true: Objekt ist nicht Bestandteil dieses Planfeststellungsverfahrens. Default = false.
   planergaenzaenderung INTEGER DEFAULT 0, -- Objekt ist Bestandtteil eines Planergänzungs- oder -äenderungsverfahrens = true (s. PFS_PlanStatus). Default = false.
   art_fkcl TEXT, -- Art des Hochspannungsmast ("Mastbild")
   hoehe REAL, -- Höhe in Metern
   traversenbreite REAL, -- Maximale Gesamtbreite der Traversen
   CONSTRAINT fk_pfs_hochspannungsmast_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES pfs_masttyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_hochspannungsmast_gehoertzupfs_fk FOREIGN KEY (gehoertzupfs_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_kanal (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzupfs_fk INTEGER NOT NULL, -- Referenz auf den Plan, zu dem das Objekt gehört
   nachrichtluebernahme INTEGER DEFAULT 0, -- Nachrichtliche Übernahme = true: Objekt ist nicht Bestandteil dieses Planfeststellungsverfahrens. Default = false.
   planergaenzaenderung INTEGER DEFAULT 0, -- Objekt ist Bestandtteil eines Planergänzungs- oder -äenderungsverfahrens = true (s. PFS_PlanStatus). Default = false.
   breite REAL, -- Angabe in m.
   CONSTRAINT fk_pfs_kanal_gehoertzupfs_fk FOREIGN KEY (gehoertzupfs_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_kraftwerk (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzupfs_fk INTEGER NOT NULL, -- Referenz auf den Plan, zu dem das Objekt gehört
   nachrichtluebernahme INTEGER DEFAULT 0, -- Nachrichtliche Übernahme = true: Objekt ist nicht Bestandteil dieses Planfeststellungsverfahrens. Default = false.
   planergaenzaenderung INTEGER DEFAULT 0, -- Objekt ist Bestandtteil eines Planergänzungs- oder -äenderungsverfahrens = true (s. PFS_PlanStatus). Default = false.
   art_fkcl TEXT, -- Art des Kraftwerks
   primaerenergie_fkcl TEXT, -- Energieträger, der in Gas- und Dampfturbinenkraftwerken in Sekundärenergie gewandelt wird
   kraftwaermekopplung INTEGER, -- Kraft-Wärme-Kopplung (KWK) ist die gleichzeitige Gewinnung von mechanischer Energie und nutzbarer Wärme, die in einem gemeinsamen thermodynamischen Prozess entstehen. Die mechanische Energie wird in der Regel unmittelbar in elektrischen Strom umgewandelt. Die Wärme wird für Heizzwecke als Nah- oder Fernwärme oder für Produktionsprozesse als Prozesswärme genutzt.
   begrenzung_fkcl TEXT, -- Bestimmung der dargestellten Fläche
   CONSTRAINT fk_pfs_kraftwerk_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES xp_kraftwerktyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_kraftwerk_begrenzung_fkcl FOREIGN KEY (begrenzung_fkcl) REFERENCES xp_infrastrukturflaeche (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_kraftwerk_gehoertzupfs_fk FOREIGN KEY (gehoertzupfs_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_kraftwerk_primaerenergie_fkcl FOREIGN KEY (primaerenergie_fkcl) REFERENCES xp_primaerenergietraeger (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_masttyp (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE pfs_messpfahl (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzupfs_fk INTEGER NOT NULL, -- Referenz auf den Plan, zu dem das Objekt gehört
   nachrichtluebernahme INTEGER DEFAULT 0, -- Nachrichtliche Übernahme = true: Objekt ist nicht Bestandteil dieses Planfeststellungsverfahrens. Default = false.
   planergaenzaenderung INTEGER DEFAULT 0, -- Objekt ist Bestandtteil eines Planergänzungs- oder -äenderungsverfahrens = true (s. PFS_PlanStatus). Default = false.
   CONSTRAINT fk_pfs_messpfahl_gehoertzupfs_fk FOREIGN KEY (gehoertzupfs_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_mittelspannungsleitung (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzupfs_fk INTEGER NOT NULL, -- Referenz auf den Plan, zu dem das Objekt gehört
   nachrichtluebernahme INTEGER DEFAULT 0, -- Nachrichtliche Übernahme = true: Objekt ist nicht Bestandteil dieses Planfeststellungsverfahrens. Default = false.
   planergaenzaenderung INTEGER DEFAULT 0, -- Objekt ist Bestandtteil eines Planergänzungs- oder -äenderungsverfahrens = true (s. PFS_PlanStatus). Default = false.
   leitungstyp_fkcl TEXT, -- Geplanter Leitungstyp
   bauweise_fkcl TEXT, -- Bauweise im Trassenabschnitt
   legeverfahren_fkcl TEXT, -- Legeverfahren/Verlegemethode im Trassenabschnitt
   CONSTRAINT fk_pfs_mittelspannungsleitung_bauweise_fkcl FOREIGN KEY (bauweise_fkcl) REFERENCES xp_bauweise (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_mittelspannungsleitung_gehoertzupfs_fk FOREIGN KEY (gehoertzupfs_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_mittelspannungsleitung_legeverfahren_fkcl FOREIGN KEY (legeverfahren_fkcl) REFERENCES xp_legeverfahren (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_mittelspannungsleitung_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_mittelspannungsleitung_arbeitsstreifen (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   arbeitsstreifenflur REAL, -- Zur Bauausführung wird ein Regelarbeitsstreifen auf freier Feldflur in Anspruch genommen, Gesamtbreite in m (zur Darstellung im Web-GIS kann auch die Klasse PFS_Baustelle genutzt werden)
   arbeitsstreifenwald REAL, -- Im Wald wird ein schmalerer Arbeitsstreifen beansprucht, Gesamtbreite in m (zur Darstellung im Web-GIS kann auch die Klasse PFS_Baustelle genutzt werden)
   arbeitsstreifenwiese REAL, -- Auf feuchten Wiesen wird ein schmalerer Arbeitsstreifen beansprucht, Gesamtbreite in m (zur Darstellung im Web-GIS kann auch die Klasse PFS_Baustelle genutzt werden)
   pfs_mittelspannungsleitung_id INTEGER NOT NULL,
   CONSTRAINT fk_pfs_mittelspannungsleitung_arbeitsstreifen_pfs_mittelspannungsleitung_id FOREIGN KEY (pfs_mittelspannungsleitung_id) REFERENCES pfs_mittelspannungsleitung (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_mittelspannungsleitung_erdkabel (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   regelueberdeckung REAL, -- Mindestabstand zwischen Oberkante des Weges und Oberkante des Rohres in m. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   aussendurchmesser REAL, -- Aussendurchmesser (DA) der Kabel in m
   anzahl INTEGER, -- Anzahl der Erdkabel
   leitungszonebreite REAL, -- Gesamtbreite des Streifens der Erdkabel in m
   pfs_mittelspannungsleitung_id INTEGER NOT NULL,
   CONSTRAINT fk_pfs_mittelspannungsleitung_erdkabel_pfs_mittelspannungsleitung_id FOREIGN KEY (pfs_mittelspannungsleitung_id) REFERENCES pfs_mittelspannungsleitung (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_mittelspannungsleitung_schutzrohr (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   regelueberdeckung REAL, -- Mindestabstand zwischen Oberkante des Weges und Oberkante des Rohres in m. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   nennweite TEXT, -- Nennweite (DN)
   aussendurchmesser REAL, -- Aussendurchmesser (DA) in m
   anzahl INTEGER, -- Anzahl der Schutzrohre
   werkstoff_fkcl TEXT, -- Werkstoff des Rohres
   zweck TEXT, -- Zweck der Verlegung (z.B. Mitverlegung Leerrohr, Steuerungskabel)
   pfs_mittelspannungsleitung_id INTEGER NOT NULL,
   CONSTRAINT fk_pfs_mittelspannungsleitung_schutzrohr_pfs_mittelspannungsleitung_id FOREIGN KEY (pfs_mittelspannungsleitung_id) REFERENCES pfs_mittelspannungsleitung (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_mittelspannungsleitung_schutzrohr_werkstoff_fkcl FOREIGN KEY (werkstoff_fkcl) REFERENCES xp_werkstoff (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_mittelspannungsleitung_schutzstreifen (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   schutzstreifen REAL, -- Dinglich zu sichernder Schutzstreifen einer (Frei-)Leitung. Angabe der Gesamtbreite in m. Bei Hochspannungsleitungen erfolgt für die parabolische Form die Angabe der maximalen Breite. (Zur Darstellung im Web-GIS kann auch der FeatureType PFS_Schutzstreifen genutzt werden)
   schutzstreifengehoelzfrei REAL, -- Breite des Schutzstreifens, der dauerhaft von Gehölzen freizuhalten ist, in m
   schutzstreifenwald REAL, -- Nur für Hochspannungsleitungen: In bewaldeten Leitungsabschnitten verläuft der Schutzstreifen parallel zur Leitungsachse und nicht in parabolischer Form. Maßgebend für die Gesamtbreite ist eine sog. Baumfallkurve, welche zur Sicherung der äußeren Leiterseile vor umstürzenden Bäumen dient.
   pfs_mittelspannungsleitung_id INTEGER NOT NULL,
   CONSTRAINT fk_pfs_mittelspannungsleitung_schutzstreifen_pfs_mittelspannungsleitung_id FOREIGN KEY (pfs_mittelspannungsleitung_id) REFERENCES pfs_mittelspannungsleitung (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_plan (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   name TEXT NOT NULL, -- Name des Plans (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML und GML)
   nummer TEXT, -- Nummer des Plans
   internalid TEXT, -- Interner Identifikator des Plans
   beschreibung TEXT, -- Kommentierende Beschreibung des Leitungsplans
   technischeplanerstellung TEXT, -- Bezeichnung der Institution oder Firma, die den Plan technisch erstellt hat
   technherstelldatum TEXT, -- Datum, an dem der Plan technisch ausgefertigt wurde
   erstellungsmassstab INTEGER, -- Der bei der Erstellung des Plans benutzte Kartenmaßstab
   status_fkcl TEXT, -- Status des Planfeststellungsverfahrens
   antragdatum TEXT, -- Datum des Antrages (oder Datum des Erläuterungsberichts)
   antragskonferenzdatum TEXT, -- Datum der Antragskonferenz
   traegerbeteiligungstartdatum TEXT, -- Startdatum der Beteiligung der Behörden, deren Aufgabenbereich durch das Vorhaben berührt wird
   traegerbeteiligungenddatum TEXT, -- Ende der Trägerbeteiligung
   auslegunginternetstartdatum TEXT, -- Startdatum der verwöchigen Veröffentlichung der Planunterlagen im Internet
   auslegunginternetenddatum TEXT, -- Enddatum für die "Auslegung" im Intenet
   planfeststellungsbeschlussdatum TEXT, -- Datum der Planfeststellung
   CONSTRAINT fk_pfs_plan_status_fkcl FOREIGN KEY (status_fkcl) REFERENCES pfs_planstatus (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_plan_auslegunggemeinden (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   gemeinde TEXT, -- Name der betroffenen Gemeinde
   auslegungstartdatum TEXT, -- Datum der Auslegung
   auslegungenddatum TEXT, -- Ende der Auslegung
   pfs_plan_id INTEGER NOT NULL,
   CONSTRAINT fk_pfs_plan_auslegunggemeinden_pfs_plan_id FOREIGN KEY (pfs_plan_id) REFERENCES pfs_plan (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_plan_beteiligte (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   nameorganisation TEXT, -- Name der Organisation
   nameperson TEXT, -- Name der Person
   strassehausnr TEXT, -- Straße und Hausnummer
   postfach TEXT, -- Postfach
   postleitzahl TEXT, -- Postleitzahl
   ort TEXT, -- Ort
   telefon TEXT, -- Telefonnummer
   mail TEXT, -- Mail-Adresse
   rolle_fkcl TEXT, -- Rolle der Person/Organisation
   pfs_plan_id INTEGER NOT NULL,
   CONSTRAINT fk_pfs_plan_beteiligte_pfs_plan_id FOREIGN KEY (pfs_plan_id) REFERENCES pfs_plan (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_plan_beteiligte_rolle_fkcl FOREIGN KEY (rolle_fkcl) REFERENCES xp_rolle (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_plan_externerdienst (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   name TEXT NOT NULL, -- Name des Dienstes
   beschreibung TEXT, -- Beschreibung der Daten
   typ_fkcl TEXT NOT NULL, -- Typ des Webservice
   url TEXT NOT NULL, -- Internetadresse des Diensteservers
   filterausdruck TEXT, -- Filterausdruck, der die url erweitert (um z.B. einzelne Features abzufragen)
   ressourcenidentifikatorgdi TEXT, -- Eindeutige Kennung des Datensatzes im Geodatenkatalog der GDI-DE (https://registry.gdi-de.org/id/...)
   pfs_plan_id INTEGER NOT NULL,
   CONSTRAINT fk_pfs_plan_externerdienst_pfs_plan_id FOREIGN KEY (pfs_plan_id) REFERENCES pfs_plan (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_plan_externerdienst_typ_fkcl FOREIGN KEY (typ_fkcl) REFERENCES ip_webservicetyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_plan_externereferenz (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   referenzname TEXT NOT NULL, -- Name des referierten Dokument innerhalb des Informationssystems
   referenzurl TEXT NOT NULL, -- URI des referierten Dokuments, bzw. Datenbank-Schlüssel. Wenn der XTrasseGML Datensatz und das referierte Dokument in einem hierarchischen Ordnersystem gespeichert sind, kann die URI auch einen relativen Pfad vom XPlanGML-Datensatz zum Dokument enthalten.
   beschreibung TEXT, -- Beschreibung des referierten Dokuments
   datum TEXT, -- Datum des referierten Dokuments
   pfs_plan_id INTEGER NOT NULL,
   CONSTRAINT fk_pfs_plan_externereferenz_pfs_plan_id FOREIGN KEY (pfs_plan_id) REFERENCES pfs_plan (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_plan_gesetzlichegrundlage (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   name TEXT, -- Name / Titel des Gesetzes
   detail TEXT, -- Detaillierte Spezifikation der gesetzlichen Grundlage mit Angabe einer Paragraphennummer
   ausfertigungdatum TEXT, -- Die Datumsangabe bezieht sich in der Regel auf das Datum der Ausfertigung des Gesetzes oder der Rechtsverordnung
   letztebekanntmdatum TEXT, -- Ist das Gesetz oder die Verordnung nach mehreren Änderungen neu bekannt gemacht worden, kann anstelle des Ausfertigungsdatums das Datum der Bekanntmachung der Neufassung angegeben werden
   letzteaenderungdatum TEXT, -- Ist ein Gesetz oder eine Rechtsverordnung nach der Veröffentlichung des amtlichen Volltextes geändert worden, kann hierauf hingewiesen werden
   pfs_plan_id INTEGER NOT NULL,
   CONSTRAINT fk_pfs_plan_gesetzlichegrundlage_pfs_plan_id FOREIGN KEY (pfs_plan_id) REFERENCES pfs_plan (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_plan_referenzrvp (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   refuuid TEXT, -- UUID des Plans
   versionname TEXT, -- Name des Plans
   datum TEXT, -- Datum der (landesplanerischen) Feststellung/Festlegung, raumodnerische Beurteilung, Entscheid der Bundesfachplanung
   pfs_plan_id INTEGER NOT NULL,
   CONSTRAINT fk_pfs_plan_referenzrvp_pfs_plan_id FOREIGN KEY (pfs_plan_id) REFERENCES pfs_plan (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_planstatus (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE pfs_schutzstreifen (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzupfs_fk INTEGER NOT NULL, -- Referenz auf den Plan, zu dem das Objekt gehört
   nachrichtluebernahme INTEGER DEFAULT 0, -- Nachrichtliche Übernahme = true: Objekt ist nicht Bestandteil dieses Planfeststellungsverfahrens. Default = false.
   planergaenzaenderung INTEGER DEFAULT 0, -- Objekt ist Bestandtteil eines Planergänzungs- oder -äenderungsverfahrens = true (s. PFS_PlanStatus). Default = false.
   CONSTRAINT fk_pfs_schutzstreifen_gehoertzupfs_fk FOREIGN KEY (gehoertzupfs_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_station (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzupfs_fk INTEGER NOT NULL, -- Referenz auf den Plan, zu dem das Objekt gehört
   nachrichtluebernahme INTEGER DEFAULT 0, -- Nachrichtliche Übernahme = true: Objekt ist nicht Bestandteil dieses Planfeststellungsverfahrens. Default = false.
   planergaenzaenderung INTEGER DEFAULT 0, -- Objekt ist Bestandtteil eines Planergänzungs- oder -äenderungsverfahrens = true (s. PFS_PlanStatus). Default = false.
   art_fkcl TEXT, -- Art der Station
   CONSTRAINT fk_pfs_station_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES xp_stationtyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_station_gehoertzupfs_fk FOREIGN KEY (gehoertzupfs_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_stationflaeche (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzupfs_fk INTEGER NOT NULL, -- Referenz auf den Plan, zu dem das Objekt gehört
   nachrichtluebernahme INTEGER DEFAULT 0, -- Nachrichtliche Übernahme = true: Objekt ist nicht Bestandteil dieses Planfeststellungsverfahrens. Default = false.
   planergaenzaenderung INTEGER DEFAULT 0, -- Objekt ist Bestandtteil eines Planergänzungs- oder -äenderungsverfahrens = true (s. PFS_PlanStatus). Default = false.
   art_fkcl TEXT, -- Art der Station
   begrenzung_fkcl TEXT, -- Bestimmung der dargestellten Fläche
   CONSTRAINT fk_pfs_stationflaeche_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES xp_stationtyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_stationflaeche_begrenzung_fkcl FOREIGN KEY (begrenzung_fkcl) REFERENCES xp_infrastrukturflaeche (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_stationflaeche_gehoertzupfs_fk FOREIGN KEY (gehoertzupfs_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_stationflaeche_einausspeisung (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   einspeiseleistung REAL, -- Einspeiseleistung in MWh/hth
   einspeisemengeprojahr REAL, -- Einspeisemenge pro Jahr in MWhth
   ausspeiseleistung REAL, -- Ausspeiseleistung in
   ausspeisemengeprojahr REAL, -- Ausspeiseleistung pro Jahr in MWhth
   pfs_stationflaeche_id INTEGER NOT NULL,
   CONSTRAINT fk_pfs_stationflaeche_einausspeisung_pfs_stationflaeche_id FOREIGN KEY (pfs_stationflaeche_id) REFERENCES pfs_stationflaeche (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_strasse (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzupfs_fk INTEGER NOT NULL, -- Referenz auf den Plan, zu dem das Objekt gehört
   nachrichtluebernahme INTEGER DEFAULT 0, -- Nachrichtliche Übernahme = true: Objekt ist nicht Bestandteil dieses Planfeststellungsverfahrens. Default = false.
   planergaenzaenderung INTEGER DEFAULT 0, -- Objekt ist Bestandtteil eines Planergänzungs- oder -äenderungsverfahrens = true (s. PFS_PlanStatus). Default = false.
   breite REAL, -- Angabe in m.
   CONSTRAINT fk_pfs_strasse_gehoertzupfs_fk FOREIGN KEY (gehoertzupfs_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_umspannwerk (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzupfs_fk INTEGER NOT NULL, -- Referenz auf den Plan, zu dem das Objekt gehört
   nachrichtluebernahme INTEGER DEFAULT 0, -- Nachrichtliche Übernahme = true: Objekt ist nicht Bestandteil dieses Planfeststellungsverfahrens. Default = false.
   planergaenzaenderung INTEGER DEFAULT 0, -- Objekt ist Bestandtteil eines Planergänzungs- oder -äenderungsverfahrens = true (s. PFS_PlanStatus). Default = false.
   begrenzung_fkcl TEXT, -- Bestimmung der dargestellten Fläche
   CONSTRAINT fk_pfs_umspannwerk_begrenzung_fkcl FOREIGN KEY (begrenzung_fkcl) REFERENCES xp_infrastrukturflaeche (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_umspannwerk_gehoertzupfs_fk FOREIGN KEY (gehoertzupfs_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_waermeleitungabschnitt (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzupfs_fk INTEGER NOT NULL, -- Referenz auf den Plan, zu dem das Objekt gehört
   nachrichtluebernahme INTEGER DEFAULT 0, -- Nachrichtliche Übernahme = true: Objekt ist nicht Bestandteil dieses Planfeststellungsverfahrens. Default = false.
   planergaenzaenderung INTEGER DEFAULT 0, -- Objekt ist Bestandtteil eines Planergänzungs- oder -äenderungsverfahrens = true (s. PFS_PlanStatus). Default = false.
   leitungstyp_fkcl TEXT, -- Geplanter Leitungstyp
   bauweise_fkcl TEXT, -- Bauweise im Trassenabschnitt
   legeverfahren_fkcl TEXT, -- Legeverfahren/Verlegemethode im Trassenabschnitt
   art_fkcl TEXT NOT NULL, -- Art des transportierten Gases
   netzebene_fkcl TEXT, -- Leitungsart im Gasnetz
   regelueberdeckung REAL, -- Mindestabstand zwischen Oberkante des Weges und Oberkante des Rohres/Kabels in m. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   nennweite TEXT, -- Die Nennweite DN ("diamètre nominal", "Durchmesser nach Norm") ist eine numerische Bezeichnung der ungefähren Durchmesser von Bauteilen in einem Rohrleitungssystem, die laut EN ISO 6708 "für Referenzzwecke verwendet wird".
   aussendurchmesser REAL, -- Außendurchmesser in m. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   werkstoff_fkcl TEXT, -- Werkstoff der Leitung
   CONSTRAINT fk_pfs_waermeleitungabschnitt_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES xp_waermeleitungtyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_waermeleitungabschnitt_bauweise_fkcl FOREIGN KEY (bauweise_fkcl) REFERENCES xp_bauweise (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_waermeleitungabschnitt_gehoertzupfs_fk FOREIGN KEY (gehoertzupfs_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_waermeleitungabschnitt_legeverfahren_fkcl FOREIGN KEY (legeverfahren_fkcl) REFERENCES xp_legeverfahren (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_waermeleitungabschnitt_leitungstyp_fkcl FOREIGN KEY (leitungstyp_fkcl) REFERENCES xp_leitungtyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_waermeleitungabschnitt_netzebene_fkcl FOREIGN KEY (netzebene_fkcl) REFERENCES xp_rohrleitungnetz (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_waermeleitungabschnitt_werkstoff_fkcl FOREIGN KEY (werkstoff_fkcl) REFERENCES xp_werkstoff (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_waermeleitungabschnitt_arbeitsstreifen (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   arbeitsstreifenflur REAL, -- Zur Bauausführung wird ein Regelarbeitsstreifen auf freier Feldflur in Anspruch genommen, Gesamtbreite in m (zur Darstellung im Web-GIS kann auch die Klasse PFS_Baustelle genutzt werden)
   arbeitsstreifenwald REAL, -- Im Wald wird ein schmalerer Arbeitsstreifen beansprucht, Gesamtbreite in m (zur Darstellung im Web-GIS kann auch die Klasse PFS_Baustelle genutzt werden)
   arbeitsstreifenwiese REAL, -- Auf feuchten Wiesen wird ein schmalerer Arbeitsstreifen beansprucht, Gesamtbreite in m (zur Darstellung im Web-GIS kann auch die Klasse PFS_Baustelle genutzt werden)
   pfs_waermeleitungabschnitt_id INTEGER NOT NULL,
   CONSTRAINT fk_pfs_waermeleitungabschnitt_arbeitsstreifen_pfs_waermeleitungabschnitt_id FOREIGN KEY (pfs_waermeleitungabschnitt_id) REFERENCES pfs_waermeleitungabschnitt (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_waermeleitungabschnitt_schutzrohr (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   regelueberdeckung REAL, -- Mindestabstand zwischen Oberkante des Weges und Oberkante des Rohres in m. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   nennweite TEXT, -- Nennweite (DN)
   aussendurchmesser REAL, -- Aussendurchmesser (DA) in m
   anzahl INTEGER, -- Anzahl der Schutzrohre
   werkstoff_fkcl TEXT, -- Werkstoff des Rohres
   zweck TEXT, -- Zweck der Verlegung (z.B. Mitverlegung Leerrohr, Steuerungskabel)
   pfs_waermeleitungabschnitt_id INTEGER NOT NULL,
   CONSTRAINT fk_pfs_waermeleitungabschnitt_schutzrohr_pfs_waermeleitungabschnitt_id FOREIGN KEY (pfs_waermeleitungabschnitt_id) REFERENCES pfs_waermeleitungabschnitt (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_pfs_waermeleitungabschnitt_schutzrohr_werkstoff_fkcl FOREIGN KEY (werkstoff_fkcl) REFERENCES xp_werkstoff (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE pfs_waermeleitungabschnitt_schutzstreifen (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   schutzstreifen REAL, -- Dinglich zu sichernder Schutzstreifen einer (Frei-)Leitung. Angabe der Gesamtbreite in m. Bei Hochspannungsleitungen erfolgt für die parabolische Form die Angabe der maximalen Breite. (Zur Darstellung im Web-GIS kann auch der FeatureType PFS_Schutzstreifen genutzt werden)
   schutzstreifengehoelzfrei REAL, -- Breite des Schutzstreifens, der dauerhaft von Gehölzen freizuhalten ist, in m
   schutzstreifenwald REAL, -- Nur für Hochspannungsleitungen: In bewaldeten Leitungsabschnitten verläuft der Schutzstreifen parallel zur Leitungsachse und nicht in parabolischer Form. Maßgebend für die Gesamtbreite ist eine sog. Baumfallkurve, welche zur Sicherung der äußeren Leiterseile vor umstürzenden Bäumen dient.
   pfs_waermeleitungabschnitt_id INTEGER NOT NULL,
   CONSTRAINT fk_pfs_waermeleitungabschnitt_schutzstreifen_pfs_waermeleitungabschnitt_id FOREIGN KEY (pfs_waermeleitungabschnitt_id) REFERENCES pfs_waermeleitungabschnitt (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE rvp_bewertungengstelleriegel (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE rvp_bewertungkonformitaet (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE rvp_bewertungskala (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE rvp_engstelle (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuip_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf den Infrastrukturplan, zu dem das Objekt gehört
   art_fkcl TEXT, -- Art des Hemmnis bzw. Konflikts
   bewertung_fkcl TEXT, -- Bewertung ob Engstelle/Riegel überwunden bzw. passiert werden kann
   CONSTRAINT fk_rvp_engstelle_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES rvp_hemmnistyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_rvp_engstelle_bewertung_fkcl FOREIGN KEY (bewertung_fkcl) REFERENCES rvp_bewertungengstelleriegel (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_rvp_engstelle_gehoertzuip_pfs_plan_fk FOREIGN KEY (gehoertzuip_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE rvp_grobkorridor (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuip_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf den Infrastrukturplan, zu dem das Objekt gehört
   CONSTRAINT fk_rvp_grobkorridor_gehoertzuip_pfs_plan_fk FOREIGN KEY (gehoertzuip_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE rvp_hemmnistyp (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE rvp_konfliktraumordnung (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuip_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf den Infrastrukturplan, zu dem das Objekt gehört
   restriktionsniveau_fkcl TEXT, -- Das allgemeine Restriktionsniveau ist als Basis einer vorhabenübergreifenden Methode zur Raumverträglichkeitsstudie in der Bundesfachplanung zu sehen, da es für die gängigen raumordnerischen Festlegungen eine planunabhängige Einstufung vornimmt. Das Restriktionsniveau beschreibt im gesamtplanerischen Kontext den Stellenwert der relevanten Erfordernisse der Raumordnung gegenüber dem Neubau einer Höchstspannungsleitung. Das spezifische Restriktionsniveau kann sich aus dem allgemeinen ableiten. Zusätzlich werden hier jedoch die relevanten Pläne und Programme in ihren konkreten textlichen Festlegungen und Begründungen ausgewertet.
   konfliktpotenzial_fkcl TEXT, -- Das Konfliktpotenzial beschreibt den Grad der Vereinbarkeit eines Vorhabens mit einer (flächenhaften) raumordnerischen Festlegung, die bei Durchführung einer konkreten Ausbauform zu erwarten ist. Das Konfliktpotenzial setzt sich zusammen aus den Auswirkungen des Vorhabens auf die raumordnerischen Festlegungen sowie deren Stellenwert (sachliche Bestimmtheit/ Kategorie nach § 3 Abs. 1 ROG) im planerischen Gesamtkontext.
   konformitaet_fkcl TEXT, -- Bewertung der Konformität mit den Erfordernissen der Raumordnung, basierend auf dem spezifischen Restriktionsniveau und dem ermittelten Konfliktpotenzial
   CONSTRAINT fk_rvp_konfliktraumordnung_gehoertzuip_pfs_plan_fk FOREIGN KEY (gehoertzuip_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_rvp_konfliktraumordnung_konfliktpotenzial_fkcl FOREIGN KEY (konfliktpotenzial_fkcl) REFERENCES rvp_bewertungskala (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_rvp_konfliktraumordnung_konformitaet_fkcl FOREIGN KEY (konformitaet_fkcl) REFERENCES rvp_bewertungkonformitaet (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_rvp_konfliktraumordnung_restriktionsniveau_fkcl FOREIGN KEY (restriktionsniveau_fkcl) REFERENCES rvp_bewertungskala (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE rvp_konfliktraumordnung_externereferenz (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   referenzname TEXT NOT NULL, -- Name des referierten Dokument innerhalb des Informationssystems
   referenzurl TEXT NOT NULL, -- URI des referierten Dokuments, bzw. Datenbank-Schlüssel. Wenn der XTrasseGML Datensatz und das referierte Dokument in einem hierarchischen Ordnersystem gespeichert sind, kann die URI auch einen relativen Pfad vom XPlanGML-Datensatz zum Dokument enthalten.
   beschreibung TEXT, -- Beschreibung des referierten Dokuments
   datum TEXT, -- Datum des referierten Dokuments
   rvp_konfliktraumordnung_id INTEGER NOT NULL,
   CONSTRAINT fk_rvp_konfliktraumordnung_externereferenz_rvp_konfliktraumordnung_id FOREIGN KEY (rvp_konfliktraumordnung_id) REFERENCES rvp_konfliktraumordnung (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE rvp_korridorsegmenttyp (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE rvp_korridorstatus (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE rvp_korridortyp (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE rvp_linienkorridor (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuip_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf den Infrastrukturplan, zu dem das Objekt gehört
   art_fkcl TEXT NOT NULL, -- Variante des Linienkorridors
   status_fkcl TEXT NOT NULL, -- Planungsstatus des Korridors
   breite REAL, -- Breite des Trassenkorridors in Metern. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   bewertung TEXT, -- Gesamtbewertung der Variante
   CONSTRAINT fk_rvp_linienkorridor_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES rvp_korridortyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_rvp_linienkorridor_gehoertzuip_pfs_plan_fk FOREIGN KEY (gehoertzuip_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_rvp_linienkorridor_status_fkcl FOREIGN KEY (status_fkcl) REFERENCES rvp_korridorstatus (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE rvp_linienkorridor_bestehtaus (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   rvp_linienkorridor_id INTEGER NOT NULL,
   rvp_linienkorridorsegment_id INTEGER NOT NULL, -- Verweis auf die Segmente, aus denen sich der Linienkorridor zusammensetzt.
   CONSTRAINT fk_rvp_linienkorridor_bestehtaus_rvp_linienkorridor_id FOREIGN KEY (rvp_linienkorridor_id) REFERENCES rvp_linienkorridor (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_rvp_linienkorridor_bestehtaus_rvp_linienkorridorsegment_id FOREIGN KEY (rvp_linienkorridorsegment_id) REFERENCES rvp_linienkorridorsegment (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE rvp_linienkorridorsegment (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuip_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf den Infrastrukturplan, zu dem das Objekt gehört
   artkorridor_fkcl TEXT, -- Art des Korridors, dem das Segegment zugewiesen wird. Bei Mehrfachbelegung in verschiedenen Varianten kann das Attribut "korridorVariante" genutzt werden.
   artsegment_fkcl TEXT, -- Art des Korridorsegments
   status_fkcl TEXT, -- Planungsstatus des Korridorsegments
   breite REAL, -- Breite des Trassenkorridors in Metern. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   bewertung TEXT, -- Bewertung im Rahmen eines Vergleichs von Trassenverläufen
   CONSTRAINT fk_rvp_linienkorridorsegment_artkorridor_fkcl FOREIGN KEY (artkorridor_fkcl) REFERENCES rvp_korridortyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_rvp_linienkorridorsegment_artsegment_fkcl FOREIGN KEY (artsegment_fkcl) REFERENCES rvp_korridorsegmenttyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_rvp_linienkorridorsegment_gehoertzuip_pfs_plan_fk FOREIGN KEY (gehoertzuip_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_rvp_linienkorridorsegment_status_fkcl FOREIGN KEY (status_fkcl) REFERENCES rvp_korridorstatus (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE rvp_linienkorridorsegment_korridorvariante (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   rvp_linienkorridorsegment_id INTEGER NOT NULL,
   korridorvariante TEXT NOT NULL, -- Wenn Korridorsegmente Bestandteil verschiedener Varianten sind, werden diese hier benannt
   CONSTRAINT fk_rvp_linienkorridorsegment_korridorvariante_rvp_linienkorridorsegment_id FOREIGN KEY (rvp_linienkorridorsegment_id) REFERENCES rvp_linienkorridorsegment (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE rvp_potenzialflaechestandort (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuip_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf den Infrastrukturplan, zu dem das Objekt gehört
   CONSTRAINT fk_rvp_potenzialflaechestandort_gehoertzuip_pfs_plan_fk FOREIGN KEY (gehoertzuip_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE rvp_raumwiderstand (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuip_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf den Infrastrukturplan, zu dem das Objekt gehört
   art_fkcl TEXT, -- Art des Raumwiderstands
   klasse_fkcl TEXT, -- Klasse des Raumwiderstands
   grossrauemigersachverhalt INTEGER, -- Fläche ist großräumig = true (Hinweis: Filterattribut für Layerstyling)
   CONSTRAINT fk_rvp_raumwiderstand_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES rvp_raumwiderstandtyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_rvp_raumwiderstand_gehoertzuip_pfs_plan_fk FOREIGN KEY (gehoertzuip_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_rvp_raumwiderstand_klasse_fkcl FOREIGN KEY (klasse_fkcl) REFERENCES rvp_raumwiderstandklasse (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE rvp_raumwiderstand_externereferenz (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   referenzname TEXT NOT NULL, -- Name des referierten Dokument innerhalb des Informationssystems
   referenzurl TEXT NOT NULL, -- URI des referierten Dokuments, bzw. Datenbank-Schlüssel. Wenn der XTrasseGML Datensatz und das referierte Dokument in einem hierarchischen Ordnersystem gespeichert sind, kann die URI auch einen relativen Pfad vom XPlanGML-Datensatz zum Dokument enthalten.
   beschreibung TEXT, -- Beschreibung des referierten Dokuments
   datum TEXT, -- Datum des referierten Dokuments
   rvp_raumwiderstand_id INTEGER NOT NULL,
   CONSTRAINT fk_rvp_raumwiderstand_externereferenz_rvp_raumwiderstand_id FOREIGN KEY (rvp_raumwiderstand_id) REFERENCES rvp_raumwiderstand (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE rvp_raumwiderstandklasse (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE rvp_raumwiderstandtyp (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE rvp_riegel (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuip_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf den Infrastrukturplan, zu dem das Objekt gehört
   art_fkcl TEXT, -- Art des Hemmnis bzw. Konflikts
   bewertung_fkcl TEXT, -- Bewertung ob Engstelle/Riegel überwunden bzw. passiert werden kann
   CONSTRAINT fk_rvp_riegel_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES rvp_hemmnistyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_rvp_riegel_bewertung_fkcl FOREIGN KEY (bewertung_fkcl) REFERENCES rvp_bewertungengstelleriegel (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_rvp_riegel_gehoertzuip_pfs_plan_fk FOREIGN KEY (gehoertzuip_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE rvp_standortinfrastruktur (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuip_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf den Infrastrukturplan, zu dem das Objekt gehört
   CONSTRAINT fk_rvp_standortinfrastruktur_gehoertzuip_pfs_plan_fk FOREIGN KEY (gehoertzuip_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE rvp_stationierungslinie (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuip_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf den Infrastrukturplan, zu dem das Objekt gehört
   CONSTRAINT fk_rvp_stationierungslinie_gehoertzuip_pfs_plan_fk FOREIGN KEY (gehoertzuip_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE rvp_suchraum (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuip_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf den Infrastrukturplan, zu dem das Objekt gehört
   art_fkcl TEXT NOT NULL, -- Art des Suchraums
   CONSTRAINT fk_rvp_suchraum_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES rvp_suchraumtyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_rvp_suchraum_gehoertzuip_pfs_plan_fk FOREIGN KEY (gehoertzuip_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE rvp_suchraumtyp (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE rvp_trassenachsetyp (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE rvp_trassenkorridor (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuip_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf den Infrastrukturplan, zu dem das Objekt gehört
   art_fkcl TEXT NOT NULL, -- Variante des Trassenkorridors
   status_fkcl TEXT NOT NULL, -- Planungsstatus des Korridors
   breite REAL, -- Breite des Trassenkorridors in Metern. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   bewertung TEXT, -- Gesamtbewertung der Variante
   CONSTRAINT fk_rvp_trassenkorridor_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES rvp_korridortyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_rvp_trassenkorridor_gehoertzuip_pfs_plan_fk FOREIGN KEY (gehoertzuip_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_rvp_trassenkorridor_status_fkcl FOREIGN KEY (status_fkcl) REFERENCES rvp_korridorstatus (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE rvp_trassenkorridor_bestehtaus (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   rvp_trassenkorridor_id INTEGER NOT NULL,
   rvp_trassenkorridorsegment_id INTEGER NOT NULL, -- Verweis auf die Segmente, aus denen sich der Trassenkorridor zusammensetzt
   CONSTRAINT fk_rvp_trassenkorridor_bestehtaus_rvp_trassenkorridor_id FOREIGN KEY (rvp_trassenkorridor_id) REFERENCES rvp_trassenkorridor (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_rvp_trassenkorridor_bestehtaus_rvp_trassenkorridorsegment_id FOREIGN KEY (rvp_trassenkorridorsegment_id) REFERENCES rvp_trassenkorridorsegment (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE rvp_trassenkorridorachse (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuip_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf den Infrastrukturplan, zu dem das Objekt gehört
   art_fkcl TEXT, -- Art der Trassenachse
   bewertung TEXT, -- Bewertung im Rahmen eines Vergleichs von Trassenverläufen
   CONSTRAINT fk_rvp_trassenkorridorachse_art_fkcl FOREIGN KEY (art_fkcl) REFERENCES rvp_trassenachsetyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_rvp_trassenkorridorachse_gehoertzuip_pfs_plan_fk FOREIGN KEY (gehoertzuip_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE rvp_trassenkorridorsegment (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuip_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf den Infrastrukturplan, zu dem das Objekt gehört
   artkorridor_fkcl TEXT, -- Art des Korridors, dem das Segments zugewiesen wird. Bei Mehrfachbelegung in verschiedenen Varianten kann das Attribut "korridorVariante" genutzt werden.
   artsegment_fkcl TEXT, -- Art des Korridorsegments
   status_fkcl TEXT, -- Planungsstatus des Korridorsegments
   breite REAL, -- Breite des Trassenkorridors in Metern. (gml:LengthType: uom=“m“ oder uom=“urn:adv:uom:m“)
   bewertung TEXT, -- Gesamtbewertung im Rahmen eines Vergleichs von Trassenverläufen
   CONSTRAINT fk_rvp_trassenkorridorsegment_artkorridor_fkcl FOREIGN KEY (artkorridor_fkcl) REFERENCES rvp_korridortyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_rvp_trassenkorridorsegment_artsegment_fkcl FOREIGN KEY (artsegment_fkcl) REFERENCES rvp_korridorsegmenttyp (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_rvp_trassenkorridorsegment_gehoertzuip_pfs_plan_fk FOREIGN KEY (gehoertzuip_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT fk_rvp_trassenkorridorsegment_status_fkcl FOREIGN KEY (status_fkcl) REFERENCES rvp_korridorstatus (code) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE rvp_trassenkorridorsegment_korridorvariante (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   rvp_trassenkorridorsegment_id INTEGER NOT NULL,
   korridorvariante TEXT NOT NULL, -- Wenn Korridorsegmente Bestandteil verschiedener Varianten sind, werden diese hier benannt.
   CONSTRAINT fk_rvp_trassenkorridorsegment_korridorvariante_rvp_trassenkorridorsegment_id FOREIGN KEY (rvp_trassenkorridorsegment_id) REFERENCES rvp_trassenkorridorsegment (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE xp_armatureinsatzgebiet (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE xp_armaturfunktion (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE xp_baugrubetyp (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE xp_bauweise (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE xp_energiespeichertyp (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE xp_gasdruckstufe (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE xp_gastyp (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE xp_gehaeusetyp (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE xp_infrastrukturflaeche (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE xp_kabeltyp (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE xp_kraftwerktyp (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE xp_legeverfahren (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE xp_leitungtyp (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE xp_planreferenz (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuplan_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf den Netzplan, zu dem das Objekt gehört
   CONSTRAINT fk_xp_planreferenz_gehoertzuplan_pfs_plan_fk FOREIGN KEY (gehoertzuplan_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE xp_planreferenz_referenz (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   referenzname TEXT NOT NULL, -- Name des referierten Dokument innerhalb des Informationssystems
   referenzurl TEXT NOT NULL, -- URI des referierten Dokuments, bzw. Datenbank-Schlüssel. Wenn der XTrasseGML Datensatz und das referierte Dokument in einem hierarchischen Ordnersystem gespeichert sind, kann die URI auch einen relativen Pfad vom XPlanGML-Datensatz zum Dokument enthalten.
   beschreibung TEXT, -- Beschreibung des referierten Dokuments
   datum TEXT, -- Datum des referierten Dokuments
   xp_planreferenz_id INTEGER NOT NULL,
   CONSTRAINT fk_xp_planreferenz_referenz_xp_planreferenz_id FOREIGN KEY (xp_planreferenz_id) REFERENCES xp_planreferenz (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE xp_primaerenergietraeger (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE xp_rohrleitungnetz (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE xp_rolle (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE xp_stationtyp (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE xp_trassenquerschnitt (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid TEXT, -- Eindeutiger Identifier des Objektes
   title TEXT NOT NULL, -- Textliche Bezeichnung des Objekts (Anmerkung: Ldproxy nutzt das Attribut für die Kodierung der Objektreferenzierung in HTML, GML und JSON)
   beschreibung TEXT, -- Kommentierende Beschreibung von Planinhalten/-objekten
   aufschrift TEXT, -- Spezifischer Text zur Beschriftung von Planinhalten
   netzbetreiber TEXT, -- Angabe des Leitungsbetreibers
   gehoertzuplan_pfs_plan_fk INTEGER NOT NULL, -- Referenz auf den Netzplan, zu dem das Objekt gehört
   CONSTRAINT fk_xp_trassenquerschnitt_gehoertzuplan_pfs_plan_fk FOREIGN KEY (gehoertzuplan_pfs_plan_fk) REFERENCES pfs_plan (_id) ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE xp_trassenquerschnitt_trassenquerschnitt (

   _id INTEGER PRIMARY KEY AUTOINCREMENT,
   referenzname TEXT NOT NULL, -- Name des referierten Dokument innerhalb des Informationssystems
   referenzurl TEXT NOT NULL, -- URI des referierten Dokuments, bzw. Datenbank-Schlüssel. Wenn der XTrasseGML Datensatz und das referierte Dokument in einem hierarchischen Ordnersystem gespeichert sind, kann die URI auch einen relativen Pfad vom XPlanGML-Datensatz zum Dokument enthalten.
   beschreibung TEXT, -- Beschreibung des referierten Dokuments
   datum TEXT, -- Datum des referierten Dokuments
   xp_trassenquerschnitt_id INTEGER NOT NULL,
   CONSTRAINT fk_xp_trassenquerschnitt_trassenquerschnitt_xp_trassenquerschnitt_id FOREIGN KEY (xp_trassenquerschnitt_id) REFERENCES xp_trassenquerschnitt (_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE xp_waermeleitungtyp (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);

CREATE TABLE xp_werkstoff (

   code TEXT NOT NULL PRIMARY KEY,
   model TEXT,
   documentation TEXT,
   description TEXT
);


SELECT AddGeometryColumn('bst_abwasserleitung', 'position', 25832, 'MULTILINESTRING', 'XY', -1);
SELECT AddGeometryColumn('bst_armatur', 'position', 25832, 'MULTIPOINT', 'XY', -1);
SELECT AddGeometryColumn('bst_baum', 'position', 25832, 'MULTIPOINT', 'XY', -1);
SELECT AddGeometryColumn('bst_energiespeicher', 'position', 25832, 'MULTIPOLYGON', 'XY', -1);
SELECT AddGeometryColumn('bst_gasleitung', 'position', 25832, 'MULTILINESTRING', 'XY', -1);
SELECT AddGeometryColumn('bst_hausanschluss', 'position', 25832, 'MULTIPOINT', 'XY', -1);
SELECT AddGeometryColumn('bst_kraftwerk', 'position', 25832, 'MULTIPOLYGON', 'XY', -1);
SELECT AddGeometryColumn('bst_mast', 'position', 25832, 'MULTIPOINT', 'XY', -1);
SELECT AddGeometryColumn('bst_richtfunkstrecke', 'position', 25832, 'MULTILINESTRING', 'XY', -1);
SELECT AddGeometryColumn('bst_schacht', 'position', 25832, 'MULTIPOINT', 'XY', -1);
SELECT AddGeometryColumn('bst_sonsteinrichtunglinie', 'position', 25832, 'MULTILINESTRING', 'XY', -1);
SELECT AddGeometryColumn('bst_station', 'position', 25832, 'MULTIPOINT', 'XY', -1);
SELECT AddGeometryColumn('bst_stationflaeche', 'position', 25832, 'MULTIPOLYGON', 'XY', -1);
SELECT AddGeometryColumn('bst_strassenablauf', 'position', 25832, 'MULTIPOINT', 'XY', -1);
SELECT AddGeometryColumn('bst_strassenbeleuchtung', 'position', 25832, 'MULTILINESTRING', 'XY', -1);
SELECT AddGeometryColumn('bst_stromleitung', 'position', 25832, 'MULTILINESTRING', 'XY', -1);
SELECT AddGeometryColumn('bst_telekommunikationsleitung', 'position', 25832, 'MULTILINESTRING', 'XY', -1);
SELECT AddGeometryColumn('bst_umspannwerk', 'position', 25832, 'MULTIPOLYGON', 'XY', -1);
SELECT AddGeometryColumn('bst_verteiler', 'position', 25832, 'MULTIPOINT', 'XY', -1);
SELECT AddGeometryColumn('bst_waermeleitung', 'position', 25832, 'MULTILINESTRING', 'XY', -1);
SELECT AddGeometryColumn('bst_wasserleitung', 'position', 25832, 'MULTILINESTRING', 'XY', -1);
SELECT AddGeometryColumn('bst_wegekante', 'position', 25832, 'LINESTRING', 'XY', -1);
SELECT AddGeometryColumn('ip_gelenkpunkt', 'position', 25832, 'MULTIPOINT', 'XY', -1);
SELECT AddGeometryColumn('ip_netzkopplungspunkt', 'position', 25832, 'MULTIPOINT', 'XY', -1);
SELECT AddGeometryColumn('ip_netzverknuepfungspunkt', 'position', 25832, 'MULTIPOINT', 'XY', -1);
SELECT AddGeometryColumn('ip_stationierungspunkt', 'position', 25832, 'POINT', 'XY', -1);
SELECT AddGeometryColumn('pfs_alternativetrasseabschnitt', 'position', 25832, 'LINESTRING', 'XY', -1);
SELECT AddGeometryColumn('pfs_armaturengruppe', 'position', 25832, 'MULTIPOINT', 'XY', -1);
SELECT AddGeometryColumn('pfs_baugrube', 'position', 25832, 'MULTIPOINT', 'XY', -1);
SELECT AddGeometryColumn('pfs_baugrubeflaeche', 'position', 25832, 'MULTIPOLYGON', 'XY', -1);
SELECT AddGeometryColumn('pfs_baustelle', 'position', 25832, 'MULTIPOLYGON', 'XY', -1);
SELECT AddGeometryColumn('pfs_energiekopplungsanlage', 'position', 25832, 'MULTIPOLYGON', 'XY', -1);
SELECT AddGeometryColumn('pfs_energiespeicher', 'position', 25832, 'MULTIPOLYGON', 'XY', -1);
SELECT AddGeometryColumn('pfs_gasversorgungsleitungabschnitt', 'position', 25832, 'LINESTRING', 'XY', -1);
SELECT AddGeometryColumn('pfs_gleis', 'position', 25832, 'MULTILINESTRING', 'XY', -1);
SELECT AddGeometryColumn('pfs_hochspannungsleitungabschnitt', 'position', 25832, 'LINESTRING', 'XY', -1);
SELECT AddGeometryColumn('pfs_hochspannungsmast', 'position', 25832, 'MULTIPOINT', 'XY', -1);
SELECT AddGeometryColumn('pfs_kanal', 'position', 25832, 'MULTILINESTRING', 'XY', -1);
SELECT AddGeometryColumn('pfs_kraftwerk', 'position', 25832, 'MULTIPOLYGON', 'XY', -1);
SELECT AddGeometryColumn('pfs_messpfahl', 'position', 25832, 'MULTIPOINT', 'XY', -1);
SELECT AddGeometryColumn('pfs_mittelspannungsleitung', 'position', 25832, 'LINESTRING', 'XY', -1);
SELECT AddGeometryColumn('pfs_plan', 'position', 25832, 'POLYGON', 'XY', -1);
SELECT AddGeometryColumn('pfs_schutzstreifen', 'position', 25832, 'MULTIPOLYGON', 'XY', -1);
SELECT AddGeometryColumn('pfs_station', 'position', 25832, 'MULTIPOINT', 'XY', -1);
SELECT AddGeometryColumn('pfs_stationflaeche', 'position', 25832, 'MULTIPOLYGON', 'XY', -1);
SELECT AddGeometryColumn('pfs_strasse', 'position', 25832, 'MULTILINESTRING', 'XY', -1);
SELECT AddGeometryColumn('pfs_umspannwerk', 'position', 25832, 'MULTIPOLYGON', 'XY', -1);
SELECT AddGeometryColumn('pfs_waermeleitungabschnitt', 'position', 25832, 'LINESTRING', 'XY', -1);
SELECT AddGeometryColumn('rvp_engstelle', 'position', 25832, 'MULTIPOINT', 'XY', -1);
SELECT AddGeometryColumn('rvp_grobkorridor', 'position', 25832, 'MULTIPOLYGON', 'XY', -1);
SELECT AddGeometryColumn('rvp_konfliktraumordnung', 'position', 25832, 'MULTIPOLYGON', 'XY', -1);
SELECT AddGeometryColumn('rvp_linienkorridor', 'position', 25832, 'MULTILINESTRING', 'XY', 0);
SELECT AddGeometryColumn('rvp_linienkorridorsegment', 'position', 25832, 'MULTILINESTRING', 'XY', -1);
SELECT AddGeometryColumn('rvp_potenzialflaechestandort', 'position', 25832, 'MULTIPOLYGON', 'XY', -1);
SELECT AddGeometryColumn('rvp_raumwiderstand', 'position', 25832, 'MULTIPOLYGON', 'XY', -1);
SELECT AddGeometryColumn('rvp_riegel', 'position', 25832, 'MULTILINESTRING', 'XY', -1);
SELECT AddGeometryColumn('rvp_standortinfrastruktur', 'position', 25832, 'MULTIPOINT', 'XY', -1);
SELECT AddGeometryColumn('rvp_stationierungslinie', 'position', 25832, 'MULTILINESTRING', 'XY', -1);
SELECT AddGeometryColumn('rvp_suchraum', 'position', 25832, 'MULTIPOLYGON', 'XY', -1);
SELECT AddGeometryColumn('rvp_trassenkorridor', 'position', 25832, 'MULTIPOLYGON', 'XY', 0);
SELECT AddGeometryColumn('rvp_trassenkorridorachse', 'position', 25832, 'MULTILINESTRING', 'XY', -1);
SELECT AddGeometryColumn('rvp_trassenkorridorsegment', 'position', 25832, 'MULTIPOLYGON', 'XY', -1);
SELECT AddGeometryColumn('xp_planreferenz', 'position', 25832, 'POLYGON', 'XY', -1);
SELECT AddGeometryColumn('xp_trassenquerschnitt', 'position', 25832, 'LINESTRING', 'XY', -1);
SELECT CreateSpatialIndex('bst_abwasserleitung', 'position');
SELECT CreateSpatialIndex('bst_armatur', 'position');
SELECT CreateSpatialIndex('bst_baum', 'position');
SELECT CreateSpatialIndex('bst_energiespeicher', 'position');
SELECT CreateSpatialIndex('bst_gasleitung', 'position');
SELECT CreateSpatialIndex('bst_hausanschluss', 'position');
SELECT CreateSpatialIndex('bst_kraftwerk', 'position');
SELECT CreateSpatialIndex('bst_mast', 'position');
SELECT CreateSpatialIndex('bst_richtfunkstrecke', 'position');
SELECT CreateSpatialIndex('bst_schacht', 'position');
SELECT CreateSpatialIndex('bst_sonsteinrichtunglinie', 'position');
SELECT CreateSpatialIndex('bst_station', 'position');
SELECT CreateSpatialIndex('bst_stationflaeche', 'position');
SELECT CreateSpatialIndex('bst_strassenablauf', 'position');
SELECT CreateSpatialIndex('bst_strassenbeleuchtung', 'position');
SELECT CreateSpatialIndex('bst_stromleitung', 'position');
SELECT CreateSpatialIndex('bst_telekommunikationsleitung', 'position');
SELECT CreateSpatialIndex('bst_umspannwerk', 'position');
SELECT CreateSpatialIndex('bst_verteiler', 'position');
SELECT CreateSpatialIndex('bst_waermeleitung', 'position');
SELECT CreateSpatialIndex('bst_wasserleitung', 'position');
SELECT CreateSpatialIndex('bst_wegekante', 'position');
SELECT CreateSpatialIndex('ip_gelenkpunkt', 'position');
SELECT CreateSpatialIndex('ip_netzkopplungspunkt', 'position');
SELECT CreateSpatialIndex('ip_netzverknuepfungspunkt', 'position');
SELECT CreateSpatialIndex('ip_stationierungspunkt', 'position');
SELECT CreateSpatialIndex('pfs_alternativetrasseabschnitt', 'position');
SELECT CreateSpatialIndex('pfs_armaturengruppe', 'position');
SELECT CreateSpatialIndex('pfs_baugrube', 'position');
SELECT CreateSpatialIndex('pfs_baugrubeflaeche', 'position');
SELECT CreateSpatialIndex('pfs_baustelle', 'position');
SELECT CreateSpatialIndex('pfs_energiekopplungsanlage', 'position');
SELECT CreateSpatialIndex('pfs_energiespeicher', 'position');
SELECT CreateSpatialIndex('pfs_gasversorgungsleitungabschnitt', 'position');
SELECT CreateSpatialIndex('pfs_gleis', 'position');
SELECT CreateSpatialIndex('pfs_hochspannungsleitungabschnitt', 'position');
SELECT CreateSpatialIndex('pfs_hochspannungsmast', 'position');
SELECT CreateSpatialIndex('pfs_kanal', 'position');
SELECT CreateSpatialIndex('pfs_kraftwerk', 'position');
SELECT CreateSpatialIndex('pfs_messpfahl', 'position');
SELECT CreateSpatialIndex('pfs_mittelspannungsleitung', 'position');
SELECT CreateSpatialIndex('pfs_plan', 'position');
SELECT CreateSpatialIndex('pfs_schutzstreifen', 'position');
SELECT CreateSpatialIndex('pfs_station', 'position');
SELECT CreateSpatialIndex('pfs_stationflaeche', 'position');
SELECT CreateSpatialIndex('pfs_strasse', 'position');
SELECT CreateSpatialIndex('pfs_umspannwerk', 'position');
SELECT CreateSpatialIndex('pfs_waermeleitungabschnitt', 'position');
SELECT CreateSpatialIndex('rvp_engstelle', 'position');
SELECT CreateSpatialIndex('rvp_grobkorridor', 'position');
SELECT CreateSpatialIndex('rvp_konfliktraumordnung', 'position');
SELECT CreateSpatialIndex('rvp_linienkorridor', 'position');
SELECT CreateSpatialIndex('rvp_linienkorridorsegment', 'position');
SELECT CreateSpatialIndex('rvp_potenzialflaechestandort', 'position');
SELECT CreateSpatialIndex('rvp_raumwiderstand', 'position');
SELECT CreateSpatialIndex('rvp_riegel', 'position');
SELECT CreateSpatialIndex('rvp_standortinfrastruktur', 'position');
SELECT CreateSpatialIndex('rvp_stationierungslinie', 'position');
SELECT CreateSpatialIndex('rvp_suchraum', 'position');
SELECT CreateSpatialIndex('rvp_trassenkorridor', 'position');
SELECT CreateSpatialIndex('rvp_trassenkorridorachse', 'position');
SELECT CreateSpatialIndex('rvp_trassenkorridorsegment', 'position');
SELECT CreateSpatialIndex('xp_planreferenz', 'position');
SELECT CreateSpatialIndex('xp_trassenquerschnitt', 'position');

INSERT INTO bst_kanaltyp (code, model, documentation, description) VALUES ('1000', 'Schmutzwasser', 'Schmutzwasser', 'Schmutzwasser');
INSERT INTO bst_kanaltyp (code, model, documentation, description) VALUES ('2000', 'Regenwasser', 'Regenwasser', 'Regenwasser');
INSERT INTO bst_kanaltyp (code, model, documentation, description) VALUES ('3000', 'Mischwasser', 'Mischwasser', 'Mischwasser');
INSERT INTO bst_masttyp (code, model, documentation, description) VALUES ('1000', 'Funkmast', 'ortsfester Funkanlagenstandort', 'Funkmast');
INSERT INTO bst_masttyp (code, model, documentation, description) VALUES ('2000', 'Sendemast', 'Zumeist Konstruktion aus Stahlfachwerk oder Stahlrohr, die zur Aufnahme von Antennen für Sendezwecke bzw. zur unmittelbaren Verwendung als Sendeantenne dient (Für digitalen Datenfunk ist häufig die Nutzung vorhandener hoher Bauwerke ausreichend)', 'Sendemast');
INSERT INTO bst_masttyp (code, model, documentation, description) VALUES ('3000', 'Telefonmast', 'Ein Telefonmast (Telegrafenmast) trägt eine oberirdisch gezogene Fernsprechleitung', 'Telefonmast');
INSERT INTO bst_masttyp (code, model, documentation, description) VALUES ('4000', 'Freileitungsmast', 'Der Freileitungsmast (Strommast) ist eine Konstruktion für die Aufhängung einer elektrischen Freileitung. Je nach Funktion lässt sich zwischen Trag-, Abspann-, Abzweig-, Kabelend- und Endabspannmasten unterscheiden. Je nach der elektrischen Spannung der Freileitung werden unterschiedliche Freileitungsmasten aus verschiedenen Materialen verwendet (Masten zur Nachrichtenübermittlung werden separat als Telefonmasten erfasst)', 'Freileitungsmast');
INSERT INTO bst_masttyp (code, model, documentation, description) VALUES ('5000', 'Strassenleuchte', 'Trägersystem der Straßenbeleuchtung. Die Leuchte wird an der Spitze eines Holz-, Stahl-, Aluminium- oder Betonmastes montiert, wobei ein Ausleger über die Straße ragt. Teilweise werden Straßenleuchten in dicht bebauten Gebieten an Seilen hängend über der Straße (Überspannungsanlage) oder an Hauswänden angebracht.', 'Straßenleuchte');
INSERT INTO bst_masttyp (code, model, documentation, description) VALUES ('6000', 'Ampel', 'Signalgeber einer Lichtsignalanlage (LSA) oder Lichtzeichenanlage (LZA). Sie dient der Steuerung des Straßen- und Schienenverkehrs.', 'Ampel');
INSERT INTO bst_netzsparte (code, model, documentation, description) VALUES ('1000', 'Telekommunikation', 'Telekommunikation', 'Telekommunikation');
INSERT INTO bst_netzsparte (code, model, documentation, description) VALUES ('2000', 'Gas', 'Gasversorgung', 'Gas');
INSERT INTO bst_netzsparte (code, model, documentation, description) VALUES ('3000', 'Elektrizitaet', 'Stromversorgung', 'Elektrizität');
INSERT INTO bst_netzsparte (code, model, documentation, description) VALUES ('4000', 'Waermeversorgung', 'Versorgung mit Fern- oder Nahwärme', 'Wärmeversorgung');
INSERT INTO bst_netzsparte (code, model, documentation, description) VALUES ('5000', 'Abwasserentsorgung', 'Abwasserentsorgung', 'Abwasserentsorgung');
INSERT INTO bst_netzsparte (code, model, documentation, description) VALUES ('6000', 'Wasserversorgung', 'Trinkwasserversorgung', 'Wasserversorgung');
INSERT INTO bst_netzsparte (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstiges Ver- bzw. Entsorgungsnetz', 'Sonstiges Netz');
INSERT INTO bst_spannung (code, model, documentation, description) VALUES ('1000', 'Niedrigspannung', 'Niedrigspannung (< 1 kV)', 'Niedrigspannung');
INSERT INTO bst_spannung (code, model, documentation, description) VALUES ('2000', 'Mittelspannung', 'Der Begriff Mittelspannung ist nicht genormt bzw. in den Grenzen nicht exakt definiert. Die oberen Grenze wird häufig  mit 30 oder 50 kV angegeben.', 'Mittelspannung');
INSERT INTO bst_spannung (code, model, documentation, description) VALUES ('3000', 'Hochspannung', 'Hochspannung', 'Hochspannung');
INSERT INTO bst_spannung (code, model, documentation, description) VALUES ('30001', 'Hochspannung_110 kV', 'Hochspannung 110 kV', 'Hochspannung 110 kV');
INSERT INTO bst_spannung (code, model, documentation, description) VALUES ('30002', 'Hoechstspannung_220 kV', 'Höchstspannung 220 kV', 'Höchstspannung 220 kV');
INSERT INTO bst_spannung (code, model, documentation, description) VALUES ('30003', 'Hoechstspannung_380 kV', 'Höchstspannung 380 kV', 'Höchstspannung 380 kV');
INSERT INTO bst_spannung (code, model, documentation, description) VALUES ('9999', 'UnbekannteSpannung', 'Unbekannte Spannung', 'Unbekannte Spannung');
INSERT INTO bst_statusaenderung (code, model, documentation, description) VALUES ('1000', 'Wiederinbetriebnahme', 'Wiederinbetriebnahme einer Leitung; Wiederinbetriebnahme eines Infrastrukturobjektes', 'Wiederinbetriebnahme');
INSERT INTO bst_statusaenderung (code, model, documentation, description) VALUES ('2100', 'Ausserbetriebnahme', 'Betriebszustand einer Leitung, in der aktuell kein Medientransport erfolgt, die Anlage jedoch für diesen Zweck weiterhin vorgehalten wird (Eine Gasleitung wird weiterhin überwacht und betriebsbereit instandgehalten, sie ist ebenso in den Korrosionsschutz eingebunden)', 'Außerbetriebnahme');
INSERT INTO bst_statusaenderung (code, model, documentation, description) VALUES ('2200', 'Stilllegung', 'Endgültige Einstellung des Betriebs ohne dass ein vollständiger Rückbau der Leitung vorgesehen ist. Die Anlage wird nach endgültiger Stilllegung so gesichert, dass von ihr keine Gefahr ausgeht.', 'Stilllegung');
INSERT INTO bst_statusaenderung (code, model, documentation, description) VALUES ('3000', 'Rueckbau', 'Rückbau einer Leitung nach endgültiger Stilllegung; Rückbau eines Infrastrukturobjektes', 'Rückbau');
INSERT INTO bst_statusaenderung (code, model, documentation, description) VALUES ('4000', 'Sanierung', 'Sanierung oder Instandsetzung bestehender Leitungen', 'Sanierung');
INSERT INTO bst_statusaenderung (code, model, documentation, description) VALUES ('40001', 'Umstellung_H2', 'Umstellung von Leitungen und Speichern für Transport und Speicherung von Wasserstoff', 'Umstellung H2');
INSERT INTO bst_statusaenderung (code, model, documentation, description) VALUES ('5000', 'AenderungErweiterung', 'NABEG § 3, Nr.1: Änderung oder  Ausbau einer Leitung in einer Bestandstrasse, wobei die bestehende Leitung grundsätzlich fortbestehen soll', 'Änderung/Erweiterung');
INSERT INTO bst_statusaenderung (code, model, documentation, description) VALUES ('6100', 'Ersatzneubau', 'NABEG § 3, Nr. 4: Errichtung einer neuen Leitung in oder unmittelbar neben einer Bestandstrasse, wobei die bestehende Leitung innerhalb von drei Jahren ersetzt wird; die Errichtung erfolgt in der Bestandstrasse, wenn sich bei Freileitungen die Mastfundamente und bei Erdkabeln die Kabel in der Bestandstrasse befinden; die Errichtung erfolgt unmittelbar neben der Bestandstrasse, wenn ein Abstand von 200 Metern zwischen den Trassenachsen nicht überschritten wird.', 'Ersatzneubau');
INSERT INTO bst_statusaenderung (code, model, documentation, description) VALUES ('6200', 'Parallelneubau', 'NABEG § 3, Nr.5: Errichtung einer neuen Leitung unmittelbar neben einer Bestandstrasse, wobei die bestehende Leitung fortbestehen soll; die Errichtung erfolgt unmittelbar neben der Bestandstrasse, wenn ein Abstand von 200 Metern zwischen den Trassenachsen nicht überschritten wird.', 'Parallelneubau');
INSERT INTO bst_statusaktuell (code, model, documentation, description) VALUES ('1000', 'InBetrieb', 'Bestandsobjekt ist in Betrieb', 'in Betrieb');
INSERT INTO bst_statusaktuell (code, model, documentation, description) VALUES ('2100', 'AusserBetriebGenommen', 'Bestandsobjekt ist temporär oder dauerhaft außer Betrieb genommen aber nicht stillgelegt', 'außer Betrieb genommen');
INSERT INTO bst_statusaktuell (code, model, documentation, description) VALUES ('2200', 'Stillgelegt', 'Bestandsobjekt ist dauerhaft stillgelegt und steht nicht mehr für eine Wiederinbetriebnahme zur Verfügung', 'stillgelegt');
INSERT INTO bst_statusaktuell (code, model, documentation, description) VALUES ('3000', 'ImRueckbau', 'Bestandsobjekt ist aktuell im Rückbau', 'im Rückbau');
INSERT INTO bst_statusaktuell (code, model, documentation, description) VALUES ('4000', 'InSanierung', 'Bestandsobjekt ist nicht in Betrieb, da Instandsetzungs- oder Sanierungsarbeiten erfolgen', 'in Sanierung');
INSERT INTO bst_statusaktuell (code, model, documentation, description) VALUES ('5000', 'InAenderung', 'Bestandsobjekt wird zurzeit geändert oder erweitertert (gemäß NABEG § 3, Nr.1)', 'in Änderung/Erweiterung');
INSERT INTO bst_statusaktuell (code, model, documentation, description) VALUES ('6000', 'InErsetzung', 'Bestandsobjekt wird zurzeit durch einen Neubau ersetzt (Ersatzneubau nach NABEG § 3, Nr. 4)', 'in Ersetzung');
INSERT INTO bst_statusaktuell (code, model, documentation, description) VALUES ('9999', 'UnbekannterStatus', 'aktueller Status des Bestandsobjektes ist unbekannt', 'unbekannter Status');
INSERT INTO bst_wegekantetyp (code, model, documentation, description) VALUES ('1000', 'Strassenkante', 'Straßenkante', 'Straßenkante');
INSERT INTO bst_wegekantetyp (code, model, documentation, description) VALUES ('2000', 'KanteFahrradweg', 'Kante Fahrradweg', 'Kante Fahrradweg');
INSERT INTO bst_wegekantetyp (code, model, documentation, description) VALUES ('3000', 'KanteGehweg', 'Kante Gehweg', 'Kante Gehweg');
INSERT INTO ip_webservicetyp (code, model, documentation, description) VALUES ('1000', 'WMS', 'Web Map Service', 'Web Map Service');
INSERT INTO ip_webservicetyp (code, model, documentation, description) VALUES ('2000', 'WFS', 'Web Feature Service', 'Web Feature Service');
INSERT INTO ip_webservicetyp (code, model, documentation, description) VALUES ('3000', 'OAF', 'OGC API Features', 'OGC API Features');
INSERT INTO ip_webservicetyp (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstiger Dienst', 'sonstiger Dienst');
INSERT INTO pfs_baustelletyp (code, model, documentation, description) VALUES ('1000', 'Lagerplatz', 'Temporär genutzte Fläche für die Lagerung von Baumaterial wie Rohre und Leitungen', 'Lagerplatz');
INSERT INTO pfs_baustelletyp (code, model, documentation, description) VALUES ('2000', 'TechnischeAnlage', 'Temporär eingerichte technische Anlage z.B. zur Behandlung von Wasser. Zu der Anlage können auch Zu- und Ablaufbecken sowie Flächen für Aufstellung und Betrieb gehören.', 'technische Anlage');
INSERT INTO pfs_baustelletyp (code, model, documentation, description) VALUES ('3000', 'BauzeitlicheZufahrt', 'Trassenzufahrt oder Überfahrt für Baufahrzeuge und einzusetzende Maschinen', 'bauzeitliche Zufahrt');
INSERT INTO pfs_baustelletyp (code, model, documentation, description) VALUES ('4000', 'Baufeld', 'geplantes Baufeld', 'Baufeld');
INSERT INTO pfs_baustelletyp (code, model, documentation, description) VALUES ('5000', 'Entwaesserung', 'Fläche zur Versickerung (bei Grundwasser) oder eine temporäre Ablaufleitung', 'Entwässerung');
INSERT INTO pfs_baustelletyp (code, model, documentation, description) VALUES ('6000', 'Arbeitsstreifen', 'Regelarbeitsstreifen auf freier Feldflur entlang der Trasse', 'Arbeitsstreifen');
INSERT INTO pfs_hochspannung (code, model, documentation, description) VALUES ('1000', '110_kV', 'Hochspannung 110 Kilovolt', '110 kV');
INSERT INTO pfs_hochspannung (code, model, documentation, description) VALUES ('2000', '220_kV', 'Höchstspannung 220 Kilovolt', '220 kV');
INSERT INTO pfs_hochspannung (code, model, documentation, description) VALUES ('3000', '380_kV', 'Höchstspannung 380 Kilovolt', '380 kV');
INSERT INTO pfs_masttyp (code, model, documentation, description) VALUES ('1000', 'Einebenenmast', 'Beim Einebenmast sind die Leiterseile in einer horizontalen Linie angeordnet und hängen in einer Ebene. Hierdurch ergeben sich die geringsten Masthöhen', 'Einebenenmast');
INSERT INTO pfs_masttyp (code, model, documentation, description) VALUES ('2000', 'Donaumast', 'Der Donaumast hat zwei Traversen, in der Regel ist die breitere Traverse mit je zwei Leiterseilen unten, die schmalere mit einem Leiterseil oben. 
Das Donau-Mastbild ist in der 380-kV-Spannungsebene die am häufigsten verwendete Mastbauform.', 'Donaumast');
INSERT INTO pfs_masttyp (code, model, documentation, description) VALUES ('3000', 'Tonnenmast', 'Beim Tonnenmast  sind die Leiterseile auf drei Traversen übereinander  angeordnet. Dies ergibt die geringstmögliche Ausladung und somit die geringste dauerhafte Flächeninanspruchnahme (Schutzbereich).', 'Tonnenmast');
INSERT INTO pfs_masttyp (code, model, documentation, description) VALUES ('4000', 'Kompaktmast', 'Maste mit einer sehr kompakten Bauform werden als Kompaktmaste oder Vollwandmast bezeichnet', 'Kompaktmast');
INSERT INTO pfs_masttyp (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstige Masten', 'sonstige Masten');
INSERT INTO pfs_planstatus (code, model, documentation, description) VALUES ('1000', 'Antrag', 'Antrag auf Planfeststellung zum Bau und Betrieb von Leitungen und Infrastrukturobjekten', 'Antrag');
INSERT INTO pfs_planstatus (code, model, documentation, description) VALUES ('2000', 'Planergaenzung', 'Planergänzung oder ergänzendes Verfahren im Sinne des § 75 Abs. 1a Satz 2 des Verwaltungsverfahrensgesetzes. Ergänzte PFS_Objekte müssen über das Attribut "planErgaezungAenderung" = true belegt werden.', 'Planergänzung');
INSERT INTO pfs_planstatus (code, model, documentation, description) VALUES ('3000', 'Planaenderung', 'Planänderung vor Fertigstellung des Vorhabens im Sinne § 76 Abs. 1 des Verwaltungsverfahrensgesetzes. Geänderte PFS_Objekte müssen über das Attribut "planErgaenzungAenderung" = true belegt werden.', 'Planänderung');
INSERT INTO rvp_bewertungengstelleriegel (code, model, documentation, description) VALUES ('1000', 'Ueberwindbar', 'Überwindbar in offener Regelbauweise ohne besondere Vorkehrungen', 'überwindbar');
INSERT INTO rvp_bewertungengstelleriegel (code, model, documentation, description) VALUES ('2000', 'BedingtUeberwindbar', 'bedingt überwindbar =  überwindbar unter Berücksichtigung von zusätzlichen Vorkehrungen / Maßnahmen, auch bautechnischer Art', 'bedingt überwindbar');
INSERT INTO rvp_bewertungengstelleriegel (code, model, documentation, description) VALUES ('3000', 'SchwerUeberwindbar', 'schwer überwindbar = überwindbar unter Berücksichtigung von aufwendigen zusätzlichen Vorkehrungen/Maßnahmen, auch bautechnischer Art', 'schwer überwindbar');
INSERT INTO rvp_bewertungengstelleriegel (code, model, documentation, description) VALUES ('4000', 'NichtUeberwindbar', 'nicht überwindbar = nicht überwindbar aus rechtlichen und/oder bautechnischen Gründen auch unter Abwägung zusätzlicher Vorkehrungen/Maßnahmen', 'nicht überwindbar');
INSERT INTO rvp_bewertungkonformitaet (code, model, documentation, description) VALUES ('1000', 'Gegeben', 'Konformität ist gegeben', 'gegeben');
INSERT INTO rvp_bewertungkonformitaet (code, model, documentation, description) VALUES ('2000', 'KannErreichtWerden', 'Konformität kann errreicht werden', 'kann erreicht werden');
INSERT INTO rvp_bewertungkonformitaet (code, model, documentation, description) VALUES ('3000', 'KannNichtErreichtWerden', 'Konformität kann nicht erreicht werden', 'kann nicht erreicht werden');
INSERT INTO rvp_bewertungskala (code, model, documentation, description) VALUES ('1000', 'SehrHoch', 'sehr hoch', 'sehr hoch');
INSERT INTO rvp_bewertungskala (code, model, documentation, description) VALUES ('2000', 'Hoch', 'hoch', 'hoch');
INSERT INTO rvp_bewertungskala (code, model, documentation, description) VALUES ('3000', 'Mittel', 'mittel', 'mittel');
INSERT INTO rvp_bewertungskala (code, model, documentation, description) VALUES ('4000', 'Gering', 'gering', 'gering');
INSERT INTO rvp_hemmnistyp (code, model, documentation, description) VALUES ('1000', 'PlanerischesHemmnis', 'Planerische Hemmnisse beziehen sich auf Planungen und Gebietsausweisungen, von denen hohe Raumwiderstände ausgehen', 'planerisches Hemmnis');
INSERT INTO rvp_hemmnistyp (code, model, documentation, description) VALUES ('2000', 'TechnischesHemmnis', 'Technische Hemmnisse sind Verkehrs- und Leitungsinfrastrukturen, die über- bzw. unterquert werden müssen.  Hinzu kommen sog. sonstige technische Hemmnisse, z. B. durch die Nähe einer Leitung zu Energieinfrastrukturen, die den Einbau von Schutzmaßnahmen erforderlich machen', 'technisches Hemmnis');
INSERT INTO rvp_hemmnistyp (code, model, documentation, description) VALUES ('9999', 'SonstigesHemmnis', 'sonstiges Hemmnis', 'sonstiges Hemmnis');
INSERT INTO rvp_korridorsegmenttyp (code, model, documentation, description) VALUES ('1000', 'AlternativesKorridorsegment', 'Alternatives Trassenkorridorsegment (auch Korridoralternative) bei Analyse und Darstellung eines Korridornetzes', 'alternatives Korridorsegment');
INSERT INTO rvp_korridorsegmenttyp (code, model, documentation, description) VALUES ('2000', 'VerworfenesKorridorsegment', 'Korridorsegment, das im Rahmen einer (Raumwiderstands-)Analyse ausgeschlossen oder nicht weiter betrachtet wird', 'verworfenes Korridorsegment');
INSERT INTO rvp_korridorsegmenttyp (code, model, documentation, description) VALUES ('3000', 'RueckbauBestandsleitung', 'Korridorsegment, in dem der Rückbau einer Bestandsleitung erfolgt', 'Rückbau Bestandsleitung');
INSERT INTO rvp_korridorsegmenttyp (code, model, documentation, description) VALUES ('9999', 'SonstigesKorridorsegment', 'sonstiges Korridorsegment', 'sonstiges Korridorsegment');
INSERT INTO rvp_korridorstatus (code, model, documentation, description) VALUES ('1000', 'InBearbeitung', 'Trassenkorridor ist Bestandteil einer laufenden Raumverträglichkeitsprüfung oder einer Rauwiderstandsanalyse', 'in Bearbeitung');
INSERT INTO rvp_korridorstatus (code, model, documentation, description) VALUES ('2000', 'ErgebnisRVP', 'Trassenkorridor ist das Ergebnis der Räumverträglichkeitsprüfung', 'Ergebnis der Raumverträglichkeitsprüfung');
INSERT INTO rvp_korridorstatus (code, model, documentation, description) VALUES ('3000', 'LandesplanerischeFeststellung', 'Abschluss der Raumverträglichkeitsprüfung durch landesplanerische Feststellung', 'Landesplanerische Festlegung');
INSERT INTO rvp_korridorstatus (code, model, documentation, description) VALUES ('4000', 'Bestand', 'Trassenkorrior um Bestandsleitungen', 'Bestandskorridor');
INSERT INTO rvp_korridortyp (code, model, documentation, description) VALUES ('1000', 'Antragskorridor', 'Trassenkorridor als Ergebnis des Verfahrens (auch Antragsvariante). Der Antragskorridor kann sich aus mehreren Segmenten zusammensetzen.', 'Antragskorridor');
INSERT INTO rvp_korridortyp (code, model, documentation, description) VALUES ('10001', 'FestgelegterTrassenkorridor', 'Festgelegter Trassenkorridor', 'festgelegter Trassenkorridor');
INSERT INTO rvp_korridortyp (code, model, documentation, description) VALUES ('10002', 'BevorzugterTrassenkorridor', 'Bevorzugter Trassenkorridor (auch präferierter oder Vorschlagstrassenkorridor)', 'präferierter Trassenkorridor');
INSERT INTO rvp_korridortyp (code, model, documentation, description) VALUES ('10003', 'VorgeschlagenerTrassenkorridor', 'Vorgeschlagener Trassenkorridor / Vorschlags(trassen)korridor / Trassenkorridorvorschlag', 'vorgeschlagener Trassenkorridor');
INSERT INTO rvp_korridortyp (code, model, documentation, description) VALUES ('2000', 'Variantenkorridor', 'Variante eines Trassenkorridors bei mehreren möglichen Trassenverläufen. Die jeweilige Varianten kann aus mehreren Segmenten bestehen.', 'Variantenkorridor');
INSERT INTO rvp_korridortyp (code, model, documentation, description) VALUES ('20001', 'AlternativerTrassenkorridor', 'Ernsthaft zu berücksichtigende bzw. in Frage kommende Alternative (im Vergleich zum Antragskorridor)', 'Alternativer Trassenkorridor');
INSERT INTO rvp_korridortyp (code, model, documentation, description) VALUES ('20002', 'PotenziellerTrassenkorridor', 'Potenzieller Trassenkorridor', 'potenzieller Trassenkorridor');
INSERT INTO rvp_korridortyp (code, model, documentation, description) VALUES ('9999', 'SonstigerKorridor', 'sonstiger Korridor', 'sonstiger Korridor');
INSERT INTO rvp_raumwiderstandklasse (code, model, documentation, description) VALUES ('1000', 'RaumwiderstandsklasseI', 'Definition der Raumwiderstandsklasse erfolgt im Rahmen der jeweiligen Analyse', 'Raumwiderstandsklasse I');
INSERT INTO rvp_raumwiderstandklasse (code, model, documentation, description) VALUES ('2000', 'RaumwiderstandsklasseII', 'Definition der Raumwiderstandsklasse erfolgt im Rahmen der jeweiligen Analyse', 'Raumwiderstandsklasse II');
INSERT INTO rvp_raumwiderstandklasse (code, model, documentation, description) VALUES ('3000', 'RaumwiderstandsklasseIII', 'Definition der Raumwiderstandsklasse erfolgt im Rahmen der jeweiligen Analyse', 'Raumwiderstandsklasse III');
INSERT INTO rvp_raumwiderstandklasse (code, model, documentation, description) VALUES ('4000', 'RaumwiderstandsklasseIV', 'Definition der Raumwiderstandsklasse erfolgt im Rahmen der jeweiligen Analyse', 'Raumwiderstandsklasse IV');
INSERT INTO rvp_raumwiderstandklasse (code, model, documentation, description) VALUES ('5000', 'RaumwiderstandsklasseV', 'Definition der Raumwiderstandsklasse erfolgt im Rahmen der jeweiligen Analyse', 'Raumwiderstandsklasse V');
INSERT INTO rvp_raumwiderstandklasse (code, model, documentation, description) VALUES ('9999', 'nichtQualifizierbar', 'nicht qualifizierbare Raumwiderstandsklasse', 'nicht qualifizierbar');
INSERT INTO rvp_raumwiderstandtyp (code, model, documentation, description) VALUES ('1000', 'FaktischerAusschlussbereich', 'Bereiche, die aufgrund bestehender Nutzungen eindeutig nicht für eine Leitungsführung geeignet sind', 'faktischer Ausschlussbereich');
INSERT INTO rvp_raumwiderstandtyp (code, model, documentation, description) VALUES ('2000', 'PlanungsrechtlicherAusschlussbereich', 'Bereiche, die nicht mit Zielen bzw. Vorranggebieten der Raumordnung vereinbar sind', 'Planungsrechtlicher Ausschlussbereich');
INSERT INTO rvp_raumwiderstandtyp (code, model, documentation, description) VALUES ('3000', 'Restriktionsbereich', 'Bereiche, die projekt- oder raumspezifisch nur bedingt für eine Leitungsführung geeignet sind', 'Restriktionsbereich');
INSERT INTO rvp_raumwiderstandtyp (code, model, documentation, description) VALUES ('4000', 'Eignungsbereich', 'Verbleibender Bereich innerhalb des Suchraums, der keiner der drei übrigen Raumwiderstandstypen zugeordnet ist', 'Eignungsbereich');
INSERT INTO rvp_suchraumtyp (code, model, documentation, description) VALUES ('1000', 'Grobkorridor', 'Grobkorridore zwischen Netzverknüpfungspunkten', 'Grobkorridor');
INSERT INTO rvp_suchraumtyp (code, model, documentation, description) VALUES ('2000', 'Trassenkorridor', 'Trassenkorridore zwischen Netzverknüpfungspunkten', 'Trassenkorridor');
INSERT INTO rvp_suchraumtyp (code, model, documentation, description) VALUES ('3000', 'Trassenfindung', 'Trassenverlauf innerhalb eines Teilraums', 'Trassenfindung');
INSERT INTO rvp_suchraumtyp (code, model, documentation, description) VALUES ('4000', 'Infrastruktur', 'Suchraum für zur Trasse gehörende Infrastruktur', 'Infrastruktur');
INSERT INTO rvp_suchraumtyp (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'sonstiges', 'sonstiges');
INSERT INTO rvp_trassenachsetyp (code, model, documentation, description) VALUES ('1000', 'Mittelachse', 'Schematische Darstellung des Verlaufs der Trasse innerhalb eines Trassenkorridors', 'Mittelachse');
INSERT INTO rvp_trassenachsetyp (code, model, documentation, description) VALUES ('2000', 'Antragstrasse', 'Potenzielle Trassenachse, die als Vorzugsvariante in den weiteren Planungsstufen fungiert', 'Antragstrasse');
INSERT INTO rvp_trassenachsetyp (code, model, documentation, description) VALUES ('3000', 'PotenzielleTrassenachse', 'Potenzieller Trassenverlauf zur Prüfung der Durchgängigkeit des Trassenkorridors an Eng- und Konfliktstellen', 'Potenzielle Trassenachse');
INSERT INTO rvp_trassenachsetyp (code, model, documentation, description) VALUES ('4000', 'Trassenalternative', 'Alternative potenzielle Trassenachse (bei einem kleinräumigen Vergleich von Trassenverläufen)', 'Trassenalternative');
INSERT INTO rvp_trassenachsetyp (code, model, documentation, description) VALUES ('5000', 'VerworfeneTrassenalternative', 'Trassenalternative, die geprüft und verworfen wurde', 'verworfene Trassenalternative');
INSERT INTO xp_armatureinsatzgebiet (code, model, documentation, description) VALUES ('1000', 'Streckenarmatur', 'Armaturen in Abständen entlang einer Leitung', 'Streckenarmatur');
INSERT INTO xp_armatureinsatzgebiet (code, model, documentation, description) VALUES ('2000', 'Ausblasearmatur', 'Dient dem kontrollierten Ableiten von Gasen und Gas-Luftgemischen innerhalb eines Rohrnetzes', 'Ausblasearmatur');
INSERT INTO xp_armatureinsatzgebiet (code, model, documentation, description) VALUES ('3000', 'Hauptabsperreinrichtung', 'Hauptabsperreinrichtung', 'Hauptabsperreinrichtung');
INSERT INTO xp_armatureinsatzgebiet (code, model, documentation, description) VALUES ('4000', 'Ein_Ausgangsarmatur', 'Eingangs- und Ausgangsarmaturen im Rohrnetz', 'Ein-/ Ausgangsarmatur');
INSERT INTO xp_armatureinsatzgebiet (code, model, documentation, description) VALUES ('5000', 'Hydrant', 'Hydrant', 'Hydrant');
INSERT INTO xp_armatureinsatzgebiet (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'sonstiges Einsatzgebiet', 'sonstiges Einsatzgebiet');
INSERT INTO xp_armaturfunktion (code, model, documentation, description) VALUES ('1000', 'Absperrarmatur', 'Absperrung von Stoffströmen durch Hähne und Klappen', 'Absperramatur');
INSERT INTO xp_armaturfunktion (code, model, documentation, description) VALUES ('2000', 'Regulierarmatur', 'Regulierung des Volumenstroms mittels Schieber und Ventilen', 'Regulierarmatur');
INSERT INTO xp_armaturfunktion (code, model, documentation, description) VALUES ('3000', 'Entlueftungsarmatur', 'Dient dem Enfernen von Gasen, insbesondere Luft, aus einer flüssigkeitsführenden Anlage', 'Entlüftungsarmatur');
INSERT INTO xp_armaturfunktion (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'sonstige Funktion', 'sonstige Funktion');
INSERT INTO xp_baugrubetyp (code, model, documentation, description) VALUES ('1000', 'Startgrube', 'Startgrube', 'Startgrube');
INSERT INTO xp_baugrubetyp (code, model, documentation, description) VALUES ('2000', 'Zielgrube', 'Zielgrube', 'Zielgrube');
INSERT INTO xp_bauweise (code, model, documentation, description) VALUES ('1000', 'OffeneBauweise', 'offene Bauweise', 'offene Bauweise');
INSERT INTO xp_bauweise (code, model, documentation, description) VALUES ('2000', 'GeschlosseneBauweise', 'geschlossene Bauweise', 'geschlossene Bauweise');
INSERT INTO xp_bauweise (code, model, documentation, description) VALUES ('3000', 'Oberirdisch', 'oberirdische Verlegung', 'oberirdische Verlegung');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('1000', 'Gasspeicher', 'Oberirdische Nieder- und Mitteldruckbehälter (Gastürme, Gasometer) sowie Hochdruckbehälter (Röhrenspeicher, Kugelspeicher) zur Aufbewahrung von Gasen aller Art', 'Gasspeicher');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('2000', 'Untergrundspeicher', 'Ein Untergrundspeicher (auch Untertagespeicher) ist ein Speicher in natürlichen oder künstlichen Hohlräumen unter der Erdoberfläche. - Untergrundspeicher gemäß Bundesberggesetz (BBergG) § 126', 'Untergrundspeicher');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('20001', 'Kavernenspeicher', 'Große, künstlich angelegte Hohlräume in mächtigen unterirdischen Salzformationen, wie z.B. Salzstöcken. Kavernenspeicher werden durch einen Solprozess bergmännisch angelegt.', 'Kavernenspeicher');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('20002', 'Porenspeicher', 'Natürliche Lagerstätten, die sich durch ihre geologische Formation zur Speicherung von Gas eignen. Sie befinden sich in porösem Gestein, in dem das Gas ähnlich einem stabilen Schwamm aufgenommen und eingelagert wird.', 'Porenspeicher');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('3000', 'Stromspeicher', 'Großspeicheranlagen im Stromnetz', 'Stromspeicher');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('30001', 'Batteriespeicher', 'Großbatteriespeicher (z.B. an einer PV-Anlage)', 'Batteriespeicher');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('30002', 'Pumpspeicherkraftwerk', 'Ein Pumpspeicherkraftwerk (PSW) speichert elektrische Energie in Form von potentieller Energie (Lageenergie) in einem Stausee', 'Pumpspeicherkraftwerk');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('4000', 'Fernwaermespeicher', 'Zumeist drucklose, mit Wasser gefüllte Behälter, die Schwankungen im Wärmebedarf des Fernwärmenetzes bei gleicher Erzeugungsleistung der Fernheizwerke ausgleichen sollen', 'Fernwärmespeicher');
INSERT INTO xp_energiespeichertyp (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstige Speicher', 'sonstige Speicher');
INSERT INTO xp_gasdruckstufe (code, model, documentation, description) VALUES ('1000', 'Niederdruck', 'Niederdruck', 'Niederdruck');
INSERT INTO xp_gasdruckstufe (code, model, documentation, description) VALUES ('2000', 'Mitteldruck', 'Mitteldruck', 'Mitteldruck');
INSERT INTO xp_gasdruckstufe (code, model, documentation, description) VALUES ('3000', 'Hochdruck', 'Hochdruck', 'Hochdruck');
INSERT INTO xp_gasdruckstufe (code, model, documentation, description) VALUES ('9999', 'UnbekannterDruck', 'Unbekannter Druck', 'Unbekannter Druck');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('1000', 'Erdgas', 'Erdgas', 'Erdgas');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('10001', 'L_Gas', 'L-Gas (low calorific gas)', 'L-Gas');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('10002', 'H_Gas', 'H-Gas (high calorific gas)', 'H-Gas');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('2000', 'Wasserstoff', 'Wasserstoff (H2)', 'Wasserstoff');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('20001', 'GruenerWasserstoff', 'Durch die Elektrolyse von Wasser hergestellter Wasserstoff unter Verwendung von Strom aus erneuerbaren Energiequellen', 'grüner Wasserstoff');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('20002', 'BlauerWasserstoff', 'Grauer Wasserstoff, bei dessen Entstehung das CO2 jedoch teilweise abgeschieden und im Erdboden gespeichert wird (CCS, Carbon Capture and Storage). Maximal 90 Prozent des CO₂ sind speicherbar.', 'blauer Wasserstoff');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('20003', 'OrangenerWasserstoff', 'Auf Basis von Abfall und Reststoffen produzierter Wasserstoff, der als CO2-frei gilt', 'orangener Wasserstoff');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('20004', 'GrauerWasserstoff', 'Mittels Dampfreformierung meist aus fossilem Erdgas hergestellter Wasserstoff. Dabei entstehen rund 10 Tonnen CO₂ pro Tonne Wasserstoff. Das CO2 wird in die Atmosphäre abgegeben.', 'grauer Wasserstoff');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('3000', 'Erdgas_H2_Gemisch', 'Erdgas-Wasserstoff-Gemisch', 'Erdgas-Wasserstoff-Gemisch');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('4000', 'Biogas', 'Biogas', 'Biogas');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('5000', 'Fluessiggas', 'Flüssiggas', 'Flüssiggas');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('6000', 'SynthetischesMethan', 'Wird durch wasserelektrolytisch erzeugten Wasserstoff und anschließende Methanisierung hergestellt', 'synthetisch erzeugtes Methan');
INSERT INTO xp_gastyp (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'sonstiges Gas', 'sonstiges Gas');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('1000', 'TK_Verteiler', 'Verteilerschränke der Telekommunikation', 'TK-Verteiler');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('10001', 'Multifunktionsgehaeuse', 'Multifunktionsgehäuse', 'Multifunktionsgehäuse');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('10002', 'GlasfaserNetzverteiler', 'Glasfaser-Netzverteiler (Gf-NVt)', 'Glasfaser-Netzverteiler (Gf- NVt)');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('10003', 'Kabelverzweiger_KVz', 'Kabelverzweiger (KVz) - (Telekom AG)', 'Kabelverzweiger ( KVz) - (Telekom  AG)');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('2000', 'Strom_Schrank', 'Schränke für die Stromversorgung, öffentliche Beleuchtung, Verkehrstechnik u.a.', 'Strom-Schrank');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('20001', 'Schaltschrank', 'Schaltschrank', 'Schaltschrank');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('20002', 'Kabelverteilerschrank', 'Kabelverteilerschrank', 'Kabelverteilerschrank');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('20003', 'Steuerschrank', 'Steuerschrank', 'Steuerschrank');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('20004', 'Trennschrank', 'Trennschrank', 'Trennschrank');
INSERT INTO xp_gehaeusetyp (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'sonstiger Schrank', 'sonstiger Schrank');
INSERT INTO xp_infrastrukturflaeche (code, model, documentation, description) VALUES ('1000', 'Betriebsgelaende', 'gesamtes Betriebsgelände bzw. Grundstücksfläche', 'Betriebsgelände');
INSERT INTO xp_infrastrukturflaeche (code, model, documentation, description) VALUES ('2000', 'EingezaeunteFlaeche', 'eingezäuntes Gelände der Infrastrukturgebäude (ohne Parkplätze und Nebengebäude)', 'eingezäunte Fläche');
INSERT INTO xp_infrastrukturflaeche (code, model, documentation, description) VALUES ('3000', 'Gebaeudeflaeche', 'Fläche eines Gebäudes, das technische Anlagen enthält', 'Gebäudefläche');
INSERT INTO xp_kabeltyp (code, model, documentation, description) VALUES ('1000', 'Glasfaserkabel', 'Glasfaserkabel', 'Glasfaserkabel');
INSERT INTO xp_kabeltyp (code, model, documentation, description) VALUES ('2000', 'Kupferkabel', 'Kupferkabel', 'Kupferkabel');
INSERT INTO xp_kabeltyp (code, model, documentation, description) VALUES ('3000', 'Hybridkabel', 'Hybridkabel', 'Hybridkabel');
INSERT INTO xp_kabeltyp (code, model, documentation, description) VALUES ('4000', 'Koaxialkabel', 'Koaxial-(TV)-Kabel', 'Koaxial-(TV)-Kabel');
INSERT INTO xp_kraftwerktyp (code, model, documentation, description) VALUES ('1000', 'ThermischeTurbine', 'Thermische arbeitende Dampfturbinen- und Gasturbinen-Kraftwerke oder Gas-und-Dampf-Kombikraftwerke', 'Thermisch arbeitende Turbine');
INSERT INTO xp_kraftwerktyp (code, model, documentation, description) VALUES ('2000', 'Windkraft', 'Eine Windkraftanlage (WKA) oder Windenergieanlage (WEA) wandelt Bewegungsenergie des Windes in elektrische Energie um und speist sie in ein Stromnetz ein. Sie werden an Land (onshore) und in Offshore-Windparks im Küstenvorfeld der Meere installiert. Eine Gruppe von Windkraftanlagen wird Windpark genannt.', 'Windkraftanlage');
INSERT INTO xp_kraftwerktyp (code, model, documentation, description) VALUES ('3000', 'Photovoltaik', 'Eine Photovoltaikanlage, auch PV-Anlage (bzw. PVA) wandelt mittels Solarzellen ein Teil der Sonnenstrahlung in elektrische Energie um.  Die Photovoltaik-Freiflächenanlage (auch Solarpark) wird auf einer freien Fläche als fest montiertes System aufgestellt, bei dem mittels einer Unterkonstruktion die Photovoltaikmodule in einem optimalen Winkel zur Sonne (Azimut) ausgerichtet sind.', 'Photovoltaik-Freinflächenanlage');
INSERT INTO xp_kraftwerktyp (code, model, documentation, description) VALUES ('4000', 'Wasserkraft', 'Ein Wasserkraftwerk wandelt die potentielle Energie des Wassers in der Regel über Turbinen in mechanische bzw. elektrische Energie um. Dies kann an Fließgewässern oder Stauseen erfolgen oder durch Strömungs- und Gezeitenkraftwerke auf dem Meer (Pumpspeicherkraftwerke s. PFS_Energiespeicheer)', 'Wasserkraftwerk');
INSERT INTO xp_kraftwerktyp (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstiges Kraftwerk', 'sonstiges Kraftwerk');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('1000', 'Konventionell_offenerGraben', 'Ausschachtung mit Schaufel, Bagger, Fräse', 'Konventionelle Verlegung im offenen Graben');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('2000', 'Pressbohrverfahren', 'Unterirdische Verlegetechnik, die in verschiedenen Varianten zur Anwendung kommt (statisch, dynamisch, ungesteuert, gesteuert) und von Herstellern spezifisch bezeichnet wird ("Modifiziertes Direct-Pipe-Verfahren").  Im Breitbandausbau auch als Erdraketentechnik bekannt. Im Rohrleitungsbau können durch hydraulische oder pneumatische Presseinrichtungen Produktenrohrkreuzungen DN 1000 bis zu 100 m grabenlos verlegt werden.', 'Pressbohrverfahren');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('3000', 'HorizontalSpuelbohrverfahren', 'Richtbohrtechnik für Horizontalbohrungen („Horizontal Directional Drilling“, HDD), die eine grabenlose Verlegung von Produkt- oder Leerrohren ermöglicht.  Die Bohrung ist anfangs meist schräg nach unten in das Erdreich gerichtet und verläuft dann in leichtem Bogen zum Ziel, wo sie schräg nach oben wieder zutage tritt.', 'Horizontal-Spülbohrverfahren');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('4000', 'Pflugverfahren', 'Erstellung eines Leitungsgrabens (Breite > 30cm) oder Schlitzes mit einem Pflugschwert durch Verdrängung der Schicht(en) und gleichzeitigem Einbringen der Glasfasermedien. Der Einsatz des Pflugverfahrens ist ausschließlich in unbefestigten Oberflächen zulässig.', 'Pflugverfahren');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('5000', 'Fraesverfahren_ungebundeOberfl', 'Fräsverfahren in ungebunden Oberflächen (Schlitzbreite: 15 bis 30 cm, Schlitztiefe: 40 bis 120 cm)', 'Fräsverfahren in ungebundenen Oberflächen');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('6000', 'Trenching', 'Erstellung eines Schlitzes (< 30 cm) in gebundenen Verkehrsflächen in verschiedenen Verfahren durch rotierende, senkrecht stehende Werkzeuge, wobei die Schicht(en) gelöst, zerkleinert und gefördert wird (werden)', 'Trenching');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('60001', 'Schleif_Saegeverfahren', 'Erstellung eines Schlitzes eine durch eine Schneideeinheit (Schlitzbreite: 1,5 bis 11 cm, Schlitztiefe: 7 bis 45 cm)', 'Schleif-/Sägeverfahren');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('60002', 'Fraesverfahren', 'Erstellung eines Schlitzes durch ein Fräswerkzeug (Kette, Rad), (Schlitzbreite: 5 bis 15 cm, Schlitztiefe: 30 bis 60 cm)', 'Fräsverfahren');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('7000', 'Rammverfahren', 'Vortriebsverfahren, welches durch hydraulisches oder pneumatisches Vibrationsrammen das Rohr unter dem Hindernis hindurch schlägt. Mit dem Rammverfahren können Produkten- oder Mantelrohrkreuzungen bis zu 100 m Vortriebslänge grabenlos verlegt werden.', 'Rammverfahren');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('8000', 'Microtunneling', 'Für den grabenlosen Vortrieb werden in dem steuerbaren Verfahren zunächst Stahlbetonrohre mit großem Nenndurchmesser verlegt,  in denen nach Durchführung der Unterquerung das eigentliche Produktenrohr eingebracht/eingezogen wird. Es kommt nur bei schwierigen Kreuzungen zur Anwendung.', 'Microtunneling');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('9000', 'oberirdischeVerlegung', 'oberirdische Verlegung mittels Holzmasten', 'oberirdische Verlegung');
INSERT INTO xp_legeverfahren (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstiges Verfahren', 'sonstiges Verfahren');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('1000', 'Erdverlegt', 'Oberkategorie für erdverlegte (Rohr-)Leitungen', 'erdverlegte (Rohr-)Leitungen');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('10001', 'Erdkabel', 'Ein Erdkabel ist ein im Erdboden verlegtes elektrisch genutztes Kabel mit einer besonders robusten Isolierung nach außen, dem Kabelmantel, der eine Zerstörung derselben durch chemische Einflüsse im Erdreich bzw. im Boden lebender Kleintiere verhindert.', 'Erdkabel');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('10002', 'Seekabel', 'Ein Seekabel (auch Unterseekabel, Unterwasserkabel) ist ein im Wesentlichen in einem Gewässer verlegtes Kabel zur Datenübertragung oder die Übertragung elektrischer Energie.', 'Seekabel');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('10003', 'Schutzrohr', 'Im Schutzrohr verlegte oder zu verlegende Kabel/Leitungen. - Schutzrohre schützen erdverlegte Leitungen vor mechanischen Einflüssen und Feuchtigkeit.', 'Schutzrohr');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('10004', 'Leerrohr', 'Über die Baumaßnahme hinaus unbelegtes Schutzrohr', 'Leerrohr (unbelegtes Schutzrohr)');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('10005', 'Leitungsbuendel', 'Bündel von Kabeln und/oder Schutzrohren in den Sparten Sparten Strom und Telekommunikation im Bestand', 'Leitungsbündel');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('10006', 'Dueker', 'Druckleitung zur Unterquerung von Straßen, Flüssen, Bahngleisen etc. Im Düker kann die Flüssigkeit das Hindernis überwinden, ohne dass Pumpen eingesetzt werden müssen.', 'Düker');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('2000', 'Oberirdisch', 'Oberirdisch verlegte Leitungen und Rohre', 'oberirdischer Verlauf');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('20001', 'Freileitung', 'Elektrische Leitung, deren spannungsführende Leiter im Freien durch die Luft geführt und meist auch nur durch die umgebende Luft voneinander und vom Erdboden isoliert sind. In der Regel werden die Leiterseile von Freileitungsmasten getragen, an denen sie mit Isolatoren befestigt sind.', 'Freileitung');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('20002', 'Heberleitung', 'Leitung zur Überquerung von Straßen oder zur Verbindung von Behältern (Gegenstück zu einem Düker)', 'Heberleitung');
INSERT INTO xp_leitungtyp (code, model, documentation, description) VALUES ('20003', 'Rohrbruecke', 'Eine Rohrbrücke oder Rohrleitungsbrücke dient dazu, einzelne oder mehrere Rohrleitungen oberirdisch über größere Entfernungen zu führen.', 'Rohrbrücke');
INSERT INTO xp_primaerenergietraeger (code, model, documentation, description) VALUES ('1000', 'FossilerBrennstoff', 'Fossile Energie wird aus Brennstoffen gewonnen, die in geologischer Vorzeit aus Abbauprodukten von toten Pflanzen und Tieren entstanden sind. Dazu gehören Braunkohle, Steinkohle, Torf, Erdgas und Erdöl.', 'fossiler Brennstoff');
INSERT INTO xp_primaerenergietraeger (code, model, documentation, description) VALUES ('2000', 'Ersatzbrennstoff', 'Ersatzbrennstoffe (EBS) bzw. Sekundärbrennstoffe (SBS) sind Brennstoffe, die aus Abfällen gewonnen werden. Dabei kann es sich sowohl um feste, flüssige oder gasförmige Abfälle aus Haushalten, Industrie oder Gewerbe handeln.', 'Ersatzbrennstoff');
INSERT INTO xp_primaerenergietraeger (code, model, documentation, description) VALUES ('3000', 'Biomasse', 'Der energietechnische Biomasse-Begriff umfasst tierische und pflanzliche Erzeugnisse, die zur Gewinnung von Heizenergie, von elektrischer Energie und als Kraftstoffe verwendet werden können (u.a. Holzpellets, Hackschnitzel, Stroh, Getreide, Altholz, Biogas). Energietechnisch relevante Biomasse kann in gasförmiger, flüssiger und fester Form vorliegen.', 'Biomasse');
INSERT INTO xp_primaerenergietraeger (code, model, documentation, description) VALUES ('4000', 'Erdwaerme', 'Geothermie bezeichnet die in den oberen Schichten der Erdkruste gespeicherte Wärme und deren Ausbeutung zur Wärme- oder Stromerzeugung. In der Energiegewinnung wird zwischen tiefer und oberflächennaher Geothermie unterschieden. Die tiefe Geothermie wird von Kraftwerken zur Stromerzeugung genutzt.', 'Erdwärme');
INSERT INTO xp_primaerenergietraeger (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstige Energieträger', 'sonstiger Energieträger');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('1000', 'Fernleitung', 'Fernleitung gemäß Umweltverträglichkeitsprüfung (UVPG), Anlage 1 und ENWG § 3, Nr. 19d/20; Leitungen der Fernleitungsnetzbetreiber', 'Fernleitung');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('2000', 'Verteilnetzleitung', 'Leitung eines Verteil(er)netzes; Leitungen der Versorgungsunternehmen', 'Verteilnetzleitung');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('3000', 'Hauptleitung', 'Hauptleitung, oberste Leitungskategorie in einem Trinkwasser und Wärmenetz', 'Hauptleitung');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('4000', 'Versorgungsleitung', 'Versorgungsleitung, auch Ortsleitung (z.B Wasserleitungen innerhalb des Versorgungsgebietes im bebauten Bereich)', 'Versorgungsleitung');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('5000', 'Zubringerleitung', 'Zubringerleitung (z.B. Wasserleitungen zwischen Wassergewinnungs- und Versorgungsgebieten)', 'Zubringerleitung');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('6000', 'Anschlussleitung', 'Anschlussleitung, Hausanschluss (z.B. Wasserleitungen von der Abzweigstelle der Versorgungsleitung bis zur Übergabestelle/Hauptabsperreinrichtung)', 'Hausanschlussleitung');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('7000', 'Verbindungsleitung', 'Verbindungsleitung (z.B. Wasserleitungen außerhalb der Versorgungsgebiete, die Versorgungsgebiete (Orte) miteinander verbinden), in der Wärmeversorung auch Transportleitung genannt (die eine Wärmeerzeuugungsinfrastruktur mit einem entfernten Versorgungsgebiet verbindet)', 'Verbindungsleitung');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('8000', 'Strassenablaufleitung', 'Straßenablaufleitung (in der Abwasserentsorgung)', 'Straßenablaufleitung');
INSERT INTO xp_rohrleitungnetz (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstige Leitung', 'sonstige Leitung');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('1000', 'Antragstellung', 'Antragsteller', 'Antragstellung');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('2000', 'BevollmaechtigtPlanung', 'Bevollmächtigt und Ersteller der Planung', 'Bevollmächtigt für Planung');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('3000', 'Bevollmaechtigt', 'Bevollmächtigtes Unternehmen', 'Bevollmächtigt');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('4000', 'Planung', 'Planendes Büro', 'Planung');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('5000', 'Bauunternehmen', 'Unternehmen, das Tiefbaumaßnahmen durchführt', 'Bauunternehmen');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('6000', 'Vorhabentraeger', 'Träger eines Vorhabens im Planfeststellungs- oder Raumordnungsverfahren', 'Vorhabenträger');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('7100', 'Planfeststellungsbehoerde', 'Zuständige Behörde eines Planfeststellungsverfahrens', 'Planfeststellungsbehörde');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('7200', 'Anhoerungsbehoerde', 'Behörde, die Anhörungsverfahren im Rahmen eines Planfeststellungsverfahrens durchführt', 'Anhörungsbehörde');
INSERT INTO xp_rolle (code, model, documentation, description) VALUES ('8000', 'Raumordnungsbehoerde', 'Zuständige Behörde einer Raumverträglichkeitsprüfung', 'Zuständig für Raumverträglichkeitsprüfung');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('1000', 'StationGas', 'Station für Medium Gas (Wasserstoff/Erdgas)', 'Station Gas');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('10001', 'Schieberstation', 'Über eine Schieberstation (Abzweigstation?)  kann mit Hilfe der dort installierten Kugelhähne der Gasfluss gestoppt bzw. umgelenkt werden', 'Schieberstation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('10002', 'Verdichterstation', 'Eine Verdichterstation (Kompressorstation) ist eine Anlage in einer Transportleitung, bei der ein Kompressor das Gas wieder komprimiert, um Rohr-Druckverluste auszugleichen und den Volumenstrom zu regeln', 'Verdichterstation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('10003', 'Regel_Messstation', 'Eine Gas-Druckregelanlage (GDRA) ist eine Anlage zur ein- oder mehrstufigen Gas-Druckreduzierung. Bei einer Gas-Druckregel- und Messanlage (GDRMA) wird zusätzlich noch die Gas-Mengenmessung vorgenommen. (Anmerkung: Einspeise- und Übergabestationen können separat erfasst werden)', 'Regel- und Messstation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('10004', 'Armaturstation', 'Kombination von Armaturengruppen wie Absperr- und und Abgangsarmaturengruppen', 'Armaturstation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('10005', 'Einspeisestation', 'Die Einspeisungs- oder Empfangsstation leitet Erdgas oder Wasserstoff in ein Transportleitungsnetz. Die Einspeisung erfolgt z.B. aus einer Produktions- oder Speicheranlage oder über ein LNG-Terminal nach der Regasifizierung.', 'Einspeisestation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('10006', 'Uebergabestation', 'Gas-Übergabestationen (auch Übernahme- oder Entnahmestation) dienen i.d.R. der Verteilung von Gas aus Transportleitungen in die Verbrauchernetze. Dafür muss das ankommende Gas heruntergeregelt werden. Wird Wasserstoff in ein Erdgasleitungsnetz übergeben, muss zusätzlich ein Mischer an der Übernahmestelle gewährleisten, dass sich Wasserstoff und Erdgas gleichmäßig durchmischen. 
Eine weitere Variante ist die Übergabe von Gas an ein Kraftwerk.', 'Übergabe-/Entnahmestation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('10007', 'Molchstation', 'Station um Molchungen zur Prüfung der Integrität der Fernleitung während der Betriebsphase durchzuführen.  
Der Molch füllt den Leitungsquerschnitt aus und wandert entweder einfach mit dem Produktstrom durch die Leitung (meist bei Öl) oder wird durch Druck durch die Leitung gepresst. Im Rahmen der Molchtechnik werden neben dem Molch noch ins System eingebaute Schleusen benötigt, durch die der Molch in die Leitungen eingesetzt bzw. herausgenommen und von hinten mit Druck belegt werden kann.', 'Molchstation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('2000', 'StationStrom', 'Station für Medium Strom', 'Station Strom');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('20001', 'Transformatorenstation', 'In einer Transformatorenstation (Umspannstation, Netzstation, Ortsnetzstation oder kurz Trafostation) wird die elektrische Energie aus dem Mittelspannungsnetz mit einer elektrischen Spannung von 10 kV bis 36 kV auf die in Niederspannungsnetzen (Ortsnetzen) verwendeten 400/230 V zur allgemeinen Versorgung transformiert', 'Transformatorenstation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('20002', 'Konverterstation', 'Ein Konverter steht an den Verbindungspunkten von Gleich- und Wechselstromleitungen. Er verwandelt Wechsel- in Gleichstrom und kann ebenso Gleichstrom wieder zurück in Wechselstrom umwandeln und diesen ins Übertragungsnetz einspeisen.', 'Konverterstation');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('20003', 'Phasenschieber', 'Phasenschiebertransformatoren (PST), auch Querregler genannt, werden zur Steuerung der Stromflüsse zwischen Übertragungsnetzen eingesetzt. Der Phasenschiebertransformator speist einen Ausgleichsstrom in das System ein, der den Laststrom in der Leitung entweder verringert oder erhöht. Sinkt der Stromfluss in einer Leitung, werden die Stromflüsse im gesamten Verbundsystem neu verteilt.', 'Phasenschieber');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('3000', 'StationWaerme', 'Station im (Fern-)Wärmenetz', 'Station (Fern-)Wärme');
INSERT INTO xp_stationtyp (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstige Station', 'sonstige Station');
INSERT INTO xp_waermeleitungtyp (code, model, documentation, description) VALUES ('1000', 'Strang', 'Schematische Darstellung als Strang (mit Vor- und Rücklauf)', 'Strang');
INSERT INTO xp_waermeleitungtyp (code, model, documentation, description) VALUES ('2000', 'Vorlauf', 'Vorlaufrohr', 'Vorlauf');
INSERT INTO xp_waermeleitungtyp (code, model, documentation, description) VALUES ('3000', 'Ruecklauf', 'Rücklaufrohr', 'Rücklauf');
INSERT INTO xp_waermeleitungtyp (code, model, documentation, description) VALUES ('4000', 'Doppelrohr', 'Vor- und Rücklauf in einem Doppelrohr', 'Doppelrohr');
INSERT INTO xp_waermeleitungtyp (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstiger Typ', 'sonstiger Typ');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('1000', 'Kunststoff', 'Kunststoff', 'Kunststoff');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('10001', 'Polyethylen_PE', 'Polyethylen (PE)', 'Polyethylen ( PE)');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('10002', 'Polyethylen_PE_HD', 'High-Density Polyethylen', 'High-Density Polyethylen');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('10003', 'Polypropylen_PP', 'Polypropylen (PP)', 'Polypropylen ( PP)');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('10004', 'Polycarbonat_PC', 'Polycarbonat (PC)', 'Polycarbonat ( PC)');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('10005', 'Polyvinylchlorid_PVC_U', 'Polyvinylchlorid (PVC-U)', 'Polyvinylchlorid ( PVC- U)');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('2000', 'Stahl', 'Stahl', 'Stahl');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('20001', 'StahlVerzinkt', 'Stahl verzinkt', 'Stahl verzinkt');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('20002', 'Stahlgitter', 'Stahlfachwerkskonstruktion (z.B. Freileitungsmast als Gittermast)', 'Stahlgitter');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('20003', 'Stahlrohr', 'Rohrförmiger Profilstahl, dessen Wand aus Stahl besteht. Stahlrohre dienen der Durchleitung von flüssigen, gasförmigen oder festen Stoffen, oder werden als statische oder konstruktive Elemente verwendet (z.B. Freileitungsmast als Stahlrohrmast)', 'Stahlrohr');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('2500', 'Stahlverbundrohr', 'Stahlverbundrohre im Rohrleitungsbau', 'Stahlverbundrohr');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('25001', 'St_PE', 'Stahlrohr mit  Kunststoffumhüllung auf PE-Basis', 'Stahlrohr mit Standard-Kunststoffumhüllung (PE)');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('25002', 'St_PP', 'Stahlrohr mit  Kunststoffumhüllung auf PP-Basis für höhere Temperatur- und Härte-Anforderungen', 'Stahlrohr mit Kunstoffumhüllung (PP)');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('25003', 'St_FZM', 'Stahlrohr mit mit Kunststoff-Umhüllung und zusätzlichem Außenschutz durch Faserzementmörtel-Ummantelung (FZM)', 'Stahlrohr mit FZM-Ummantelung');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('25004', 'St_GFK', 'Stahlrohr mit mit Kunststoff-Umhüllung und zusätzlichem Außenschutz aus glasfaserverstärktem Kunststoff (GFK) für höchste mechanische Abriebfestigkeit bei grabenlosem Rohrvortrieb', 'Stahlrohr mit GFK-Ummantelung');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('25005', 'St_ZM_PE', 'Stahlrohr mit Zementmörtelauskleidung und PE-Außenschutz (z.B. Abwasserohr)', 'Stahl-Verbundrohr (ZM-PE)');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('3000', 'Gusseisen', 'Gusseisen', 'Gusseisen');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('30001', 'GGG_ZM', 'duktiles Gussrohr mit Zementmörtelauskleidung (z.B Abwasserrohr)', 'duktiles Gussrohr mit ZM-Auskleidung');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('30002', 'GGG_ZM_PE', 'duktiles Gussrohr mit Zementmörtelauskleidung und PE-Außenschutz (z.B. Abwasserrohr)', 'duktiles Guss-Verbundrohr (ZM-PE)');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('4000', 'Beton', 'Beton (z.B. Schacht)', 'Beton');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('5000', 'Holz', 'Holz (z.B. Holzmast)', 'Holz');
INSERT INTO xp_werkstoff (code, model, documentation, description) VALUES ('9999', 'Sonstiges', 'Sonstiger Werkstoff', 'sonstiger Werkstoff');
