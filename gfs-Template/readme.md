## GFS-Template

Beim erstmaligen Öffnen von GML-Dateien in QGIS wird vom GDAL-Treiber automatisch eine gleichnamige Datei mit der Endung gfs erzeugt. Hierbei handelt es sich um ein Template, das das Mapping der Daten beeinflusst. Existiert (im gleichen Ordner) eine GFS-Datei mit gleichem Namen wie die GML-Datei, greift der GDAL-Treiber auf diese beim Einlesen zurück.

Die hier zur Verfügung gestellte Templates der XTrasse-Profile wirken sich beim Import einer GML-Instanz in QGIS folgendermaßen aus:

- Es werden sämtliche im XTrasse-Profil definierten Layer erzeugt (und nicht nur die mit belegten Objekten). Im Import-Menü von QGIS lässt sich dies über die Option "Leere Vektorlayer anzeigen" steuern (ohne die Nutzung des Templates bleibt diese Option wirkungslos).

- Es werden in den Attributtabellen der Layer sämtliche möglichen Attribute dargestellt (und nicht nur die belegten).

- Die *xlink:hrefs* werden eingelesen (ohne Template werden sie nicht in die Attributtabellen übernommen). 

## Hinweis zur Nutzung

Das Template wird beim Einlesen einer GML-Instanz vom GDAL-Treiber nur dann automatisch genutzt, wenn beide Dateien im *gleichen Ordner* liegen, vor dem Suffix den *gleichen Namen* besitzen und die GFS-Datei *jüngeren Datums* ist als die GML-Datei. 

Das Datum der Dateien spielt keine Rolle, wenn im QGIS-Import-Menü der Pfad zum Template angegeben wird. Um einen vollständigen Layerbaum zu erzeugen, müssen allerdings auch über diesen Weg Template und Instanz im gleichen Ordner liegen und über den gleichen Namen verfügen.  




